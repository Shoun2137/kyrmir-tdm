
// Code lines counter - (gci -include *.cs,*.xaml -recurse | select-string .).Count

// Optional...
srand(getTickCount());

// RESPAWN SETTINGS
DEFAULT_POSITION <- function(world) {
    local x, y, z, angle;
    switch(world) {
        case "NEWWORLD\\NEWWORLD.ZEN": x = 0; y = 100; z = 0; angle = 1; break;
        case "ADDONWORLD\\ADDONWORLD.ZEN": x = 0; y = 0; z = 0; angle = 1; break;
        case "ADDONWORLD.ZEN": x = 0; y = 0; z = 0; angle = 1; break;
        case "COLONY.ZEN": x = 7972; y = 5625; z = 36971; angle = 312; break;
        case "ARENA.ZEN": x = 3050; y = -890; z = 1015; angle = 1; break;
        default:
            x = 0; y = 100; z = 0; angle = 1; break;
    }
    return { x = x, y = y, z = z, angle = angle };   
}

DEFAULT_STATS <- {
    defaultEq = [
        [ "ITMW_1H_01", 1 ],
        [ "ITPO_HEALTH_00", 2 ]
    ],
    strength = 10,
    dexterity = 10,
    mana = 10,
    health = 200
};

// CHATLIST
enum ChatType 
{
    Guild = 0,
    Global = 1,       
    Moderation = 2
}

local chat_manager = getChatManager();

chat_manager.addChat(
    Chat.Instance(ChatType.Guild, "GUILD_CHAT")
    .bindReceiverFunc(function(senderId, receiverId)
    {
        return getPlayerGuild(senderId) == getPlayerGuild(receiverId);
    })
);

chat_manager.addChat(
    Chat.Instance(ChatType.Global, "GLOBAL_CHAT")
);

chat_manager.addChat(
    Chat.Instance(ChatType.Moderation, "ADMIN_CHAT")
    .bindPermissionFunc(function(playerId)
    {
        return getPlayerPersmission(playerId) >= PermissionLevel.Moderator;
    })
    .bindReceiverFunc(function(senderId, receiverId)
    {
        return getPlayerPersmission(receiverId) >= PermissionLevel.Moderator;
    })
);

// GUILDS
registerGuild(Guild.Instance("OLD_CAMP", "COLONY.ZEN", 100, 0, 0)
    .setRespawnPosition(-198, 246, -1469)
    .registerArmor(0, "ITAR_SO_01")
    .registerArmor(1, "ITAR_SO_02")
    .registerArmor(2, "ITAR_SO_03")
    .registerArmor(3, "ITAR_SO_04")
    .registerArmor(4, "ITAR_SO_05")
    .registerArmor(5, "ITAR_SO_06")
    .registerArmor(6, "ITAR_SO_07")
    .registerArmor(7, "ITAR_SO_08")
    .registerArmor(8, "ITAR_SO_09")
    .registerArmor(9, "ITAR_SO_10")
);

registerGuild(Guild.Instance("SWAMP_CAMP", "COLONY.ZEN", 113, 194, 106)
    .setRespawnPosition(49337, -3835, -4897)
    .registerArmor(0, "ITAR_ONB_01")
    .registerArmor(1, "ITAR_ONB_02")
    .registerArmor(2, "ITAR_ONB_03")
    .registerArmor(3, "ITAR_ONB_04")
    .registerArmor(4, "ITAR_ONB_05")
    .registerArmor(5, "ITAR_ONB_06")
    .registerArmor(6, "ITAR_ONB_07")
    .registerArmor(7, "ITAR_ONB_08")
    .registerArmor(8, "ITAR_ONB_09")
    .registerArmor(9, "ITAR_ONB_10")
);

registerGuild(Guild.Instance("NEW_CAMP", "COLONY.ZEN", 100, 96, 201)
    .setRespawnPosition(-46527, 1915, 13599)
    .registerArmor(0, "ITAR_NO_01")
    .registerArmor(1, "ITAR_NO_02")
    .registerArmor(2, "ITAR_NO_03")
    .registerArmor(3, "ITAR_NO_04")
    .registerArmor(4, "ITAR_NO_05")
    .registerArmor(5, "ITAR_NO_06")
    .registerArmor(6, "ITAR_NO_07")
    .registerArmor(7, "ITAR_NO_08")
    .registerArmor(8, "ITAR_NO_09")
    .registerArmor(9, "ITAR_NO_10")
);

registerGuild(Guild.Instance("PIRATE_CAMP", "ADDONWORLD.ZEN", 100, 0, 0)
    .setRespawnPosition(-35202, -1873, 20144)
    .registerArmor(0, "ITAR_PIR_01")
    .registerArmor(1, "ITAR_PIR_02")
    .registerArmor(2, "ITAR_PIR_03")
    .registerArmor(3, "ITAR_PIR_04")
    .registerArmor(4, "ITAR_PIR_05")
    .registerArmor(5, "ITAR_PIR_06")
    .registerArmor(6, "ITAR_PIR_07")
    .registerArmor(7, "ITAR_PIR_08")
    .registerArmor(8, "ITAR_PIR_09")
    .registerArmor(9, "ITAR_PIR_10")
);

registerGuild(Guild.Instance("BANDIT_CAMP", "ADDONWORLD.ZEN", 113, 194, 106)
    .setRespawnPosition(31311, -4440, 10824)
    .registerArmor(0, "ITAR_BDT_01")
    .registerArmor(1, "ITAR_BDT_02")
    .registerArmor(2, "ITAR_BDT_03")
    .registerArmor(3, "ITAR_BDT_04")
    .registerArmor(4, "ITAR_BDT_05")
    .registerArmor(5, "ITAR_BDT_06")
    .registerArmor(6, "ITAR_BDT_07")
    .registerArmor(7, "ITAR_BDT_08")
    .registerArmor(8, "ITAR_BDT_09")
    .registerArmor(9, "ITAR_BDT_10")
);

addArea(Area.Area("OLD_CAMP", "COLONY.ZEN")
    .addPoint(-7488.75, 5047.66)
    .addPoint(-7042.73, 5287.11)
    .addPoint(-6702.73, 5273.98)
    .addPoint(-6207.81, 4399.84)
    .addPoint(-5530.39, 5308.98)
    .addPoint(-3358.12, 5687.58)
    .addPoint(-1336.41, 5707.27)
    .addPoint(-475.234, 5651.64)
    .addPoint(1135.94, 5730.62)
    .addPoint(2347.27, 5596.48)
    .addPoint(3775.78, 5553.67)
    .addPoint(6010.86, 5227.66)
    .addPoint(6923.59, 4011.56)
    .addPoint(7161.88, 2155.78)
    .addPoint(7466.64, 597.188)
    .addPoint(7238.36, -957.891)
    .addPoint(7173.36, -2215.16)
    .addPoint(6871.41, -3564.84)
    .addPoint(5853.12, -4720.94)
    .addPoint(4938.44, -5445.16)
    .addPoint(3908.98, -5434.69)
    .addPoint(3131.56, -6274.69)
    .addPoint(642.344, -7103.05)
    .addPoint(-1224.45, -7537.66)
    .addPoint(-3115.94, -7617.66)
    .addPoint(-4465, -6926.02)
    .addPoint(-5629.06, -6435.94)
    .addPoint(-6927.03, -6153.44)
    .addPoint(-7818.91, -5271.95)
    .addPoint(-8004.38, -4294.3)
    .addPoint(-8057.19, -2821.25)
    .addPoint(-7983.83, -1513.28)
    .addPoint(-7997.89, -488.516)
    .addPoint(-8147.19, 313.906)
    .addPoint(-8285.23, 2331.56)
    .addPoint(-7808.59, 3316.41)
    .addPoint(-7477.81, 3758.67)
    .addPoint(-7571.88, 4714.14)
);

addArea(Area.Area("NEW_CAMP", "COLONY.ZEN")
    .addPoint(-36543.046875, 11687.031250)
    .addPoint(-36258.515625, 11275.703125)
    .addPoint(-36080.156250, 10826.406250)
    .addPoint(-39496.875000, 6865.781250)
    .addPoint(-40342.031250, 6547.031250)
    .addPoint(-41437.109375, 5556.484375)
    .addPoint(-43765.859375, 4826.328125)
    .addPoint(-45735.546875, 5152.343750)
    .addPoint(-47186.171875, 6061.406250)
    .addPoint(-49263.828125, 7756.250000)
    .addPoint(-49667.500000, 9410.937500)
    .addPoint(-49647.890625, 13161.718750)
    .addPoint(-48062.578125, 15167.734375)
    .addPoint(-46113.828125, 15464.921875)
    .addPoint(-40013.281250, 13379.296875)
    .addPoint(-37417.812500, 11755.546875)
);

addArea(Area.Area("SWAMP_CAMP", "COLONY.ZEN")
    .addPoint(42627.343750, -7226.328125)
    .addPoint(41149.453125, -9613.125000)
    .addPoint(40862.968750, -11162.968750)
    .addPoint(40527.968750, -13123.984375)
    .addPoint(40215.078125, -15559.375000)
    .addPoint(40270.000000, -16413.984375)
    .addPoint(42333.984375, -17595.937500)
    .addPoint(48069.531250, -16867.812500)
    .addPoint(51432.031250, -15816.250000)
    .addPoint(54660.625000, -14318.828125)
    .addPoint(56866.640625, -10270.703125)
    .addPoint(57034.609375, -7814.687500)
    .addPoint(54851.250000, -5141.640625)
    .addPoint(53547.812500, -3343.281250)
    .addPoint(50433.437500, -395.156250)
    .addPoint(46650.078125, -2662.500000)
    .addPoint(45717.968750, -3755.468750)
    .addPoint(44604.609375, -4604.140625)
    .addPoint(43184.140625, -6998.906250)
);

addArea(Area.Area("PIRATE_CAMP", "ADDONWORLD.ZEN")
    .addPoint(-32732, 16314)
    .addPoint(-33784, 15515)
    .addPoint(-34199, 14806)
    .addPoint(-34695, 14055)
    .addPoint(-35555, 13788)
    .addPoint(-38242, 14444)
    .addPoint(-39106, 15864)
    .addPoint(-38557, 19372)
    .addPoint(-37275, 21713)
    .addPoint(-33670, 25707)
    .addPoint(-31885, 25808)
    .addPoint(-30856, 23820)
    .addPoint(-31115, 19416)
    .addPoint(-32594, 16236)
);

addArea(Area.Area("BANDIT_CAMP", "ADDONWORLD.ZEN")
    .addPoint(26556, 8205)
    .addPoint(27010, 7050)
    .addPoint(27492, 5787)
    .addPoint(28282, 5365)
    .addPoint(29990, 5099)
    .addPoint(33817, 8381)
    .addPoint(32174, 13768)
    .addPoint(29132, 13934)
    .addPoint(25780, 12345)
    .addPoint(25956, 10082)
);