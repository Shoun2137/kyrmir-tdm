
/********************* ITAR *********************/

registerTranslation("ITAR_SO_01", LANG.PL, "�achy Skaza�ca");
registerTranslation("ITAR_SO_02", LANG.PL, "Lekki Str�j Cienia");
registerTranslation("ITAR_SO_03", LANG.PL, "�redni Str�j Cienia");
registerTranslation("ITAR_SO_04", LANG.PL, "Lekka Zbroja Stra�nika");
registerTranslation("ITAR_SO_05", LANG.PL, "�rednia Zbroja Stra�nika");
registerTranslation("ITAR_SO_06", LANG.PL, "Ci�ka Zbroja Stra�nika");
registerTranslation("ITAR_SO_07", LANG.PL, "Szata Nowicjusza Ognia");
registerTranslation("ITAR_SO_08", LANG.PL, "Szata Maga Ognia");
registerTranslation("ITAR_SO_09", LANG.PL, "Ostateczna Szata");
registerTranslation("ITAR_SO_10", LANG.PL, "Ostateczna Zbroja");
registerTranslation("ITAR_NO_01", LANG.PL, "Spodnie Szkodnika");
registerTranslation("ITAR_NO_02", LANG.PL, "Lekki Str�j Szkodnika");
registerTranslation("ITAR_NO_03", LANG.PL, "�redni Str�j Szkodnika");
registerTranslation("ITAR_NO_04", LANG.PL, "Lekka Zbroja Najemnika");
registerTranslation("ITAR_NO_05", LANG.PL, "�rednia Zbroja Najemnika");
registerTranslation("ITAR_NO_06", LANG.PL, "Ci�ka Zbroja Najemnika");
registerTranslation("ITAR_NO_07", LANG.PL, "Szata Nowicjusza Wody");
registerTranslation("ITAR_NO_08", LANG.PL, "Szata Maga Wody");
registerTranslation("ITAR_NO_09", LANG.PL, "Ostateczna Szata");
registerTranslation("ITAR_NO_10", LANG.PL, "Ostateczna Zbroja");
registerTranslation("ITAR_ONB_01", LANG.PL, "Przepaska Nowicjusza");
registerTranslation("ITAR_ONB_02", LANG.PL, "Lekka Zbroja Nowicjusza");
registerTranslation("ITAR_ONB_03", LANG.PL, "�rednia Zbroja Nowicjusza");
registerTranslation("ITAR_ONB_04", LANG.PL, "Lekka Zbroja �wi�tynna");
registerTranslation("ITAR_ONB_05", LANG.PL, "�rednia Zbroja �wi�tynna");
registerTranslation("ITAR_ONB_06", LANG.PL, "Ci�ka Zbroja �wi�tynna");
registerTranslation("ITAR_ONB_07", LANG.PL, "Szata Guru");
registerTranslation("ITAR_ONB_08", LANG.PL, "Zdobiona Szata Guru");
registerTranslation("ITAR_ONB_09", LANG.PL, "Ostateczna Szata");
registerTranslation("ITAR_ONB_10", LANG.PL, "Ostateczna Zbroja");
registerTranslation("ITAR_PIR_01" LANG.PL, "Spodnie Majtka");
registerTranslation("ITAR_PIR_02" LANG.PL, "Ubranie Rybaka");
registerTranslation("ITAR_PIR_03" LANG.PL, "Lekka Zbroja Pirata");
registerTranslation("ITAR_PIR_04" LANG.PL, "�rednia Zbroja Pirata");
registerTranslation("ITAR_PIR_05" LANG.PL, "Ci�ka Zbroja Pirata");
registerTranslation("ITAR_PIR_06" LANG.PL, "Zbroja �eglarza Burz");
registerTranslation("ITAR_PIR_07" LANG.PL, "Szata Nowicjusza Wody");
registerTranslation("ITAR_PIR_08" LANG.PL, "Szata Maga Wody");
registerTranslation("ITAR_PIR_09" LANG.PL, "Ostateczna Szata");
registerTranslation("ITAR_PIR_10" LANG.PL, "Ostateczna Zbroja");
registerTranslation("ITAR_BDT_01" LANG.PL, "�achy Skaza�ca");
registerTranslation("ITAR_BDT_02" LANG.PL, "Sk�rzany Pancerz");
registerTranslation("ITAR_BDT_03" LANG.PL, "Lekka Zbroja Bandyty");
registerTranslation("ITAR_BDT_04" LANG.PL, "�rednia Zbroja Bandyty");
registerTranslation("ITAR_BDT_05" LANG.PL, "Zbroja Bandyty Stra�nika");
registerTranslation("ITAR_BDT_06" LANG.PL, "Elitarna Zbroja Bandyty");
registerTranslation("ITAR_BDT_07" LANG.PL, "Ubranie Nowicjusza");
registerTranslation("ITAR_BDT_08" LANG.PL, "Szata Nekromanty");
registerTranslation("ITAR_BDT_09" LANG.PL, "Ostateczna Szata");
registerTranslation("ITAR_BDT_10" LANG.PL, "Ostateczna Zbroja");

/********************* ITMW *********************/

registerTranslation("ITMW_1H_01", LANG.PL, "Laga");
registerTranslation("ITMW_1H_02", LANG.PL, "Sztylet");
registerTranslation("ITMW_1H_03", LANG.PL, "Kiepski Kr�tki Miecz");
registerTranslation("ITMW_1H_04", LANG.PL, "Ci�ka Pa�ka z Kolcami");
registerTranslation("ITMW_1H_05", LANG.PL, "Kiepski Miecz");
registerTranslation("ITMW_1H_06", LANG.PL, "Top�r Marynarski");
registerTranslation("ITMW_1H_07", LANG.PL, "Miecz Paladyna");
registerTranslation("ITMW_1H_08", LANG.PL, "D�ugi Miecz");
registerTranslation("ITMW_1H_09", LANG.PL, "Dobry D�ugi Miecz");
registerTranslation("ITMW_1H_10", LANG.PL, "Dobry Miecz P�torar�czny");
registerTranslation("ITMW_1H_11", LANG.PL, "Magiczne Ostrze na Smoki");
registerTranslation("ITMW_1H_12", LANG.PL, "Szpon Beliara");
registerTranslation("ITMW_2H_01", LANG.PL, "Zardzewia�y Miecz Dwur�czny");
registerTranslation("ITMW_2H_02", LANG.PL, "Kostur S�dziego");
registerTranslation("ITMW_2H_03", LANG.PL, "Lekki Miecz Dwur�czny");
registerTranslation("ITMW_2H_04", LANG.PL, "Miecz Dwur�czny Paladyna");
registerTranslation("ITMW_2H_05", LANG.PL, "Runa Mocy");
registerTranslation("ITMW_2H_06", LANG.PL, "Ci�ki Miecz Dwur�czny");
registerTranslation("ITMW_2H_07", LANG.PL, "Smocza Zguba");
registerTranslation("ITMW_2H_08", LANG.PL, "�wi�ty Kat");
registerTranslation("ITMW_2H_09", LANG.PL, "Ostrze Burzy");
registerTranslation("ITMW_2H_10", LANG.PL, "Du�e Magiczne Ostrze na Smoki");
registerTranslation("ITMW_2H_11", LANG.PL, "Orkowy Miecz Wojenny");

/********************* ITRW *********************/


registerTranslation("ITRW_BOW_ARROW", LANG.PL, "Strza�a");
registerTranslation("ITRW_CROSSBOW_BOLT", LANG.PL, "Be�t");
registerTranslation("ITRW_MAGICBOW_ARROW", LANG.PL, "Magiczna Strza�a");
registerTranslation("ITRW_MAGICCROSSBOW_BOLT", LANG.PL, "Magiczny Be�t");
registerTranslation("ITRW_BOW_01", LANG.PL, "Kr�tki �uk");
registerTranslation("ITRW_BOW_02", LANG.PL, "�uk Wierzbowy");
registerTranslation("ITRW_BOW_03", LANG.PL, "�uk My�liwski");
registerTranslation("ITRW_BOW_04", LANG.PL, "�uk z Wi�zu");
registerTranslation("ITRW_BOW_05", LANG.PL, "�uk Kompozytowy");
registerTranslation("ITRW_BOW_06", LANG.PL, "�uk Jesionowy");
registerTranslation("ITRW_BOW_07", LANG.PL, "D�ugi �uk");
registerTranslation("ITRW_BOW_08", LANG.PL, "�uk Bukowy");
registerTranslation("ITRW_BOW_09", LANG.PL, "Ko�ciany �uk");
registerTranslation("ITRW_BOW_10", LANG.PL, "�uk D�bowy");
registerTranslation("ITRW_BOW_11", LANG.PL, "Magiczny �uk");
registerTranslation("ITRW_CROSSBOW_01", LANG.PL, "Stara Kusza");
registerTranslation("ITRW_CROSSBOW_02", LANG.PL, "Lekka Kusza");
registerTranslation("ITRW_CROSSBOW_03", LANG.PL, "Dobra Kusza");
registerTranslation("ITRW_CROSSBOW_04", LANG.PL, "Kusza My�liwska");
registerTranslation("ITRW_CROSSBOW_05", LANG.PL, "Kusza Bojowa");
registerTranslation("ITRW_CROSSBOW_06", LANG.PL, "Wzmocniona Kusza");
registerTranslation("ITRW_CROSSBOW_07", LANG.PL, "Elitarna Kusza");
registerTranslation("ITRW_CROSSBOW_08", LANG.PL, "Ci�ka Kusza");
registerTranslation("ITRW_CROSSBOW_09", LANG.PL, "Kusza Nordmarczyka");
registerTranslation("ITRW_CROSSBOW_10", LANG.PL, "Kusza �owcy Smok�w");
registerTranslation("ITRW_CROSSBOW_11", LANG.PL, "Magiczna Kusza");

/********************* ITRU *********************/

registerTranslation("ITRU_FIREBOLT", LANG.PL, "Ognista Strza�a");
registerTranslation("ITRU_ICEBOLT", LANG.PL, "Lodowa Strza�a");
registerTranslation("ITRU_LIGHTHEAL", LANG.PL, "Lekkie Leczenie Ran");
registerTranslation("ITRU_ZAP", LANG.PL, "Ma�a B�yskawica");
registerTranslation("ITRU_MEDIUMHEAL", LANG.PL, "�rednie Leczenie Ran");
registerTranslation("ITRU_ICELANCE", LANG.PL, "Lodowa Lanca");
registerTranslation("ITRU_HEAL_TARGET", LANG.PL, "Leczenie Innych");
registerTranslation("ITRU_HEAL_TARGET_DESC", LANG.PL, "Leczy innego gracza (100 punkt�w zdrowia)");
registerTranslation("ITRU_FIRESTORM", LANG.PL, "Ma�a Burza Ognista");
registerTranslation("ITRU_FULLHEAL", LANG.PL, "Ci�kie Leczenie Ran");
registerTranslation("ITRU_INSTANTFIREBALL", LANG.PL, "Kula Ognia");
registerTranslation("ITRU_LIGHTNINGFLASH", LANG.PL, "�wi�ta Strza�a");

/********************* ITSC *********************/

registerTranslation("ITSC_MASSDEATH", LANG.PL, "�miertelna Fala");

/********************* ITPO *********************/

registerTranslation("ITPO_HEALTH_00", LANG.PL, "Mikstura Nowicjusza");
registerTranslation("ITPO_HEALTH_01", LANG.PL, "Ma�a Mikstura Zdrowia");
registerTranslation("ITPO_HEALTH_02", LANG.PL, "�rednia Mikstura Zdrowia");
registerTranslation("ITPO_HEALTH_03", LANG.PL, "Du�a Mikstura Zdrowia");
registerTranslation("ITPO_HEALTH_04", LANG.PL, "Du�a Mikstura Zdrowia");
registerTranslation("ITPO_MANA_01", LANG.PL, "Ma�a Mikstura Many");
registerTranslation("ITPO_MANA_02", LANG.PL, "�rednia Mikstura Many");
registerTranslation("ITPO_MANA_03", LANG.PL, "Du�a Mikstura Many");
registerTranslation("ITPO_SPEED", LANG.PL, "Mikstura Szybko�ci");
registerTranslation("ITPO_SPEED_EFFECT", LANG.PL, "Sprint na 30 sekund");

/********************* ITMI *********************/

registerTranslation("ITMI_GOLD", LANG.PL, "Z�oto");
registerTranslation("ITMI_GOLD_DESC", LANG.PL, "Po prostu z�oto...");
registerTranslation("ITMI_FOCUS_RED", LANG.PL, "Czerwony Kamie� Ogniskuj�cy");
registerTranslation("ITMI_FOCUS_RED_DESC", LANG.PL, "Bonus: 50 Punkt�w Zdrowia - Wypada po twojej �mierci");
registerTranslation("ITMI_FOCUS_YELLOW", LANG.PL, "��ty Kamie� Ogniskuj�cy");
registerTranslation("ITMI_FOCUS_YELLOW_DESC", LANG.PL, "Bonus: 10 Si�y - Wypada po twojej �mierci");
registerTranslation("ITMI_FOCUS_GRAY", LANG.PL, "Szary Zw�j");
registerTranslation("ITMI_FOCUS_GRAY_DESC", LANG.PL, "Przemiana w Brzytw� - By si� odmieni� kliknij Z - Wypada po twojej �mierci");
registerTranslation("ITMI_FOCUS_VIOLET", LANG.PL, "Fioletowy Zw�j");
registerTranslation("ITMI_FOCUS_VIOLET_DESC", LANG.PL, "Przemiana w Pe�zacza Wojownika - By si� odmieni� kliknij Z - Wypada po twojej �mierci");
registerTranslation("ITMI_FOCUS_BLUE", LANG.PL, "Niebieski Kamie� Ogniskuj�cy");
registerTranslation("ITMI_FOCUS_BLUE_DESC", LANG.PL, "Bonus: 20 Many - Wypada po twojej �mierci");
registerTranslation("ITMI_FOCUS_GREEN", LANG.PL, "Zielony Kamie� Ogniskuj�cy");
registerTranslation("ITMI_FOCUS_GREEN_DESC", LANG.PL, "Bonus: 10 Zr�czno�ci - Wypada po twojej �mierci");
