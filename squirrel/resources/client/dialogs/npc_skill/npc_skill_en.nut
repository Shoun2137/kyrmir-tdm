
registerTranslation("NPC_SKILL_INITIAL_0", LANG.EN, "What can you teach me?");
registerTranslation("NPC_SKILL_INITIAL_1", LANG.EN, "I can teach you any fighting skills...");

registerTranslation("NPC_SKILL_END_TITLE", LANG.EN, "Bye...");
registerTranslation("NPC_SKILL_END_0", LANG.EN, "Bye...");
registerTranslation("NPC_SKILL_END_1", LANG.EN, "See you later...");

registerTranslation("NPC_SKILL_TRAINING_GROUND_TITLE", LANG.EN, "I want some training...");

registerTranslation("NPC_SKILL_1H_INITIAL_TITLE", LANG.EN, "Learning to fight with One-Handed Weapons");
registerTranslation("NPC_SKILL_1H_INITIAL_0", LANG.EN, "What can you teach me?");
registerTranslation("NPC_SKILL_1H_INITIAL_1", LANG.EN, "I can teach you how to better wield One-Handed Weapons...");

registerTranslation("NPC_SKILL_1H_FIVE_POINT_TITLE", LANG.EN, "One-Handed Weapons +5, Cost: 5 LP...");
registerTranslation("NPC_SKILL_1H_FIVE_POINT_0", LANG.EN, "Great, you learned something...");

registerTranslation("NPC_SKILL_1H_FIVE_POINT_CANT_TITLE", LANG.EN, "One-Handed Weapons +5, Cost: 5 LP...");
registerTranslation("NPC_SKILL_1H_FIVE_POINT_CANT_0", LANG.EN, "I can't teach you anything, you lack experience...");

registerTranslation("NPC_SKILL_1H_TEN_POINT_TITLE", LANG.EN, "One-Handed Weapons +10, Cost: 10 LP...");
registerTranslation("NPC_SKILL_1H_TEN_POINT_0", LANG.EN, "Great, you learned something...");

registerTranslation("NPC_SKILL_1H_TEN_POINT_CANT_TITLE", LANG.EN, "One-Handed Weapons +10, Cost: 10 LP...");
registerTranslation("NPC_SKILL_1H_TEN_POINT_CANT_0", LANG.EN, "I can't teach you anything, you lack experience...");

registerTranslation("NPC_SKILL_1H_END_TITLE", LANG.EN, "Bye...");
registerTranslation("NPC_SKILL_1H_END_0", LANG.EN, "Bye...");
registerTranslation("NPC_SKILL_1H_END_1", LANG.EN, "See you later...");

registerTranslation("NPC_SKILL_2H_INITIAL_TITLE", LANG.EN, "Learning to fight with Two-Handed Weapons");
registerTranslation("NPC_SKILL_2H_INITIAL_0", LANG.EN, "What can you teach me?");
registerTranslation("NPC_SKILL_2H_INITIAL_1", LANG.EN, "I can teach you how to better wield Two-Handed Weapons...");

registerTranslation("NPC_SKILL_2H_FIVE_POINT_TITLE", LANG.EN, "Two-Handed Weapons +5, Cost: 5 LP...");
registerTranslation("NPC_SKILL_2H_FIVE_POINT_0", LANG.EN, "Great, you learned something...");

registerTranslation("NPC_SKILL_2H_FIVE_POINT_CANT_TITLE", LANG.EN, "Two-Handed Weapons +5, Cost: 5 LP...");
registerTranslation("NPC_SKILL_2H_FIVE_POINT_CANT_0", LANG.EN, "I can't teach you anything, you lack experience...");

registerTranslation("NPC_SKILL_2H_TEN_POINT_TITLE", LANG.EN, "Two-Handed Weapons +10, Cost: 10 LP...");
registerTranslation("NPC_SKILL_2H_TEN_POINT_0", LANG.EN, "Great, you learned something...");

registerTranslation("NPC_SKILL_2H_TEN_POINT_CANT_TITLE", LANG.EN, "Two-Handed Weapons +10, Cost: 10 LP...");
registerTranslation("NPC_SKILL_2H_TEN_POINT_CANT_0", LANG.EN, "I can't teach you anything, you lack experience...");

registerTranslation("NPC_SKILL_2H_END_TITLE", LANG.EN, "Bye...");
registerTranslation("NPC_SKILL_2H_END_0", LANG.EN, "Bye...");
registerTranslation("NPC_SKILL_2H_END_1", LANG.EN, "See you later...");

registerTranslation("NPC_SKILL_BOW_INITIAL_TITLE", LANG.EN, "Learning to fight with Bows");
registerTranslation("NPC_SKILL_BOW_INITIAL_0", LANG.EN, "What can you teach me?");
registerTranslation("NPC_SKILL_BOW_INITIAL_1", LANG.EN, "I can teach you how to better wield Bows...");

registerTranslation("NPC_SKILL_BOW_FIVE_POINT_TITLE", LANG.EN, "Bows +5, Cost: 5 LP...");
registerTranslation("NPC_SKILL_BOW_FIVE_POINT_0", LANG.EN, "Great, you learned something...");

registerTranslation("NPC_SKILL_BOW_FIVE_POINT_CANT_TITLE", LANG.EN, "Bows +5, Cost: 5 LP...");
registerTranslation("NPC_SKILL_BOW_FIVE_POINT_CANT_0", LANG.EN, "I can't teach you anything, you lack experience...");

registerTranslation("NPC_SKILL_BOW_TEN_POINT_TITLE", LANG.EN, "Bows +10, Cost: 10 LP...");
registerTranslation("NPC_SKILL_BOW_TEN_POINT_0", LANG.EN, "Great, you learned something...");

registerTranslation("NPC_SKILL_BOW_TEN_POINT_CANT_TITLE", LANG.EN, "Bows +10, Cost: 10 LP...");
registerTranslation("NPC_SKILL_BOW_TEN_POINT_CANT_0", LANG.EN, "I can't teach you anything, you lack experience...");

registerTranslation("NPC_SKILL_BOW_END_TITLE", LANG.EN, "Bye...");
registerTranslation("NPC_SKILL_BOW_END_0", LANG.EN, "Bye...");
registerTranslation("NPC_SKILL_BOW_END_1", LANG.EN, "See you later...");

registerTranslation("NPC_SKILL_CBOW_INITIAL_TITLE", LANG.EN, "Learning to fight with Crossbows");
registerTranslation("NPC_SKILL_CBOW_INITIAL_0", LANG.EN, "What can you teach me?");
registerTranslation("NPC_SKILL_CBOW_INITIAL_1", LANG.EN, "I can teach you how to better wield Crossbows...");

registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_TITLE", LANG.EN, "Crossbows +5, Cost: 5 LP...");
registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_0", LANG.EN, "Great, you learned something...");

registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_CANT_TITLE", LANG.EN, "Crossbows +5, Cost: 5 LP...");
registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_CANT_0", LANG.EN, "I can't teach you anything, you lack experience...");

registerTranslation("NPC_SKILL_CBOW_TEN_POINT_TITLE", LANG.EN, "Crossbows +10, Cost: 10 LP...");
registerTranslation("NPC_SKILL_CBOW_TEN_POINT_0", LANG.EN, "Great, you learned something...");

registerTranslation("NPC_SKILL_CBOW_TEN_POINT_CANT_TITLE", LANG.EN, "Crossbows +10, Cost: 10 LP...");
registerTranslation("NPC_SKILL_CBOW_TEN_POINT_CANT_0", LANG.EN, "I can't teach you anything, you lack experience...");

registerTranslation("NPC_SKILL_CBOW_END_TITLE", LANG.EN, "Bye...");
registerTranslation("NPC_SKILL_CBOW_END_0", LANG.EN, "Bye...");
registerTranslation("NPC_SKILL_CBOW_END_1", LANG.EN, "See you later...");

registerTranslation("NPC_STRENGTH_INITIAL_TITLE", LANG.EN, "Train Strength");
registerTranslation("NPC_STRENGTH_INITIAL_0", LANG.EN, "What can you teach me?");
registerTranslation("NPC_STRENGTH_INITIAL_1", LANG.EN, "I can teach you how to be stronger...");

registerTranslation("NPC_STRENGTH_FIVE_POINT_TITLE", LANG.EN, "Strength +5, Cost: 5 LP...");
registerTranslation("NPC_STRENGTH_FIVE_POINT_0", LANG.EN, "Great, you learned something...");

registerTranslation("NPC_STRENGTH_FIVE_POINT_CANT_TITLE", LANG.EN, "Strength +5, Cost: 5 LP...");
registerTranslation("NPC_STRENGTH_FIVE_POINT_CANT_0", LANG.EN, "I can't teach you anything, you lack experience...");

registerTranslation("NPC_STRENGTH_TEN_POINT_TITLE", LANG.EN, "Strength +10, Cost: 10 LP...");
registerTranslation("NPC_STRENGTH_TEN_POINT_0", LANG.EN, "Great, you learned something...");

registerTranslation("NPC_STRENGTH_TEN_POINT_CANT_TITLE", LANG.EN, "Strength +10, Cost: 10 LP...");
registerTranslation("NPC_STRENGTH_TEN_POINT_CANT_0", LANG.EN, "I can't teach you anything, you lack experience...");

registerTranslation("NPC_STRENGTH_END_TITLE", LANG.EN, "Bye...");
registerTranslation("NPC_STRENGTH_END_0", LANG.EN, "Bye...");
registerTranslation("NPC_STRENGTH_END_1", LANG.EN, "See you later...");

registerTranslation("NPC_DEXTERITY_INITIAL_TITLE", LANG.EN, "Train Dexterity");
registerTranslation("NPC_DEXTERITY_INITIAL_0", LANG.EN, "What can you teach me?");
registerTranslation("NPC_DEXTERITY_INITIAL_1", LANG.EN, "I can teach you how to be more agile...");

registerTranslation("NPC_DEXTERITY_FIVE_POINT_TITLE", LANG.EN, "Dexterity +5, Cost: 5 LP...");
registerTranslation("NPC_DEXTERITY_FIVE_POINT_0", LANG.EN, "Great, you learned something...");

registerTranslation("NPC_DEXTERITY_FIVE_POINT_CANT_TITLE", LANG.EN, "Dexterity +5, Cost: 5 LP...");
registerTranslation("NPC_DEXTERITY_FIVE_POINT_CANT_0", LANG.EN, "I can't teach you anything, you lack experience...");

registerTranslation("NPC_DEXTERITY_TEN_POINT_TITLE", LANG.EN, "Dexterity +10, Cost: 10 LP...");
registerTranslation("NPC_DEXTERITY_TEN_POINT_0", LANG.EN, "Great, you learned something...");

registerTranslation("NPC_DEXTERITY_TEN_POINT_CANT_TITLE", LANG.EN, "Dexterity +10, Cost: 10 LP...");
registerTranslation("NPC_DEXTERITY_TEN_POINT_CANT_0", LANG.EN, "I can't teach you anything, you lack experience...");

registerTranslation("NPC_DEXTERITY_END_TITLE", LANG.EN, "Bye...");
registerTranslation("NPC_DEXTERITY_END_0", LANG.EN, "Bye...");
registerTranslation("NPC_DEXTERITY_END_1", LANG.EN, "See you later...");

registerTranslation("NPC_MANA_INITIAL_TITLE", LANG.EN, "Teach me how I can increase my magic energy...");
registerTranslation("NPC_MANA_INITIAL_0", LANG.EN, "Teach me how I can increase my magic energy...");
registerTranslation("NPC_MANA_INITIAL_1", LANG.EN, "Of course...");

registerTranslation("NPC_MANA_FIVE_POINT_TITLE", LANG.EN, "Mana +5, Cost: 5 LP...");
registerTranslation("NPC_MANA_FIVE_POINT_0", LANG.EN, "Great, you learned something...");


registerTranslation("NPC_MANA_FIVE_POINT_CANT_TITLE", LANG.EN, "Mana +5, Cost: 5 LP...");
registerTranslation("NPC_MANA_FIVE_POINT_CANT_0", LANG.EN, "I can't teach you anything, you lack experience...");

registerTranslation("NPC_MANA_TEN_POINT_TITLE", LANG.EN, "Mana +10, Cost: 10 LP...");
registerTranslation("NPC_MANA_TEN_POINT_0", LANG.EN, "Great, you learned something...");

registerTranslation("NPC_MANA_TEN_POINT_CANT_TITLE", LANG.EN, "Mana +10, Cost: 10 LP...");
registerTranslation("NPC_MANA_TEN_POINT_CANT_0", LANG.EN, "I can't teach you anything, you lack experience...");

registerTranslation("NPC_MANA_END_TITLE", LANG.EN, "Bye...");
registerTranslation("NPC_MANA_END_0", LANG.EN, "Bye...");
registerTranslation("NPC_MANA_END_1", LANG.EN, "See you later...");

registerTranslation("NPC_MAGIC_INITIAL_TITLE", LANG.EN, "Teach me to use the magic of the higher circle...");
registerTranslation("NPC_MAGIC_INITIAL_0", LANG.EN, "Teach me to use the magic of the higher circle...");
registerTranslation("NPC_MAGIC_INITIAL_1", LANG.EN, "Of course...");

registerTranslation("NPC_MAGIC_ONE_LEVEL_TITLE", LANG.EN, "Magic circle +1, Cost: 10 LP...");
registerTranslation("NPC_MAGIC_ONE_LEVEL_0", LANG.EN, "Great, you learned something...");

registerTranslation("NPC_MAGIC_ONE_LEVEL_CANT_TITLE", LANG.EN, "Magic circle +1, Cost: 10 LP...");
registerTranslation("NPC_MAGIC_ONE_LEVEL_CANT_0", LANG.EN, "I can't teach you anything, you lack experience...");

registerTranslation("NPC_MAGIC_END_TITLE", LANG.EN, "Bye...");
registerTranslation("NPC_MAGIC_END_0", LANG.EN, "Bye...");
registerTranslation("NPC_MAGIC_END_1", LANG.EN, "See you later...");