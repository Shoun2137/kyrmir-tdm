
registerTranslation("NPC_SKILL_INITIAL_0", LANG.DE, "Was kannst du mir beibringen?");
registerTranslation("NPC_SKILL_INITIAL_1", LANG.DE, "Ich kann dir alle Kampftechniken beibringen...");

registerTranslation("NPC_SKILL_END_TITLE", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_SKILL_END_0", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_SKILL_END_1", LANG.DE, "Bis zum n�chsten Mal...");

registerTranslation("NPC_SKILL_TRAINING_GROUND_TITLE", LANG.DE, "Ich m�chte eine Trainingseinheit haben...");

registerTranslation("NPC_SKILL_1H_INITIAL_TITLE", LANG.DE, "Lerne mit den Einh�ndigen Waffen zu k�mpfen");
registerTranslation("NPC_SKILL_1H_INITIAL_0", LANG.DE, "Was kannst du mir beibringen?");
registerTranslation("NPC_SKILL_1H_INITIAL_1", LANG.DE, "Ich kann dir zeigen, wie du deine Einhandwaffe besser f�hren kannst...");

registerTranslation("NPC_SKILL_1H_FIVE_POINT_TITLE", LANG.DE, "Einhandwaffen +5, Preis: 5 LP...");
registerTranslation("NPC_SKILL_1H_FIVE_POINT_0", LANG.DE, "Gut, du hast schon etwas dazu gelernt...");

registerTranslation("NPC_SKILL_1H_FIVE_POINT_CANT_TITLE", LANG.DE, "Einhandwaffen +5, Preis: 5 LP...");
registerTranslation("NPC_SKILL_1H_FIVE_POINT_CANT_0", LANG.DE, "Ich kann dir nichts beibringen, dir fehlt es an der n�tigen Erfahrung...");


registerTranslation("NPC_SKILL_1H_TEN_POINT_TITLE", LANG.DE, "Einhandwaffen +10, Preis: 10 LP...");
registerTranslation("NPC_SKILL_1H_TEN_POINT_0", LANG.DE, "Gut so, du hast schon einiges dazu gelernt...");

registerTranslation("NPC_SKILL_1H_TEN_POINT_CANT_TITLE", LANG.DE, "Einhandwaffen +10, Preis: 10 LP...");
registerTranslation("NPC_SKILL_1H_TEN_POINT_CANT_0", LANG.DE, "Ich kann dir nichts beibringen, dir fehlt es an der n�tigen Erfahrung...");

registerTranslation("NPC_SKILL_1H_END_TITLE", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_SKILL_1H_END_0", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_SKILL_1H_END_1", LANG.DE, "Bis zum n�chsten Mal...");

registerTranslation("NPC_SKILL_2H_INITIAL_TITLE", LANG.DE, "Lerne mit den Zweih�ndigen Waffen zu k�mpfen");
registerTranslation("NPC_SKILL_2H_INITIAL_0", LANG.DE, "Was kannst du mir beibringen?");
registerTranslation("NPC_SKILL_2H_INITIAL_1", LANG.DE, "Ich kann dir zeigen, wie du deine Zweihandwaffe besser f�hren kannst...");

registerTranslation("NPC_SKILL_2H_FIVE_POINT_TITLE", LANG.DE, "Zweihandwaffen +5, Preis: 5 LP...");
registerTranslation("NPC_SKILL_2H_FIVE_POINT_0", LANG.DE, "Gut so, du hast schon einiges dazu gelernt...");

registerTranslation("NPC_SKILL_2H_FIVE_POINT_CANT_TITLE", LANG.DE, "Zweihandwaffen +5, Preis: 5 LP...");
registerTranslation("NPC_SKILL_2H_FIVE_POINT_CANT_0", LANG.DE, "Ich kann dir nichts beibringen, dir fehlt es an der n�tigen Erfahrung...");

registerTranslation("NPC_SKILL_2H_TEN_POINT_TITLE", LANG.DE, "Zweihandwaffen +10, Preis: 10 LP...");
registerTranslation("NPC_SKILL_2H_TEN_POINT_0", LANG.DE, "Gut so, du hast schon einiges dazu gelernt...");

registerTranslation("NPC_SKILL_2H_TEN_POINT_CANT_TITLE", LANG.DE, "Zweihandwaffen +10, Preis: 10 LP...");
registerTranslation("NPC_SKILL_2H_TEN_POINT_CANT_0", LANG.DE, "Ich kann dir nichts beibringen, dir fehlt es an der n�tigen Erfahrung...");

registerTranslation("NPC_SKILL_2H_END_TITLE", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_SKILL_2H_END_0", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_SKILL_2H_END_1", LANG.DE, "Bis zum n�chsten Mal...");

registerTranslation("NPC_SKILL_BOW_INITIAL_TITLE", LANG.DE, "Lerne das schie�en mit B�gen");
registerTranslation("NPC_SKILL_BOW_INITIAL_0", LANG.DE, "Was kannst du mir beibringen?");
registerTranslation("NPC_SKILL_BOW_INITIAL_1", LANG.DE, "Ich kann dir beibringen, wie du B�gen besser f�hren kannst...");

registerTranslation("NPC_SKILL_BOW_FIVE_POINT_TITLE", LANG.DE, "B�gen +5, Preis: 5 LP...");
registerTranslation("NPC_SKILL_BOW_FIVE_POINT_0", LANG.DE, "Gut so, du hast schon einiges dazu gelernt...");

registerTranslation("NPC_SKILL_BOW_FIVE_POINT_CANT_TITLE", LANG.DE, "B�gen +5, Preis: 5 LP...");
registerTranslation("NPC_SKILL_BOW_FIVE_POINT_CANT_0", LANG.DE, "Ich kann dir nichts beibringen, dir fehlt es an der n�tigen Erfahrung...");

registerTranslation("NPC_SKILL_BOW_TEN_POINT_TITLE", LANG.DE, "B�gen +10, Preis: 10 LP...");
registerTranslation("NPC_SKILL_BOW_TEN_POINT_0", LANG.DE, "Gut so, du hast schon einiges dazu gelernt...");

registerTranslation("NPC_SKILL_BOW_TEN_POINT_CANT_TITLE", LANG.DE, "B�gen +10, Preis: 10 LP...");
registerTranslation("NPC_SKILL_BOW_TEN_POINT_CANT_0", LANG.DE, "Ich kann dir nichts beibringen, dir fehlt es an der n�tigen Erfahrung...");

registerTranslation("NPC_SKILL_BOW_END_TITLE", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_SKILL_BOW_END_0", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_SKILL_BOW_END_1", LANG.DE, "Bis zum n�chsten Mal...");

registerTranslation("NPC_SKILL_CBOW_INITIAL_TITLE", LANG.DE, "Lerne das schie�en mit Armbr�sten");
registerTranslation("NPC_SKILL_CBOW_INITIAL_0", LANG.DE, "Was kannst du mir beibringen?");
registerTranslation("NPC_SKILL_CBOW_INITIAL_1", LANG.DE, "Ich kann dir beibringen, wie du Armbr�ste besser f�hren kannst...");

registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_TITLE", LANG.DE, "Armbr�ste +5, Preis: 5 LP...");
registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_0", LANG.DE, "Gut so, du hast schon einiges dazu gelernt...");

registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_CANT_TITLE", LANG.DE, "Armbr�ste +5, Preis: 5 LP...");
registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_CANT_0", LANG.DE, "Ich kann dir nichts beibringen, dir fehlt es an der n�tigen Erfahrung...");

registerTranslation("NPC_SKILL_CBOW_TEN_POINT_TITLE", LANG.DE, "Armbr�ste +10, Preis: 10 LP...");
registerTranslation("NPC_SKILL_CBOW_TEN_POINT_0", LANG.DE, "Gut so, du hast schon einiges dazu gelernt...");

registerTranslation("NPC_SKILL_CBOW_TEN_POINT_CANT_TITLE", LANG.DE, "Armbr�ste +10, Preis: 10 LP...");
registerTranslation("NPC_SKILL_CBOW_TEN_POINT_CANT_0", LANG.DE, "Ich kann dir nichts beibringen, dir fehlt es an der n�tigen Erfahrung...");

registerTranslation("NPC_SKILL_CBOW_END_TITLE", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_SKILL_CBOW_END_0", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_SKILL_CBOW_END_1", LANG.DE, "Bis zum n�chsten Mal...");

registerTranslation("NPC_STRENGTH_INITIAL_TITLE", LANG.DE, "St�rke Trainieren");
registerTranslation("NPC_STRENGTH_INITIAL_0", LANG.DE, "Was kannst du mir beibringen?");
registerTranslation("NPC_STRENGTH_INITIAL_1", LANG.DE, "Ich kann dir beibringen, wie du st�rker wirst...");

registerTranslation("NPC_STRENGTH_FIVE_POINT_TITLE", LANG.DE, "St�rke +5, Preis: 5 LP...");
registerTranslation("NPC_STRENGTH_FIVE_POINT_0", LANG.DE, "Gut so, du hast schon einiges dazu gelernt...");

registerTranslation("NPC_STRENGTH_FIVE_POINT_CANT_TITLE", LANG.DE, "St�rke +5, Preis: 5 LP...");
registerTranslation("NPC_STRENGTH_FIVE_POINT_CANT_0", LANG.DE, "Ich kann dir nichts beibringen, dir fehlt es an der n�tigen Erfahrung...");

registerTranslation("NPC_STRENGTH_TEN_POINT_TITLE", LANG.DE, "St�rke +10, Preis: 10 LP...");
registerTranslation("NPC_STRENGTH_TEN_POINT_0", LANG.DE, "Gut so, du hast schon einiges dazu gelernt...");

registerTranslation("NPC_STRENGTH_TEN_POINT_CANT_TITLE", LANG.DE, "St�rke +10, Preis: 10 LP...");
registerTranslation("NPC_STRENGTH_TEN_POINT_CANT_0", LANG.DE, "Ich kann dir nichts beibringen, dir fehlt es an der n�tigen Erfahrung...");

registerTranslation("NPC_STRENGTH_END_TITLE", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_STRENGTH_END_0", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_STRENGTH_END_1", LANG.DE, "Bis zum n�chsten Mal...");

registerTranslation("NPC_DEXTERITY_INITIAL_TITLE", LANG.DE, "Geschick Trainieren");
registerTranslation("NPC_DEXTERITY_INITIAL_0", LANG.DE, "Was kannst du mir beibringen?");
registerTranslation("NPC_DEXTERITY_INITIAL_1", LANG.DE, "Ich kann dir beibringen, wie du Geschickter wirst...");

registerTranslation("NPC_DEXTERITY_FIVE_POINT_TITLE", LANG.DE, "Geschick +5, Preis: 5 LP...");
registerTranslation("NPC_DEXTERITY_FIVE_POINT_0", LANG.DE, "Gut so, du hast schon einiges dazu gelernt...");

registerTranslation("NPC_DEXTERITY_FIVE_POINT_CANT_TITLE", LANG.DE, "Geschick +5, Preis: 5 LP...");
registerTranslation("NPC_DEXTERITY_FIVE_POINT_CANT_0", LANG.DE, "Ich kann dir nichts beibringen, dir fehlt es an der n�tigen Erfahrung...");

registerTranslation("NPC_DEXTERITY_TEN_POINT_TITLE", LANG.DE, "Geschick +10, Preis: 10 LP...");
registerTranslation("NPC_DEXTERITY_TEN_POINT_0", LANG.DE, "Gut so, du hast schon einiges dazu gelernt...");

registerTranslation("NPC_DEXTERITY_TEN_POINT_CANT_TITLE", LANG.DE, "Geschick +10, Preis: 10 LP...");
registerTranslation("NPC_DEXTERITY_TEN_POINT_CANT_0", LANG.DE, "Ich kann dir nichts beibringen, dir fehlt es an der n�tigen Erfahrung...");

registerTranslation("NPC_DEXTERITY_END_TITLE", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_DEXTERITY_END_0", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_DEXTERITY_END_1", LANG.DE, "Bis zum n�chsten Mal...");

registerTranslation("NPC_MANA_INITIAL_TITLE", LANG.DE, "Zeig mir, wie ich meine magische Kraft steigern kann...");
registerTranslation("NPC_MANA_INITIAL_0", LANG.DE, "Zeig mir, wie ich meine magische Kraft steigern kann...");
registerTranslation("NPC_MANA_INITIAL_1", LANG.DE, "Ja, nat�rlich...");

registerTranslation("NPC_MANA_FIVE_POINT_TITLE", LANG.DE, "Mana +5, Cost: 5 LP...");
registerTranslation("NPC_MANA_FIVE_POINT_0", LANG.DE, "Gut so, du hast schon einiges dazu gelernt...");

registerTranslation("NPC_MANA_FIVE_POINT_CANT_TITLE", LANG.DE, "Mana +5, Cost: 5 LP...");
registerTranslation("NPC_MANA_FIVE_POINT_CANT_0", LANG.DE, "Ich kann dir nichts beibringen, dir fehlt es an der n�tigen Erfahrung...");

registerTranslation("NPC_MANA_TEN_POINT_TITLE", LANG.DE, "Mana +10, Cost: 10 LP...");
registerTranslation("NPC_MANA_TEN_POINT_0", LANG.DE, "Gut so, du hast schon einiges dazu gelernt...");

registerTranslation("NPC_MANA_TEN_POINT_CANT_TITLE", LANG.DE, "Mana +10, Cost: 10 LP...");
registerTranslation("NPC_MANA_TEN_POINT_CANT_0", LANG.DE, "Ich kann dir nichts beibringen, dir fehlt es an der n�tigen Erfahrung...");

registerTranslation("NPC_MANA_END_TITLE", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_MANA_END_0", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_MANA_END_1", LANG.DE, "Bis zum n�chsten Mal...");

registerTranslation("NPC_MAGIC_INITIAL_TITLE", LANG.DE, "Lehre mich, die Magischen Kreise der Magie...");
registerTranslation("NPC_MAGIC_INITIAL_0", LANG.DE, "Lehre mich, die Magischen Kreise der Magie...");
registerTranslation("NPC_MAGIC_INITIAL_1", LANG.DE, "Ja, nat�rlich...");

registerTranslation("NPC_MAGIC_ONE_LEVEL_TITLE", LANG.DE, "Magischer kreis +1, Preis: 10 LP...");
registerTranslation("NPC_MAGIC_ONE_LEVEL_0", LANG.DE, "Gut so, du hast schon einiges dazu gelernt...");

registerTranslation("NPC_MAGIC_ONE_LEVEL_CANT_TITLE", LANG.DE, "Magischer kreis +1, Preis: 10 LP...");
registerTranslation("NPC_MAGIC_ONE_LEVEL_CANT_0", LANG.DE, "Ich kann dir nichts beibringen, dir fehlt es an der n�tigen Erfahrung...");

registerTranslation("NPC_MAGIC_END_TITLE", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_MAGIC_END_0", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_MAGIC_END_1", LANG.DE, "Bis zum n�chsten Mal...");