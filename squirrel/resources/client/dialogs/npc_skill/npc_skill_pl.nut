
registerTranslation("NPC_SKILL_INITIAL_0", LANG.PL, "Czego mo�esz mnie nauczy�?");
registerTranslation("NPC_SKILL_INITIAL_1", LANG.PL, "Mog� nauczy� Ci� ka�dej umiej�tno�ci bitewnej...");

registerTranslation("NPC_SKILL_END_TITLE", LANG.PL, "�egnaj...");
registerTranslation("NPC_SKILL_END_0", LANG.PL, "�egnaj...");
registerTranslation("NPC_SKILL_END_1", LANG.PL, "Do widzenia...");

registerTranslation("NPC_SKILL_TRAINING_GROUND_TITLE", LANG.PL, "Chc� potrenowa�...");

registerTranslation("NPC_SKILL_1H_INITIAL_TITLE", LANG.PL, "Nauka walki broni� jednor�czn�");
registerTranslation("NPC_SKILL_1H_INITIAL_0", LANG.PL, "Czego mo�esz mnie nauczy�?");
registerTranslation("NPC_SKILL_1H_INITIAL_1", LANG.PL, "Mog� nauczy� Ci� jak lepiej pos�ugiwa� si� broni� jednor�czn�...");

registerTranslation("NPC_SKILL_1H_FIVE_POINT_TITLE", LANG.PL, "Bro� jednor�czna +5, Koszt: 5 PN...");
registerTranslation("NPC_SKILL_1H_FIVE_POINT_0", LANG.PL, "�wietnie, teraz umiesz wi�cej...");

registerTranslation("NPC_SKILL_1H_FIVE_POINT_CANT_TITLE", LANG.PL, "Bro� jednor�czna +5, Koszt: 5 PN...");
registerTranslation("NPC_SKILL_1H_FIVE_POINT_CANT_0", LANG.PL, "Nie mog� Ci� niczego nauczy�, brak Ci do�wiadczenia...");

registerTranslation("NPC_SKILL_1H_TEN_POINT_TITLE", LANG.PL, "Bro� jednor�czna +10, Koszt: 10 PN...");
registerTranslation("NPC_SKILL_1H_TEN_POINT_0", LANG.PL, "�wietnie, teraz umiesz wi�cej...");

registerTranslation("NPC_SKILL_1H_TEN_POINT_CANT_TITLE", LANG.PL, "Bro� jednor�czna +10, Koszt: 10 PN...");
registerTranslation("NPC_SKILL_1H_TEN_POINT_CANT_0", LANG.PL, "Nie mog� Ci� niczego nauczy�, brak Ci do�wiadczenia...");

registerTranslation("NPC_SKILL_1H_END_TITLE", LANG.PL, "�egnaj...");
registerTranslation("NPC_SKILL_1H_END_0", LANG.PL, "�egnaj...");
registerTranslation("NPC_SKILL_1H_END_1", LANG.PL, "Do widzenia...");

registerTranslation("NPC_SKILL_2H_INITIAL_TITLE", LANG.PL, "Nauka walki broni� dwur�czn�");
registerTranslation("NPC_SKILL_2H_INITIAL_0", LANG.PL, "Czego mo�esz mnie nauczy�?");
registerTranslation("NPC_SKILL_2H_INITIAL_1", LANG.PL, "Mog� nauczy� Ci� jak lepiej pos�ugiwa� si� broni� dwur�czn�...");

registerTranslation("NPC_SKILL_2H_FIVE_POINT_TITLE", LANG.PL, "Bro� dwur�czna +5, Koszt: 5 PN...");
registerTranslation("NPC_SKILL_2H_FIVE_POINT_0", LANG.PL, "�wietnie, teraz umiesz wi�cej...");

registerTranslation("NPC_SKILL_2H_FIVE_POINT_CANT_TITLE", LANG.PL, "Bro� dwur�czna +5, Koszt: 5 PN...");
registerTranslation("NPC_SKILL_2H_FIVE_POINT_CANT_0", LANG.PL, "Nie mog� Ci� niczego nauczy�, brak Ci do�wiadczenia...");

registerTranslation("NPC_SKILL_2H_TEN_POINT_TITLE", LANG.PL, "Bro� dwur�czna +10, Koszt: 10 PN...");
registerTranslation("NPC_SKILL_2H_TEN_POINT_0", LANG.PL, "�wietnie, teraz umiesz wi�cej...");

registerTranslation("NPC_SKILL_2H_TEN_POINT_CANT_TITLE", LANG.PL, "Bro� dwur�czna +10, Koszt: 10 PN...");
registerTranslation("NPC_SKILL_2H_TEN_POINT_CANT_0", LANG.PL, "Nie mog� Ci� niczego nauczy�, brak Ci do�wiadczenia...");

registerTranslation("NPC_SKILL_2H_END_TITLE", LANG.PL, "�egnaj...");
registerTranslation("NPC_SKILL_2H_END_0", LANG.PL, "�egnaj...");
registerTranslation("NPC_SKILL_2H_END_1", LANG.PL, "Do widzenia...");

registerTranslation("NPC_SKILL_BOW_INITIAL_TITLE", LANG.PL, "Nauka walki �ukiem");
registerTranslation("NPC_SKILL_BOW_INITIAL_0", LANG.PL, "Czego mo�esz mnie nauczy�?");
registerTranslation("NPC_SKILL_BOW_INITIAL_1", LANG.PL, "Mog� nauczy� Ci� jak lepiej pos�ugiwa� si� �ukiem...");

registerTranslation("NPC_SKILL_BOW_FIVE_POINT_TITLE", LANG.PL, "�uki +5, Koszt: 5 PN...");
registerTranslation("NPC_SKILL_BOW_FIVE_POINT_0", LANG.PL, "�wietnie, teraz umiesz wi�cej...");

registerTranslation("NPC_SKILL_BOW_FIVE_POINT_CANT_TITLE", LANG.PL, "�uki +5, Koszt: 5 PN...");
registerTranslation("NPC_SKILL_BOW_FIVE_POINT_CANT_0", LANG.PL, "Nie mog� Ci� niczego nauczy�, brak Ci do�wiadczenia...");

registerTranslation("NPC_SKILL_BOW_TEN_POINT_TITLE", LANG.PL, "�uki +10, Koszt: 10 PN...");
registerTranslation("NPC_SKILL_BOW_TEN_POINT_0", LANG.PL, "�wietnie, teraz umiesz wi�cej...");

registerTranslation("NPC_SKILL_BOW_TEN_POINT_CANT_TITLE", LANG.PL, "�uki +10, Koszt: 10 PN...");
registerTranslation("NPC_SKILL_BOW_TEN_POINT_CANT_0", LANG.PL, "Nie mog� Ci� niczego nauczy�, brak Ci do�wiadczenia...");

registerTranslation("NPC_SKILL_BOW_END_TITLE", LANG.PL, "�egnaj...");
registerTranslation("NPC_SKILL_BOW_END_0", LANG.PL, "�egnaj...");
registerTranslation("NPC_SKILL_BOW_END_1", LANG.PL, "Do widzenia...");

registerTranslation("NPC_SKILL_CBOW_INITIAL_TITLE", LANG.PL, "Nauka walki kusz�");
registerTranslation("NPC_SKILL_CBOW_INITIAL_0", LANG.PL, "Czego mo�esz mnie nauczy�?");
registerTranslation("NPC_SKILL_CBOW_INITIAL_1", LANG.PL, "Mog� nauczy� Ci� jak lepiej pos�ugiwa� si� kusz�...");

registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_TITLE", LANG.PL, "Kusze +5, Koszt: 5 PN...");
registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_0", LANG.PL, "�wietnie, teraz umiesz wi�cej...");

registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_CANT_TITLE", LANG.PL, "Kusze +5, Koszt: 5 PN...");
registerTranslation("NPC_SKILL_CBOW_FIVE_POINT_CANT_0", LANG.PL, "Nie mog� Ci� niczego nauczy�, brak Ci do�wiadczenia...");

registerTranslation("NPC_SKILL_CBOW_TEN_POINT_TITLE", LANG.PL, "Kusze +10, Koszt: 10 PN...");
registerTranslation("NPC_SKILL_CBOW_TEN_POINT_0", LANG.PL, "�wietnie, teraz umiesz wi�cej...");

registerTranslation("NPC_SKILL_CBOW_TEN_POINT_CANT_TITLE", LANG.PL, "Kusze +10, Koszt: 10 PN...");
registerTranslation("NPC_SKILL_CBOW_TEN_POINT_CANT_0", LANG.PL, "Nie mog� Ci� niczego nauczy�, brak Ci do�wiadczenia...");

registerTranslation("NPC_SKILL_CBOW_END_TITLE", LANG.PL, "�egnaj...");
registerTranslation("NPC_SKILL_CBOW_END_0", LANG.PL, "�egnaj...");
registerTranslation("NPC_SKILL_CBOW_END_1", LANG.PL, "Do widzenia...");

registerTranslation("NPC_STRENGTH_INITIAL_TITLE", LANG.PL, "Trening si�y");
registerTranslation("NPC_STRENGTH_INITIAL_0", LANG.PL, "Czego mo�esz mnie nauczy�?");
registerTranslation("NPC_STRENGTH_INITIAL_1", LANG.PL, "Mog� nauczy� Ci� jak by� silniejszym...");

registerTranslation("NPC_STRENGTH_FIVE_POINT_TITLE", LANG.PL, "Si�a +5, Koszt: 5 PN...");
registerTranslation("NPC_STRENGTH_FIVE_POINT_0", LANG.PL, "�wietnie, teraz umiesz wi�cej...");

registerTranslation("NPC_STRENGTH_FIVE_POINT_CANT_TITLE", LANG.PL, "Si�a +5, Koszt: 5 PN...");
registerTranslation("NPC_STRENGTH_FIVE_POINT_CANT_0", LANG.PL, "Nie mog� Ci� niczego nauczy�, brak Ci do�wiadczenia...");

registerTranslation("NPC_STRENGTH_TEN_POINT_TITLE", LANG.PL, "Si�a +10, Koszt: 10 PN...");
registerTranslation("NPC_STRENGTH_TEN_POINT_0", LANG.PL, "�wietnie, teraz umiesz wi�cej...");

registerTranslation("NPC_STRENGTH_TEN_POINT_CANT_TITLE", LANG.PL, "Si�a +10, Koszt: 10 PN...");
registerTranslation("NPC_STRENGTH_TEN_POINT_CANT_0", LANG.PL, "Nie mog� Ci� niczego nauczy�, brak Ci do�wiadczenia...");

registerTranslation("NPC_STRENGTH_END_TITLE", LANG.PL, "�egnaj...");
registerTranslation("NPC_STRENGTH_END_0", LANG.PL, "�egnaj...");
registerTranslation("NPC_STRENGTH_END_1", LANG.PL, "Do widzenia...");

registerTranslation("NPC_DEXTERITY_INITIAL_TITLE", LANG.PL, "Trening zr�czno�ci");
registerTranslation("NPC_DEXTERITY_INITIAL_0", LANG.PL, "Czego mo�esz mnie nauczy�?");
registerTranslation("NPC_DEXTERITY_INITIAL_1", LANG.PL, "Mog� nauczy� Ci� jak by� zr�czniejszym...");

registerTranslation("NPC_DEXTERITY_FIVE_POINT_TITLE", LANG.PL, "Zr�czno�� +5, Koszt: 5 PN...");
registerTranslation("NPC_DEXTERITY_FIVE_POINT_0", LANG.PL, "�wietnie, teraz umiesz wi�cej...");

registerTranslation("NPC_DEXTERITY_FIVE_POINT_CANT_TITLE", LANG.PL, "Zr�czno�� +5, Koszt: 5 PN...");
registerTranslation("NPC_DEXTERITY_FIVE_POINT_CANT_0", LANG.PL, "Nie mog� Ci� niczego nauczy�, brak Ci do�wiadczenia...");

registerTranslation("NPC_DEXTERITY_TEN_POINT_TITLE", LANG.PL, "Zr�czno�� +10, Koszt: 10 PN...");
registerTranslation("NPC_DEXTERITY_TEN_POINT_0", LANG.PL, "�wietnie, teraz umiesz wi�cej...");

registerTranslation("NPC_DEXTERITY_TEN_POINT_CANT_TITLE", LANG.PL, "Zr�czno�� +10, Koszt: 10 PN...");
registerTranslation("NPC_DEXTERITY_TEN_POINT_CANT_0", LANG.PL, "Nie mog� Ci� niczego nauczy�, brak Ci do�wiadczenia...");

registerTranslation("NPC_DEXTERITY_END_TITLE", LANG.PL, "�egnaj...");
registerTranslation("NPC_DEXTERITY_END_0", LANG.PL, "�egnaj...");
registerTranslation("NPC_DEXTERITY_END_1", LANG.PL, "Do widzenia...");

registerTranslation("NPC_MANA_INITIAL_TITLE", LANG.PL, "Naucz mnie jak mog� zwi�kszy� swoj� energie magiczn�...");
registerTranslation("NPC_MANA_INITIAL_0", LANG.PL, "Naucz mnie jak mog� zwi�kszy� swoj� energie magiczn�...");
registerTranslation("NPC_MANA_INITIAL_1", LANG.PL, "Oczywi�cie...");

registerTranslation("NPC_MANA_FIVE_POINT_TITLE", LANG.PL, "Mana +5, Koszt: 5 PN...");
registerTranslation("NPC_MANA_FIVE_POINT_0", LANG.PL, "�wietnie, teraz umiesz wi�cej...");

registerTranslation("NPC_MANA_FIVE_POINT_CANT_TITLE", LANG.PL, "Mana +5, Koszt: 5 PN...");
registerTranslation("NPC_MANA_FIVE_POINT_CANT_0", LANG.PL, "Nie mog� Ci� niczego nauczy�, brak Ci do�wiadczenia...");

registerTranslation("NPC_MANA_TEN_POINT_TITLE", LANG.PL, "Mana +10, Koszt: 10 PN...");
registerTranslation("NPC_MANA_TEN_POINT_0", LANG.PL, "�wietnie, teraz umiesz wi�cej...");

registerTranslation("NPC_MANA_TEN_POINT_CANT_TITLE", LANG.PL, "Mana +10, Koszt: 10 PN...");
registerTranslation("NPC_MANA_TEN_POINT_CANT_0", LANG.PL, "Nie mog� Ci� niczego nauczy�, brak Ci do�wiadczenia...");

registerTranslation("NPC_MANA_END_TITLE", LANG.PL, "�egnaj...");
registerTranslation("NPC_MANA_END_0", LANG.PL, "�egnaj...");
registerTranslation("NPC_MANA_END_1", LANG.PL, "Do widzenia...");

registerTranslation("NPC_MAGIC_INITIAL_TITLE", LANG.PL, "Naucz mnie u�ywa� magii wy�szego kr�gu...");
registerTranslation("NPC_MAGIC_INITIAL_0", LANG.PL, "Naucz mnie u�ywa� magii wy�szego kr�gu...");
registerTranslation("NPC_MAGIC_INITIAL_1", LANG.PL, "Oczywi�cie...");

registerTranslation("NPC_MAGIC_ONE_LEVEL_TITLE", LANG.PL, "Kr�g magii +1, Koszt: 10 PN...");
registerTranslation("NPC_MAGIC_ONE_LEVEL_0", LANG.PL, "�wietnie, teraz umiesz wi�cej...");

registerTranslation("NPC_MAGIC_ONE_LEVEL_CANT_TITLE", LANG.PL, "Kr�g magii +1, Koszt: 10 PN...");
registerTranslation("NPC_MAGIC_ONE_LEVEL_CANT_0", LANG.PL, "Nie mog� Ci� niczego nauczy�, brak Ci do�wiadczenia...");

registerTranslation("NPC_MAGIC_END_TITLE", LANG.PL, "�egnaj...");
registerTranslation("NPC_MAGIC_END_0", LANG.PL, "�egnaj...");
registerTranslation("NPC_MAGIC_END_1", LANG.PL, "Do widzenia...");