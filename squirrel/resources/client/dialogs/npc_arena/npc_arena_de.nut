
registerTranslation("NPC_ARENA_INITIAL_0", LANG.DE, "Hallo?");
registerTranslation("NPC_ARENA_INITIAL_1", LANG.DE, "Guten Tag...");

registerTranslation("NPC_ARENA_BACK_TO_WORLD_TITLE", LANG.DE, "Ich m�chte in die Hauptwelt zur�ckkehren...");

registerTranslation("NPC_ARENA_END_TITLE", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_ARENA_END_0", LANG.DE, "Auf Wiedersehen...");
registerTranslation("NPC_ARENA_END_1", LANG.DE, "Bis zum n�chsten Mal...");