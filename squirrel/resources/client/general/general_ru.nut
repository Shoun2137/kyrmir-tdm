
/********************* OTHERS *********************/

registerTranslation("INTERACTION_KEY_0", LANG.RU, "��� �������������� ������� �������");
registerTranslation("USE_ONLY_DURING_S_RUN", LANG.RU, "������ ����������, ����� ������������ �����");
registerTranslation("CHECK_YOUR_MANA", LANG.RU, "� ��� ������������ ����!");

/********************* MESSAGES *********************/

registerTranslation("POP-UP_MSG_LEVEL_UP", LANG.RU, "�� �������� ������ %d");
registerTranslation("POP-UP_MSG_RENDER", LANG.RU, "�� ������ ������ �������� ���������� 1 ��� � 15 ������...");
registerTranslation("POP-UP_MSG_DEBUG_COMBINATION" LANG.RU, "��������� ��������� ������, ������ ��� ����� ������������ ��� ����������");
registerTranslation("POP-UP_MSG_VOICECALL" LANG.RU, "��������� ��������� ������, ������ ��� ����� ������������ ��������� �������");
registerTranslation("POP-UP_MSG_EXIT", LANG.RU, "%d ������ �� ������...");
registerTranslation("POP-UP_MSG_GUILD", LANG.RU, "�� �� ������ ������� �������, ���� �������� 3-�� ��� ����� �������� ������!");
registerTranslation("POP-UP_MSG_LEVEL_UP", LANG.RU, "�� �������� ������ %d");
registerTranslation("POP-UP_MSG_EXP-GAIN", LANG.RU, "�� �������� %d ����� �����!");
registerTranslation("POP-UP_MSG_ITEM-GAIN", LANG.RU, "�� �������� ������� ;%s; x %d!");
registerTranslation("SERVER_MESSAGE_JOIN", LANG.RU, "����� %s ������������� � ����!");
registerTranslation("SERVER_MESSAGE_LEFT", LANG.RU, "����� %s ������� ����!");

/********************* CHATS *********************/

registerTranslation("GLOBAL_CHAT", LANG.RU, "����������");
registerTranslation("GUILD_CHAT", LANG.RU, "�������");
registerTranslation("ADMIN_CHAT", LANG.RU, "�����");

/********************* CHAT MESSAGES *********************/ 

registerTranslation("CHAT_ERROR", LANG.RU, "��������� ����� ��������� ������� ���������!");
registerTranslation("CHAT_NO_PERMISSION", LANG.RU, "� ��� ��� ���� �� ������������� ���� �������!");
registerTranslation("CHAT_MUTED", LANG.RU, "�� ���������!");

/********************* DIALOG VIEW  *********************/

registerTranslation("DIALOG_VIEW_AVAILABLE", LANG.RU, "��������");
registerTranslation("DIALOG_VIEW_TYPE_TITLE", LANG.RU, "��� ������");
registerTranslation("DIALOG_VIEW_REQUIREMENT_TITLE", LANG.RU, "���������� � ��������");
registerTranslation("DIALOG_VIEW_PROTECTION_TITLE", LANG.RU, "������ ��");
registerTranslation("DIALOG_VIEW_DESCRIPTION_TITLE", LANG.RU, "��������");
registerTranslation("DIALOG_VIEW_ITEM_PRICE", LANG.RU, "���� �� �������");
registerTranslation("DIALOG_VIEW_MANA_USAGE", LANG.RU, "������ ����");
registerTranslation("DIALOG_VIEW_COST", LANG.RU, "������");
registerTranslation("DIALOG_VIEW_COST_NO_DATA", LANG.RU, "��� ������");
registerTranslation("DIALOG_VIEW_PLACEHOLDER", LANG.RU, "����������");
registerTranslation("DIALOG_VIEW_END", LANG.RU, "����� ������");
registerTranslation("DIALOG_VIEW_BUY", LANG.RU, "������");
registerTranslation("DIALOG_VIEW_BUY_SUCCES", LANG.RU, "���� ������ ����������� �������...");
registerTranslation("DIALOG_VIEW_BUY_FAULT", LANG.RU, "��������� 5 ������ �� ���������� �������������...");
registerTranslation("DIALOG_VIEW_BUY_FAULT_AMOUNT", LANG.RU, "����� �������� �� ������������� ��������...");
registerTranslation("DIALOG_VIEW_CANNOT_BUY", LANG.RU, "� ��� ������������ �������� ��� ������� ����� ��������...");
registerTranslation("DIALOG_VIEW_EFFECT_TITLE", LANG.RU, "������");

/********************* MAP VIEW *********************/

registerTranslation("MAP_VIEW_RESPAWN", LANG.RU, "� ����� ������");

/********************* EGUIPMENT VIEW *********************/

registerTranslation("EQUIPMENT_VIEW_TYPE_TILE", LANG.RU, "��� ������");
registerTranslation("EQUIPMENT_VIEW_REQUIREMENT_TITLE", LANG.RU, "���������� � ��������");
registerTranslation("EQUIPMENT_VIEW_PROTECTION_TITLE", LANG.RU, "������ ��");
registerTranslation("EQUIPMENT_VIEW_DAMAGE_TITLE", LANG.RU, "���� ��");
registerTranslation("EQUIPMENT_VIEW_MANA_USAGE" LANG.RU, "������ ����");
registerTranslation("EQUIPMENT_VIEW_DESCRIPTION_TITLE", LANG.RU, "��������");
registerTranslation("EQUIPMENT_VIEW_EQUIP_FAULT", LANG.RU, "�� �� ������ ������������ ���� �������...");
registerTranslation("EQUIPMENT_VIEW_INFO", LANG.RU, "��� ���������� �������� � ���� �������:\n0, 1, 2, 3, 4, 5, 6, 7, 8, 9\n��� ������ ���������� �������� ����������� - Q!\n��� �������������/������ ���������� ��������\n������� - E ��� ����� ������ ����!");
registerTranslation("EQUIPMENT_VIEW_DROP_INPUT", LANG.RU, "������� �����");
registerTranslation("EQUIPMENT_VIEW_DROP_BUTTON", LANG.RU, "���������");
registerTranslation("EQUIPMENT_VIEW_DROP_EQUIPED", LANG.RU, "������� �����������!");
registerTranslation("EQUIPMENT_VIEW_DROP_AMOUNT", LANG.RU, "������������ ���������!");
registerTranslation("EQUIPMENT_VIEW_DROP_BOUNDED", LANG.RU, "���� ������� �������� � ������ ���������!");
registerTranslation("EQUIPMENT_VIEW_EFFECT_TITLE", LANG.RU, "������");

/********************* LOGIN VIEW *********************/

registerTranslation("LOGIN_VIEW_TITLE", LANG.RU, "����� ���������� � Kyrmir TDM");
registerTranslation("LOGIN_VIEW_SUBTITLE", LANG.RU, "������� ���� ��� ������������ � ������.");
registerTranslation("LOGIN_VIEW_USERNAME", LANG.RU, "��� ������������");
registerTranslation("LOGIN_VIEW_PASSWORD", LANG.RU, "������");
registerTranslation("LOGIN_VIEW_BTN_LOGIN", LANG.RU, "�����");
registerTranslation("LOGIN_VIEW_BTN_REGISTER", LANG.RU, "�����������");
registerTranslation("LOGIN_VIEW_LANG_SELECT" LANG.RU, "�������� ���� ����");

registerTranslation("LOGIN_VIEW_MSG_USERNAME", LANG.RU, "��� ������������ �������!");
registerTranslation("LOGIN_VIEW_MSG_EMPTY_FIELD", LANG.RU, "�� �������� ���� ������!");
registerTranslation("LOGIN_VIEW_MSG_FAULT", LANG.RU, "��������� ������ �������!");
registerTranslation("LOGIN_VIEW_MSG_NOT_EXIST", LANG.RU, "����� ������� ������ �� ����������!");

/********************* REGISTER VIEW *********************/

registerTranslation("REGISTER_VIEW_TITLE", LANG.RU, "����� ���������� � Kyrmir TDM");
registerTranslation("REGISTER_VIEW_SUBTITLE", LANG.RU, "������� ���� ��� ������������ � ������.");
registerTranslation("REGISTER_VIEW_INFO_0", LANG.RU, "���� �������� ������������ �������, ����� ��� ������ �������");
registerTranslation("REGISTER_VIEW_INFO_1", LANG.RU, "��� ������� 6 ��������, ��� ������� 1 �����, ��� ������� ���� ��������� �����!");
registerTranslation("REGISTER_VIEW_INFO_2", LANG.RU, "����� �� ��������� ������������ ���������� ������!");
registerTranslation("REGISTER_VIEW_USERNAME", LANG.RU, "��� ������������");
registerTranslation("REGISTER_VIEW_PASSWORD", LANG.RU, "������");
registerTranslation("REGISTER_VIEW_PASSWORD_REPEAT", LANG.RU, "��������� ������");
registerTranslation("REGISTER_VIEW_BTN_REGISTER", LANG.RU, "�����������");
registerTranslation("REGISTER_VIEW_BTN_BACK", LANG.RU, "�����");
registerTranslation("REGISTER_VIEW_MSG_PASSWORDS_MATCH", LANG.RU, "������ �� ���������!");
registerTranslation("REGISTER_VIEW_MSG_PASSWORD", LANG.RU, "������ �� ������������� ����������� ������������!");
registerTranslation("REGISTER_VIEW_MSG_USERNAME", LANG.RU, "��� ������������ �� ������������� �����������!");
registerTranslation("REGISTER_VIEW_MSG_EMPTY_FIELD", LANG.RU, "�� �������� ���� ������!");
registerTranslation("REGISTER_VIEW_MSG_SUCCES", LANG.RU, "�� ������� ���������������� ����� �������! �� ������ ����� � �������!");
registerTranslation("REGISTER_VIEW_MSG_FAULT", LANG.RU, "���������� ���������������� �������");
registerTranslation("REGISTER_VIEW_MSG_DATA_USED", LANG.RU, "���� ���� ��� ����������!");

registerTranslation("VISUAL_VIEW_TITLE", LANG.RU, "��������� ���������");
registerTranslation("VISUAL_VIEW_FACES_DESCRIPTION", LANG.RU, "�� ������ ������������ ���������\n���� ��� ��������� �� ������ ���!");
registerTranslation("VISUAL_VIEW_HEAD", LANG.RU, "�������� ������ ������");
registerTranslation("VISUAL_VIEW_TORSO", LANG.RU, "�������� �������� ����");
registerTranslation("VISUAL_VIEW_SEX", LANG.RU, "������� ���");
registerTranslation("VISUAL_VIEW_SAVE_VISUAL", LANG.RU, "���������");
registerTranslation("VISUAL_VIEW_RESET_VISUAL", LANG.RU, "��������");
registerTranslation("VISUAL_VIEW_CAMERA", LANG.RU, "������� ������");

registerTranslation("GUILD_VIEW_TITLE", LANG.RU, "�������� ���� ������!");
registerTranslation("GUILD_VIEW_BTN_CAMP", LANG.RU, "�������� ������");

/********************* MAIN VIEW *********************/

registerTranslation("MAIN_VIEW_OPTIONS", LANG.RU, "�����");
registerTranslation("MAIN_VIEW_VISUAL", LANG.RU, "���������� ���������");
registerTranslation("MAIN_VIEW_HELP", LANG.RU, "������");
registerTranslation("MAIN_VIEW_GUILD", LANG.RU, "������� �������");
registerTranslation("MAIN_VIEW_EXIT", LANG.RU, "�����");
registerTranslation("MAIN_VIEW_LANG_SELECT" LANG.RU, "�������� ���� ����");

/********************* ANIMATIONS VIEW *********************/

registerTranslation("VIEW_ANIMATION_TITLE", LANG.RU, "��������");

/********************* OPTIONS VIEW *********************/

registerTranslation("OPTIONS_VIEW_RESOLUTION", LANG.RU, "����������");
registerTranslation("OPTIONS_VIEW_DISABLE_MUSIC_SYSTEM", LANG.RU, "��������� ����");
registerTranslation("OPTIONS_VIEW_SOUND_VOLUME", LANG.RU, "��������� �����");
registerTranslation("OPTIONS_VIEW_MUSIC_VOLUME", LANG.RU, "��������� ������");
registerTranslation("OPTIONS_VIEW_CHAT_LINES", LANG.RU, "������ ����");
registerTranslation("OPTIONS_VIEW_SAVE_LOGIN", LANG.RU, "��������� ������ ��� �����");
registerTranslation("OPTIONS_VIEW_WEAPON_TRAIL", LANG.RU, "��������� �����");
registerTranslation("OPTIONS_VIEW_YES", LANG.RU, "��");
registerTranslation("OPTIONS_VIEW_NO", LANG.RU, "���");

registerTranslation("HELP_VIEW_TITLE", LANG.RU, "������� � ��������� ������");
registerTranslation("HELP_VIEW_RENDER", LANG.RU, "F1 F2 F3 F4 - ��������� ��������� ����������");
registerTranslation("HELP_VIEW_PLAYERLIST", LANG.RU, "F5 - ������ �������");
registerTranslation("HELP_VIEW_ANIMLIST", LANG.RU, "F6 - ������ ��������");
registerTranslation("HELP_VIEW_HIDE_UI", LANG.RU, "F12 - ������ ���������������� ���������");
registerTranslation("HELP_VIEW_STATS", LANG.RU, "B - ���������� ���������");
registerTranslation("HELP_VIEW_TALK", LANG.RU, "CTRL - �������� � NPC");
registerTranslation("HELP_VIEW_SKIP", LANG.RU, "������ - ���������� �������� � NPC");
registerTranslation("HELP_VIEW_SCROLL", LANG.RU, "Mouse Scroll - ���������� ����������� �������� ��� �������� �������");
registerTranslation("HELP_VIEW_DEBUG", LANG.RU, "CTRL + ALT + F8 - ���� �� �� ������ ��������� ������-��, ����������� ��� ����������");
registerTranslation("HELP_VIEW_MAP", LANG.RU, "M - �������� �����");
registerTranslation("HELP_VIEW_CHAT_CHANGE", LANG.RU, "CTRL + X - ����������� ������� ����");
registerTranslation("HELP_VIEW_CHAT_OPEN", LANG.RU, "T - ���");
registerTranslation("HELP_VIEW_QUICKSLOT", LANG.RU, "Quickslots - �������: 1-2-3-4-5-6-7-8-9-0");
registerTranslation("HELP_VIEW_VOICE_COMMAND_0", LANG.RU, "��������� �������: (�� ������ ������������ �� ������ 6 ������)");
registerTranslation("HELP_VIEW_VOICE_COMMAND_1", LANG.RU, "Num 9 - ����, ����!");
registerTranslation("HELP_VIEW_VOICE_COMMAND_2", LANG.RU, "Num 8 - ������ ������, ����");
registerTranslation("HELP_VIEW_VOICE_COMMAND_3", LANG.RU, "Num 7 - �������");
registerTranslation("HELP_VIEW_VOICE_COMMAND_4", LANG.RU, "Num 6 - Help");
registerTranslation("HELP_VIEW_VOICE_COMMAND_5", LANG.RU, "Num 5 - Insult");
registerTranslation("HELP_VIEW_VOICE_COMMAND_6", LANG.RU, "Num 4 - ���� �����");
registerTranslation("HELP_VIEW_COMMANDS", LANG.RU, "������� ����");
registerTranslation("HELP_VIEW_COMMANDS_PM", LANG.RU, "������ ��������� - /pw player_id text ������: /pw 5 ������");

registerTranslation("STATS_VIEW_TITLE", LANG.RU, "���������� ������");
registerTranslation("STATS_VIEW_CHARACTER", LANG.RU, "��������");
registerTranslation("STATS_VIEW_ATRIBUTES", LANG.RU, "��������");
registerTranslation("STATS_VIEW_PROTECTION", LANG.RU, "������ ���������");
registerTranslation("STATS_VIEW_SKILLS", LANG.RU, "������");
registerTranslation("STATS_VIEW_GUILD", LANG.RU, "�������");

registerTranslation("STATUS_VIEW_TARGET", LANG.RU, "���� ������");
registerTranslation("STATUS_VIEW_BEGIN", LANG.RU, "����� �������� �����");
registerTranslation("STATUS_VIEW_NO_ROUND", LANG.RU, "����� �����������");
registerTranslation("STATUS_VIEW_BONUS_EMPTY", LANG.RU, "����� ����������");
registerTranslation("STATUS_VIEW_ROUND_BONUS", LANG.RU, "Bonus exp: %.2fx");
registerTranslation("STATUS_VIEW_KILL", LANG.RU, "%s ��� ���� %s(%d)!");
registerTranslation("STATUS_VIEW_STREAK", LANG.RU, "����� %s ���� %d �������, ���� ������ %s!");
registerTranslation("STATUS_VIEW_LEVEL_THREE", LANG.RU, "����� %s ������ 3-�� ������!");
registerTranslation("STATUS_VIEW_BOSS_KILL", LANG.RU, "%s ��� ���� %s!");
registerTranslation("STATUS_VIEW_BOSS_RESPAWN", LANG.RU, "���� %s �������� �� �����!");
registerTranslation("STATUS_VIEW_KILLS", LANG.RU, "�������");
registerTranslation("STATUS_VIEW_DEATHS", LANG.RU, "�������");
registerTranslation("STATUS_VIEW_ASSISTS", LANG.RU, "������");
registerTranslation("STATUS_VIEW_ENTER_CAMP", LANG.RU, "��������� ���� ��������� � �������� ������!");

/********************* PLAYERLIST VIEW *********************/

registerTranslation("PLAYER_LIST_VIEW_TITLE", LANG.RU, "������ �������");
registerTranslation("PLAYER_LIST_VIEW_ONLINE", LANG.RU, "������-������");
registerTranslation("PLAYER_LIST_VIEW_MEMBERS", LANG.RU, "������ � �������");
registerTranslation("PLAYER_LIST_VIEW_ID", LANG.RU, "Id");
registerTranslation("PLAYER_LIST_VIEW_NICKNAME", LANG.RU, "�������");
registerTranslation("PLAYER_LIST_VIEW_PING", LANG.RU, "����");
registerTranslation("PLAYER_LIST_VIEW_KILLS", LANG.RU, "�������");
registerTranslation("PLAYER_LIST_VIEW_DEATHS", LANG.RU, "�������");
registerTranslation("PLAYER_LIST_VIEW_ASSISTS", LANG.RU, "������");

/********************* RANKLIST VIEW *********************/

registerTranslation("RANK_LIST_VIEW_NUMBER", LANG.RU, "No");
registerTranslation("RANK_LIST_VIEW_TITLE", LANG.RU, "������ �������");
registerTranslation("RANK_LIST_VIEW_NICKNAME", LANG.RU, "���");
registerTranslation("RANK_LIST_VIEW_KILLS", LANG.RU, "��������");
registerTranslation("RANK_LIST_VIEW_DEATHS", LANG.RU, "������");
registerTranslation("RANK_LIST_VIEW_ASSISTS", LANG.RU, "������");
registerTranslation("RANK_LIST_VIEW_KDA", LANG.RU, "����");
registerTranslation("RANK_LIST_VIEW_DMG", LANG.RU, "����������");

registerTranslation("LEVEL", LANG.RU, "�������");
registerTranslation("EXPERIENCE", LANG.RU, "���� �����");
registerTranslation("EXPERIENCE_NEXT_LEVEL", LANG.RU, "���� ���������� ������");
registerTranslation("LEARN_POINTS", LANG.RU, "���� ��������");
registerTranslation("STRENGTH", LANG.RU, "����");
registerTranslation("DEXTERITY", LANG.RU, "��������");
registerTranslation("MANA", LANG.RU, "����");
registerTranslation("HEALTH", LANG.RU, "��������");
registerTranslation("MAGIC_LEVEL", LANG.RU, "������� �����");
registerTranslation("GUILD_NAME", LANG.RU, "�������� �������");

/********************* MULTIUSE *********************/

registerTranslation("REQUIREMENT_TYPE_0", LANG.RU, "�������");
registerTranslation("REQUIREMENT_TYPE_1", LANG.RU, "����� �����");
registerTranslation("REQUIREMENT_TYPE_2", LANG.RU, "����");
registerTranslation("REQUIREMENT_TYPE_3", LANG.RU, "��������");
registerTranslation("REQUIREMENT_TYPE_4", LANG.RU, "����");
registerTranslation("REQUIREMENT_TYPE_5", LANG.RU, "��������");

registerTranslation("DAMAGE_TYPE_2", LANG.RU, "���� �� ���������");
registerTranslation("DAMAGE_TYPE_4", LANG.RU, "���� �� ��������");
registerTranslation("DAMAGE_TYPE_64", LANG.RU, "���� �� �����");
registerTranslation("DAMAGE_TYPE_8", LANG.RU, "���� �� ����");
registerTranslation("DAMAGE_TYPE_32", LANG.RU, "���� �� �����");

registerTranslation("WEAPON_MODE_3", LANG.RU, "����������");
registerTranslation("WEAPON_MODE_4", LANG.RU, "���������");
registerTranslation("WEAPON_MODE_5", LANG.RU, "���");
registerTranslation("WEAPON_MODE_6", LANG.RU, "�������");
registerTranslation("WEAPON_MODE_7", LANG.RU, "�����");

registerTranslation("WEAPON_SKILL_0", LANG.RU, "���������� ������");
registerTranslation("WEAPON_SKILL_1", LANG.RU, "��������� ������");
registerTranslation("WEAPON_SKILL_2", LANG.RU, "���");
registerTranslation("WEAPON_SKILL_3", LANG.RU, "��������");

registerTranslation("EFFECT_TYPE_" + ItemEffect.HealthRecovery, LANG.RU, "��������������� ���� ��������");
registerTranslation("EFFECT_TYPE_" + ItemEffect.HealthPercentRecovery, LANG.RU, "��������������� ���� �������� (�������)");
registerTranslation("EFFECT_TYPE_" + ItemEffect.ManaRecovery, LANG.RU, "��������������� ���� ����");
registerTranslation("EFFECT_TYPE_" + ItemEffect.ManaPercentRecovery, LANG.RU, "��������������� ���� ���� (�������)");

/********************* ANIMS *********************/

registerTranslation("T_STAND_2_SIT", LANG.RU, "������");
registerTranslation("T_STAND_2_SLEEPGROUND", LANG.RU, "�����");
registerTranslation("T_STAND_2_PEE", LANG.RU, "������");
registerTranslation("S_PRAY", LANG.RU, "��������");
registerTranslation("S_DEAD", LANG.RU, "�������");
registerTranslation("S_LGUARD", LANG.RU, "����� 1");
registerTranslation("S_HGUARD", LANG.RU, "����� 2");
registerTranslation("T_SEARCH", LANG.RU, "�����");
registerTranslation("T_PLUNDER", LANG.RU, "��������");
registerTranslation("S_WASH", LANG.RU, "��������");
registerTranslation("R_SCRATCHEGG", LANG.RU, "��������");
registerTranslation("S_FIRE_VICTIM", LANG.RU, "�����");
registerTranslation("T_1HSINSPECT", LANG.RU, "��������� ������");
registerTranslation("T_GREETNOV", LANG.RU, "�����������");
registerTranslation("T_1HSFREE", LANG.RU, "������������ �����");
registerTranslation("T_WATCHFIGHT_OHNO", LANG.RU, "������ ��");
registerTranslation("T_YES", LANG.RU, "��");
registerTranslation("T_NO", LANG.RU, "���");
registerTranslation("R_SCRATCHHEAD", LANG.RU, "�������� 2");
registerTranslation("T_BORINGKICK", LANG.RU, "������");

/********************* GUILDS *********************/

registerTranslation("OLD_CAMP", LANG.RU, "������ ������");
registerTranslation("SWAMP_CAMP", LANG.RU, "������ ���������");
registerTranslation("NEW_CAMP", LANG.RU, "����� ������");
registerTranslation("PIRATE_CAMP", LANG.RU, "��������� ������");
registerTranslation("BANDIT_CAMP", LANG.RU, "������ �����������");