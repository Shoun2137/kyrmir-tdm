
/********************* OTHERS *********************/

registerTranslation("INTERACTION_KEY_0", LANG.EN, "To interact click key");
registerTranslation("USE_ONLY_DURING_S_RUN", LANG.EN, "Stand still to use the potion");
registerTranslation("CHECK_YOUR_MANA", LANG.EN, "You don't have enough mana!");

/********************* MESSAGES *********************/

registerTranslation("POP-UP_MSG_LEVEL_UP", LANG.EN, "You have reached level %d");
registerTranslation("POP-UP_MSG_RENDER", LANG.EN, "You can change you rendering range 1 time per 15 seconds...");
registerTranslation("POP-UP_MSG_DEBUG_COMBINATION" LANG.EN, "Wait a few seconds before using this combination again");
registerTranslation("POP-UP_MSG_VOICECALL" LANG.EN, "Wait a few seconds before using the voice command again");
registerTranslation("POP-UP_MSG_EXIT", LANG.EN, "%d seconds to exit...");
registerTranslation("POP-UP_MSG_GUILD", LANG.EN, "You can't change guilds once you've reached level 3 or higher!");
registerTranslation("POP-UP_MSG_LEVEL_UP", LANG.EN, "You have reached level %d");
registerTranslation("POP-UP_MSG_EXP-GAIN", LANG.EN, "You received %d experience points!");
registerTranslation("POP-UP_MSG_ITEM-GAIN", LANG.EN, "You have received an item ;%s; x %d!");
registerTranslation("SERVER_MESSAGE_JOIN", LANG.EN, "Player %s joined the game!");
registerTranslation("SERVER_MESSAGE_LEFT", LANG.EN, "Player %s left the game!");

/********************* CHATS *********************/

registerTranslation("GLOBAL_CHAT", LANG.EN, "Global");
registerTranslation("GUILD_CHAT", LANG.EN, "Faction");
registerTranslation("ADMIN_CHAT", LANG.EN, "Admin");

/********************* CHAT MESSAGES *********************/ 

registerTranslation("CHAT_ERROR", LANG.EN, "Wait before sending another message!");
registerTranslation("CHAT_NO_PERMISSION", LANG.EN, "You don't have permissions to use this command!");
registerTranslation("CHAT_MUTED", LANG.EN, "You are muted!");

/********************* DIALOG VIEW  *********************/

registerTranslation("DIALOG_VIEW_AVAILABLE", LANG.EN, "Available");
registerTranslation("DIALOG_VIEW_TYPE_TITLE", LANG.EN, "Weapon type");
registerTranslation("DIALOG_VIEW_REQUIREMENT_TITLE", LANG.EN, "Item requirements");
registerTranslation("DIALOG_VIEW_PROTECTION_TITLE", LANG.EN, "Protection against");
registerTranslation("DIALOG_VIEW_DESCRIPTION_TITLE", LANG.EN, "Desc");
registerTranslation("DIALOG_VIEW_ITEM_PRICE", LANG.EN, "per unit");
registerTranslation("DIALOG_VIEW_MANA_USAGE", LANG.EN, "Mana cost");
registerTranslation("DIALOG_VIEW_COST", LANG.EN, "gold");
registerTranslation("DIALOG_VIEW_COST_NO_DATA", LANG.EN, "No data");
registerTranslation("DIALOG_VIEW_PLACEHOLDER", LANG.EN, "Quantity");
registerTranslation("DIALOG_VIEW_END", LANG.EN, "End trade");
registerTranslation("DIALOG_VIEW_BUY", LANG.EN, "Buy");
registerTranslation("DIALOG_VIEW_BUY_SUCCES", LANG.EN, "Your trade ended successfuly...");
registerTranslation("DIALOG_VIEW_BUY_FAULT", LANG.EN, "Wait 5 seconds until next use...");
registerTranslation("DIALOG_VIEW_BUY_FAULT_AMOUNT", LANG.EN, "Item number is out of scale...");
registerTranslation("DIALOG_VIEW_CANNOT_BUY", LANG.EN, "You have insufficient resources to buy this item...");
registerTranslation("DIALOG_VIEW_EFFECT_TITLE", LANG.EN, "Effect");

/********************* MAP VIEW *********************/

registerTranslation("MAP_VIEW_RESPAWN", LANG.EN, "Any moment");

/********************* EGUIPMENT VIEW *********************/

registerTranslation("EQUIPMENT_VIEW_TYPE_TILE", LANG.EN, "Weapon type");
registerTranslation("EQUIPMENT_VIEW_REQUIREMENT_TITLE", LANG.EN, "Item requirements");
registerTranslation("EQUIPMENT_VIEW_PROTECTION_TITLE", LANG.EN, "Protection against");
registerTranslation("EQUIPMENT_VIEW_DAMAGE_TITLE", LANG.EN, "Damage against");
registerTranslation("EQUIPMENT_VIEW_MANA_USAGE" LANG.EN, "Mana cost");
registerTranslation("EQUIPMENT_VIEW_DESCRIPTION_TITLE", LANG.EN, "Description");
registerTranslation("EQUIPMENT_VIEW_EQUIP_FAULT", LANG.EN, "You cannot use this item...");
registerTranslation("EQUIPMENT_VIEW_INFO", LANG.EN, "To assign an item to a slot click:\n0, 1, 2, 3, 4, 5, 6, 7, 8, 9\nTo drop the selected item use - Q!\nTo use/replace the selected item\nclick - E or LEFT MOUSE BUTTON!");
registerTranslation("EQUIPMENT_VIEW_DROP_INPUT", LANG.EN, "Enter amount");
registerTranslation("EQUIPMENT_VIEW_DROP_BUTTON", LANG.EN, "Drop");
registerTranslation("EQUIPMENT_VIEW_DROP_EQUIPED", LANG.EN, "The item is used!");
registerTranslation("EQUIPMENT_VIEW_DROP_AMOUNT", LANG.EN, "Not enough items!");
registerTranslation("EQUIPMENT_VIEW_DROP_BOUNDED", LANG.EN, "This item is tied to your character!");
registerTranslation("EQUIPMENT_VIEW_EFFECT_TITLE", LANG.EN, "Effect");

/********************* LOGIN VIEW *********************/

registerTranslation("LOGIN_VIEW_TITLE", LANG.EN, "Welcome to Kyrmir TDM");
registerTranslation("LOGIN_VIEW_SUBTITLE", LANG.EN, "Enter your username & password.");
registerTranslation("LOGIN_VIEW_USERNAME", LANG.EN, "Username");
registerTranslation("LOGIN_VIEW_PASSWORD", LANG.EN, "Password");
registerTranslation("LOGIN_VIEW_BTN_LOGIN", LANG.EN, "Login");
registerTranslation("LOGIN_VIEW_BTN_REGISTER", LANG.EN, "Registration");
registerTranslation("LOGIN_VIEW_LANG_SELECT" LANG.EN, "Choose your language");
registerTranslation("LOGIN_VIEW_MSG_USERNAME", LANG.EN, "The username is wrong!");
registerTranslation("LOGIN_VIEW_MSG_EMPTY_FIELD", LANG.EN, "You left the box blank!");
registerTranslation("LOGIN_VIEW_MSG_FAULT", LANG.EN, "The data entered is incorrect!");
registerTranslation("LOGIN_VIEW_MSG_NOT_EXIST", LANG.EN, "Such an account does not exist!");

/********************* REGISTER VIEW *********************/

registerTranslation("REGISTER_VIEW_TITLE", LANG.EN, "Welcome to Kyrmir TDM");
registerTranslation("REGISTER_VIEW_SUBTITLE", LANG.EN, "Enter your username & password.");
registerTranslation("REGISTER_VIEW_INFO_0", LANG.EN, "Our security policy requires that your password include");
registerTranslation("REGISTER_VIEW_INFO_1", LANG.EN, "at least 6 characters, at least 1 number, at least one capital letter!");
registerTranslation("REGISTER_VIEW_INFO_2", LANG.EN, "Also remember to use unique passwords!");
registerTranslation("REGISTER_VIEW_USERNAME", LANG.EN, "Username");
registerTranslation("REGISTER_VIEW_PASSWORD", LANG.EN, "Password");
registerTranslation("REGISTER_VIEW_PASSWORD_REPEAT", LANG.EN, "Repeat password");
registerTranslation("REGISTER_VIEW_BTN_REGISTER", LANG.EN, "Register");
registerTranslation("REGISTER_VIEW_BTN_BACK", LANG.EN, "Back");
registerTranslation("REGISTER_VIEW_MSG_PASSWORDS_MATCH", LANG.EN, "Passwords don't match!");
registerTranslation("REGISTER_VIEW_MSG_PASSWORD", LANG.EN, "The password does not meet the security requirements!");
registerTranslation("REGISTER_VIEW_MSG_USERNAME", LANG.EN, "Username does not meet the requirements!");
registerTranslation("REGISTER_VIEW_MSG_EMPTY_FIELD", LANG.EN, "You left the box blank!");
registerTranslation("REGISTER_VIEW_MSG_SUCCES", LANG.EN, "You have succesfully registered new account! You can login!");
registerTranslation("REGISTER_VIEW_MSG_FAULT", LANG.EN, "Unable to register account");
registerTranslation("REGISTER_VIEW_MSG_DATA_USED", LANG.EN, "This account already exists!");





registerTranslation("VISUAL_VIEW_TITLE", LANG.EN, "Customize character");
registerTranslation("VISUAL_VIEW_FACES_DESCRIPTION", LANG.EN, "You can use mouse scroll to navigate\nthrough face list!");
registerTranslation("VISUAL_VIEW_HEAD", LANG.EN, "Select head model");
registerTranslation("VISUAL_VIEW_TORSO", LANG.EN, "Select body texture");
registerTranslation("VISUAL_VIEW_SEX", LANG.EN, "Select sex");
registerTranslation("VISUAL_VIEW_SAVE_VISUAL", LANG.EN, "Save");
registerTranslation("VISUAL_VIEW_RESET_VISUAL", LANG.EN, "Reset");
registerTranslation("VISUAL_VIEW_CAMERA", LANG.EN, "Change camera");
registerTranslation("GUILD_VIEW_TITLE", LANG.EN, "Select your camp!");
registerTranslation("GUILD_VIEW_BTN_CAMP", LANG.EN, "Active players");

/********************* MAIN VIEW *********************/

registerTranslation("MAIN_VIEW_OPTIONS", LANG.EN, "Options");
registerTranslation("MAIN_VIEW_VISUAL", LANG.EN, "Visual change");
registerTranslation("MAIN_VIEW_HELP", LANG.EN, "Help");
registerTranslation("MAIN_VIEW_GUILD", LANG.EN, "Change guild");
registerTranslation("MAIN_VIEW_EXIT", LANG.EN, "Exit");
registerTranslation("MAIN_VIEW_LANG_SELECT" LANG.EN, "Choose your language");

/********************* ANIMATIONS VIEW *********************/

registerTranslation("VIEW_ANIMATION_TITLE", LANG.EN, "Animation list");

/********************* OPTIONS VIEW *********************/

registerTranslation("OPTIONS_VIEW_RESOLUTION", LANG.EN, "Resolution");
registerTranslation("OPTIONS_VIEW_DISABLE_MUSIC_SYSTEM", LANG.EN, "Turn off the sound");
registerTranslation("OPTIONS_VIEW_SOUND_VOLUME", LANG.EN, "Sound volume");
registerTranslation("OPTIONS_VIEW_MUSIC_VOLUME", LANG.EN, "Music volume");
registerTranslation("OPTIONS_VIEW_CHAT_LINES", LANG.EN, "Chat lines");
registerTranslation("OPTIONS_VIEW_SAVE_LOGIN", LANG.EN, "Save login details");
registerTranslation("OPTIONS_VIEW_WEAPON_TRAIL", LANG.EN, "Weapon trails");
registerTranslation("OPTIONS_VIEW_YES", LANG.EN, "Yes");
registerTranslation("OPTIONS_VIEW_NO", LANG.EN, "No");
registerTranslation("HELP_VIEW_TITLE", LANG.EN, "Commands and keyboard shortcuts");
registerTranslation("HELP_VIEW_RENDER", LANG.EN, "F1 F2 F3 F4 - Change the rendering range");
registerTranslation("HELP_VIEW_PLAYERLIST", LANG.EN, "F5 - Player list");
registerTranslation("HELP_VIEW_ANIMLIST", LANG.EN, "F6 - Animation list");
registerTranslation("HELP_VIEW_HIDE_UI", LANG.EN, "F12 - Hide UI");
registerTranslation("HELP_VIEW_STATS", LANG.EN, "B - Character statistics");
registerTranslation("HELP_VIEW_TALK", LANG.EN, "CTRL - Talk with NPC");
registerTranslation("HELP_VIEW_SKIP", LANG.EN, "Space - Skip talk with NPC");
registerTranslation("HELP_VIEW_SCROLL", LANG.EN, "Mouse Scroll - Scroll through the merchant's assortment or dialogue options");
registerTranslation("HELP_VIEW_DEBUG", LANG.EN, "CTRL + ALT + F8 - If you can't get out of somewhere, use this combination");
registerTranslation("HELP_VIEW_MAP", LANG.EN, "M - Show map");
registerTranslation("HELP_VIEW_CHAT_CHANGE", LANG.EN, "CTRL + X - Switch the chat tab");
registerTranslation("HELP_VIEW_CHAT_OPEN", LANG.EN, "T - Chat");
registerTranslation("HELP_VIEW_QUICKSLOT", LANG.EN, "Quickslots - keys: 1-2-3-4-5-6-7-8-9-0");
registerTranslation("HELP_VIEW_VOICE_COMMAND_0", LANG.EN, "Voice commands: (You can use them every 6 seconds)");
registerTranslation("HELP_VIEW_VOICE_COMMAND_1", LANG.EN, "Num 9 - Damn, run!");
registerTranslation("HELP_VIEW_VOICE_COMMAND_2", LANG.EN, "Num 8 - Stop running, coward");
registerTranslation("HELP_VIEW_VOICE_COMMAND_3", LANG.EN, "Num 7 - Alarm");
registerTranslation("HELP_VIEW_VOICE_COMMAND_4", LANG.EN, "Num 6 - Help");
registerTranslation("HELP_VIEW_VOICE_COMMAND_5", LANG.EN, "Num 5 - Insult");
registerTranslation("HELP_VIEW_VOICE_COMMAND_6", LANG.EN, "Num 4 - Enemy dead");
registerTranslation("HELP_VIEW_COMMANDS", LANG.EN, "Chat commands");
registerTranslation("HELP_VIEW_COMMANDS_PM", LANG.EN, "Private message - /pw player_id text example: /pw 5 hello");
registerTranslation("STATS_VIEW_TITLE", LANG.EN, "Player stats");
registerTranslation("STATS_VIEW_CHARACTER", LANG.EN, "Character");
registerTranslation("STATS_VIEW_ATRIBUTES", LANG.EN, "Attributes");
registerTranslation("STATS_VIEW_PROTECTION", LANG.EN, "Character protection");
registerTranslation("STATS_VIEW_SKILLS", LANG.EN, "Skills");
registerTranslation("STATS_VIEW_GUILD", LANG.EN, "Faction");

registerTranslation("STATUS_VIEW_TARGET", LANG.EN, "Round target");
registerTranslation("STATUS_VIEW_BEGIN", LANG.EN, "Round will begin in");
registerTranslation("STATUS_VIEW_NO_ROUND", LANG.EN, "There is no round");
registerTranslation("STATUS_VIEW_BONUS_EMPTY", LANG.EN, "Bonus exp");
registerTranslation("STATUS_VIEW_ROUND_BONUS", LANG.EN, "Bonus exp: %.2fx");
registerTranslation("STATUS_VIEW_KILL", LANG.EN, "%s was killed by %s(%d)!");
registerTranslation("STATUS_VIEW_STREAK", LANG.EN, "Player %s killed %d players, by killing player %s!");
registerTranslation("STATUS_VIEW_LEVEL_THREE", LANG.EN, "Player %s has reached level 3!");
registerTranslation("STATUS_VIEW_BOSS_KILL", LANG.EN, "%s was killed by %s!");
registerTranslation("STATUS_VIEW_BOSS_RESPAWN", LANG.EN, "Boss %s appeared on the map!");
registerTranslation("STATUS_VIEW_KILLS", LANG.EN, "Kills");
registerTranslation("STATUS_VIEW_DEATHS", LANG.EN, "Deaths");
registerTranslation("STATUS_VIEW_ASSISTS", LANG.EN, "Assists");
registerTranslation("STATUS_VIEW_ENTER_CAMP", LANG.EN, "Heal yourself for free at the rune vendor!");

/********************* PLAYERLIST VIEW *********************/

registerTranslation("PLAYER_LIST_VIEW_TITLE", LANG.EN, "Player List");
registerTranslation("PLAYER_LIST_VIEW_ONLINE", LANG.EN, "Online Players");
registerTranslation("PLAYER_LIST_VIEW_MEMBERS", LANG.EN, "Players in team");
registerTranslation("PLAYER_LIST_VIEW_ID", LANG.EN, "Id");
registerTranslation("PLAYER_LIST_VIEW_NICKNAME", LANG.EN, "Nickname");
registerTranslation("PLAYER_LIST_VIEW_PING", LANG.EN, "Ping");
registerTranslation("PLAYER_LIST_VIEW_KILLS", LANG.EN, "Kills");
registerTranslation("PLAYER_LIST_VIEW_DEATHS", LANG.EN, "Deaths");
registerTranslation("PLAYER_LIST_VIEW_ASSISTS", LANG.EN, "Assists");

/********************* RANKLIST VIEW *********************/

registerTranslation("RANK_LIST_VIEW_NUMBER", LANG.EN, "No");
registerTranslation("RANK_LIST_VIEW_TITLE", LANG.EN, "Leaderboard");
registerTranslation("RANK_LIST_VIEW_NICKNAME", LANG.EN, "Nickname");
registerTranslation("RANK_LIST_VIEW_KILLS", LANG.EN, "Kills");
registerTranslation("RANK_LIST_VIEW_DEATHS", LANG.EN, "Deaths");
registerTranslation("RANK_LIST_VIEW_ASSISTS", LANG.EN, "Assists");
registerTranslation("RANK_LIST_VIEW_KDA", LANG.EN, "Score");
registerTranslation("RANK_LIST_VIEW_DMG", LANG.EN, "Damage");

/********************* STATS *********************/

registerTranslation("LEVEL", LANG.EN, "Level");
registerTranslation("EXPERIENCE", LANG.EN, "Experience");
registerTranslation("EXPERIENCE_NEXT_LEVEL", LANG.EN, "Next level experience");
registerTranslation("LEARN_POINTS", LANG.EN, "Learn points");
registerTranslation("STRENGTH", LANG.EN, "Strength");
registerTranslation("DEXTERITY", LANG.EN, "Dexterity");
registerTranslation("MANA", LANG.EN, "Mana");
registerTranslation("HEALTH", LANG.EN, "Health");
registerTranslation("MAGIC_LEVEL", LANG.EN, "Magic level");
registerTranslation("GUILD_NAME", LANG.EN, "Faction name");

/********************* MULTIUSE *********************/

registerTranslation("REQUIREMENT_TYPE_0", LANG.EN, "Level");
registerTranslation("REQUIREMENT_TYPE_1", LANG.EN, "Magic level");
registerTranslation("REQUIREMENT_TYPE_2", LANG.EN, "Strength");
registerTranslation("REQUIREMENT_TYPE_3", LANG.EN, "Dexterity");
registerTranslation("REQUIREMENT_TYPE_4", LANG.EN, "Mana");
registerTranslation("REQUIREMENT_TYPE_5", LANG.EN, "Health");

registerTranslation("DAMAGE_TYPE_2", LANG.EN, "Blunt Damage");
registerTranslation("DAMAGE_TYPE_4", LANG.EN, "Edge Damage");
registerTranslation("DAMAGE_TYPE_64", LANG.EN, "Point Damage");
registerTranslation("DAMAGE_TYPE_8", LANG.EN, "Fire Damage");
registerTranslation("DAMAGE_TYPE_32", LANG.EN, "Magic Damage");

registerTranslation("WEAPON_MODE_3", LANG.EN, "One Handed");
registerTranslation("WEAPON_MODE_4", LANG.EN, "Two Handed");
registerTranslation("WEAPON_MODE_5", LANG.EN, "Bow");
registerTranslation("WEAPON_MODE_6", LANG.EN, "Crossbow");
registerTranslation("WEAPON_MODE_7", LANG.EN, "Magic");

registerTranslation("WEAPON_SKILL_0", LANG.EN, "One Handed Weapon");
registerTranslation("WEAPON_SKILL_1", LANG.EN, "Two Handed weapon");
registerTranslation("WEAPON_SKILL_2", LANG.EN, "Bows");
registerTranslation("WEAPON_SKILL_3", LANG.EN, "Crossbows");

registerTranslation("EFFECT_TYPE_" + ItemEffect.HealthRecovery, LANG.EN, "Restores health points");
registerTranslation("EFFECT_TYPE_" + ItemEffect.HealthPercentRecovery, LANG.EN, "Restores health points (percentage)");
registerTranslation("EFFECT_TYPE_" + ItemEffect.ManaRecovery, LANG.EN, "Restores mana points");
registerTranslation("EFFECT_TYPE_" + ItemEffect.ManaPercentRecovery, LANG.EN, "Restores mana points (percentage)");

/********************* ANIMS *********************/

registerTranslation("T_STAND_2_SIT", LANG.EN, "Sit");
registerTranslation("T_STAND_2_SLEEPGROUND", LANG.EN, "Sleep");
registerTranslation("T_STAND_2_PEE", LANG.EN, "Pee");
registerTranslation("S_PRAY", LANG.EN, "Pray");
registerTranslation("S_DEAD", LANG.EN, "Dead");
registerTranslation("S_LGUARD", LANG.EN, "Guard 1");
registerTranslation("S_HGUARD", LANG.EN, "Guard 2");
registerTranslation("T_SEARCH", LANG.EN, "Search");
registerTranslation("T_PLUNDER", LANG.EN, "Plunder");
registerTranslation("S_WASH", LANG.EN, "Wash");
registerTranslation("R_SCRATCHEGG", LANG.EN, "Scratch");
registerTranslation("S_FIRE_VICTIM", LANG.EN, "Dance");
registerTranslation("T_1HSINSPECT", LANG.EN, "Check weapon");
registerTranslation("T_GREETNOV", LANG.EN, "Bow down");
registerTranslation("T_1HSFREE", LANG.EN, "Training");
registerTranslation("T_WATCHFIGHT_OHNO", LANG.EN, "Cheer on");
registerTranslation("T_YES", LANG.EN, "Yes");
registerTranslation("T_NO", LANG.EN, "No");
registerTranslation("R_SCRATCHHEAD", LANG.EN, "Scratch 2");
registerTranslation("T_BORINGKICK", LANG.EN, "Kicking");

/********************* GUILDS *********************/

registerTranslation("OLD_CAMP", LANG.EN, "Old Camp");
registerTranslation("SWAMP_CAMP", LANG.EN, "Swamp Camp");
registerTranslation("NEW_CAMP", LANG.EN, "New Camp");
registerTranslation("PIRATE_CAMP", LANG.EN, "Pirate Camp");
registerTranslation("BANDIT_CAMP", LANG.EN, "Bandit Camp");
