
local NPC_POTION = createNpc("NPC_POTION", "PC_HERO", "COLONY.ZEN");

NPC_POTION.setVisual("Hum_Body_Naked0", 1, "Hum_Head_Bald", 22);
NPC_POTION.equipArmor(Items.id("ITAR_BARKEEPER"));
NPC_POTION.equipMeleeWeapon(Items.id("ITMW_1H_MACE_L_03"));

NPC_POTION.playAni("S_HGUARD");

NPC_POTION.setPosition(842, 247, -360);
NPC_POTION.setAngle(273);

NPC_POTION.setCollision(false);
NPC_POTION.setImmortal(true);

NPC_POTION.spawn();

///////////////////////////////////////////////////////////////////////////////

local NPC_POTION_TRADE = Dialog.TradeOption("NPC_CROSSBOW_INITIAL", "ITMI_GOLD")
    .addItem("ITPO_HEALTH_01", 35)
    .addItem("ITPO_HEALTH_02", 50)
    .addItem("ITPO_HEALTH_03", 75)
    .addItem("ITPO_MANA_01", 35)
    .addItem("ITPO_MANA_02", 50)
    .addItem("ITPO_MANA_03", 75)
    .addItem("ITPO_SPEED", 10000);

registerDialogNpc(NPC_POTION.getId(), function(playerId) {
    return NPC_POTION_TRADE;
});