
// STANDARD CBOW AI:
// orcshaman_sti_ai.nut

const DAMAGE_MELEE = 150;
const DAMAGE_SPEEL = 150;

const ATTACK_MELEE_SPEED = 320;
const ATTACK_RANGED_SPEED = 1700;

enum AI_STATE
{
    NONE = -1,
    WARNING = 0, // WARNING_STATE
    FIGHT_OPEN = 1, // SKIP_WARNING_STATE
    FIGHT_CLOSE = 2,
}

local warningState = null,
    fightCloseState = null,
    fightOpenState = null;

warningState = function(bot)
{
    local warnTime = bot.getWarnTime();

    if(warnTime == -1) 
    {
        local template = bot.getTemplate();

        bot.setWarnTime(getTickCount() + template.getWarnTime());
        bot.setWeaponMode(WEAPONMODE_2HS);

        bot.playAni("T_WARN");
    }
    else 
    {
        if(warnTime > getTickCount())
        {
            if(bot.getDistanceToEnemy() <= bot.getTemplate().getHitDistance())
            {
                bot.setFight(AI_STATE.FIGHT_CLOSE);
                    fightCloseState(bot);
            }
        }
        else
        {
            bot.setFight(AI_STATE.FIGHT_OPEN);
                fightOpenState(bot);          
        }  
    }
};

fightCloseState = function(bot)
{
    local template = bot.getTemplate();

    local enemyDistance = bot.getDistanceToEnemy();
    local hitDistance = template.getHitDistance();

    if(enemyDistance <= hitDistance)
    {
        if(bot.isAttackingWithMelee() == false)
        {
            bot.setWeaponMode(WEAPONMODE_2HS);
            bot.setDamage(DAMAGE_EDGE, DAMAGE_MELEE);

            bot.attackMelee(ATTACK_SWORD_RIGHT, ATTACK_MELEE_SPEED);
        } 
        else 
        {
            if(enemyDistance <= (hitDistance * 0.75))
                bot.parade(PARADE.JUMPB);
            else 
            {
                if(bot.getTimeFromLastHit() < 1500) 
                    bot.parade(PARADE.BLOCK_0);
            }
        }
    } 
    else 
    {
        bot.setFight(AI_STATE.FIGHT_OPEN);
            fightOpenState(bot);
    }
};

fightOpenState = function(bot)
{
    if(bot.isObstacle())
    {
        if(bot.getEnemyNumber() > 1)
        {
            bot.setActiveSpell(Items.id("ITRU_FIRESTORM"));
        }
    }
    else 
    {
        local template = bot.getTemplate();

        if(bot.getDistanceToEnemy() > template.getHitDistance())
        {
            bot.setActiveSpell(Items.id("ITRU_BELIARSRAGE"));

            if(bot.isAttackingWithMagic() == false)
            {
                bot.setWeaponMode(WEAPONMODE_MAG);
                bot.setDamage(DAMAGE_BLUNT, DAMAGE_SPEEL);

                bot.attackMagic(ATTACK_RANGED_SPEED);
            }
        } 
        else 
        {
            bot.setFight(AI_STATE.FIGHT_CLOSE);
                fightCloseState(bot);
        }
    }
};

ORCSHAMAN_SIT_AI <- function(bot)
{
    local state = bot.getFight();
    if(state == -1) bot.setFight(AI_STATE.WARNING);
    
    switch(state)
    {
        case AI_STATE.WARNING: 
            warningState(bot); 
        break;
        case AI_STATE.FIGHT_CLOSE: 
            fightCloseState(bot); 
        break;
        case AI_STATE.FIGHT_OPEN: 
            fightOpenState(bot); 
        break;
    }
};