
// STANDARD 2H AI:
// orcwarrior_roam.nut
// skeleton.nut
// skeleton_lord.nut

enum AI_STATE
{
    NONE = -1,
    WARNING = 0, // WARNING_STATE
    FOLLOW = 1, // SKIP_WARNING_STATE
    FOLLOW_OBSTACLE_L = 2,
    FOLLOW_OBSTACLE_R = 3,
    FIGHT_CLOSE = 4,
}

local warningState = null,
    followState = null,
    obstacleStateL = null,
    obstacleStateR = null,
    fightState = null;

local function isLeft(bot)
{
    local bPosition = bot.getPosition(), tPosition = getPlayerPosition(bot.getTarget()),
        angle = getVectorAngle(bPosition.x, bPosition.z, tPosition.x, tPosition.z);
    
    return (((bPosition.x + sin(angle) * 5000) - bPosition.x) * (tPosition.z - bPosition.z) - ((bPosition.z + cos(angle) * 5000) - bPosition.z) * (tPosition.x - bPosition.x)) > 0;
}

warningState = function(bot)
{
    local warnTime = bot.getWarnTime();

    if(warnTime == -1) 
    {
        bot.setWarnTime(getTickCount() + bot.getTemplate().getWarnTime());
        bot.setWeaponMode(WEAPONMODE_2HS);

        if(bot.getInstance() == "SKELETON")
            bot.playAni("T_IGETYOU");
        else 
            bot.playAni("T_WARN");
    }
    else 
    {
        if(warnTime <= getTickCount() || bot.getDistanceToEnemy() < 600)
        {
            bot.setFight(AI_STATE.FOLLOW);
                followState(bot);
        }
        else if(bot.getDistanceToEnemy() < bot.getTemplate().getHitDistance())
        {
            bot.setFight(AI_STATE.FIGHT_CLOSE);
                fightState(bot);
        }
    }
};

followState = function(bot)
{       
    bot.setWeaponMode(WEAPONMODE_2HS);

    if(bot.isObstacle())
    {        
        if(isLeft(bot))
        {
            bot.setFight(AI_STATE.FOLLOW_OBSTACLE_L); 
                obstacleStateL(bot);
        }
        else 
        {
            bot.setFight(AI_STATE.FOLLOW_OBSTACLE_R);
                obstacleStateR(bot);
        }
    }
    else 
    {
        local hitDistance = bot.getTemplate().getHitDistance();

        if(bot.getDistanceToEnemy() > hitDistance)
        {
            if(bot.isFollowing() == false) bot.follow(hitDistance * 0.6);
        }
        else 
        {
            bot.setFight(AI_STATE.FIGHT_CLOSE);
                fightState(bot);
        }
    }
};

obstacleStateL = function(bot)
{    
    if(bot.isObstacle())
    {
        if(isLeft(bot))
        {
            bot.setFight(AI_STATE.FOLLOW_OBSTACLE_R);
                obstacleStateR(bot);
        }
        else 
            bot.playAni("T_2HRUNSTRAFEL");        
    } 
    else 
    {
        if(bot.getDistanceToEnemy() > bot.getTemplate().getHitDistance())
        {
            bot.setFight(AI_STATE.FOLLOW);
                followState(bot);
        }
        else
        {
            bot.setFight(AI_STATE.FIGHT_CLOSE);
                fightState(bot);
        }
    }    
};

obstacleStateR = function(bot)
{
    if(bot.isObstacle())
    {
        if(isLeft(bot) == false)
        {
            bot.setFight(AI_STATE.FOLLOW_OBSTACLE_L);
                obstacleStateL(bot);
        }
        else 
            bot.playAni("T_2HRUNSTRAFER");
    }
    else 
    {
        if(bot.getDistanceToEnemy() > bot.getTemplate().getHitDistance())
        {
            bot.setFight(AI_STATE.FOLLOW);
                followState(bot);
        }
        else
        {
            bot.setFight(AI_STATE.FIGHT_CLOSE);
                fightState(bot);
        }
    }    
};

fightState = function(bot)
{
    local enemyDistance = bot.getDistanceToEnemy();

    local template = bot.getTemplate();
    local hitDistance = template.getHitDistance();
    
    bot.setWeaponMode(WEAPONMODE_2HS);

    if(enemyDistance <= hitDistance)
    {
        if(bot.isInAttackMode() == false)
            bot.attackMelee(ATTACK_SWORD_RIGHT, template.getAttackSpeed());
        else 
        {
            if(enemyDistance <= (hitDistance * 0.75))
                bot.parade(PARADE.JUMPB);
            else 
            {
                if(bot.getTimeFromLastHit() < 1500) 
                    bot.parade(PARADE.BLOCK_0);
            }
        }
    } 
    else 
    {
        bot.setFight(AI_STATE.FOLLOW);
            followState(bot);
    }
};

TWO_HAND_AI <- function(bot)
{
    local state = bot.getFight();
    if(state == -1) bot.setFight(AI_STATE.WARNING);
    
    switch(state)
    {
        case AI_STATE.WARNING: 
            warningState(bot); 
        break;
        case AI_STATE.FOLLOW: 
            followState(bot); 
        break;
        case AI_STATE.FOLLOW_OBSTACLE_R: 
            obstacleStateR(bot); 
        break;
        case AI_STATE.FOLLOW_OBSTACLE_L: 
            obstacleStateL(bot); 
        break;
        case AI_STATE.FIGHT_CLOSE: 
            fightState(bot); 
        break;
    }
};