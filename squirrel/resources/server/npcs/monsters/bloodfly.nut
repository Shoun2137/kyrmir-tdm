
local BLOODFLY = Bot.Template("BLOODFLY", "KYRMIR_BLOODFLY"); 

BLOODFLY.setLevel(0);
BLOODFLY.setMagicLevel(0);

BLOODFLY.setHealth(50);
BLOODFLY.setRespawnTime(60)

BLOODFLY.setDamage(DAMAGE_EDGE, 25);

BLOODFLY.setProtection(DAMAGE_EDGE, 0);
BLOODFLY.setProtection(DAMAGE_BLUNT, 500);
BLOODFLY.setProtection(DAMAGE_FIRE, 0);
BLOODFLY.setProtection(DAMAGE_MAGIC, 10);
BLOODFLY.setProtection(DAMAGE_POINT, 20);

// Fight system
BLOODFLY.setWarnTime(6);
BLOODFLY.setHitDistance(180);
BLOODFLY.setChaseDistance(1800);
BLOODFLY.setDetectionDistance(1200);
BLOODFLY.setAttackSpeed(1800);

BLOODFLY.setInitiativeFunction(STANDARD_ANIMAL_AI);

BLOODFLY.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 2))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("T_STAND_2_EAT"); break;	
            case 2: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");			
    }
}
, 7);

registerMonsterTemplate("KYRMIR_BLOODFLY", BLOODFLY);

// Drop
registerMonsterReward("KYRMIR_BLOODFLY", Reward.Content(50)
    .addDrop(Reward.Drop("ITMI_GOLD", 35, 35))
);

// Spawnlist
spawnBot(BLOODFLY, -28969, -852, 10777, "ADDONWORLD.ZEN");
spawnBot(BLOODFLY, -27795, -1030, 10309, "ADDONWORLD.ZEN");
spawnBot(BLOODFLY, -29643, -789, 9981, "ADDONWORLD.ZEN");
spawnBot(BLOODFLY, -16839, -503, -4980, "ADDONWORLD.ZEN");
spawnBot(BLOODFLY, -15811, -376, -5834, "ADDONWORLD.ZEN");
