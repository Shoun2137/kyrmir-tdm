
local MOLERAT = Bot.Template("MOLERAT", "KYRMIR_MOLERAT"); 

MOLERAT.setLevel(0);
MOLERAT.setMagicLevel(0);

MOLERAT.setHealth(50);
MOLERAT.setRespawnTime(60);

MOLERAT.setDamage(DAMAGE_EDGE, 25);

MOLERAT.setProtection(DAMAGE_EDGE, 0);
MOLERAT.setProtection(DAMAGE_BLUNT, 500);
MOLERAT.setProtection(DAMAGE_FIRE, 0);
MOLERAT.setProtection(DAMAGE_MAGIC, 10);
MOLERAT.setProtection(DAMAGE_POINT, 20);

// Fight system
MOLERAT.setWarnTime(6);
MOLERAT.setHitDistance(180);
MOLERAT.setChaseDistance(1800);
MOLERAT.setDetectionDistance(1200);
MOLERAT.setAttackSpeed(1800);

MOLERAT.setInitiativeFunction(STANDARD_ANIMAL_AI);

MOLERAT.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");			
    }
}
, 8);

registerMonsterTemplate("KYRMIR_MOLERAT", MOLERAT);

// Drop
registerMonsterReward("KYRMIR_MOLERAT", Reward.Content(50)
    .addDrop(Reward.Drop("ITMI_GOLD", 35, 35))
);

// Spawnlist
spawnBot(MOLERAT, -29734, -935, 13995, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -29967, -878, 12820, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -29372, -952, 14114, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -28049, -896, 14733, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -27377, -856, 14086, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -26468, -985, 12195, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -26706, -943, 11374, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -26999, -924, 11395, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -27794, -961, 9679, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -14002, -269, 353, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -14887, -1035, -1769, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -14271, -1176, -2295, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -14389, -958, -1767, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -26416, -977, 11986, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -29510, -835, 12152, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -30397, -751, 9186, "ADDONWORLD.ZEN");
spawnBot(MOLERAT, -32034, -229, 9452, "ADDONWORLD.ZEN");
