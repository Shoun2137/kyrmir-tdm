
local GOBBO_SKELETON = Bot.Template("GOBBO_SKELETON", "KYRMIR_GOBBO_SKELETON"); 

GOBBO_SKELETON.setLevel(0);
GOBBO_SKELETON.setMagicLevel(0);

GOBBO_SKELETON.setHealth(180);
GOBBO_SKELETON.setRespawnTime(210)

GOBBO_SKELETON.setDamage(DAMAGE_EDGE, 140);

GOBBO_SKELETON.setProtection(DAMAGE_EDGE, 0);
GOBBO_SKELETON.setProtection(DAMAGE_BLUNT, 500);
GOBBO_SKELETON.setProtection(DAMAGE_FIRE, 0);
GOBBO_SKELETON.setProtection(DAMAGE_MAGIC, 20);
GOBBO_SKELETON.setProtection(DAMAGE_POINT, 30);

// Fight system
GOBBO_SKELETON.setWarnTime(6);
GOBBO_SKELETON.setHitDistance(120);
GOBBO_SKELETON.setChaseDistance(1800);
GOBBO_SKELETON.setDetectionDistance(1200);
GOBBO_SKELETON.setAttackSpeed(2100);

GOBBO_SKELETON.setInitiativeFunction(STANDARD_ANIMAL_AI);

GOBBO_SKELETON.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 3))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("R_ROAM3"); break;
        case 3: bot.playAni("T_PERCEPTION"); break;	
    }

    bot.setAngle(rand() % 360);
});

registerMonsterTemplate("KYRMIR_GOBBO_SKELETON", GOBBO_SKELETON);

// Drop
registerMonsterReward("KYRMIR_GOBBO_SKELETON", Reward.Content(230)
    .addDrop(Reward.Drop("ITMI_GOLD", 120, 120))
);

// Spawnlist
spawnBot(GOBBO_SKELETON, 3795, -3364, 30154, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 4313, -3263, 29544, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 3685, -3344, 30845, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 4023, -3348, 30676, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 2991, -3301, 30371, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 3332, -3348, 30612, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, -5600, -3027, 34308, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, -5300, -3027, 35252, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, -5422, -3028, 35159, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, -5760, -3027, 36074, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 1910, -2524, 17842, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 2293, -2516, 18171, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 1782, -2412, 19555, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, -23663, -2808, -3075, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, -23469, -2804, -2833, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, -23099, -2830, -2771, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, -20768, -3603, -17278, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, -20803, -3648, -17403, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 98, -834, -563, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, -58, -834, -608, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, -208, -830, -600, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 11, -850, -508, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 77, -832, -361, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 57, -833, 734, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 73, -841, 1781, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 250, -838, 1752, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 288, -820, 1000, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 931, -830, 2019, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 27299, -2170, -18688, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 26866, -2166, -19172, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 26234, -2187, -19206, "ADDONWORLD.ZEN");
spawnBot(GOBBO_SKELETON, 8525, -5096, 8551, "ADDONWORLD.ZEN");
