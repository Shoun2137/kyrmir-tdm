
local ORCBITER = Bot.Template("ORCBITER", "KYRMIR_ORCBITER"); 

ORCBITER.setLevel(0);
ORCBITER.setMagicLevel(0);

ORCBITER.setHealth(85);
ORCBITER.setRespawnTime(80);

ORCBITER.setDamage(DAMAGE_EDGE, 60);

ORCBITER.setProtection(DAMAGE_EDGE, 0);
ORCBITER.setProtection(DAMAGE_BLUNT, 500);
ORCBITER.setProtection(DAMAGE_FIRE, 0);
ORCBITER.setProtection(DAMAGE_MAGIC, 10);
ORCBITER.setProtection(DAMAGE_POINT, 20);

// Fight system
ORCBITER.setWarnTime(6);
ORCBITER.setHitDistance(150);
ORCBITER.setChaseDistance(1800);
ORCBITER.setDetectionDistance(1200);
ORCBITER.setAttackSpeed(2100);

ORCBITER.setInitiativeFunction(STANDARD_ANIMAL_AI);

ORCBITER.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");			
    }
});

registerMonsterTemplate("KYRMIR_ORCBITER", ORCBITER);

// Drop
registerMonsterReward("KYRMIR_ORCBITER", Reward.Content(100)
    .addDrop(Reward.Drop("ITMI_GOLD", 60, 60))
);

// Spawnlist
spawnBot(ORCBITER, -17457, -2398, 17229, "ADDONWORLD.ZEN");
spawnBot(ORCBITER, -17426, -2397, 16880, "ADDONWORLD.ZEN");
spawnBot(ORCBITER, -17761, -2432, 17659, "ADDONWORLD.ZEN");
spawnBot(ORCBITER, -17112, -3073, 19441, "ADDONWORLD.ZEN");
spawnBot(ORCBITER, -16751, -3165, 19761, "ADDONWORLD.ZEN");
spawnBot(ORCBITER, -19415, -3199, 22267, "ADDONWORLD.ZEN");
spawnBot(ORCBITER, -18916, -3279, 21918, "ADDONWORLD.ZEN");
spawnBot(ORCBITER, -18975, -3262, 22453, "ADDONWORLD.ZEN");
spawnBot(ORCBITER, -6422, -1775, 21273, "ADDONWORLD.ZEN");
spawnBot(ORCBITER, -5843, -1792, 20710, "ADDONWORLD.ZEN");
spawnBot(ORCBITER, -5733, -1790, 20708, "ADDONWORLD.ZEN");
spawnBot(ORCBITER, -16711, -3453, 24139, "ADDONWORLD.ZEN");
spawnBot(ORCBITER, -16782, -3456, 23954, "ADDONWORLD.ZEN");
spawnBot(ORCBITER, -18025, -3193, 23514, "ADDONWORLD.ZEN");
