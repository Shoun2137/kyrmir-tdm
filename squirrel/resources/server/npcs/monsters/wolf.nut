
local WOLF = Bot.Template("WOLF", "KYRMIR_WOLF"); 

WOLF.setLevel(0);
WOLF.setMagicLevel(0);

WOLF.setHealth(75);
WOLF.setRespawnTime(100);

WOLF.setDamage(DAMAGE_EDGE, 50);

WOLF.setProtection(DAMAGE_EDGE, 0);
WOLF.setProtection(DAMAGE_BLUNT, 500);
WOLF.setProtection(DAMAGE_FIRE, 0);
WOLF.setProtection(DAMAGE_MAGIC, 10);
WOLF.setProtection(DAMAGE_POINT, 20);

// Fight system
WOLF.setWarnTime(6);
WOLF.setHitDistance(200);
WOLF.setChaseDistance(1800);
WOLF.setDetectionDistance(1200);
WOLF.setAttackSpeed(1800);

WOLF.setInitiativeFunction(STANDARD_ANIMAL_AI);

WOLF.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(rand() % 360);

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");		
    }
}
, 7);

registerMonsterTemplate("KYRMIR_WOLF", WOLF);

// Drop
registerMonsterReward("KYRMIR_WOLF", Reward.Content(100)
    .addDrop(Reward.Drop("ITMI_GOLD", 50, 50))
);

// Spawnlist
spawnBot(WOLF, -23594, -879, 15464, "ADDONWORLD.ZEN");
spawnBot(WOLF, -23279, -953, 15742, "ADDONWORLD.ZEN");
spawnBot(WOLF, -22013, -1230, 16571, "ADDONWORLD.ZEN");
spawnBot(WOLF, -22885, -1074, 17563, "ADDONWORLD.ZEN");
spawnBot(WOLF, -23093, -1063, 17824, "ADDONWORLD.ZEN");
spawnBot(WOLF, -24774, -318, 10054, "ADDONWORLD.ZEN");
spawnBot(WOLF, -23840, -246, 9514, "ADDONWORLD.ZEN");
spawnBot(WOLF, -22723, -269, 8993, "ADDONWORLD.ZEN");
spawnBot(WOLF, -23033, -2462, 5567, "ADDONWORLD.ZEN");
spawnBot(WOLF, -22895, -2292, 5215, "ADDONWORLD.ZEN");
spawnBot(WOLF, -23081, -2935, 6813, "ADDONWORLD.ZEN");
spawnBot(WOLF, -16833, -830, 1802, "ADDONWORLD.ZEN");
spawnBot(WOLF, -16744, -684, 1621, "ADDONWORLD.ZEN");
spawnBot(WOLF, -16663, -712, 1706, "ADDONWORLD.ZEN");
spawnBot(WOLF, -16237, -772, 2049, "ADDONWORLD.ZEN");
spawnBot(WOLF, -19529, -2216, 17946, "ADDONWORLD.ZEN");
spawnBot(WOLF, -20003, -2223, 18078, "ADDONWORLD.ZEN");
spawnBot(WOLF, -19547, -2205, 18517, "ADDONWORLD.ZEN");
spawnBot(WOLF, -21480, -3718, -11990, "ADDONWORLD.ZEN");
spawnBot(WOLF, -21263, -3758, -11905, "ADDONWORLD.ZEN");
spawnBot(WOLF, -22567, -3980, -10267, "ADDONWORLD.ZEN");
spawnBot(WOLF, -22581, -4244, -9119, "ADDONWORLD.ZEN");
