
local ORCSHAMAN_SIT = Bot.Template("ORCSHAMAN_SIT", "KYRMIR_ORCSHAMAN_SIT"); 

ORCSHAMAN_SIT.setLevel(0);
ORCSHAMAN_SIT.setMagicLevel(6);

ORCSHAMAN_SIT.setHealth(1200);
ORCSHAMAN_SIT.setRespawnTime(300);

ORCSHAMAN_SIT.setMeleeWeapon(Items.id("ITMW_2H_ORCAXE_01"));
ORCSHAMAN_SIT.setSkillWeapon(WEAPON_2H, 100);

ORCSHAMAN_SIT.setDamage(DAMAGE_EDGE, 150); // SET IN AI FILE BUT NOW WE ARE USING 2H AI

ORCSHAMAN_SIT.setProtection(DAMAGE_EDGE, 0);
ORCSHAMAN_SIT.setProtection(DAMAGE_BLUNT, 500);
ORCSHAMAN_SIT.setProtection(DAMAGE_FIRE, 0);
ORCSHAMAN_SIT.setProtection(DAMAGE_MAGIC, 0);
ORCSHAMAN_SIT.setProtection(DAMAGE_POINT, 0);

// Fight system
ORCSHAMAN_SIT.setWarnTime(6);
ORCSHAMAN_SIT.setHitDistance(240);
ORCSHAMAN_SIT.setChaseDistance(3600);
ORCSHAMAN_SIT.setDetectionDistance(1800);
// ORCSHAMAN_SIT.setAttackSpeed(2500); // SET IN AI FILE

ORCSHAMAN_SIT.setInitiativeFunction(ORCSHAMAN_SIT_AI); // ORCSHAMAN_SIT_AI

ORCSHAMAN_SIT.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    bot.playAni("T_STAND_2_VICTIM_SLE");
        bot.setAngle(rand() % 360);
});

registerMonsterTemplate("KYRMIR_ORCSHAMAN_SIT", ORCSHAMAN_SIT);

// Drop
registerMonsterReward("KYRMIR_ORCSHAMAN_SIT", Reward.Content(425)
    .addDrop(Reward.Drop("ITMI_GOLD", 225, 225))
);

// Spawnlist
spawnBot(ORCSHAMAN_SIT, -12804, -3913, 32024, "ADDONWORLD.ZEN");
spawnBot(ORCSHAMAN_SIT, -12770, -3956, 34203, "ADDONWORLD.ZEN");
spawnBot(ORCSHAMAN_SIT, -20088, -2959, -12540, "ADDONWORLD.ZEN");
spawnBot(ORCSHAMAN_SIT, -616, -859, -9285, "ADDONWORLD.ZEN");
spawnBot(ORCSHAMAN_SIT, 4924, -864, -1735, "ADDONWORLD.ZEN");
spawnBot(ORCSHAMAN_SIT, 3940, -864, -1768, "ADDONWORLD.ZEN");
spawnBot(ORCSHAMAN_SIT, 5751, -993, 18, "ADDONWORLD.ZEN");
spawnBot(ORCSHAMAN_SIT, 3663, -1046, -765, "ADDONWORLD.ZEN");
