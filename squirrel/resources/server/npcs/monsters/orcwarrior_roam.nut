
local ORCWARRIOR_ROAM = Bot.Template("ORCWARRIOR_ROAM", "KYRMIR_ORCWARRIOR_ROAM"); 

ORCWARRIOR_ROAM.setLevel(0);
ORCWARRIOR_ROAM.setMagicLevel(0);

ORCWARRIOR_ROAM.setHealth(1000);
ORCWARRIOR_ROAM.setRespawnTime(300);

ORCWARRIOR_ROAM.setMeleeWeapon(Items.id("ITMW_2H_ORCAXE_01"));
ORCWARRIOR_ROAM.setSkillWeapon(WEAPON_2H, 100);

ORCWARRIOR_ROAM.setDamage(DAMAGE_EDGE, 130);

ORCWARRIOR_ROAM.setProtection(DAMAGE_EDGE, 0);
ORCWARRIOR_ROAM.setProtection(DAMAGE_BLUNT, 500);
ORCWARRIOR_ROAM.setProtection(DAMAGE_FIRE, 0);
ORCWARRIOR_ROAM.setProtection(DAMAGE_MAGIC, 0);
ORCWARRIOR_ROAM.setProtection(DAMAGE_POINT, 0);

// Fight system
ORCWARRIOR_ROAM.setWarnTime(6);
ORCWARRIOR_ROAM.setHitDistance(240);
ORCWARRIOR_ROAM.setChaseDistance(3600);
ORCWARRIOR_ROAM.setDetectionDistance(1300);
ORCWARRIOR_ROAM.setAttackSpeed(360);

ORCWARRIOR_ROAM.setInitiativeFunction(TWO_HAND_AI);

ORCWARRIOR_ROAM.setRoutineFunction(function(bot)
{
{
    if(bot.getLastAni() == "S_WALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 1))
        {
            case 0: bot.playAni("T_STAND_2_EAT"); break;	
            case 1: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_WALKL");			
    }
}
});

registerMonsterTemplate("KYRMIR_ORCWARRIOR_ROAM", ORCWARRIOR_ROAM);

// Drop
registerMonsterReward("KYRMIR_ORCWARRIOR_ROAM", Reward.Content(400)
    .addDrop(Reward.Drop("ITMI_GOLD", 200, 200))
);

// Spawnlist
spawnBot(ORCWARRIOR_ROAM, 1348, -995, -6429, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, 556, -957, -7864, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -1507, -977, -6742, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -2398, -954, -5262, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, 602, -647, -10054, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -1809, -725, -9382, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -3287, -614, -9692, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, 2855, -714, -8656, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -10791, -5197, -18835, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -9726, -5165, -22929, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -7260, -4819, -23935, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -14800, -3451, -14472, "ADDONWORLD.ZEN");
spawnBot(ORCWARRIOR_ROAM, -14825, -3424, -14582, "ADDONWORLD.ZEN");
