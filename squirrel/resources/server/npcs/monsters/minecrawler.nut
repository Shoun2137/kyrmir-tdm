
local MINECRAWLER = Bot.Template("MINECRAWLER", "KYRMIR_MINECRAWLER"); 

MINECRAWLER.setLevel(0);
MINECRAWLER.setMagicLevel(0);

MINECRAWLER.setHealth(100);
MINECRAWLER.setRespawnTime(110);

MINECRAWLER.setDamage(DAMAGE_EDGE, 75);

MINECRAWLER.setProtection(DAMAGE_EDGE, 0);
MINECRAWLER.setProtection(DAMAGE_BLUNT, 500);
MINECRAWLER.setProtection(DAMAGE_FIRE, 0);
MINECRAWLER.setProtection(DAMAGE_MAGIC, 15);
MINECRAWLER.setProtection(DAMAGE_POINT, 0);

// Fight system
MINECRAWLER.setWarnTime(6);
MINECRAWLER.setHitDistance(230);
MINECRAWLER.setChaseDistance(1800);
MINECRAWLER.setDetectionDistance(1200);
MINECRAWLER.setAttackSpeed(1800);

MINECRAWLER.setInitiativeFunction(STANDARD_ANIMAL_AI);

MINECRAWLER.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 3))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("T_STAND_2_EAT"); break;	
            case 3: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 300)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }
           
        bot.playAni("S_FISTWALKL");		
    }
}
, 2);

registerMonsterTemplate("KYRMIR_MINECRAWLER", MINECRAWLER);

// Drop
registerMonsterReward("KYRMIR_MINECRAWLER", Reward.Content(150)
    .addDrop(Reward.Drop("ITMI_GOLD", 75, 75))
);

// Spawnlist
spawnBot(MINECRAWLER, -15225, -1801, 15888, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLER, -15343, -1791, 15661, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLER, -14082, -1821, 15570, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLER, -15434, -1240, 13861, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLER, -15510, -1748, 13050, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLER, 2410, -2548, 15264, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLER, 2277, -2606, 14292, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLER, 2858, -2612, 14359, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLER, 176, -2952, 14394, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLER, -2072, -2653, 14286, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLER, 4492, -2822, 15643, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLER, 4503, -2960, 13491, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLER, -167, -2667, 13228, "ADDONWORLD.ZEN");
