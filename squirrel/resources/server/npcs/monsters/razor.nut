
local RAZOR = Bot.Template("RAZOR", "KYRMIR_RAZOR"); 

RAZOR.setLevel(0);
RAZOR.setMagicLevel(0);

RAZOR.setHealth(200);
RAZOR.setRespawnTime(270);

RAZOR.setDamage(DAMAGE_EDGE, 140);

RAZOR.setProtection(DAMAGE_EDGE, 0);
RAZOR.setProtection(DAMAGE_BLUNT, 500);
RAZOR.setProtection(DAMAGE_FIRE, 0);
RAZOR.setProtection(DAMAGE_MAGIC, 25);
RAZOR.setProtection(DAMAGE_POINT, 35);

// Fight system
RAZOR.setWarnTime(6);
RAZOR.setHitDistance(150);
RAZOR.setChaseDistance(1800);
RAZOR.setDetectionDistance(1200);
RAZOR.setAttackSpeed(1800);

RAZOR.setInitiativeFunction(STANDARD_ANIMAL_AI);

RAZOR.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");			
    }
}
, 8);

registerMonsterTemplate("KYRMIR_RAZOR", RAZOR);

// Drop
registerMonsterReward("KYRMIR_RAZOR", Reward.Content(400)
    .addDrop(Reward.Drop("ITMI_GOLD", 200, 200))
);

// Spawnlist
