
local LURKER = Bot.Template("LURKER", "KYRMIR_LURKER"); 

LURKER.setLevel(0);
LURKER.setMagicLevel(0);

LURKER.setHealth(95);
LURKER.setRespawnTime(90);

LURKER.setDamage(DAMAGE_EDGE, 70);

LURKER.setProtection(DAMAGE_EDGE, 0);
LURKER.setProtection(DAMAGE_BLUNT, 500);
LURKER.setProtection(DAMAGE_FIRE, 0);
LURKER.setProtection(DAMAGE_MAGIC, 10);
LURKER.setProtection(DAMAGE_POINT, 20);

// Fight system
LURKER.setWarnTime(6);
LURKER.setHitDistance(190);
LURKER.setChaseDistance(1800);
LURKER.setDetectionDistance(1200);
LURKER.setAttackSpeed(1800);

LURKER.setInitiativeFunction(STANDARD_ANIMAL_AI);

LURKER.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 4))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("R_ROAM3"); break;
        case 3: bot.playAni("T_STAND_2_EAT"); break;	
        case 4: bot.playAni("T_PERCEPTION"); break;	
    }

    bot.setAngle(rand() % 360);
});

registerMonsterTemplate("KYRMIR_LURKER", LURKER);

// Drop
registerMonsterReward("KYRMIR_LURKER", Reward.Content(140)
    .addDrop(Reward.Drop("ITMI_GOLD", 70, 70))
);

// Spawnlist
spawnBot(LURKER, -19168, -278, 8986, "ADDONWORLD.ZEN");
spawnBot(LURKER, -18516, -200, 8342, "ADDONWORLD.ZEN");
spawnBot(LURKER, -22424, -4490, 8189, "ADDONWORLD.ZEN");
spawnBot(LURKER, -21953, -4504, 8036, "ADDONWORLD.ZEN");
spawnBot(LURKER, -23236, -4506, 7924, "ADDONWORLD.ZEN");
spawnBot(LURKER, -22199, -4432, 7029, "ADDONWORLD.ZEN");
spawnBot(LURKER, -27745, -4409, 5361, "ADDONWORLD.ZEN");
spawnBot(LURKER, -27505, -4359, 4951, "ADDONWORLD.ZEN");
spawnBot(LURKER, -11738, -4056, 25651, "ADDONWORLD.ZEN");
spawnBot(LURKER, -10655, -4141, 25943, "ADDONWORLD.ZEN");
spawnBot(LURKER, -9732, -4223, 24824, "ADDONWORLD.ZEN");
spawnBot(LURKER, -8386, -3673, 24890, "ADDONWORLD.ZEN");
spawnBot(LURKER, -27845, -2032, 27887, "ADDONWORLD.ZEN");
spawnBot(LURKER, -28731, -2017, 27872, "ADDONWORLD.ZEN");
spawnBot(LURKER, -25593, -2118, 24653, "ADDONWORLD.ZEN");
