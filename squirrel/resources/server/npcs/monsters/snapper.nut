
local SNAPPER = Bot.Template("SNAPPER", "KYRMIR_SNAPPER"); 

SNAPPER.setLevel(0);
SNAPPER.setMagicLevel(0);

SNAPPER.setHealth(125);
SNAPPER.setRespawnTime(185);

SNAPPER.setDamage(DAMAGE_EDGE, 120);

SNAPPER.setProtection(DAMAGE_EDGE, 0);
SNAPPER.setProtection(DAMAGE_BLUNT, 500);
SNAPPER.setProtection(DAMAGE_FIRE, 0);
SNAPPER.setProtection(DAMAGE_MAGIC, 20);
SNAPPER.setProtection(DAMAGE_POINT, 30);

// Fight system
SNAPPER.setWarnTime(6);
SNAPPER.setHitDistance(150);
SNAPPER.setChaseDistance(1800);
SNAPPER.setDetectionDistance(1200);
SNAPPER.setAttackSpeed(2100);

SNAPPER.setInitiativeFunction(STANDARD_ANIMAL_AI);

SNAPPER.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(rand() % 360);

        switch(random(0, 3))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("T_STAND_2_EAT"); break;	
            case 3: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");		
    }
}
, 7);

registerMonsterTemplate("KYRMIR_SNAPPER", SNAPPER);

// Drop
registerMonsterReward("KYRMIR_SNAPPER", Reward.Content(200)
    .addDrop(Reward.Drop("ITMI_GOLD", 100, 100))
);

// Spawnlist
spawnBot(SNAPPER, -12303, -3695, 18867, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -12455, -3677, 18815, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -14167, -3868, 21226, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -7888, -3159, 22752, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -7495, -3264, 23444, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -7494, -3231, 23696, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -5987, -2832, 23464, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -4964, -2863, 23513, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -4483, -3010, 23418, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, 9630, -3203, -3211, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, 9996, -3231, -3744, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, 9901, -3230, -3885, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, 7152, -2048, -4507, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, 8151, -2000, -8500, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, 6147, -1757, -8942, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -13328, -2570, -15330, "ADDONWORLD.ZEN");
spawnBot(SNAPPER, -13832, -2563, -15874, "ADDONWORLD.ZEN");
