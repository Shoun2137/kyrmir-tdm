
local WARAN = Bot.Template("WARAN", "KYRMIR_WARAN"); 

WARAN.setLevel(0);
WARAN.setMagicLevel(0);

WARAN.setHealth(90);
WARAN.setRespawnTime(110);

WARAN.setDamage(DAMAGE_EDGE, 65);

WARAN.setProtection(DAMAGE_EDGE, 0);
WARAN.setProtection(DAMAGE_BLUNT, 500);
WARAN.setProtection(DAMAGE_FIRE, 0);
WARAN.setProtection(DAMAGE_MAGIC, 10);
WARAN.setProtection(DAMAGE_POINT, 20);

// Fight system
WARAN.setWarnTime(6);
WARAN.setHitDistance(260);
WARAN.setChaseDistance(1800);
WARAN.setDetectionDistance(1200);
WARAN.setAttackSpeed(1800);

WARAN.setInitiativeFunction(STANDARD_ANIMAL_AI);

WARAN.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(rand() % 360);

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");		
    }
}
, 7);

registerMonsterTemplate("KYRMIR_WARAN", WARAN);

// Drop
registerMonsterReward("KYRMIR_WARAN", Reward.Content(130)
    .addDrop(Reward.Drop("ITMI_GOLD", 70, 70))
);

// Spawnlist
spawnBot(WARAN, -28949, -4355, 9151, "ADDONWORLD.ZEN");
spawnBot(WARAN, -28566, -4588, 8560, "ADDONWORLD.ZEN");
spawnBot(WARAN, -27297, -4187, 9907, "ADDONWORLD.ZEN");
spawnBot(WARAN, -26248, -3874, 8920, "ADDONWORLD.ZEN");
spawnBot(WARAN, -25461, -3780, 9028, "ADDONWORLD.ZEN");
spawnBot(WARAN, -25332, -3534, 7999, "ADDONWORLD.ZEN");
spawnBot(WARAN, -24493, -3131, 7554, "ADDONWORLD.ZEN");
spawnBot(WARAN, -35536, -1845, 6688, "ADDONWORLD.ZEN");
spawnBot(WARAN, -32256, -1940, 5423, "ADDONWORLD.ZEN");
spawnBot(WARAN, -35524, -1853, 8144, "ADDONWORLD.ZEN");
spawnBot(WARAN, -37061, -1901, 9651, "ADDONWORLD.ZEN");
spawnBot(WARAN, -37620, -1964, 10765, "ADDONWORLD.ZEN");
spawnBot(WARAN, 29349, -3482, 20299, "ADDONWORLD.ZEN");
spawnBot(WARAN, 27456, -3333, 16510, "ADDONWORLD.ZEN");
spawnBot(WARAN, 28461, -3357, 17593, "ADDONWORLD.ZEN");
