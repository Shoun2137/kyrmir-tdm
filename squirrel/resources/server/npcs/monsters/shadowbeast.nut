
local SHADOWBEAST = Bot.Template("SHADOWBEAST", "KYRMIR_SHADOWBEAST"); 

SHADOWBEAST.setLevel(0);
SHADOWBEAST.setMagicLevel(0);

SHADOWBEAST.setHealth(300);
SHADOWBEAST.setRespawnTime(630);

SHADOWBEAST.setDamage(DAMAGE_EDGE, 160);

SHADOWBEAST.setProtection(DAMAGE_EDGE, 0);
SHADOWBEAST.setProtection(DAMAGE_BLUNT, 500);
SHADOWBEAST.setProtection(DAMAGE_FIRE, 0);
SHADOWBEAST.setProtection(DAMAGE_MAGIC, 15);
SHADOWBEAST.setProtection(DAMAGE_POINT, 25);

// Fight system
SHADOWBEAST.setWarnTime(6);
SHADOWBEAST.setHitDistance(250);
SHADOWBEAST.setChaseDistance(1800);
SHADOWBEAST.setDetectionDistance(1200);
SHADOWBEAST.setAttackSpeed(2100);

SHADOWBEAST.setInitiativeFunction(STANDARD_ANIMAL_AI);

SHADOWBEAST.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 4))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
        case 4: bot.playAni("S_SLEEP"); break;	
    }

    bot.setAngle(rand() % 360);
});

registerMonsterTemplate("KYRMIR_SHADOWBEAST", SHADOWBEAST);

// Drop
registerMonsterReward("KYRMIR_SHADOWBEAST", Reward.Content(600)
    .addDrop(Reward.Drop("ITMI_GOLD", 300, 300))
);

// Spawnlist
spawnBot(SHADOWBEAST, -10222, -3534, -23790, "ADDONWORLD.ZEN");
