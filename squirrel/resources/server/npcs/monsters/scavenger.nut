
local SCAVENGER = Bot.Template("SCAVENGER", "KYRMIR_SCAVENGER"); 

SCAVENGER.setLevel(0);
SCAVENGER.setMagicLevel(0);

SCAVENGER.setHealth(50);
SCAVENGER.setRespawnTime(60);

SCAVENGER.setDamage(DAMAGE_EDGE, 25);

SCAVENGER.setProtection(DAMAGE_EDGE, 0);
SCAVENGER.setProtection(DAMAGE_BLUNT, 500);
SCAVENGER.setProtection(DAMAGE_FIRE, 0);
SCAVENGER.setProtection(DAMAGE_MAGIC, 10);
SCAVENGER.setProtection(DAMAGE_POINT, 20);

// Fight system
SCAVENGER.setWarnTime(6);
SCAVENGER.setHitDistance(240);
SCAVENGER.setChaseDistance(1800);
SCAVENGER.setDetectionDistance(1200);
SCAVENGER.setAttackSpeed(1800);

SCAVENGER.setInitiativeFunction(STANDARD_ANIMAL_AI);

SCAVENGER.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");			
    }
}
, 8);

registerMonsterTemplate("KYRMIR_SCAVENGER", SCAVENGER);

// Drop
registerMonsterReward("KYRMIR_SCAVENGER", Reward.Content(50)
    .addDrop(Reward.Drop("ITMI_GOLD", 35, 35))
);

// Spawnlist
spawnBot(SCAVENGER, -29037, -927, 14251, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -29227, -892, 13058, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -31305, -964, 12378, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -30061, -813, 12062, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -27463, -803, 15019, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -26459, -795, 13769, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -33254, -164, 10051, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -33104, -151, 10264, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -33598, -170, 9801, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -28623, -804, 11312, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -29269, -811, 11764, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -26374, -796, 13948, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -25917, -813, 15032, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -26322, -703, 9934, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -26374, -694, 9856, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -30383, -674, 9430, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -30185, -635, 9791, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -32493, -168, 10300, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -30466, -868, 12908, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -30097, -914, 15485, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -29596, -920, 15123, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -28596, -903, 14902, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -32210, -201, 7040, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -31788, -183, 7032, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -24954, -856, 14651, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -25550, -527, 9906, "ADDONWORLD.ZEN");
spawnBot(SCAVENGER, -23778, -229, 8808, "ADDONWORLD.ZEN");
