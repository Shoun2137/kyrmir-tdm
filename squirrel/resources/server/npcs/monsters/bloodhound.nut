
local BLOODHOUND = Bot.Template("BLOODHOUND", "KYRMIR_BLOODHOUND"); 

BLOODHOUND.setLevel(0);
BLOODHOUND.setMagicLevel(0);

BLOODHOUND.setHealth(200);
BLOODHOUND.setRespawnTime(275)

BLOODHOUND.setDamage(DAMAGE_EDGE, 140);

BLOODHOUND.setProtection(DAMAGE_EDGE, 0);
BLOODHOUND.setProtection(DAMAGE_BLUNT, 500);
BLOODHOUND.setProtection(DAMAGE_FIRE, 0);
BLOODHOUND.setProtection(DAMAGE_MAGIC, 20);
BLOODHOUND.setProtection(DAMAGE_POINT, 30);

// Fight system
BLOODHOUND.setWarnTime(6);
BLOODHOUND.setHitDistance(200);
BLOODHOUND.setChaseDistance(1800);
BLOODHOUND.setDetectionDistance(1200);
BLOODHOUND.setAttackSpeed(1800);

BLOODHOUND.setInitiativeFunction(STANDARD_ANIMAL_AI);

BLOODHOUND.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");			
    }
}
, 6);

registerMonsterTemplate("KYRMIR_BLOODHOUND", BLOODHOUND);

// Drop
registerMonsterReward("KYRMIR_BLOODHOUND", Reward.Content(400)
    .addDrop(Reward.Drop("ITMI_GOLD", 200, 200))
);

// Spawnlist
spawnBot(BLOODHOUND, -20404, -320, -2129, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -15601, -3411, 22141, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -15845, -3591, 24420, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -14150, -3611, 23381, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -2445, -3741, 22922, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -939, -3809, 23605, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, 947, -3636, 22502, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, 0, -3690, 22143, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -12442, -3949, 23114, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -10920, -4123, 30615, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -12419, -3956, 24706, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -12268, -4006, 26912, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -11065, -4159, 30004, "ADDONWORLD.ZEN");
spawnBot(BLOODHOUND, -10828, -4174, 29553, "ADDONWORLD.ZEN");


