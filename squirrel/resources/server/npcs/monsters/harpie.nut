
local HARPIE = Bot.Template("HARPIE", "KYRMIR_HARPIE"); 

HARPIE.setLevel(0);
HARPIE.setMagicLevel(0);

HARPIE.setHealth(170);
HARPIE.setRespawnTime(185);

HARPIE.setDamage(DAMAGE_EDGE, 140);

HARPIE.setProtection(DAMAGE_EDGE, 0);
HARPIE.setProtection(DAMAGE_BLUNT, 500);
HARPIE.setProtection(DAMAGE_FIRE, 0);
HARPIE.setProtection(DAMAGE_MAGIC, 15);
HARPIE.setProtection(DAMAGE_FIRE, 0);

// Fight system
HARPIE.setWarnTime(6);
HARPIE.setHitDistance(190);
HARPIE.setChaseDistance(1800);
HARPIE.setDetectionDistance(1200);
HARPIE.setAttackSpeed(2100);

HARPIE.setInitiativeFunction(STANDARD_ANIMAL_AI);

HARPIE.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 3))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 1: bot.playAni("R_ROAM3"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
    }

    bot.setAngle(rand() % 360);
});

registerMonsterTemplate("KYRMIR_HARPIE", HARPIE);

// Drop
registerMonsterReward("KYRMIR_HARPIE", Reward.Content(200)
    .addDrop(Reward.Drop("ITMI_GOLD", 100, 100))
);

// Spawnlist
spawnBot(HARPIE, -7688, -4074, 26895, "ADDONWORLD.ZEN");
spawnBot(HARPIE, -8039, -4154, 27859, "ADDONWORLD.ZEN");
spawnBot(HARPIE, 2999, 38, -2995, "ADDONWORLD.ZEN");
