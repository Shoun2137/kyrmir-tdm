
local BLATTCRAWLER = Bot.Template("BLATTCRAWLER", "KYRMIR_MINIBOSS_GREEN"); 

BLATTCRAWLER.setLevel(0);
BLATTCRAWLER.setMagicLevel(6);

BLATTCRAWLER.setHealth(2000);
BLATTCRAWLER.setDamage(DAMAGE_EDGE, 150);
BLATTCRAWLER.setRespawnTime(900)

BLATTCRAWLER.setProtection(DAMAGE_EDGE, 0);
BLATTCRAWLER.setProtection(DAMAGE_BLUNT, 500);
BLATTCRAWLER.setProtection(DAMAGE_FIRE, 0);
BLATTCRAWLER.setProtection(DAMAGE_MAGIC, 0);
BLATTCRAWLER.setProtection(DAMAGE_FIRE, 0);

// Fight system
BLATTCRAWLER.setWarnTime(6);
BLATTCRAWLER.setHitDistance(200);
BLATTCRAWLER.setChaseDistance(3600);
BLATTCRAWLER.setDetectionDistance(1200);
BLATTCRAWLER.setAttackSpeed(2100);
BLATTCRAWLER.setInitiativeFunction(STANDARD_ANIMAL_AI);

BLATTCRAWLER.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 4))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
        case 4: bot.playAni("S_SLEEP"); break;	
    }

    bot.setAngle(rand() % 360);
});

BLATTCRAWLER.setRespawnRequirement(function() 
{
    local artifact = getArtifactByInstance("ITMI_FOCUS_GREEN");
    
    if(artifact)
        return !artifact.isSummoned();

    return true;
});

registerMonsterTemplate("KYRMIR_MINIBOSS_GREEN", BLATTCRAWLER);

// Drop
registerMonsterReward("KYRMIR_MINIBOSS_GREEN", Reward.Content(600)
    .addDrop(Reward.Drop("ITMI_GOLD", 400, 400))
    .addDrop(Reward.Drop("ITMI_FOCUS_GREEN", 1, 1))
);

// Spawnlist
registerBoss(spawnBot(BLATTCRAWLER, -26466, -241, -11524, "COLONY.ZEN"));
registerBoss(spawnBot(BLATTCRAWLER, -24592, -2613, -861, "ADDONWORLD.ZEN"));