
local BLACKWOLF = Bot.Template("BLACKWOLF", "KYRMIR_MINIBOSS_RED"); 

BLACKWOLF.setLevel(0);
BLACKWOLF.setMagicLevel(6);

BLACKWOLF.setHealth(2000);
BLACKWOLF.setDamage(DAMAGE_EDGE, 150);
BLACKWOLF.setRespawnTime(900)

BLACKWOLF.setProtection(DAMAGE_EDGE, 0);
BLACKWOLF.setProtection(DAMAGE_BLUNT, 500);
BLACKWOLF.setProtection(DAMAGE_FIRE, 0);
BLACKWOLF.setProtection(DAMAGE_MAGIC, 0);
BLACKWOLF.setProtection(DAMAGE_FIRE, 0);

// Fight system
BLACKWOLF.setWarnTime(6);
BLACKWOLF.setHitDistance(200);
BLACKWOLF.setChaseDistance(3600);
BLACKWOLF.setDetectionDistance(1200);
BLACKWOLF.setAttackSpeed(2100);
BLACKWOLF.setInitiativeFunction(STANDARD_ANIMAL_AI);

BLACKWOLF.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 4))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
        case 4: bot.playAni("S_SLEEP"); break;	
    }

    bot.setAngle(rand() % 360);
});

BLACKWOLF.setRespawnRequirement(function() 
{
    local artifact = getArtifactByInstance("ITMI_FOCUS_RED");
    
    if(artifact)
        return !artifact.isSummoned();

    return true;
});

registerMonsterTemplate("KYRMIR_MINIBOSS_RED", BLACKWOLF);

// Drop
registerMonsterReward("KYRMIR_MINIBOSS_RED", Reward.Content(600)
    .addDrop(Reward.Drop("ITMI_GOLD", 400, 400))
    .addDrop(Reward.Drop("ITMI_FOCUS_RED", 1, 1))
);

// Spawnlist
registerBoss(spawnBot(BLACKWOLF, 3287, 8314, -19991, "COLONY.ZEN"));
registerBoss(spawnBot(BLACKWOLF, 3464, -3295, 30718, "ADDONWORLD.ZEN"));

