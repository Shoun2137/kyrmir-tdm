
local SCAVENGER_DEMON = Bot.Template("SCAVENGER_DEMON", "KYRMIR_MINIBOSS_BLUE"); 

SCAVENGER_DEMON.setLevel(0);
SCAVENGER_DEMON.setMagicLevel(6);

SCAVENGER_DEMON.setHealth(2000);
SCAVENGER_DEMON.setDamage(DAMAGE_EDGE, 150);
SCAVENGER_DEMON.setRespawnTime(900)

SCAVENGER_DEMON.setProtection(DAMAGE_EDGE, 0);
SCAVENGER_DEMON.setProtection(DAMAGE_BLUNT, 500);
SCAVENGER_DEMON.setProtection(DAMAGE_FIRE, 0);
SCAVENGER_DEMON.setProtection(DAMAGE_MAGIC, 0);
SCAVENGER_DEMON.setProtection(DAMAGE_FIRE, 0);

// Fight system
SCAVENGER_DEMON.setWarnTime(6);
SCAVENGER_DEMON.setHitDistance(200);
SCAVENGER_DEMON.setChaseDistance(3600);
SCAVENGER_DEMON.setDetectionDistance(1200);
SCAVENGER_DEMON.setAttackSpeed(2100);
SCAVENGER_DEMON.setInitiativeFunction(STANDARD_ANIMAL_AI);

SCAVENGER_DEMON.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 4))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
        case 4: bot.playAni("S_SLEEP"); break;	
    }

    bot.setAngle(rand() % 360);
});

SCAVENGER_DEMON.setRespawnRequirement(function() {
    local artifact = getArtifactByInstance("ITMI_FOCUS_BLUE");
    if(artifact)
        return !artifact.isSummoned();

    return true;
});

registerMonsterReward("KYRMIR_MINIBOSS_BLUE", Reward.Content(600)
    .addDrop(Reward.Drop("ITMI_GOLD", 400, 400))
    .addDrop(Reward.Drop("ITMI_FOCUS_BLUE", 1, 1))
);

// Spawnlist
registerBoss(spawnBot(SCAVENGER_DEMON, 48646, -546, 353, "COLONY.ZEN"));
registerBoss(spawnBot(SCAVENGER_DEMON, 9, -26, -11439, "ADDONWORLD.ZEN"));
