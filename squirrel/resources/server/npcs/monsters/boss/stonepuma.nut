
local STONEPUMA = Bot.Template("STONEPUMA", "KYRMIR_BOSS_WHITE"); 

STONEPUMA.setLevel(0);
STONEPUMA.setMagicLevel(6);

STONEPUMA.setHealth(30000);
STONEPUMA.setDamage(DAMAGE_EDGE, 180);
STONEPUMA.setRespawnTime(1800)

STONEPUMA.setProtection(DAMAGE_EDGE, 0);
STONEPUMA.setProtection(DAMAGE_BLUNT, 500);
STONEPUMA.setProtection(DAMAGE_FIRE, 0);
STONEPUMA.setProtection(DAMAGE_MAGIC, 0);
STONEPUMA.setProtection(DAMAGE_FIRE, 0);

// Fight system
STONEPUMA.setWarnTime(6);
STONEPUMA.setHitDistance(200);
STONEPUMA.setChaseDistance(3600);
STONEPUMA.setDetectionDistance(1200);
STONEPUMA.setAttackSpeed(2100);

STONEPUMA.setInitiativeFunction(STANDARD_ANIMAL_AI);

STONEPUMA.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() != null) return;

    switch(random(0, 4))
    {
        case 0: bot.playAni("R_ROAM1"); break;				
        case 1: bot.playAni("R_ROAM2"); break;
        case 2: bot.playAni("T_STAND_2_EAT"); break;	
        case 3: bot.playAni("T_PERCEPTION"); break;	
        case 4: bot.playAni("S_SLEEP"); break;	
    }

    bot.setAngle(rand() % 360);
});

registerMonsterTemplate("KYRMIR_BOSS_WHITE", STONEPUMA);

// Drop
registerMonsterReward("KYRMIR_BOSS_WHITE", Reward.Content(2500)
    .addDrop(Reward.Drop("ITMI_GOLD", 2500, 2500))
    .addDrop(Reward.Drop("ITAR_ONB_09", 1, 1)
        .bindRequirementFunc(function(playerId) {
            return getPlayerGuild(playerId) == "SWAMP_CAMP";
        })    
    )
    .addDrop(Reward.Drop("ITAR_SO_09", 1, 1)
        .bindRequirementFunc(function(playerId) {
            return getPlayerGuild(playerId) == "OLD_CAMP";
        })   
    )
    .addDrop(Reward.Drop("ITAR_NO_09", 1, 1)
        .bindRequirementFunc(function(playerId) {
            return getPlayerGuild(playerId) == "NEW_CAMP";
        })
    )  
);

// Spawnlist
registerBoss(spawnBot(STONEPUMA, -337.344, 2698.2, 21345.9, "COLONY.ZEN"));
registerBoss(spawnBot(STONEPUMA, -22151, -3106, -26264, "ADDONWORLD.ZEN"));