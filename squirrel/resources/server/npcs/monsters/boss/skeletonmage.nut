
local SKELETONMAGE = Bot.Template("SKELETONMAGE", "KYRMIR_BOSS_VIOLET"); 

SKELETONMAGE.setLevel(0);
SKELETONMAGE.setMagicLevel(6);

SKELETONMAGE.setHealth(30000);
SKELETONMAGE.setDamage(DAMAGE_EDGE, 180);
SKELETONMAGE.setRespawnTime(1800)

SKELETONMAGE.setProtection(DAMAGE_EDGE, 0);
SKELETONMAGE.setProtection(DAMAGE_BLUNT, 500);
SKELETONMAGE.setProtection(DAMAGE_FIRE, 0);
SKELETONMAGE.setProtection(DAMAGE_MAGIC, 0);
SKELETONMAGE.setProtection(DAMAGE_FIRE, 0);

// Fight system
SKELETONMAGE.setWarnTime(6);
SKELETONMAGE.setHitDistance(200);
SKELETONMAGE.setChaseDistance(3600);
SKELETONMAGE.setDetectionDistance(1200);
SKELETONMAGE.setAttackSpeed(2100);

SKELETONMAGE.setInitiativeFunction(SKELETONMAGE_AI);

registerMonsterTemplate("KYRMIR_BOSS_VIOLET", SKELETONMAGE);

// Drop
registerMonsterReward("KYRMIR_BOSS_VIOLET", Reward.Content(2500)
    .addDrop(Reward.Drop("ITMI_GOLD", 2500, 2500))
    .addDrop(Reward.Drop("ITRU_LIGHTNINGFLASH", 1, 1))
);

// Spawnlist
registerBoss(spawnBot(SKELETONMAGE, 23075.3, 7924.38, -39572.8, "COLONY.ZEN"));
registerBoss(spawnBot(SKELETONMAGE, 1960, -2261, 19004, "ADDONWORLD.ZEN"));
