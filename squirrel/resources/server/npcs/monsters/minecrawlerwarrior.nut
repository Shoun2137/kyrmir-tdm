
local MINECRAWLERWARRIOR = Bot.Template("MINECRAWLERWARRIOR", "KYRMIR_MINECRAWLERWARRIOR"); 

MINECRAWLERWARRIOR.setLevel(0);
MINECRAWLERWARRIOR.setMagicLevel(0);

MINECRAWLERWARRIOR.setHealth(150);
MINECRAWLERWARRIOR.setRespawnTime(190);

MINECRAWLERWARRIOR.setDamage(DAMAGE_EDGE, 125);

MINECRAWLERWARRIOR.setProtection(DAMAGE_EDGE, 0);
MINECRAWLERWARRIOR.setProtection(DAMAGE_BLUNT, 500);
MINECRAWLERWARRIOR.setProtection(DAMAGE_FIRE, 0);
MINECRAWLERWARRIOR.setProtection(DAMAGE_MAGIC, 15);
MINECRAWLERWARRIOR.setProtection(DAMAGE_POINT, 0);

// Fight system
MINECRAWLERWARRIOR.setWarnTime(6);
MINECRAWLERWARRIOR.setHitDistance(230);
MINECRAWLERWARRIOR.setChaseDistance(1800);
MINECRAWLERWARRIOR.setDetectionDistance(1200);
MINECRAWLERWARRIOR.setAttackSpeed(1800);

MINECRAWLERWARRIOR.setInitiativeFunction(STANDARD_ANIMAL_AI);

MINECRAWLERWARRIOR.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 3))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("T_STAND_2_EAT"); break;	
            case 3: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 300)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }
           
        bot.playAni("S_FISTWALKL");		
    }
}
, 2);

registerMonsterTemplate("KYRMIR_MINECRAWLERWARRIOR", MINECRAWLERWARRIOR);

// Drop
registerMonsterReward("KYRMIR_MINECRAWLERWARRIOR", Reward.Content(250)
    .addDrop(Reward.Drop("ITMI_GOLD", 125, 125))
);

// Spawnlist
spawnBot(MINECRAWLERWARRIOR, -14486, -1871, 11978, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLERWARRIOR, -4409, -1944, 16940, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLERWARRIOR, -5125, -1817, 17599, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLERWARRIOR, 6130, -2819, 13598, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLERWARRIOR, 5592, -2841, 13056, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLERWARRIOR, 4530, -2942, 14902, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLERWARRIOR, -2990, -2475, 14390, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLERWARRIOR, -355, -2822, 15777, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLERWARRIOR, 970, -2854, 13371, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLERWARRIOR, 3100, -2738, 13533, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLERWARRIOR, 3196, -2803, 12553, "ADDONWORLD.ZEN");
spawnBot(MINECRAWLERWARRIOR, 2996, -2780, 12420, "ADDONWORLD.ZEN");
