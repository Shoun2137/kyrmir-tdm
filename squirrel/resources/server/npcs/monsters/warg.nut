
local WARG = Bot.Template("WARG", "KYRMIR_WARG"); 

WARG.setLevel(0);
WARG.setMagicLevel(0);

WARG.setHealth(175);
WARG.setRespawnTime(210);

WARG.setDamage(DAMAGE_EDGE, 130);

WARG.setProtection(DAMAGE_EDGE, 0);
WARG.setProtection(DAMAGE_BLUNT, 500);
WARG.setProtection(DAMAGE_FIRE, 0);
WARG.setProtection(DAMAGE_MAGIC, 20);
WARG.setProtection(DAMAGE_POINT, 30);

// Fight system
WARG.setWarnTime(6);
WARG.setHitDistance(240);
WARG.setChaseDistance(1800);
WARG.setDetectionDistance(1200);
WARG.setAttackSpeed(2100);

WARG.setInitiativeFunction(STANDARD_ANIMAL_AI);

WARG.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(rand() % 360);

        switch(random(0, 4))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("R_ROAM2"); break;
            case 2: bot.playAni("R_ROAM3"); break;
            case 3: bot.playAni("T_STAND_2_EAT"); break;	
            case 4: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");		
    }
}
, 7);

registerMonsterTemplate("KYRMIR_WARG", WARG);

// Drop
registerMonsterReward("KYRMIR_WARG", Reward.Content(270)
    .addDrop(Reward.Drop("ITMI_GOLD", 120, 120))
);

// Spawnlist
spawnBot(WARG, -20012, -2974, -14263, "ADDONWORLD.ZEN");
spawnBot(WARG, -19540, -3017, -14038, "ADDONWORLD.ZEN");
spawnBot(WARG, -19563, -3008, -14149, "ADDONWORLD.ZEN");
spawnBot(WARG, -20064, -3258, -16109, "ADDONWORLD.ZEN");
spawnBot(WARG, 3290, -1970, -13508, "ADDONWORLD.ZEN");
spawnBot(WARG, 4020, -1939, -13722, "ADDONWORLD.ZEN");
spawnBot(WARG, 5363, -1931, -12892, "ADDONWORLD.ZEN");
spawnBot(WARG, 6201, -2032, -11471, "ADDONWORLD.ZEN");
spawnBot(WARG, 5714, -1794, -10481, "ADDONWORLD.ZEN");
spawnBot(WARG, 4282, -1441, -11267, "ADDONWORLD.ZEN");
spawnBot(WARG, -2467, -1652, -14220, "ADDONWORLD.ZEN");
spawnBot(WARG, -4056, -974, -13071, "ADDONWORLD.ZEN");
spawnBot(WARG, -3057, -694, -12638, "ADDONWORLD.ZEN");
spawnBot(WARG, -2451, -801, -13542, "ADDONWORLD.ZEN");
spawnBot(WARG, -822, -796, -13495, "ADDONWORLD.ZEN");
spawnBot(WARG, -10725, -3321, -13321, "ADDONWORLD.ZEN");
spawnBot(WARG, -11817, -3356, -13560, "ADDONWORLD.ZEN");
spawnBot(WARG, -10730, -3257, -15025, "ADDONWORLD.ZEN");
spawnBot(WARG, -11905, -3546, -15293, "ADDONWORLD.ZEN");
spawnBot(WARG, -12002, -3671, -16696, "ADDONWORLD.ZEN");
spawnBot(WARG, -13857, -2722, -20697, "ADDONWORLD.ZEN");
spawnBot(WARG, -14142, -2792, -20319, "ADDONWORLD.ZEN");
spawnBot(WARG, -13515, -3273, -22097, "ADDONWORLD.ZEN");
spawnBot(WARG, 4169, -1312, -9530, "ADDONWORLD.ZEN");
spawnBot(WARG, 3547, -1363, -7727, "ADDONWORLD.ZEN");
spawnBot(WARG, -17282, -2745, -10903, "ADDONWORLD.ZEN");
