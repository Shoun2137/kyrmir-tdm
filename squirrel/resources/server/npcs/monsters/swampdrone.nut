local SWAMPDRONE = Bot.Template("SWAMPDRONE", "KYRMIR_SWAMPDRONE"); 

SWAMPDRONE.setLevel(0);
SWAMPDRONE.setMagicLevel(0);

SWAMPDRONE.setHealth(50);
SWAMPDRONE.setRespawnTime(60)

SWAMPDRONE.setDamage(DAMAGE_EDGE, 25);

SWAMPDRONE.setProtection(DAMAGE_EDGE, 0);
SWAMPDRONE.setProtection(DAMAGE_BLUNT, 500);
SWAMPDRONE.setProtection(DAMAGE_FIRE, 0);
SWAMPDRONE.setProtection(DAMAGE_MAGIC, 10);
SWAMPDRONE.setProtection(DAMAGE_POINT, 20);

// Fight system
SWAMPDRONE.setWarnTime(6);
SWAMPDRONE.setHitDistance(180);
SWAMPDRONE.setChaseDistance(1800);
SWAMPDRONE.setDetectionDistance(1200);
SWAMPDRONE.setAttackSpeed(1800);

SWAMPDRONE.setInitiativeFunction(STANDARD_ANIMAL_AI);

SWAMPDRONE.setRoutineFunction(function(bot)
{
    if(bot.getLastAni() == "S_FISTWALKL")
    {
        bot.setAngle(random(0, 360));

        switch(random(0, 2))
        {
            case 0: bot.playAni("R_ROAM1"); break;				
            case 1: bot.playAni("T_STAND_2_EAT"); break;	
            case 2: bot.playAni("T_PERCEPTION"); break;	
        }
    }
    else
    {
        local position = bot.getPosition(), respawn = bot.getRespawnPosition();

        if(getDistance2d(position.x, position.z, respawn.x, respawn.z) > 400)
        {   
            local vector = getVectorAngle(position.x, position.z, respawn.x, respawn.z);
                bot.setAngle(vector);
        }

        bot.playAni("S_FISTWALKL");			
    }
}
, 7);

registerMonsterTemplate("KYRMIR_SWAMPDRONE", SWAMPDRONE);

// Drop
registerMonsterReward("KYRMIR_SWAMPDRONE", Reward.Content(50)
    .addDrop(Reward.Drop("ITMI_GOLD", 35, 35))
);

// Spawnlist
spawnBot(SWAMPDRONE, 20687, -5258, 3718, "ADDONWORLD.ZEN");
spawnBot(SWAMPDRONE, 20321, -5130, 2398, "ADDONWORLD.ZEN");
spawnBot(SWAMPDRONE, 21506, -4996, 684, "ADDONWORLD.ZEN");
spawnBot(SWAMPDRONE, 21973, -4996, -207, "ADDONWORLD.ZEN");
spawnBot(SWAMPDRONE, 23062, -5036, -869, "ADDONWORLD.ZEN");
spawnBot(SWAMPDRONE, 23524, -5258, -2442, "ADDONWORLD.ZEN");
spawnBot(SWAMPDRONE, 28626, -4650, -8154, "ADDONWORLD.ZEN");
spawnBot(SWAMPDRONE, 25180, -4582, -7071, "ADDONWORLD.ZEN");
spawnBot(SWAMPDRONE, 23392, -4567, -7125, "ADDONWORLD.ZEN");
spawnBot(SWAMPDRONE, 24823, -5171, -5008, "ADDONWORLD.ZEN");
spawnBot(SWAMPDRONE, 14242, -4949, -4076, "ADDONWORLD.ZEN");
spawnBot(SWAMPDRONE, 13114, -5195, -7379, "ADDONWORLD.ZEN");
spawnBot(SWAMPDRONE, 12874, -5071, -8124, "ADDONWORLD.ZEN");
spawnBot(SWAMPDRONE, 14754, -5159, 6428, "ADDONWORLD.ZEN");
spawnBot(SWAMPDRONE, 18709, -5120, 4418, "ADDONWORLD.ZEN");
