
local NPC_ARENA = createNpc("NPC_ARENA", "PC_HERO", "ARENA.ZEN");

NPC_ARENA.setVisual("Hum_Body_Naked0", 1, "Hum_Head_Bald", 0);
NPC_ARENA.equipArmor(Items.id("EBR_ARMOR_G1_H2"));
NPC_ARENA.equipMeleeWeapon(Items.id("ITMW_1H_SPECIAL_04"));
NPC_ARENA.equipRangedWeapon(Items.id("ITRW_CROSSBOW_M_02"));

NPC_ARENA.playAni("S_HGUARD");

NPC_ARENA.setPosition(3271, -890, 1023);
NPC_ARENA.setAngle(240);

NPC_ARENA.setCollision(false);
NPC_ARENA.setImmortal(true);

NPC_ARENA.spawn();

///////////////////////////////////////////////////////////////////////////////

local NPC_ARENA_LIST = Dialog.List();

NPC_ARENA_LIST
.addOption(Dialog.EndOption("NPC_ARENA_BACK_TO_WORLD")
    .bindEffectFunc(function(playerId) 
    {
        take_gear(playerId);

        local guild = getGuildById(getPlayerGuild(playerId));
        
        if(guild)
        {
            setPlayerWorld(playerId, guild.getHomeWorld());
  
                local respawn = guild.getRespawnPosition();
            setPlayerPosition(playerId, respawn.x, respawn.y, respawn.z);
        }
        else 
        {
                local defaultPosition = DEFAULT_POSITION(getPlayerWorld(playerId));
            setPlayerPosition(playerId, defaultPosition.x, defaultPosition.y, defaultPosition.z);
        }
    })
)
.addOption(Dialog.EndOption("NPC_ARENA_END"));

registerDialogNpc(NPC_ARENA.getId(), function(playerId) {
    return Dialog.ListOption("NPC_ARENA_INITIAL", NPC_ARENA_LIST);
});
