
/********************************** OLD CAMP **********************************/

addItemTemplate(
    Item.Armor("ITAR_SO_01", Items.id("VLK_ARMOR_G1_L"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 15)
        .setProtection(DAMAGE_EDGE, 15)
        .setProtection(DAMAGE_POINT, 30)
        .setProtection(DAMAGE_MAGIC, 0)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_SO_02", Items.id("STT_ARMOR_G1_M"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 30)
        .setProtection(DAMAGE_EDGE, 30)
        .setProtection(DAMAGE_POINT, 50)
        .setProtection(DAMAGE_MAGIC, 5)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_SO_03", Items.id("STT_ARMOR_G1_H"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 60)
        .setProtection(DAMAGE_EDGE, 60)
        .setProtection(DAMAGE_POINT, 80)
        .setProtection(DAMAGE_MAGIC, 10)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_SO_04", Items.id("GRD_ARMOR_G1_L"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 90)
        .setProtection(DAMAGE_EDGE, 90)
        .setProtection(DAMAGE_POINT, 110)
        .setProtection(DAMAGE_MAGIC, 25)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_SO_05", Items.id("GRD_ARMOR_G1_M"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 120)
        .setProtection(DAMAGE_EDGE, 120)
        .setProtection(DAMAGE_POINT, 140)
        .setProtection(DAMAGE_MAGIC, 30)
        .setRequirement(ItemRequirement.Strength, 20)
);

addItemTemplate(
    Item.Armor("ITAR_SO_06", Items.id("GRD_ARMOR_G1_H"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 150)
        .setProtection(DAMAGE_EDGE, 150)
        .setProtection(DAMAGE_POINT, 160)
        .setProtection(DAMAGE_MAGIC, 40)
        .setRequirement(ItemRequirement.Strength, 80)
);

addItemTemplate(
    Item.Armor("ITAR_SO_07", Items.id("ITAR_NOV_L"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 60)
        .setProtection(DAMAGE_EDGE, 60)
        .setProtection(DAMAGE_POINT, 80)
        .setProtection(DAMAGE_MAGIC, 30)
        .setRequirement(ItemRequirement.Mana, 30)
);

addItemTemplate(
    Item.Armor("ITAR_SO_08", Items.id("KDF_ARMOR_G1_L"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 120)
        .setProtection(DAMAGE_EDGE, 120)
        .setProtection(DAMAGE_POINT, 140)
        .setProtection(DAMAGE_MAGIC, 40)
        .setRequirement(ItemRequirement.Mana, 50)
);

addItemTemplate(
    Item.Armor("ITAR_SO_09", Items.id("KDF_ARMOR_G1_H"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 140)
        .setProtection(DAMAGE_EDGE, 140)
        .setProtection(DAMAGE_POINT, 150)
        .setProtection(DAMAGE_MAGIC, 50)
        .setRequirement(ItemRequirement.Mana, 50)
);
 
addItemTemplate(
    Item.Armor("ITAR_SO_10", Items.id("EBR_ARMOR_G1_H2"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 160)
        .setProtection(DAMAGE_EDGE, 160)
        .setProtection(DAMAGE_POINT, 160)
        .setProtection(DAMAGE_MAGIC, 50)
        .setRequirement(ItemRequirement.Strength, 80)
);

/********************************** NEW CAMP **********************************/

addItemTemplate(
    Item.Armor("ITAR_NO_01", Items.id("SLD_ARMOR_G1_L"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 15)
        .setProtection(DAMAGE_EDGE, 15)
        .setProtection(DAMAGE_POINT, 30)
        .setProtection(DAMAGE_MAGIC, 0)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_NO_02", Items.id("ORG_ARMOR_G1_L"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 30)
        .setProtection(DAMAGE_EDGE, 30)
        .setProtection(DAMAGE_POINT, 50)
        .setProtection(DAMAGE_MAGIC, 5)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_NO_03", Items.id("ORG_ARMOR_G1_M"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 60)
        .setProtection(DAMAGE_EDGE, 60)
        .setProtection(DAMAGE_POINT, 80)
        .setProtection(DAMAGE_MAGIC, 10)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_NO_04", Items.id("ORG_ARMOR_G1_H"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 90)
        .setProtection(DAMAGE_EDGE, 90)
        .setProtection(DAMAGE_POINT, 110)
        .setProtection(DAMAGE_MAGIC, 25)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_NO_05", Items.id("SLD_ARMOR_G1_M"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 120)
        .setProtection(DAMAGE_EDGE, 120)
        .setProtection(DAMAGE_POINT, 140)
        .setProtection(DAMAGE_MAGIC, 30)
        .setRequirement(ItemRequirement.Strength, 20)
);

addItemTemplate(
    Item.Armor("ITAR_NO_06", Items.id("SLD_ARMOR_G1_H"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 150)
        .setProtection(DAMAGE_EDGE, 150)
        .setProtection(DAMAGE_POINT, 160)
        .setProtection(DAMAGE_MAGIC, 40)
        .setRequirement(ItemRequirement.Strength, 80)
);

addItemTemplate(
    Item.Armor("ITAR_NO_07", Items.id("ITAR_RANGER_ADDON"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 60)
        .setProtection(DAMAGE_EDGE, 60)
        .setProtection(DAMAGE_POINT, 80)
        .setProtection(DAMAGE_MAGIC, 30)
        .setRequirement(ItemRequirement.Mana, 30)
);

addItemTemplate(
    Item.Armor("ITAR_NO_08", Items.id("KDW_ARMOR_G1_L"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 120)
        .setProtection(DAMAGE_EDGE, 120)
        .setProtection(DAMAGE_POINT, 140)
        .setProtection(DAMAGE_MAGIC, 40)
        .setRequirement(ItemRequirement.Mana, 50)
);

addItemTemplate(
    Item.Armor("ITAR_NO_09", Items.id("KDW_ARMOR_G1_H"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 140)
        .setProtection(DAMAGE_EDGE, 140)
        .setProtection(DAMAGE_POINT, 150)
        .setProtection(DAMAGE_MAGIC, 50)
        .setRequirement(ItemRequirement.Mana, 50)
);

addItemTemplate(
    Item.Armor("ITAR_NO_10", Items.id("ORE_ARMOR_G1_H"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 160)
        .setProtection(DAMAGE_EDGE, 160)
        .setProtection(DAMAGE_POINT, 160)
        .setProtection(DAMAGE_MAGIC, 50)
        .setRequirement(ItemRequirement.Strength, 80)
);

/********************************** NEW SWAMP CAMP **********************************/

addItemTemplate(
    Item.Armor("ITAR_ONB_01", Items.id("NOV_ARMOR_G1_L"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 15)
        .setProtection(DAMAGE_EDGE, 15)
        .setProtection(DAMAGE_POINT, 30)
        .setProtection(DAMAGE_MAGIC, 0)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_ONB_02", Items.id("NOV_ARMOR_G1_M"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 30)
        .setProtection(DAMAGE_EDGE, 30)
        .setProtection(DAMAGE_POINT, 50)
        .setProtection(DAMAGE_MAGIC, 5)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_ONB_03", Items.id("NOV_ARMOR_G1_H"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 60)
        .setProtection(DAMAGE_EDGE, 60)
        .setProtection(DAMAGE_POINT, 80)
        .setProtection(DAMAGE_MAGIC, 10)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_ONB_04", Items.id("TPL_ARMOR_G1_L"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 90)
        .setProtection(DAMAGE_EDGE, 90)
        .setProtection(DAMAGE_POINT, 110)
        .setProtection(DAMAGE_MAGIC, 25)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_ONB_05", Items.id("TPL_ARMOR_G1_M"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 120)
        .setProtection(DAMAGE_EDGE, 120)
        .setProtection(DAMAGE_POINT, 140)
        .setProtection(DAMAGE_MAGIC, 30)
        .setRequirement(ItemRequirement.Strength, 20)
);

addItemTemplate(
    Item.Armor("ITAR_ONB_06", Items.id("TPL_ARMOR_G1_H"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 150)
        .setProtection(DAMAGE_EDGE, 150)
        .setProtection(DAMAGE_POINT, 160)
        .setProtection(DAMAGE_MAGIC, 40)
        .setRequirement(ItemRequirement.Strength, 80)
);

addItemTemplate(
    Item.Armor("ITAR_ONB_07", Items.id("GUR_ARMOR_G1_M"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 60)
        .setProtection(DAMAGE_EDGE, 60)
        .setProtection(DAMAGE_POINT, 80)
        .setProtection(DAMAGE_MAGIC, 30)
        .setRequirement(ItemRequirement.Mana, 30)
);

addItemTemplate(
    Item.Armor("ITAR_ONB_08", Items.id("GUR_ARMOR_G1_H"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 120)
        .setProtection(DAMAGE_EDGE, 120)
        .setProtection(DAMAGE_POINT, 140)
        .setProtection(DAMAGE_MAGIC, 40)
        .setRequirement(ItemRequirement.Mana, 50)
);

addItemTemplate(
    Item.Armor("ITAR_ONB_09", Items.id("ITAR_XARDAS"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 140)
        .setProtection(DAMAGE_EDGE, 140)
        .setProtection(DAMAGE_POINT, 150)
        .setProtection(DAMAGE_MAGIC, 50)
        .setRequirement(ItemRequirement.Mana, 50)
);

addItemTemplate(
    Item.Armor("ITAR_ONB_10", Items.id("ITAR_DJG_CRAWLER"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 160)
        .setProtection(DAMAGE_EDGE, 160)
        .setProtection(DAMAGE_POINT, 160)
        .setProtection(DAMAGE_MAGIC, 50)
        .setRequirement(ItemRequirement.Strength, 80)
);

/********************************** PIRATE CAMP **********************************/

addItemTemplate(
    Item.Armor("ITAR_PIR_01", Items.id("ITAR_PIR_LL"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 15)
        .setProtection(DAMAGE_EDGE, 15)
        .setProtection(DAMAGE_POINT, 30)
        .setProtection(DAMAGE_MAGIC, 0)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_PIR_02", Items.id("ITAR_PIR_L"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 30)
        .setProtection(DAMAGE_EDGE, 30)
        .setProtection(DAMAGE_POINT, 50)
        .setProtection(DAMAGE_MAGIC, 5)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_PIR_03", Items.id("ITAR_PIR_L_ADDON"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 60)
        .setProtection(DAMAGE_EDGE, 60)
        .setProtection(DAMAGE_POINT, 80)
        .setProtection(DAMAGE_MAGIC, 10)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_PIR_04", Items.id("ITAR_PIR_M_ADDON"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 90)
        .setProtection(DAMAGE_EDGE, 90)
        .setProtection(DAMAGE_POINT, 110)
        .setProtection(DAMAGE_MAGIC, 25)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_PIR_05", Items.id("ITAR_PIR_M"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 120)
        .setProtection(DAMAGE_EDGE, 120)
        .setProtection(DAMAGE_POINT, 140)
        .setProtection(DAMAGE_MAGIC, 30)
        .setRequirement(ItemRequirement.Strength, 20)
);

addItemTemplate(
    Item.Armor("ITAR_PIR_06", Items.id("ITAR_PIR_H"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 150)
        .setProtection(DAMAGE_EDGE, 150)
        .setProtection(DAMAGE_POINT, 160)
        .setProtection(DAMAGE_MAGIC, 40)
        .setRequirement(ItemRequirement.Strength, 80)
);

addItemTemplate(
    Item.Armor("ITAR_PIR_07", Items.id("ITAR_RANGER_ADDON"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 60)
        .setProtection(DAMAGE_EDGE, 60)
        .setProtection(DAMAGE_POINT, 80)
        .setProtection(DAMAGE_MAGIC, 30)
        .setRequirement(ItemRequirement.Mana, 30)
);

addItemTemplate(
    Item.Armor("ITAR_PIR_08", Items.id("KDW_ARMOR_G1_L"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 120)
        .setProtection(DAMAGE_EDGE, 120)
        .setProtection(DAMAGE_POINT, 140)
        .setProtection(DAMAGE_MAGIC, 40)
        .setRequirement(ItemRequirement.Mana, 50)
);

addItemTemplate(
    Item.Armor("ITAR_PIR_09", Items.id("KDW_ARMOR_G1_H"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 140)
        .setProtection(DAMAGE_EDGE, 140)
        .setProtection(DAMAGE_POINT, 150)
        .setProtection(DAMAGE_MAGIC, 50)
        .setRequirement(ItemRequirement.Mana, 50)
);
 
addItemTemplate(
    Item.Armor("ITAR_PIR_10", Items.id("ITAR_PIR_H_ADDON"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 160)
        .setProtection(DAMAGE_EDGE, 160)
        .setProtection(DAMAGE_POINT, 160)
        .setProtection(DAMAGE_MAGIC, 50)
        .setRequirement(ItemRequirement.Strength, 80)
);

/********************************** BANDIT CAMP **********************************/

addItemTemplate(
    Item.Armor("ITAR_BDT_01", Items.id("VLK_ARMOR_G1_L"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 15)
        .setProtection(DAMAGE_EDGE, 15)
        .setProtection(DAMAGE_POINT, 30)
        .setProtection(DAMAGE_MAGIC, 0)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_BDT_02", Items.id("ITAR_LEATHER_L"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 30)
        .setProtection(DAMAGE_EDGE, 30)
        .setProtection(DAMAGE_POINT, 50)
        .setProtection(DAMAGE_MAGIC, 5)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_BDT_03", Items.id("ITAR_BDT_M"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 60)
        .setProtection(DAMAGE_EDGE, 60)
        .setProtection(DAMAGE_POINT, 80)
        .setProtection(DAMAGE_MAGIC, 10)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_BDT_04", Items.id("ITAR_BDT_H"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 90)
        .setProtection(DAMAGE_EDGE, 90)
        .setProtection(DAMAGE_POINT, 110)
        .setProtection(DAMAGE_MAGIC, 25)
        .setRequirement(ItemRequirement.Strength, 10)
);

addItemTemplate(
    Item.Armor("ITAR_BDT_05", Items.id("GRD_ARMOR_G1_M"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 120)
        .setProtection(DAMAGE_EDGE, 120)
        .setProtection(DAMAGE_POINT, 140)
        .setProtection(DAMAGE_MAGIC, 30)
        .setRequirement(ItemRequirement.Strength, 20)
);

addItemTemplate(
    Item.Armor("ITAR_BDT_06", Items.id("GRD_ARMOR_G1_H"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 150)
        .setProtection(DAMAGE_EDGE, 150)
        .setProtection(DAMAGE_POINT, 160)
        .setProtection(DAMAGE_MAGIC, 40)
        .setRequirement(ItemRequirement.Strength, 80)
);

addItemTemplate(
    Item.Armor("ITAR_BDT_07", Items.id("NOV_ARMOR_G1_H"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 60)
        .setProtection(DAMAGE_EDGE, 60)
        .setProtection(DAMAGE_POINT, 80)
        .setProtection(DAMAGE_MAGIC, 30)
        .setRequirement(ItemRequirement.Mana, 30)
);

addItemTemplate(
    Item.Armor("ITAR_BDT_08", Items.id("ITAR_XARDAS"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 120)
        .setProtection(DAMAGE_EDGE, 120)
        .setProtection(DAMAGE_POINT, 140)
        .setProtection(DAMAGE_MAGIC, 40)
        .setRequirement(ItemRequirement.Mana, 50)
);

addItemTemplate(
    Item.Armor("ITAR_BDT_09", Items.id("ITAR_DEMENTOR"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 140)
        .setProtection(DAMAGE_EDGE, 140)
        .setProtection(DAMAGE_POINT, 150)
        .setProtection(DAMAGE_MAGIC, 50)
        .setRequirement(ItemRequirement.Mana, 50)
);
 
addItemTemplate(
    Item.Armor("ITAR_BDT_10", Items.id("ITAR_RAVEN_ADDON"), ItemType.Armor)
        .setInteractionType(ItemInteraction.Equippable)
        .setProtection(DAMAGE_BLUNT, 160)
        .setProtection(DAMAGE_EDGE, 160)
        .setProtection(DAMAGE_POINT, 160)
        .setProtection(DAMAGE_MAGIC, 50)
        .setRequirement(ItemRequirement.Strength, 80)
);