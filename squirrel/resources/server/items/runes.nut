
addItemTemplate(
    Item.Spell("ITRU_FIREBOLT", Items.id("ITRU_FIREBOLT"), ItemType.Rune)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_MAGIC, 30)
        .setRequirement(ItemRequirement.MagicLevel, 1)
        .setManaUsage(2)
);

addItemTemplate(
    Item.Spell("ITRU_ICEBOLT", Items.id("ITRU_ICEBOLT"), ItemType.Rune)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_MAGIC, 40)
        .setRequirement(ItemRequirement.MagicLevel, 2)
        .setManaUsage(2)
);

addItemTemplate(
    Item.Spell("ITRU_LIGHTHEAL", Items.id("ITRU_LIGHTHEAL"), ItemType.Rune)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setRequirement(ItemRequirement.MagicLevel, 2)
        .setManaUsage(20)
        .bindEffectFunc(function(target, caster) 
        {
            setPlayerHealth(caster, getPlayerHealth(caster) + 50);    
                return false;
        })
);

addItemTemplate(
    Item.Spell("ITRU_ZAP", Items.id("ITRU_ZAP"), ItemType.Rune)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_MAGIC, 55)
        .setRequirement(ItemRequirement.MagicLevel, 3)
        .setManaUsage(5)
);

addItemTemplate(
    Item.Spell("ITRU_MEDIUMHEAL", Items.id("ITRU_MEDIUMHEAL"), ItemType.Rune)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setRequirement(ItemRequirement.MagicLevel, 4)
        .setManaUsage(30)
        .bindEffectFunc(function(target, caster) 
        {
            setPlayerHealth(caster, getPlayerHealth(caster) + 100);    
        })
);

addItemTemplate(
    Item.Spell("ITRU_ICELANCE", Items.id("ITRU_ICELANCE"), ItemType.Rune)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_MAGIC, 70)
        .setRequirement(ItemRequirement.MagicLevel, 4)
        .setManaUsage(5)
);

addItemTemplate(
    Item.Spell("ITRU_HEAL_TARGET", Items.id("ITRU_SHRINK"), ItemType.Rune)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setRequirement(ItemRequirement.MagicLevel, 5)
        .setManaUsage(30)
        .setInstant(true)
        .bindEffectFunc(function(target, caster) 
        {
            if(typeof(target) == "integer")
            {
                if(getPlayerGuild(target) == getPlayerGuild(caster))
                {
                    setPlayerHealth(target, getPlayerHealth(target) + 100);
                }
                else 
                {
                    setPlayerHealth(target, getPlayerHealth(target) - 25);
                    setPlayerHealth(caster, getPlayerHealth(caster) + 50);
                }
            }
            else 
            {
                target.setHealth(target.getHealth() - 25);
                setPlayerHealth(caster, getPlayerHealth(caster) + 50);    
            }
            
            return true;
        })
);

addItemTemplate(
    Item.Spell("ITRU_INSTANTFIREBALL", Items.id("ITRU_INSTANTFIREBALL"), ItemType.Rune)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_MAGIC, 90)
        .setRequirement(ItemRequirement.MagicLevel, 5)
        .setManaUsage(5)
);

addItemTemplate(
    Item.Spell("ITRU_FULLHEAL", Items.id("ITRU_FULLHEAL"), ItemType.Rune)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setRequirement(ItemRequirement.MagicLevel, 6)
        .setManaUsage(50)
        .bindEffectFunc(function(target, caster) 
        {
            setPlayerHealth(caster, getPlayerHealth(caster) + 150);    
        })
);

addItemTemplate(
    Item.Spell("ITRU_FIRESTORM", Items.id("ITRU_FIRESTORM"), ItemType.Rune)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_MAGIC, 80)
        .setRequirement(ItemRequirement.MagicLevel, 6)
        .setManaUsage(10)
);

addItemTemplate(
    Item.Spell("ITRU_LIGHTNINGFLASH", Items.id("ITRU_LIGHTNINGFLASH"), ItemType.Rune)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_MAGIC, 110)
        .setRequirement(ItemRequirement.MagicLevel, 6)
        .setManaUsage(5)
);

addItemTemplate(
    Item.Spell("ITSC_MASSDEATH", Items.id("ITSC_MASSDEATH"), ItemType.Scroll)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_MAGIC, 180)
        .setRequirement(ItemRequirement.MagicLevel, 6)
        .setManaUsage(150)
);