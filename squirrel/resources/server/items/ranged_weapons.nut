
/********************************** Munition **********************************/

ITRW_BOW_ARROW <- Item.Standard("ITRW_BOW_ARROW", Items.id("ITRW_ARROW"), ItemType.Munition)
    .setInteractionType(ItemInteraction.None);

ITRW_CROSSBOW_BOLT <- Item.Standard("ITRW_CROSSBOW_BOLT", Items.id("ITRW_BOLT"), ItemType.Munition)
    .setInteractionType(ItemInteraction.None);

/*ITRW_MAGICBOW_ARROW <- Item.Standard("ITRW_MAGICBOW_ARROW", Items.id("ITRW_ADDON_MAGICARROW"), ItemType.Munition)
    .setInteractionType(ItemInteraction.None);

ITRW_MAGICCROSSBOW_BOLT <- Item.Standard("ITRW_MAGICCROSSBOW_BOLT", Items.id("ITRW_ADDON_MAGICBOLT"), ItemType.Munition)
    .setInteractionType(ItemInteraction.None);*/

addItemTemplate(ITRW_BOW_ARROW);
addItemTemplate(ITRW_CROSSBOW_BOLT);
//addItemTemplate(ITRW_MAGICBOW_ARROW);
//addItemTemplate(ITRW_MAGICCROSSBOW_BOLT);

/********************************** Bows **********************************/

addItemTemplate(
    Item.WeaponRanged("ITRW_BOW_01", Items.id("ITRW_BOW_L_01"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_BOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 10)
        .setRequirement(ItemRequirement.Dexterity, 10)
        .setMunition(ITRW_BOW_ARROW)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_BOW_02", Items.id("ITRW_BOW_L_02"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_BOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 20)
        .setRequirement(ItemRequirement.Dexterity, 20)
        .setMunition(ITRW_BOW_ARROW)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_BOW_03", Items.id("ITRW_BOW_L_03"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_BOW) 
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 30)
        .setRequirement(ItemRequirement.Dexterity, 30)
        .setMunition(ITRW_BOW_ARROW)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_BOW_04", Items.id("ITRW_BOW_L_04"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_BOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 40)
        .setRequirement(ItemRequirement.Dexterity, 40)
        .setMunition(ITRW_BOW_ARROW)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_BOW_05", Items.id("ITRW_BOW_M_01"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_BOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 50)
        .setRequirement(ItemRequirement.Dexterity, 50)
        .setMunition(ITRW_BOW_ARROW)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_BOW_06", Items.id("ITRW_BOW_M_02"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_BOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 60)
        .setRequirement(ItemRequirement.Dexterity, 60)
        .setMunition(ITRW_BOW_ARROW)
);
 
addItemTemplate(
    Item.WeaponRanged("ITRW_BOW_07", Items.id("ITRW_BOW_M_03"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_BOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 70)
        .setRequirement(ItemRequirement.Dexterity, 70)
        .setMunition(ITRW_BOW_ARROW)
);
 
addItemTemplate(
    Item.WeaponRanged("ITRW_BOW_08", Items.id("ITRW_BOW_M_04"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_BOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 80)
        .setRequirement(ItemRequirement.Dexterity, 80)
        .setMunition(ITRW_BOW_ARROW)
);
 
addItemTemplate(
    Item.WeaponRanged("ITRW_BOW_09", Items.id("ITRW_BOW_H_01"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_BOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 90)
        .setRequirement(ItemRequirement.Dexterity, 90)
        .setMunition(ITRW_BOW_ARROW)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_BOW_10", Items.id("ITRW_BOW_H_02"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_BOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 100)
        .setRequirement(ItemRequirement.Dexterity, 100)
        .setMunition(ITRW_BOW_ARROW)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_BOW_11", Items.id("ITRW_ADDON_MAGICBOW"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_BOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 110)
        .setRequirement(ItemRequirement.Dexterity, 100)
        .setMunition(ITRW_BOW_ARROW)
);

/********************************** Crossbows **********************************/

addItemTemplate(
    Item.WeaponRanged("ITRW_CROSSBOW_01", Items.id("ITRW_CROSSBOW_L_01"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_CBOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 10)
        .setRequirement(ItemRequirement.Dexterity, 10)
        .setMunition(ITRW_CROSSBOW_BOLT)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_CROSSBOW_02", Items.id("ITRW_MIL_CROSSBOW"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_CBOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 20)
        .setRequirement(ItemRequirement.Dexterity, 20)
        .setMunition(ITRW_CROSSBOW_BOLT)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_CROSSBOW_03", Items.id("ITRW_MIL_CROSSBOW"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_CBOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 30)
        .setRequirement(ItemRequirement.Dexterity, 30)
        .setMunition(ITRW_CROSSBOW_BOLT)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_CROSSBOW_04", Items.id("ITRW_CROSSBOW_L_02"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_CBOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 40)
        .setRequirement(ItemRequirement.Dexterity, 40)
        .setMunition(ITRW_CROSSBOW_BOLT)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_CROSSBOW_05", Items.id("ITRW_CROSSBOW_M_02"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_CBOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 50)
        .setRequirement(ItemRequirement.Dexterity, 50)
        .setMunition(ITRW_CROSSBOW_BOLT)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_CROSSBOW_06", Items.id("ITRW_CROSSBOW_M_01"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_CBOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 60)
        .setRequirement(ItemRequirement.Dexterity, 60)
        .setMunition(ITRW_CROSSBOW_BOLT)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_CROSSBOW_07", Items.id("ITRW_CROSSBOW_M_01"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_CBOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 70)
        .setRequirement(ItemRequirement.Dexterity, 70)
        .setMunition(ITRW_CROSSBOW_BOLT)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_CROSSBOW_08", Items.id("ITRW_CROSSBOW_M_01"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_CBOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 80)
        .setRequirement(ItemRequirement.Dexterity, 80)
        .setMunition(ITRW_CROSSBOW_BOLT)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_CROSSBOW_09", Items.id("ITRW_CROSSBOW_H_01"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_CBOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 90)
        .setRequirement(ItemRequirement.Dexterity, 90)
        .setMunition(ITRW_CROSSBOW_BOLT)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_CROSSBOW_10", Items.id("ITRW_CROSSBOW_H_02"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_CBOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 100)
        .setRequirement(ItemRequirement.Dexterity, 100)
        .setMunition(ITRW_CROSSBOW_BOLT)
);

addItemTemplate(
    Item.WeaponRanged("ITRW_CROSSBOW_11", Items.id("ITRW_ADDON_MAGICCROSSBOW"), ItemType.WeaponRanged)
        .setWeaponMode(WEAPONMODE_CBOW)
        .setInteractionType(ItemInteraction.QuickSlot)
        .setDamage(DAMAGE_POINT, 110)
        .setRequirement(ItemRequirement.Dexterity, 100)
        .setMunition(ITRW_CROSSBOW_BOLT)
);