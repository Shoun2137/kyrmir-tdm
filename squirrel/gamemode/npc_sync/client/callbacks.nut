
// Register packet callback
PacketReceive.add(PacketIDs.Npc, getNpcManager());

const NPC_POSITION_TICKINTERVAL = 200;
const NPC_STATUS_TICKINTERVAL = 50;

local NPC_POSITION_NEXTTICK = null, 
    NPC_STATUS_NEXTTICK = null;

addEventHandler("onPlayerHit", function(killerId, playerId, desc)
{
    if(killerId != -1)
    {
            local instance = getPlayerInstance(killerId);
        enable_DamageAnims(instance.find("TROLL") != null || instance.find("GOLEM") != null);
    }

    if(isPlayerDead(killerId) == false)
    {
        getNpcManager().hit(killerId, playerId, getContext(DAMAGE_CTX));
    }

    eventValue(0);
});

addEventHandler("onPlayerSpellCast", function(playerId, itemId)
{
    local focusedNpcId = getFocusNpc();

    if(playerId != heroId || focusedNpcId == -1)  
        return;
    
    local hero = getHero();
    
    if(hero.getEquipmentModule().spellCast_Check())
    {
        local template = hero.getCurrentWeapon().getTemplate();

        if(template.isInstant())
        {
            getNpcManager().hit(heroId, focusedNpcId, template.getId());
        }
    }
});

addEventHandler("onWorldChange", Npc.Manager.changeWorld.bindenv(Npc.Manager));

setTimer(function() 
{ 
    local tick = getTickCount(), npcManager = getNpcManager();

	if(tick > NPC_POSITION_NEXTTICK) 
    {         
        	npcManager.positionUpdate();
        NPC_POSITION_NEXTTICK = tick + NPC_POSITION_TICKINTERVAL;
    }

	if(tick > NPC_STATUS_NEXTTICK) 
    {         
        	npcManager.statusUpdate();
        NPC_STATUS_NEXTTICK = tick + NPC_STATUS_TICKINTERVAL;
    }

    npcManager.followTarget();
}
, 50, 0);