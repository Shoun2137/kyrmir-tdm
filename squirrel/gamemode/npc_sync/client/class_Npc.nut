
class Npc.Instance 
{
    _id = -1;

    _localId = -1;
    _stream = false;

    _spawn = false;

    _name = "null";
    _instance = "PC_HERO";
    
    _groupId = -1;

    _color = null;
    _visual = null;

    _health = 10;
    _maxHealth = 10;
    
    _collision = true;

    _position = null;
    _angle = 0;

    _targetId = -1;

    _attackMode = false;
    _attackPattern = 0;

    _attackInterval = null;
    _animationInterval = null;
    _jumpInterval = null;

    _positionChanged = false;
    _positionLastChangeTime = null;

    _lastDebugAttackTime = null;

    _attackTime = 0;
    _parade = -1;

    _obstacle = false;

    _follow = false;
    _followDistance = 150;

    _weaponSkill = null;
    _activeSpellId = -1;

    _meleeWeaponId = -1;
    _rangedWeaponId = -1;
    _helmetId = -1;
    _armorId = -1;

    _animation = null;    
    _weaponMode = 0;

    _lastBitFlag = null;

    constructor(id, name, instance)
    {   
        _lastBitFlag = null;

        _id = id;

        _localId = -1;
        _stream = false;

        _spawn = false;

        _name = name;
        _instance = instance;

        _groupId = -1;

        _color = { r = 255, g = 255, b = 255 };
        _visual = instance != "PC_HERO" ? null : { bodyModel = "Hum_Body_Naked0", bodyTxt = 9, headModel = "Hum_Head_Pony", headTxt = 18 };

        _health = 10;
        _maxHealth = 10;

        _collision = true;

        _position = { x = 0, y = 0, z = 0 };
        _angle = 0;

        _targetId = -1;

        _attackMode = false;
        _attackPattern = 0;

        _attackInterval = null;
        _animationInterval = null;
        _jumpInterval = null;
        
        _positionChanged = false;
        _positionLastChangeTime = null;
        
        _lastDebugAttackTime = null;

        _attackTime = 0;
        _parade = -1;

        _follow = false;
        _followDistance = 150;

        _weaponSkill = {};
        _activeSpellId = -1;

        _meleeWeaponId = -1;
        _rangedWeaponId = -1;
        _helmetId = -1;
        _armorId = -1;

        _animation = null;
        _weaponMode = 0;
    }

//:public
    function getId() { return _id; }

    function getLocalId() { return _localId; }

    function turnStream(stream)
    {
        _stream = stream;
    }

    function isStreaming() 
    { 
        return _localId != -1 && isPlayerDead(_localId) == false && _stream; 
    }

    function destroy()
    {
        if(_localId != -1)
        {            
            destroyNpc(_localId);
                _localId = -1;
        }
    }

    function spawn()
    {
        if(_spawn) return;

        _localId = createNpc(getTranslation(_name));

        spawnNpc(_localId);
        setPlayerInstance(_localId, _instance);

        setPlayerAngle(_localId, _angle);
        setPlayerPosition(_localId, _position.x, _position.y, _position.z);	
        
        //setPlayerOnFloor(_localId);

        setPlayerColor(_localId, _color.r, _color.g, _color.b);

        if(_visual) 
        {
            setPlayerVisual(_localId, _visual.bodyModel, _visual.bodyTxt, _visual.headModel, _visual.headTxt);
        }

        setPlayerStrength(_localId, 9999);
        setPlayerDexterity(_localId, 9999);

        setPlayerMagicLevel(_localId, 6);

        if(_armorId != -1) ::equipItem(_localId, _armorId);
        if(_helmetId  != -1) ::equipItem(_localId, _helmetId);
        if(_meleeWeaponId != -1) ::equipItem(_localId, _meleeWeaponId);
        if(_rangedWeaponId != -1) ::equipItem(_localId, _rangedWeaponId);
        if(_activeSpellId != -1)  ::equipItem(_localId, _activeSpellId);

        setPlayerMaxHealth(_localId, _maxHealth);
        setPlayerHealth(_localId, _health);   
        setPlayerMaxMana(_localId, 999999);		
        setPlayerMana(_localId, 999999);

        setPlayerCollision(_localId, _collision);

        setPlayerSkillWeapon(_localId, WEAPON_1H, getSkillWeapon(WEAPON_1H));
        setPlayerSkillWeapon(_localId, WEAPON_2H, getSkillWeapon(WEAPON_2H));
        setPlayerSkillWeapon(_localId, WEAPON_BOW, getSkillWeapon(WEAPON_BOW));
        setPlayerSkillWeapon(_localId, WEAPON_CBOW, getSkillWeapon(WEAPON_CBOW));

        _animationInterval = null;
        
        setPlayerWeaponMode(_localId, WEAPONMODE_NONE);	

        if(_weaponMode == WEAPONMODE_RAPIER) 
        {
            applyPlayerOverlay(_localId, Mds.id("HUMANS_RAPIER_ST3.MDS"));
        }

        setPlayerWeaponMode(_localId, _weaponMode);	
        
        if(_health <= 0) 
        {
            ::playAni(_localId, "S_DEAD");
        }

        _spawn = true;
    }

    function unspawn()
    {
        if(_spawn == true)
        {
            destroyNpc(_localId);
                _spawn = false;

            _stream = false;
            _localId = -1;
        }
    }

    function respawn()
    {
        unspawn();
            spawn();
    }

    function isInStream() { return _spawn; }

    function setName(name)
    {
        if(_localId != -1) setPlayerName(_localId, _name = name);
    }

    function refreshName()
    {
        if(_localId != -1) setPlayerName(_localId, getTranslation(_name));
    }

    function getName() { return _name; }

    function setInstance(instance) 
    {
        if(_localId != -1) setPlayerInstance(_localId, instance);
    }

    function getInstance() { return _instance; }

    function setGroup(groupId)
    {
        _groupId = groupId;
    }

    function getGroup() { return _groupId; }

    function setColor(r, g, b)
	{
        _color.r = r;
        _color.g = g;
        _color.b = b;
        
        if(_localId != -1) 
            setPlayerColor(_localId, r, g, b);
	}

	function getColor() { return _color; }

	function setVisual(bodyModel, bodyTxt, headModel, headTxt) 
	{
        _visual.bodyModel = bodyModel;
        _visual.bodyTxt = bodyTxt;
        _visual.headModel = headModel;
        _visual.headTxt = headTxt;

        if(_localId != -1) 
            setPlayerVisual(_localId, bodyModel, bodyTxt, headModel, headTxt);
	}

	function getVisual() { return _visual; }

	function setHealth(health)
	{
        if(health <= 0)
        {   
            _health = 0;

            if(_localId != -1) 
            {
                setPlayerHealth(_localId, _health);	
            }
            
            if(_weaponMode != WEAPONMODE_NONE) 
            {
                _attackMode = false;
                _attackPattern = 0;

                _weaponMode = WEAPONMODE_NONE;

                if(_localId != -1) 
                    ::setPlayerWeaponMode(_localId, WEAPONMODE_NONE);
            }

            _obstacle = false;

            _follow = false;
            _followDistance = 150;

            _targetId = -1;
            _animation = null;

            if(_localId != -1)  
                ::playAni(_localId, "T_DEADB");
        }    
        else 
        {
                local oldHp = _health;
            setPlayerHealth(_localId, _health = health);	

            if(oldHp == 0 && health > 0) 
            {
                respawn();

                if(_localId != -1)  
                    ::playAni(_localId, "S_RUN");
            }
        }

    	return this;	
	}
			
	function getHealth() { return _health; }

    function isAlive() { return _health > 0; }

    function setCollision(collision)
    {
        _collision = collision;
            return this;
    }

    function isCollisionEnabled() { return _collision; } 

	function setMaxHealth(maxHealth)
	{
        _maxHealth = maxHealth;

        if(_localId != -1) 
            setPlayerMaxHealth(_localId, maxHealth);

    	return this;
	}

	function getMaxHealth() { return _maxHealth; }

    function hit(killerId)
    {
        if(_localId != -1) hitPlayer(killerId, _localId);
    }

	function setPosition(x, y, z)
	{
        _position.x = x;
        _position.y = y; 
        _position.z = z;

        if(_localId != -1) 
            setPlayerPosition(_localId, x, y, z);
	}

    function getPosition() { return _position; }
    
    function isPositionChanged() { return _positionChanged; }

    function setAngle(angle)
    {
        _angle = angle;

        if(_localId != -1) 
            setPlayerAngle(_localId, angle);
    }

    function getAngle() { return _angle; }

    function setTarget(targetId) 
    {
        if(targetId == -1)
        {
            if(_follow)
            {
                if(_localId != -1) 
                    ::stopAni(_localId);

                _follow = false;
                _followDistance = 150;  
            }
            
            _attackMode = false;
        }
        
        _targetId = targetId;
    }

    function getTarget() { return _targetId; }

    function attackFist(attackTime) 
    {
        if(_animation || _follow)
        {
            if(_animation != null && _localId != -1)
                ::stopAni(_localId);
            
            _animation = null;

            _follow = false;
            _followDistance = 150;
        }

        _attackPattern = ATTACK_FRONT;

        _attackMode = true;
        _attackTime = attackTime;
    }

    function attackMelee(pattern, attackTime)
	{	
        if(_animation || _follow)
        {
            if(_animation != null && _localId != -1)
                ::stopAni(_localId);

            _animation = null;

            _follow = false;
            _followDistance = 150;  
        }

        if(_health >= 0)
        {
            _attackPattern = pattern;

            _attackMode = true;
            _attackTime = attackTime;
        }
    }

    function attackRanged(attackTime) 
    {
        if(_animation || _follow)
        {
            if(_animation != null && _localId != -1)
                ::stopAni(_localId);
                
            _animation = null;

            _follow = false;
            _followDistance = 150;  
        }
        
        if(_health >= 0)
        {
            _attackMode = true;  
            _attackTime = attackTime;
        }
    }

    function attackMagic(attackTime) 
    {
        if(_animation || _follow)
        {
            if(_localId != -1) ::stopAni(_localId);

            _animation = null;

            _follow = false;
            _followDistance = 150;  
        }
        
        if(_health >= 0)
        {
            _attackMode = true;  
            _attackTime = attackTime;
        }
    }

    function stopAttack()
    {
        _attackMode = false;
        _attackTime = 0;

        _attackPattern = 0;
    }

//:private
    function _followAnimation()
    {
        local animation = getPlayerAni(_localId);

        switch(_weaponMode)
        {
            case WEAPONMODE_NONE:
            case WEAPONMODE_FIST: 
            {
                if(animation != "S_FISTRUNL")
                {
                    ::stopAni(_localId, animation);
                    ::playAni(_localId, "S_FISTRUNL"); 
                }
            }
            break;
            case WEAPONMODE_1HS:
            case WEAPONMODE_RAPIER:
            { 
                if(animation != "S_1HRUNL") 
                {
                    ::stopAni(_localId, animation);
                    ::playAni(_localId, "S_1HRUNL"); 
                }
            }
            break;
            case WEAPONMODE_2HS: 
            {
                if(animation != "S_2HRUNL")
                {
                    ::stopAni(_localId, animation);
                    ::playAni(_localId, "S_2HRUNL"); 
                }
            }
            break;
            case WEAPONMODE_BOW: 
            {
                if(animation != "S_BOWRUNL")
                {
                    ::stopAni(_localId, animation);
                    ::playAni(_localId, "S_BOWRUNL"); 
                }
            }
            break;
            case WEAPONMODE_CBOW: 
            {
                if(animation != "S_CBOWRUNL")
                {
                    ::stopAni(_localId, animation);
                    ::playAni(_localId, "S_CBOWRUNL"); 
                }
            }
            break;
            case WEAPONMODE_MAG: 
            {
                if(animation != "S_MAGRUNL")
                {
                    ::stopAni(_localId, animation);
                    ::playAni(_localId, "S_MAGRUNL"); 
                }
            }
            break;
        }
    }

    function _followAnimation_Stop()
    {
        switch(_weaponMode)
        {
            case WEAPONMODE_NONE:
            case WEAPONMODE_FIST: 
                ::stopAni(_localId, "S_FISTRUNL");
            break;
            case WEAPONMODE_1HS:
            case WEAPONMODE_RAPIER:
                ::stopAni(_localId, "S_1HRUNL");
            break;
            case WEAPONMODE_2HS: 
                ::stopAni(_localId, "S_2HRUNL");
            break;
            case WEAPONMODE_BOW: 
                ::stopAni(_localId, "S_BOWRUNL");
            break;
            case WEAPONMODE_CBOW: 
                ::stopAni(_localId, "S_CBOWRUNL");
            break;
            case WEAPONMODE_MAG: 
                ::stopAni(_localId, "S_MAGRUNL");
            break;
        }

        _follow = false;
    }

//:public
    function follow(distance)
    {
        if(_follow == false && _targetId != -1)
        {
            _follow = true;
            _followDistance = distance;

            _attackMode = false;
            _attackTime = 0;

            _attackPattern = 0;
        }
    }

    function isObstacle() { return _obstacle; }

    function isFollowing() { return _follow; }
    
    function parade(paradeType)
    {
        if(_animation)
        {
            if(_localId != -1) ::stopAni(_localId);
                _animation = null;
        }

        _parade = paradeType;
    }

    function setSkillWeapon(skillType, skill) 
    {
        _weaponSkill[skillType] <- skill;
            return this;
    }

    function getSkillWeapon(skillType) { return skillType in _weaponSkill ? _weaponSkill[skillType] : 0; }

    function setActiveSpell(spellId)
    {
        _activeSpellId = spellId;
        
        if(_localId != -1) 
            ::equipItem(_localId, spellId);

        return this;
    }
    
    function getActiveSpell() { return _activeSpellId; }

    function equipItem(itemType, itemId)
    {
        switch(itemType)
        {
            case NPC_ITEM_IDS.MELEE_WEAPON: _meleeWeaponId = itemId; break;
            case NPC_ITEM_IDS.RANGED_WEAPON: _rangedWeaponId = itemId; break;
            case NPC_ITEM_IDS.ARMOR: _armorId = itemId; break;
            case NPC_ITEM_IDS.HELMET: _helmetId = itemId; break;
        }
        
        if(_localId != -1) ::equipItem(_localId, itemId);

        return this;
    }

    function equipMeleeWeapon(meleeWeaponId)
    {
        _meleeWeaponId = meleeWeaponId;

        if(_localId != -1) 
            ::equipItem(_localId, meleeWeaponId);

        return this;
    }

    function getMeleeWeapon() { return _meleeWeaponId; }

    function equipRangedWeapon(rangedWeaponId)
    {
        _rangedWeaponId = rangedWeaponId;

        if(_localId != -1) 
            ::equipItem(_localId, rangedWeaponId);

        return this;
    }

    function getRangedWeapon() { return _rangedWeaponId; }

    function equipArmor(armorId)
    {
        _armorId = armorId;

        if(_localId != -1) 
            ::equipItem(_localId, armorId);

        return this;
    }

    function getArmor() { return _armorId; }

    function equipHelmet(helmetId)
    {
        _helmetId = helmetId;

        if(_localId != -1) 
            ::equipItem(_localId, helmetId);

        return this;
    }

    function getHelmet() { return _helmetId; }

    function unequipItem(itemId)
    {
        switch(itemId)
        {
            case _meleeWeaponId: _meleeWeaponId = -1; break;
            case _rangedWeaponId: _rangedWeaponId = -1;  break;
            case _armorId: _armorId = -1;  break;
            case _helmetId: _helmetId = -1;  break;
        }

        ::unequipItem(_localId, itemId);
            return this;
    }

    function playAni(animation)
    {
        if(_follow)
        {
            _follow = false;
            _followDistance = 150;  
        }
        
        _attackMode = false;
        _attackTime = 0;

        _attackPattern = 0;

        if(_animation != null && _health > 0)
        {
            if(_localId != -1)
            {
                ::stopAni(_localId);
                ::playAni(_localId, animation);
            }

            _animationInterval = getTickCount() + 3000;		
        }

        _animation = animation;
    }

    function stopAni() 
    {
        if(_animation != null && _localId != -1)
            ::stopAni(_localId);

        _animation = null;
    }

    function getLastAni() { return _animation; }

    function setWeaponMode(weaponMode)
    {
        if(_localId != -1)
        {
            if(_weaponMode == WEAPONMODE_RAPIER) 
            {
                removePlayerOverlay(_localId, Mds.id("HUMANS_RAPIER_ST3.MDS"));
            }

            setPlayerWeaponMode(_localId, WEAPONMODE_NONE);

            if(weaponMode == WEAPONMODE_RAPIER) 
            {
                applyPlayerOverlay(_localId, Mds.id("HUMANS_RAPIER_ST3.MDS"));
                    setPlayerWeaponMode(_localId, WEAPONMODE_1HS);
            }
            else 
            {
                setPlayerWeaponMode(_localId, weaponMode);
            }
        }
        
        _weaponMode = weaponMode;

        return this;
    }

    function getWeaponMode() { return _weaponMode; }

    function setLastBitFlag(bitFlag)
    {
        _lastBitFlag = bitFlag;
    }

    function getLastBitFlag() { return _lastBitFlag; }

//:private
    function _forceRunAttack(tick)
    {
        if(_attackInterval > tick) return;

        switch(_weaponMode)
        {
            case WEAPONMODE_FIST: 
                ::playAni(_localId, "T_FISTATTACKMOVE");
            break;
            case WEAPONMODE_1HS:
                //::playAni(_localId, "T_1HATTACKMOVE");
                return;
            break;
            case WEAPONMODE_2HS:
                //::playAni(_localId, "T_2HATTACKMOVE");
                return;
            break;
        }
            
            hitPlayer(_localId, _targetId);
        _attackInterval = tick + 2000;		
    }

    function _isInForceAttackRun() 
    { 
        return getPlayerAni(_localId).find("ATTACKMOVE") != null; 
    }

    function _forceAttack(tick)
    {
        if(_attackInterval > tick) return;

        switch(_weaponMode)
        {
            case WEAPONMODE_FIST: 
            {
                if(_instance.find("TROLL") != null || _instance.find("GOLEM") != null)
                {
                    ::playAni(_localId, "T_FISTATTACKMOVE");
                        hitPlayer(_localId, _targetId);
                }
                else attackPlayer(_localId, _targetId, ATTACK_FRONT);
            }
            break;
            case WEAPONMODE_1HS:
            case WEAPONMODE_2HS:
            case WEAPONMODE_RAPIER:
            {
                attackPlayer(_localId, _targetId, _attackPattern);
                print("NIE WIEM " + _attackPattern);
                print(ATTACK_SWORD_FRONT)
                if(_attackPattern != ATTACK_SWORD_FRONT)
                {
                    _attackPattern = (_attackPattern == ATTACK_SWORD_LEFT ? ATTACK_SWORD_RIGHT : ATTACK_SWORD_LEFT);
                }
            } 
            break;
            case WEAPONMODE_BOW:
            case WEAPONMODE_CBOW:      
                attackPlayerRanged(_localId, _targetId);
            break;
            case WEAPONMODE_MAG:
                    ::playAni(_localId, "T_FIBSHOOT_2_STAND");
                attackPlayerMagic(_localId, _targetId, _activeSpellId);
            break;
        }
        
        _attackInterval = tick + _attackTime;		
    }

    function _forceParade()
    {
        if(_parade != -1)
        {                        
            switch(_weaponMode)
            {
                case WEAPONMODE_FIST:
                    ::playAni(_localId, "T_FISTPARADEJUMPB");
                break;
                case WEAPONMODE_1HS:
                case WEAPONMODE_RAPIER:
                {
                    switch(_parade)
                    {
                        case PARADE.JUMPB: ::playAni(_localId, "T_1HPARADEJUMPB"); break;
                        case PARADE.BLOCK_0: ::playAni(_localId, "T_1HPARADE_0"); break;
                        case PARADE.BLOCK_1: ::playAni(_localId, "T_1HPARADE_0_A2"); break;
                        case PARADE.BLOCK_2: ::playAni(_localId, "T_1HPARADE_0_A3"); break;
                    }
                } 
                break;
                case WEAPONMODE_2HS:
                {
                    switch(_parade)
                    {
                        case PARADE.JUMPB: ::playAni(_localId, "T_2HPARADEJUMPB"); break;
                        case PARADE.BLOCK_0: ::playAni(_localId, "T_2HPARADE_0"); break;
                        case PARADE.BLOCK_1: ::playAni(_localId, "T_2HPARADE_0_A2"); break;
                        case PARADE.BLOCK_2: ::playAni(_localId, "T_2HPARADE_0_A3"); break;
                    }
                } 
                break;
            }
            
            _parade = -1;
        } 

        return getPlayerAni(_localId).find("PARADE") != null; 
    }

//:public
    function followTarget()
    {           
        local tick = getTickCount();

        if(_spawn)
        {
            if(_isInForceAttackRun()) return;

            if(_attackMode == false && _follow == false && _animationInterval < tick)
            {
                if(_animation != null && _health > 0)
                {
                    local currentAnimation = getPlayerAni(_localId);
                    
                    if(currentAnimation != _animation)
                    { 
                        ::playAni(_localId, _animation);
                    }
                }

                _animationInterval = tick + 3500;		
            }
            
            local position = getPlayerPosition(_localId);

            if(_positionLastChangeTime == null 
                || (tick - _positionLastChangeTime) > 1000)
            {
                _positionChanged = getDistance3d(_position.x, _position.y, _position.z, position.x, position.y, position.z) >= 25;

                _position.x = position.x;
                _position.y = position.y;
                _position.z = position.z;

                _positionLastChangeTime = tick;
            }

            if(_targetId != -1)
            {                
                local tPosition = getPlayerPosition(_targetId);
                if(tPosition == null) return;

                if(_follow)
                {
                    if(_jumpInterval == null || tick >= _jumpInterval)
                    {
                            _followAnimation();
                        _jumpInterval = null;
                    }

                    if(_lastDebugAttackTime == null)
                    {
                        if(_positionChanged == false)
                        {
                            _lastDebugAttackTime = tick;
                        }
                        else 
                        {
                            _lastDebugAttackTime = null;
                        }
                    }
                    
                    local distance3d = getDistance3d(position.x, position.y, position.z, tPosition.x, tPosition.y, tPosition.z);
                    
                    if(_lastDebugAttackTime != null && (tick - _lastDebugAttackTime) > 3000)
                    {
                        if(distance3d <= 700)
                        {
                            if(distance3d <= _followDistance)
                            {                            
                                _followAnimation_Stop();
                                
                                _lastDebugAttackTime = null;     
                                _jumpInterval = null;

                                _lastBitFlag = null;
                            }

                            _forceRunAttack(tick);
                        }
                        else 
                        {
                            if(_jumpInterval == null)
                            {
                                    local angle = getPlayerAngle(_localId) / 180 * PI, position = getPlayerPosition(_localId);
                                setPlayerPosition(_localId, position.x + sin(angle) * 160, position.y + 85, position.z + cos(angle) * 160); 
                                
                                _jumpInterval = tick + 500;
                            }
                        }

                        _lastDebugAttackTime = null;
                    }
                    else           
                    {                        
                        if(getDistance2d(position.x, position.z, tPosition.x, tPosition.z) <= _followDistance)
                        {
                            ::stopAni(_localId);
                                
                            _follow = false;
                            
                            _lastDebugAttackTime = null;
                            _jumpInterval = null;
                            
                            _lastBitFlag = null;

                            if(distance3d <= (_followDistance + _followDistance * 0.50))
                            {
                                _forceRunAttack(tick);
                            }
                        }
                    }
                }
                
                local vectorAngle = getVectorAngle(position.x, position.z, tPosition.x, tPosition.z);
                if(abs(_angle - vectorAngle) > 2) setPlayerAngle(_localId, _angle = vectorAngle);
                
                if(_targetId == heroId)
                {
                        local vob = Vob(getPlayerPtr(_localId)), targetVob = Vob(getPlayerPtr(_targetId));
                    _obstacle = GameWorld.traceRayFirstHit(position, (targetVob.bbox3dWorld.center - vob.bbox3dWorld.center) * 0.95, TRACERAY_VOB_IGNORE_NO_CD_DYN) != null;
                }

                if(_attackMode)
			    {	                    
                    if(_forceParade() == false) _forceAttack(tick);
                }	
            }
            else
            {
                if(isStreaming() == false) return;

                if(_animation != null && _animation.find("WALKL") != null)
                {
                    local angle = getPlayerAngle(_localId), radians = angle / 180 * PI;

                    local vecTarget = Vec3(
                        position.x + sin(radians) * 350,
                        position.y + 50,
                        position.z + cos(radians) * 350
                    );
                    
                        local vob = Vob(getPlayerPtr(_localId));
                    _obstacle = GameWorld.traceRayFirstHit(position, (vecTarget - vob.bbox3dWorld.center) * 0.95, 0) != null;
                    
                    if(_obstacle) setPlayerAngle(_localId, angle + 10);
                }
            }
        }
    }
}