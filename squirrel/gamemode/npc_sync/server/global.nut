
// Attack constants 
const ATTACK_FRONT = 0;
const ATTACK_SWORD_LEFT = 1;
const ATTACK_SWORD_RIGHT = 2;
const ATTACK_SWORD_FRONT = 3;

// Others
const NPC_PACKET_SIZE = 20;