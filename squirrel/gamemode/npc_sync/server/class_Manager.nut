
local playerNumber = 0;

class Npc.Manager extends Npc.GridManager
{
    static _syncList = {};

//:protected
    static function _writeDataIntoPacket(packet, npc)
    {
        local armor = npc.getArmor();
        local helmet = npc.getHelmet();
        local melee = npc.getMeleeWeapon();
        local ranged = npc.getRangedWeapon();

        local flags = 0;

        if(armor != -1) flags = flags | 1;
        if(helmet != -1) flags = flags | 2;
        if(melee != -1) flags = flags | 4;
        if(ranged != -1) flags = flags | 8;

        packet.writeInt8(flags);

        local instance = npc.getInstance();

        packet.writeInt16(npc.getId()); 
        packet.writeString(npc.getName()); 
        packet.writeString(instance);
        packet.writeInt16(npc.getGroup())
        packet.writeInt32(npc.getMaxHealth());
        packet.writeBool(npc.isCollisionEnabled())

        packet.writeInt8(npc.getSkillWeapon(WEAPON_1H));
        packet.writeInt8(npc.getSkillWeapon(WEAPON_2H));
        packet.writeInt8(npc.getSkillWeapon(WEAPON_BOW));
        packet.writeInt8(npc.getSkillWeapon(WEAPON_CBOW));

        local color = npc.getColor();

        packet.writeInt8(color.r);
        packet.writeInt8(color.g);
        packet.writeInt8(color.b);

        if(instance == "PC_HERO")
        {
            local visual = npc.getVisual();

            packet.writeString(visual.bodyModel); 
            packet.writeInt16(visual.bodyTxt); 
            packet.writeString(visual.headModel); 
            packet.writeInt16(visual.headTxt); 	
        }
            
        if(armor != -1) packet.writeInt16(armor);
        if(helmet != -1) packet.writeInt16(helmet);
        if(melee != -1) packet.writeInt16(melee);
        if(ranged != -1) packet.writeInt16(ranged);     
    }

//:public
    static function init()
    {
        print("+ Npc.Manager: We have created " + _syncList.len() + " npcs!")
    }

    static function playerJoin(playerId)
    {
        _syncList[playerId] <- Npc.Player(playerId);
            playerNumber += 1;

        local npcNumber = _syncList.len() - playerNumber; 

        if(npcNumber > 0)
        {
            local packet = null;
            
            local maxSlots = getMaxSlots(), 
                chunkCalc = 0;

            local lastPosition = npcNumber - 1;
            
            foreach(entityId, npc in _syncList)
            {
                if(entityId >= maxSlots)
                {          
                    if((chunkCalc % NPC_PACKET_SIZE) == 0)
                    {
                        if(packet != null)
                        {
                            packet.send(playerId, RELIABLE_ORDERED);
                            packet = null;
                        }

                        packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.NPC_LIST);

                        local chunkSize = npcNumber - chunkCalc;

                        if(chunkSize < NPC_PACKET_SIZE)
                            packet.writeInt16(chunkSize);
                        else 
                            packet.writeInt16(NPC_PACKET_SIZE);
                    }

                    _writeDataIntoPacket(packet, npc);
                        chunkCalc++;
                }
            }

            if(packet != null) packet.send(playerId, RELIABLE_ORDERED);
        }
    }

    static function playerDisconnect(playerId)
    {
        local player = _syncList[playerId];
        
        local cell = player.getCurrentCell();
        if(cell) cell.removePlayerVisitor(player);

        delete _syncList[playerId];
            playerNumber -= 1;

        foreach(entity in _syncList)
        {
            if(entity instanceof Npc.Instance)
            {
                entity.removeFromStream(playerId);
            }
        }
    }
    
    static function playerChangeWorld(playerId, world)
    {
        local player = _syncList[playerId];
        
        local cell = player.getCurrentCell();
        if(cell) cell.removePlayerVisitor(player);
        
        player.setCurrentCell(null);
        
        foreach(entity in _syncList)
        {
            if(entity instanceof Npc.Instance)
            {
                entity.removeFromStream(playerId);
            }
        }
    }
    
    static function createNpc(name, instance, world, virtualWorld)
    {
        local npc = Npc.Instance(name, instance);

        _syncList[npc.getId()] <- npc;

        if(world != null) 
            npc.setWorld(world);

        if(virtualWorld != -1) 
            npc.setVirtualWorld(virtualWorld);

        local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.CREATE_NPC);
            _writeDataIntoPacket(packet, npc);
        packet.sendToAll(RELIABLE_ORDERED);   

        return npc;
    }

    static function destroyNpc(npcId)
    {
        if(npcId in _syncList)
        { 
            local npc = _syncList[npcId];

            npc.getCurrentCell().removeVisitor(npc);
            npc.destroyNpc();

            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.DESTROY_NPC);
                packet.writeInt16(npc.getId());
            packet.sendToAll(RELIABLE_ORDERED);   
            
            delete _syncList[npcId];
        }
    }

    static function getSyncEntityById(entityId) { return entityId in _syncList ? _syncList[entityId] : null; }

    static function receivePositionUpdate(playerId, packet)
    {
        local packetSize = packet.readInt16(), updatePack = {};

        for(local i = 0; i < packetSize; i++)
        {
            local npcId = packet.readInt16();

            local x = packet.readInt32();
            local y = packet.readInt32();
            local z = packet.readInt32();

            local angle = packet.readInt16();

            if((npcId in _syncList) == false) continue;

            local npc = _syncList[npcId], streamerId = npc.getStreamerId();
            
            if(streamerId != playerId) 
                continue;

            local stream = npc.getStreamList(), npcCell = npc.getCurrentCell();

            foreach(playerId in stream)
            {     
                if(streamerId != playerId)
                {
                    if(_syncList[streamerId].getCurrentCell() != npcCell)
                    {
                        if(_syncList[playerId].getCurrentCell() == npcCell) 
                            npc.setStreamer(playerId);
                    }

                    if(playerId in updatePack)
                    {
                        updatePack[playerId].push([ npcId, x, y, z ]);
                    }
                    else 
                    {
                        updatePack[playerId] <- [];
                        updatePack[playerId].push([ npcId, x, y, z ]);
                    }
                }
            }

            npc.receivePositionUpdate(x, y, z, angle);
        }

        foreach(playerId, data in updatePack)
        {
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.POSITION_C);
            
                packet.writeInt16(data.len());

            foreach(dataRow in data) 
            {
                packet.writeInt16(dataRow[0]);
                packet.writeFloat(dataRow[1]);
                packet.writeFloat(dataRow[2]);
                packet.writeFloat(dataRow[3]);
            }

            packet.send(playerId, UNRELIABLE_SEQUENCED);
        }  
    }

    static function receiveStatusUpdate(playerId, packet)
    {
        local npcNumber = packet.readInt16();
        
        for(local i = 0; i < npcNumber; i++)
        {
            local npcId = packet.readInt16(), bitFlag = packet.readInt8();

            if((npcId in _syncList) == false) 
                continue;

            _syncList[npcId].receiveStatusUpdate(bitFlag);
        }
    }

    static function receiveHit(packet)
    {
        local killerId = packet.readInt16(), targetId = packet.readInt16();

        if(targetId >= getMaxSlots())
        {
            if(_syncList[targetId].isImmortal()) 
                return;
        
            callEvent("Npc.onHit", killerId, targetId, packet.readInt16());
        }
        else 
            callEvent("Npc.onHit", killerId, targetId, -1);
    }

    static function updateCell(visitor, oldCell, newCell)
    {
        if(visitor instanceof Npc.Instance)
        {
            if(oldCell)
            {                
                local stream = visitor.getStreamList(), 
                    removeList = [];

                foreach(playerId in stream)
                {
                    local playerCell = _syncList[playerId].getCurrentCell();
                    
                    if(playerCell)
                    {
                        if(newCell != null)
                        {
                            if(!playerCell.isCellInRange(newCell.getColumn(), newCell.getRow()))
                                removeList.push(playerId);
                        }
                        else 
                            removeList.push(playerId);
                    }
                }  

                foreach(playerId in removeList)
                {
                    visitor.removeFromStream(playerId);
                }
            }

            if(newCell)
            {
                if(visitor.isSpawned() == false) return;

                local inRangeCells = getGrid(visitor.getWorld()).getInRangeCells(newCell.getColumn(), newCell.getRow());

                foreach(cell in inRangeCells)
                {
                    local playerVisitorsList = cell.getPlayerVisitors();
                    if(playerVisitorsList == null) continue;

                    foreach(cellVisitor in playerVisitorsList)
                    {
                        local cellVisitorId = cellVisitor.getId();

                        if(!visitor.isPlayerInStream(cellVisitorId) && visitor.getVirtualWorld() == getPlayerVirtualWorld(cellVisitorId))
                        {
                            visitor.addToStream(cellVisitorId);
                        }
                    }
                }
            }
        }
        else 
        {
            local oldStreamList = [], newStreamList = [];

            if(oldCell)
            {
                local inRangeCells = getGrid(getPlayerWorld(visitor.getId())).getInRangeCells(oldCell.getColumn(), oldCell.getRow());
                
                foreach(cell in inRangeCells)
                {
                    local visitorsList = cell.getVisitors();
                    if(visitorsList == null) continue;

                    foreach(cellVisitor in visitorsList)
                    {
                        oldStreamList.push(cellVisitor);
                    }
                }
            }

            if(newCell)
            {
                local inRangeCells = getGrid(getPlayerWorld(visitor.getId())).getInRangeCells(newCell.getColumn(), newCell.getRow());
                
                foreach(cell in inRangeCells)
                {
                    local visitorsList = cell.getVisitors();
                    if(visitorsList == null) continue;

                    foreach(cellVisitor in visitorsList)
                    {
                        local visitorId = visitor.getId();

                        if(oldStreamList.find(cellVisitor) == null && cellVisitor.isSpawned() 
                            && cellVisitor.getVirtualWorld() == getPlayerVirtualWorld(visitorId))
                        {
                            cellVisitor.addToStream(visitorId);
                        }

                        newStreamList.push(cellVisitor);
                    }
                }
            }

            foreach(cellVisitor in oldStreamList)
            {
                if(newStreamList.find(cellVisitor) == null)
                {
                    cellVisitor.removeFromStream(visitor.getId());
                }
            }
        }
    }

    static function packetRead(playerId, packet)
    {
        local packetType = packet.readInt8();
        
        switch(packetType)
        {
            case LIST_PACKET_NPC_IDS.POSITION_C: receivePositionUpdate(playerId, packet); break;	
            case LIST_PACKET_NPC_IDS.FOLLOW: receiveStatusUpdate(playerId, packet); break;	
            case LIST_PACKET_NPC_IDS.HIT_C: receiveHit(packet); break;	
        }
    } 
}

getNpcManager <- @() Npc.Manager;

// Npc 

createNpc <- @(name, instance, world = null, virtualWorld = -1) Npc.Manager.createNpc(name, instance, world, virtualWorld);
destroyNpc <- @(npcId) Npc.Manager.destroyNpc(npcId);

getSyncEntity <- @(entityId) Npc.Manager.getSyncEntityById(entityId);