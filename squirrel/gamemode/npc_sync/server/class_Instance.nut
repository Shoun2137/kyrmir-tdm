
class Npc.Instance extends Npc.Model
{
    _spawn = false;

    constructor(name, instance)
    {
            _spawn = false;
        base.constructor(name, instance);
    }

#public:
    function setCurrentCell(currentCell)
    {
        _currentCell = currentCell;
    }

    function getCurrentCell() { return _currentCell; }

    function setStreamer(streamerId)
    { 
        if(_streamerId != -1)
        {
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.STREAMER);
                packet.writeInt16(_id);    
            packet.send(_streamerId, RELIABLE_ORDERED);
        }

        _streamerId = streamerId;

        if(streamerId != -1)
        {
            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.STREAMER);
                packet.writeInt16(_id);    
            packet.send(streamerId, RELIABLE_ORDERED);
        }
    }

    function getStreamerId() { return _streamerId; } 

    function addToStream(playerId)
    {
        _streamList.push(playerId);

        local flags = 0;

        if(_animation) flags = flags | 1;
        if(_targetId != -1) flags = flags | 2;
        if(_attackMode) flags = flags | 4;
        if(_follow) flags = flags | 8;

        local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.ADD_TO_STREAM);

        packet.writeInt16(_id);    
        packet.writeInt16(flags);

        packet.writeInt32(_health);
        
        packet.writeInt8(_weaponMode);
        packet.writeInt16(_activeSpellId);

        packet.writeInt16(_angle);
        
        packet.writeFloat(_position.x);
        packet.writeFloat(_position.y);
        packet.writeFloat(_position.z);

        if(_animation) packet.writeString(_animation);
        if(_targetId != -1) packet.writeInt16(_targetId);

        if(_attackMode)
        {
            packet.writeInt16(_attackTime);
            packet.writeInt8(_attackPattern);
        }

        if(_follow) packet.writeInt16(_followDistance);
        
        if(_streamerId == -1) 
        {
            _streamerId = playerId;
            packet.writeBool(true);
        }
        else 
            packet.writeBool(false);

        packet.send(playerId, RELIABLE_ORDERED);
    }

    function removeFromStream(playerId)
    {
        local id = _streamList.find(playerId);        

        if(id != null) 
        {
            _streamList.remove(id);

            local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.REMOVE_FROM_STREAM);
                packet.writeInt16(_id);    
            packet.send(playerId, RELIABLE_ORDERED);
            
            if(_streamerId == playerId) 
            {
                _streamerId = -1;

                if(_streamList.len() > 0) 
                {
                    local streamerId = _streamList[0];

                    local packet = Packet(PacketIDs.Npc, LIST_PACKET_NPC_IDS.STREAMER);
                        packet.writeInt16(_id);    
                    packet.send(_streamerId = streamerId, RELIABLE_ORDERED);
                }
            }
        }
    }

    function isPlayerInStream(playerId)
    {
        return _streamList.find(playerId) != null ? true : false;
    }

    function getStreamList() { return _streamList; }

    function spawn() 
    {
        _spawn = true;
    }

    function unspawn()
    {
        _spawn = false;

        // REMOVE STREAMER
        // CLEAR STREAM LIST
        // REMOVE FROM CELL
    }

    function isSpawned() { return _spawn; }

    function receivePositionUpdate(x, y, z, angle)
    {
        _position.x = x;
        _position.y = y;
        _position.z = z;

        _angle = angle;
    }

    function receiveStatusUpdate(status)
    {
        _follow = (status & 1) == 1;;
        _obstacle = (status & 2) == 2;
    }
}