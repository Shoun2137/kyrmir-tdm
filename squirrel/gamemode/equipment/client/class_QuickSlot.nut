
class Equipment.QuickSlot
{
    static _slotList = {};

//:public
    static function assignItemToSlot(slotId, item)
    {
        if(slotId >= 0 && slotId <= 9) 
        {
                _slotList[slotId] <- item;
	    	callEvent("Equipment.onQuickSlotUpdate", slotId, item);
        }
    } 

    static function freeSlotByItem(item)
    {
        foreach(index, listItem in _slotList)
        {
            if(listItem != item) continue;

                delete _slotList[index];
            callEvent("Equipment.onQuickSlotUpdate", index, null);
            
            break;
        }
    }

    static function getItemBySlot(slotId) { return slotId in _slotList ? _slotList[slotId] : null; }

    static function isItemOnSlotList(item)
    {
        foreach(listItem in _slotList)
        {   
            if(listItem == item)
                return true;
        }

        return false;  
    }

    static function isSlotFree(slotId)
    {
        if(slotId < 0 || slotId > 9) 
            return false;
        
        return !(slotId in _slotList);
    }

    static function getFreeSlot()
    {   
        for(local id = 0; id <= 10; id++)
        {
            local slotId = SLOT_ORDER[id];

            if(slotId in _slotList == false)
                return slotId;
        }

        return 0;
    }

    static function getSlots() { return _slotList; }
}

getSlotManager <- @() Equipment.QuickSlot;

// Equipment - QuickSlot
getItemBySlot <- @(slotId) Equipment.QuickSlot.getItemBySlot(slotId);
