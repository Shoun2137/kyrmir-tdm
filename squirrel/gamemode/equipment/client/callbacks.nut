
// Register packet callback
PacketReceive.add(PacketIDs.Equipment, getEquipmentManager());

addEventHandler("onPlayerDead", function(playerId)
{       
    if(playerId != heroId) 
        return;

    getHero().getEquipmentModule().heroDead();
});

addEventHandler("onPlayerShoot", function(playerId, itemId)
{       
    if(playerId != heroId) 
        return;

    try 
    {
        local module = getHero().getEquipmentModule();

        if(module.shoot_Check())
        {
            module.shoot();
        }
        else 
        {
            GUI.GameText(GUI.PositionPr(50, 50), "DEBUG_YOUR_WEAPON", "FONT_DEFAULT", 1500.0, true);               
                cancelEvent();
        }
    }
    catch(error)
    {
        print("onPlayerShoot: " + error);
    }
});

addEventHandler("onPlayerSpellCast", function(playerId, itemId)
{               
    if(playerId != heroId) 
        return;

    try 
    {
        local module = getHero().getEquipmentModule(), castCheck = module.spellCast_Check();

        if(castCheck == true) 
        {
            module.spellCast();
        }
        else 
            if(castCheck == false) 
            {
                GUI.GameText(GUI.PositionPr(50, 50), "CHECK_YOUR_MANA", "FONT_DEFAULT", 1500.0, true);               
                    cancelEvent();
            }
            else 
            {
                setPlayerWeaponMode(heroId, WEAPONMODE_NONE);
            }
    }
    catch(error)
    {
        print("onPlayerSpellCast: " + error);
    }
});

addEventHandler("onPlayerChangeWeaponMode", function(playerId, oldWeaponMode, newWeaponMode)
{
        if(playerId != heroId) return;
    getHero().getEquipmentModule().heroChangeWeaponMode(newWeaponMode);
});