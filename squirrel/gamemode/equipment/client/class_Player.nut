
class Equipment.Player
{
    _equipped = null;
    _itemList = null;

    _currentWeapon = null;
    _lockChange = false;

    constructor()
    {
        _equipped = {};
        _itemList = [];

        _currentWeapon = null;
        _lockChange = false;
    }

//:private
    function _resetContext()
    {
        setContext(EQUIPMENT_CTX, -1);
        setContext(DAMAGE_CTX, -1);
    }

    function _canDrawWeapon(itemId)
    {
        return isItemEquipped(itemId) && isPlayerInWater(heroId) == false && isPlayerDead(heroId) == false && _lockChange == false;
    }

    function _canEquipPhysicaly(template)
    {
        return template.isSpell() == false && template.isUsable() == false;
    }

//:public
    function server_equipItem(itemId, slotId)
    {
        local item = hasItem(itemId);
        if(item == null) return;

        local template = item.getTemplate();

        if(slotId != -1)
        {
            local quickSlot = getSlotManager();

            if(_canEquipPhysicaly(template))
            {
                local typeId = template.getType();

                local prevItem = getEquippedByType(typeId);
                if(prevItem != null) quickSlot.freeSlotByItem(prevItem);

                    _equipped[typeId] <- item;
                _equipItem(heroId, template.getVisualId());
            }

            quickSlot.assignItemToSlot(slotId, item);
        }
        else
        {
                _equipped[template.getType()] <- item;
            _equipItem(heroId, template.getVisualId());
        }
    }

    function server_unequipItem(itemId)
    {
        if(isItemEquipped(itemId) == false)
            return;

        local item = hasItem(itemId), template = item.getTemplate();

        if(template.isQuickSlotPossible())
        {
            getSlotManager().freeSlotByItem(item);
        }

        if(_canEquipPhysicaly(template))
        {
            _unequipItem(heroId, template.getVisualId());
                delete _equipped[template.getType()];
        }
    }

    function server_useItem(itemId)
    {
        local item = hasItem(itemId);
        if(item == null) return;

        local template = item.getTemplate();
        if(template.isUsable() == false) return;

        local diff = item.getAmount() - 1;

        if(diff > 0)
            item.setAmount(diff);
        else
        {
            if(template.isQuickSlotPossible())
            {
                getSlotManager().freeSlotByItem(item);
            }

            _itemList.remove(_itemList.find(item));
        }

        _useItem(heroId, template.getVisualId());
    }

    function server_giveItem(itemId, amount)
    {
        local item = hasItem(itemId);

        if(item != null)
            item.setAmount(item.getAmount() + amount);
        else
        {
            local template = getItemTemplateById(itemId);
            if(template != null) _itemList.push(Equipment.Item(template, amount));
        }

        callEvent("Equipment.onHeroGiveItem", itemId, amount);
    }

    function server_removeItem(itemId, amount)
    {
        local item = hasItem(itemId);

        if(item != null)
        {
            local diff = item.getAmount() - amount;

            if(diff > 0)
                item.setAmount(diff);
            else
            {
                if(item.getTemplate().isQuickSlotPossible())
                {
                    getSlotManager().freeSlotByItem(item);
                }

                _itemList.remove(_itemList.find(item));
            }

            callEvent("Equipment.onHeroRemoveItem", itemId, amount);
        }
    }

    function server_setInventory(itemList)
    {
        foreach(data in itemList)
        {
            local itemId = data.itemId, amount = data.amount,
                slotId = data.slotId;

            local item = hasItem(itemId);

            if(item != null)
                item.setAmount(item.getAmount() + amount);
            else
            {
                local template = getItemTemplateById(itemId);
                if(template != null) _itemList.push(Equipment.Item(template, amount));
            }

            if(slotId != NULL_QUICK_SLOT) server_equipItem(itemId, slotId);
        }
    }

    function server_clear()
    {
        _equipped.clear();

        foreach(item in _itemList)
        {
            if(item.getTemplate().isQuickSlotPossible())
            {
                getSlotManager().freeSlotByItem(item);
            }
        }

        _itemList.clear();
    }

    function equipItem(itemId, slotId = -1)
    {
        local item = hasItem(itemId);
        if(item == null) return false;

        local template = item.getTemplate(), isEquippable = template.isEquippable();

        if(isEquippable == false && template.isQuickSlotPossible() == false)
            return false;

        if(isEquippable && template.canBeEquippedByHero() == false)
            return false;

        local typeId = template.getType(), prevItem = getEquippedByType(typeId);

        if(_currentWeapon == prevItem && getPlayerWeaponMode(heroId) != WEAPONMODE_NONE)
            return false;

        local quickSlot = getSlotManager();

        if(prevItem)
        {
                delete _equipped[typeId];
            _unequipItem(heroId, prevItem.getTemplate().getVisualId());

            quickSlot.freeSlotByItem(prevItem);
        }

        if(template.isQuickSlotPossible() == false)
        {
                _equipped[typeId] <- item;
            _equipItem(heroId, template.getVisualId());

            slotId = -1;
        }
        else
        {
            quickSlot.freeSlotByItem(item);

            if(slotId == -1) slotId = quickSlot.getFreeSlot()
            prevItem = quickSlot.getItemBySlot(slotId);

            if(prevItem)
            {
                local prevItem_template = prevItem.getTemplate();

                if(_canEquipPhysicaly(prevItem_template))
                {
                        delete _equipped[prevItem_template.getType()];
                    _unequipItem(heroId, prevItem_template.getVisualId());
                }

                quickSlot.freeSlotByItem(prevItem);
            }

            if(_canEquipPhysicaly(template))
            {
                    _equipped[typeId] <- item;
                _equipItem(heroId, template.getVisualId());
            }

            quickSlot.assignItemToSlot(slotId, item);
        }

        local packet = Packet(PacketIDs.Equipment, Equipment_PacketIDs.Equip);

            packet.writeInt16(itemId);
            packet.writeInt8(slotId);

        packet.send(RELIABLE_ORDERED);

        return true;
    }

    function unequipItem(itemId)
    {
        if(isItemEquipped(itemId) == false)
            return false;

        local item = hasItem(itemId);

        if(_currentWeapon == item && getPlayerWeaponMode(heroId) != WEAPONMODE_NONE)
            return false;

        local template = item.getTemplate();

        if(template.isQuickSlotPossible()) getSlotManager().freeSlotByItem(item);

        if(_canEquipPhysicaly(template))
        {
                delete _equipped[template.getType()];
            _unequipItem(heroId, template.getVisualId());
        }

        local packet = Packet(PacketIDs.Equipment, Equipment_PacketIDs.Unequip);
            packet.writeInt16(itemId);
        packet.send(RELIABLE_ORDERED);

        return true;
    }

    function useItem(itemId)
    {
        local item = hasItem(itemId);

        if(item != null)
        {
            local animation = getPlayerAni(heroId);

            if(animation != "S_RUN" && animation != "S_WALK")
                return false;

            local template = item.getTemplate();

            if(template.isUsable() == false)
                return false;

            local diff = item.getAmount() - 1;

            if(diff > 0)
                item.setAmount(diff);
            else
            {
                if(template.isQuickSlotPossible())
                {
                    getSlotManager().freeSlotByItem(item);
                }

                _itemList.remove(_itemList.find(item));
            }

            if(_hasItem(heroId, template.getVisualId()))
            {
                _useItem(heroId, template.getVisualId());

                local packet = Packet(PacketIDs.Equipment, Equipment_PacketIDs.Use);
                    packet.writeInt16(itemId);
                packet.send(RELIABLE);

                return true;
            }
        }

        return false;
    }

    function drawWeapon(itemId)
    {
        if(_canDrawWeapon(itemId) == false)
            return false;

        local item = hasItem(itemId);
        if(item == _currentWeapon) return false;

        local template = item.getTemplate();

        if(template instanceof Item.Weapon == false)
            return false;

        if(template.getType() == ItemType.WeaponRanged)
        {
            local munition = hasItem(template.getMunition().getId());
            if(munition == null) return false;
        }

        _lockChange = true;

        if(_currentWeapon != null)
        {
            _removeWeapon(heroId);
        }

        setContext(DAMAGE_CTX, template.getId());
        _drawWeapon(heroId, template.getWeaponMode());

        _currentWeapon = item;

        return true;
    }

    function removeWeapon()
    {
        if(_lockChange == false)
        {
            _removeWeapon(heroId);
                _currentWeapon = null;

            _resetContext();
        }
    }

    function readySpell(itemId)
    {
        if(_canDrawWeapon(itemId) == false)
            return false;

        local item = hasItem(itemId);

        if(item == _currentWeapon)
            return false;

        local template = item.getTemplate();

        if(template instanceof Item.Spell)
        {
            _lockChange = true;

            if(_currentWeapon != null)
            {
                local currentTemplate = _currentWeapon.getTemplate();

                if(currentTemplate instanceof Item.Spell)
                {
                    _lockChange = false;
                    _unequipItem(heroId, currentTemplate.getVisualId());
                }
                else
                {
                    _removeWeapon(heroId);
                }
            }

            _currentWeapon = item;

            setContext(EQUIPMENT_CTX, itemId);
            setContext(DAMAGE_CTX, itemId);

            _equipItem(heroId, template.getVisualId(), 0);
            _readySpell(heroId, 0, template.getManaUsage());

            return true;
        }
    }

    function unreadySpell()
    {
        if(_lockChange == false)
        {
            // _unreadySpell will stop character but it works on same basics as _removeWeapon
            _removeWeapon(heroId);
                _currentWeapon = null;

            _resetContext();
        }
    }

    function dropItem(itemId, amount)
    {
        if(isItemEquipped(itemId)) return false;

        local item = hasItem(itemId);
        if(item == null) return false;

        local template = item.getTemplate();
        if(template.isBounded()) return false;

        if(item.getAmount() < amount)
            return false;

        local diff = item.getAmount() - amount;

        if(diff > 0)
            item.setAmount(diff);
        else
            _itemList.remove(_itemList.find(item));

        callEvent("Equipment.onHeroRemoveItem", itemId, amount);

        local packet = Packet(PacketIDs.Equipment, Equipment_PacketIDs.Drop);

            packet.writeInt16(itemId);
            packet.writeInt32(amount);

        packet.send(RELIABLE);

        return true;
    }

    function heroChangeWeaponMode(weaponMode)
    {
        if(getPlayerInstance(heroId) != "PC_HERO")
            _resetContext();
        else
        {
            if(weaponMode == WEAPONMODE_NONE || weaponMode == WEAPONMODE_FIST)
            {
                if(_currentWeapon == false || _lockChange == false)
                {
                    _currentWeapon = null;
                    _lockChange = false;

                    _resetContext();
                }
            }
            else if(_currentWeapon && _currentWeapon.getTemplate().getWeaponMode() == weaponMode)
            {
                _lockChange = false;
            }
            else
            {
                print("heroChangeWeaponMode: Error flag!");
            }
        }
    }

    function heroDead()
    {
        _lockChange = false; _currentWeapon = null;
    }

    function shoot()
    {
        local item = hasItem(_currentWeapon.getTemplate().getMunition().getId()),
            diff = item.getAmount() - 1;

        if(diff > 0)
            item.setAmount(diff);
        else
        {
            _itemList.remove(_itemList.find(item));
                _currentWeapon = null;
        }
    }

    function shoot_Check()
    {
        if(_currentWeapon != null)
        {
            return hasItem(_currentWeapon.getTemplate().getMunition().getId()) != null;
        }

        return false;
    }

    function spellCast()
    {
        local template = _currentWeapon.getTemplate();

        if(template.getType() == ItemType.Scroll)
        {
            local item = hasItem(template.getId()),
                diff = item.getAmount() - 1;

            if(diff > 0)
                item.setAmount(diff);
            else
            {
                if(template.isQuickSlotPossible())
                {
                    getSlotManager().freeSlotByItem(item);
                }

                _itemList.remove(_itemList.find(item));
                    _currentWeapon = null;
            }
        }
    }

    function spellCast_Check()
    {
        if(_currentWeapon != null)
        {
            return _currentWeapon.getTemplate().getManaUsage() <= getPlayerMana(heroId);
        }

        return null;
    }

    function debug()
    {
        setPlayerWeaponMode(heroId, WEAPONMODE_NONE);

            _currentWeapon = null;
            _lockChange = false;

        _resetContext();
    }

    function getMeleeWeapon() { return ItemType.WeaponMelee in _equipped ? _equipped[ItemType.WeaponMelee] : null; }

    function getRangedWeapon() { return ItemType.WeaponRanged in _equipped ? _equipped[ItemType.WeaponRanged] : null; }

    function getHelmet() { return ItemType.Helmet in _equipped ? _equipped[ItemType.Helmet] : null; }

    function getBelt() { return ItemType.Belt in _equipped ? _equipped[ItemType.Belt] : null; }

    function getArmor() { return ItemType.Armor in _equipped ? _equipped[ItemType.Armor] : null; }

    function getRing() { return ItemType.Ring in _equipped ? _equipped[ItemType.Ring] : null; }

    function getAmulet() { return ItemType.Amulet in _equipped ? _equipped[ItemType.Amulet] : null; }

    function getEquippedByType(typeId) { return typeId in _equipped ? _equipped[typeId] : null; }

    function isItemEquipped(itemId)
    {
        local item = hasItem(itemId);
        if(item == null) return false;

        local template = item.getTemplate();

        if(template.isQuickSlotPossible())
        {
            return getSlotManager().isItemOnSlotList(item);
        }
        else
        {
            foreach(item in _equipped)
            {
                if(item.getTemplate().getId() == itemId)
                    return true;
            }
        }

        return false;
    }

    function hasItem(itemId)
    {
        foreach(item in _itemList)
        {
            if(item.getTemplate().getId() == itemId)
                return item;
        }

        return null;
    }

    function getEq()
    {
        local items = clone(_itemList);

        if(_itemList.len() > 4)
        {
            items.sort(compare)
        }

        return items;
    }

    function getCurrentWeapon() { return _currentWeapon; }

    function getProtection(damageType)
    {
        local protection = 0;

        for(local index = ItemType.Helmet; index <= ItemType.Amulet; index++)
        {
            if(index in _equipped) protection += _equipped[index].getTemplate().getProtection(damageType);
        }

        return protection;
    }

//:protected
    static function compare(item_a, item_b)
    {
        local type_a = item_a.getTemplate().getType(),
            type_b = item_b.getTemplate().getType();

        if(type_a < type_b) return -1;
        if(type_a > type_b) return 1;

        return 0;
    }
}