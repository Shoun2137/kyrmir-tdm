
class Equipment.QuickSlot
{
    _slotList = null;
    
    constructor()
    {
        _slotList = {};
    }

//:public
    function assignItemToSlot(slotId, item)
    {
        if(slotId >= 0 && slotId <= 9) 
        {
            _slotList[slotId] <- item;
        }
    } 

    function freeSlotByItem(item)
    {
        foreach(index, listItem in _slotList)
        {
            if(listItem == item)
            {
                delete _slotList[index];
                break;
            }
        }
    }

    function getItemBySlot(slotId) { return slotId in _slotList ? _slotList[slotId] : null; }

    function isItemOnSlotList(item)
    {
        foreach(listItem in _slotList)
        {   
            if(listItem == item)
                return true;
        }

        return false;  
    }

    function isSlotFree(slotId)
    {
        if(slotId < 0 || slotId > 9) 
            return false;
        
        return !(slotId in _slotList);
    }

    function getFreeSlot()
    {   
        for(local id = 0; id <= 10; id++)
        {
            local slotId = SLOT_ORDER[id];

            if(slotId in _slotList == false)
                return slotId;
        }

        return -1;
    }

    function getSlots() { return _slotList; }

    function clear()
    {
        _slotList.clear();
    }
}