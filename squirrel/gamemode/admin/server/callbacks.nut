
// Register packet callback
PacketReceive.add(PacketIDs.Admin, Admin);

addEventHandler("onExit", Admin.exit.bindenv(Admin));

addEventHandler("onPlayerJoin", Admin.playerJoin.bindenv(Admin));

addEventHandler("onPlayerUseCheat", Admin.playerUseCheat.bindenv(Admin));

 