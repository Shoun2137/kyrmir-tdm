
class Data.Manager 
{
    static function register(username, password)
    {                
        if(validate_username(username) && validate_password(password))
        {   
            local result = MySQL.QuerySelect(SQL_SOCKET, "player", "id")
                .where(
                    [ "username", username, MySQLCondition.Equal ]
                )
                .exec()
                .assoc();

            if(result == null)
            {		
                local world = getServerWorld(), defaultPosition = DEFAULT_POSITION(world);
                local salt = code(6);
                       
                MySQL.QueryInsert(SQL_SOCKET, "player",
                    [ "username", username ],
                    [ "password", md5(sha256(password + salt)) ],
                    [ "salt", salt ],
                    [ "permission", 0 ],
                    [ "experience", 0 ],
                    [ "experience_next", getNextLevelExperience(0) ],
                    [ "strength", DEFAULT_STATS.strength ],
                    [ "dexterity", DEFAULT_STATS.dexterity ],       
                    [ "health", DEFAULT_STATS.health ],
                    [ "max_health", DEFAULT_STATS.health ],
                    [ "mana", DEFAULT_STATS.mana ],
                    [ "max_mana", DEFAULT_STATS.mana ],
                    [ "world", mysql_escape_string(world) ],
                    [ "pos_x", defaultPosition.x ],
                    [ "pos_y", defaultPosition.y ],
                    [ "pos_z", defaultPosition.z ],
                    [ "angle", defaultPosition.angle ]              
                )
                .exec();

                local lastId = SQL_SOCKET.getLastInsertedId();

                MySQL.QueryInsert(SQL_SOCKET, "player_stats",
                    [ "player_id", lastId ],
                    [ "monster_streak", 0 ],
                    [ "player_streak", 0 ],
                    [ "kills", 0 ],
                    [ "deaths", 0 ],
                    [ "assists", 0 ],
                    [ "dealt_damage", 0 ]
                )
                .exec();

                foreach(item in DEFAULT_STATS.defaultEq)
                {
                    MySQL.QueryInsert(SQL_SOCKET, "player_eq",
                        [ "player_id", lastId ],
                        [ "instance", item[0] ],
                        [ "amount", item[1] ]
                    )   
                    .exec();
                }

                return DataPlayer.Succes;
            }
        }

        return DataPlayer.Fault;
    }
    
    static function login(username, password)
    {
        if(validate_username(username) && validate_password(password))
        {             
            local result = MySQL.QuerySelect(SQL_SOCKET, "player", "*")
                .where([ "username", username, MySQLCondition.Equal ])
                .exec()
                .assoc();
            
            if(result)
            {
                if(result["password"] == md5(sha256(password + result["salt"])))
                {
                    return result;
                }

                return DataPlayer.BadPassword;
            }

            return DataPlayer.NotExist;
        }

        return null;
    }

    static function loadStats(playerBaseId)
    {
        return MySQL.QuerySelect(SQL_SOCKET, "player_stats", "*")
            .where( [ "player_id", playerBaseId, MySQLCondition.Equal ] )
            .exec()
            .assoc();
    }

    static function loadEquipment(playerBaseId)
    {
        local result = MySQL.QuerySelect(SQL_SOCKET, "player_eq", "*")
            .where( [ "player_id", playerBaseId, MySQLCondition.Equal ] )
            .exec();

        if(result)
        {
            if(result.row_num() > 0)
            {
                local row = null, itemList = [];

                while(row = result.assoc())
                {
                    itemList.push([ getItemTemplateIdByInstance(row["instance"]), row["amount"], row["slot_id"] ]);
                }

                return itemList;
            }
        }
        
        return null;
    }

    static function updateOnlineStatus(playerBaseId, status)
    {
        MySQL.QueryUpdate(SQL_SOCKET, "player", [ "online", status ])
            .where([ "id", playerBaseId, MySQLCondition.Equal ])
            .exec();
    }

    static function playerUpdateItem(playerId, instance, oldAmount, amount)
    {
        local playerBaseId = getPlayerBaseId(playerId);

        if(amount > 0) 
        {
            if(oldAmount != 0)
            {
                MySQL.QueryUpdate(SQL_SOCKET, "player_eq", 
                    [ "amount", amount ]
                )
                .where(
                    [ "player_id", playerBaseId, MySQLCondition.Equal, MySQLCondition.AND ],
                    [ "instance", instance, MySQLCondition.Equal ]
                )
                .exec();

                print(format("UPDATE %s : %d - %d", instance, oldAmount, amount));
            }
            else 
            {
                MySQL.QueryInsert(SQL_SOCKET, "player_eq",
                    [ "player_id", playerBaseId ],
                    [ "instance", instance ],
                    [ "amount", amount ]
                )
                .exec();

                print(format("INSERT %s : %d - %d", instance, oldAmount, amount));
            }
        }
        else 
        {
            MySQL.QueryDelete(SQL_SOCKET, "player_eq")
                .where( 
                    [ "player_id", playerBaseId, MySQLCondition.Equal, MySQLCondition.AND ],
                    [ "instance", instance, MySQLCondition.Equal ]                 
                )
                .exec();

            print(format("DELETE %s : %d - %d", instance, oldAmount, amount));
        }
    }

    static function playerEquipItem(playerId, instance, slotId)
    {
        local playerBaseId = getPlayerBaseId(playerId);

        MySQL.QueryUpdate(SQL_SOCKET, "player_eq", 
            [ "slot_id", slotId ]
        )
        .where(
            [ "player_id", playerBaseId, MySQLCondition.Equal, MySQLCondition.AND ],
            [ "instance", instance, MySQLCondition.Equal ]
        )
        .exec();

        print(format("EQUIP %s : %d", instance, slotId));
    }

    static function playerUnequipItem(playerId, instance)
    {
        local playerBaseId = getPlayerBaseId(playerId);

        MySQL.QueryUpdate(SQL_SOCKET, "player_eq", 
            [ "slot_id", NULL_QUICK_SLOT ]
        )
        .where(
            [ "player_id", playerBaseId, MySQLCondition.Equal, MySQLCondition.AND ],
            [ "instance", instance, MySQLCondition.Equal ]
        )
        .exec();
        
        print(format("UNEQUIP %s", instance));
    }

    static function playerClear(playerId)
    {
        local playerBaseId = getPlayerBaseId(playerId);

        MySQL.QueryDelete(SQL_SOCKET, "player_eq")
        .where( 
            [ "player_id", playerBaseId, MySQLCondition.Equal ]
        )
        .exec();
    }

    static function playerChangeVisual(playerId, bodyModel, bodyTxt, headModel, headTxt)
    {
        local playerBaseId = getPlayerBaseId(playerId);

        MySQL.QueryUpdate(SQL_SOCKET, "player", 
            [ "body_model", bodyModel ],
            [ "body_texture", bodyTxt ],
            [ "head_model", headModel ],
            [ "head_texture", headTxt ]  
        )
        .where([ "id", playerBaseId, MySQLCondition.Equal ])
        .exec();
    }

    static function updatePlayer(playerId)
    {
        local playerBaseId = getPlayerBaseId(playerId),
            position = getPlayerPosition(playerId);

        MySQL.QueryUpdate(SQL_SOCKET, "player", 
            [ "guild", getPlayerGuild(playerId) ],
            [ "world", mysql_escape_string(getPlayerWorld(playerId)) ],
            [ "pos_x", position.x ],
            [ "pos_y", position.y ],
            [ "pos_z", position.z ],
            [ "angle", getPlayerAngle(playerId) ],
            [ "strength", getPlayerStrength_Core(playerId) ],
            [ "dexterity", getPlayerDexterity_Core(playerId) ],
            [ "health", getPlayerHealth(playerId) ],
            [ "max_health", getPlayerMaxHealth_Core(playerId) ],
            [ "mana", getPlayerMana(playerId) ],
            [ "max_mana", getPlayerMaxMana_Core(playerId) ],
            [ "magic_level", getPlayerMagicLevel(playerId) ],
            [ "experience", getPlayerExperience(playerId) ],
            [ "experience_next", getPlayerExperienceNextLevel(playerId) ],
            [ "level", getPlayerLevel(playerId) ],
            [ "learn_points", getPlayerLearnPoints(playerId) ],
            [ "skill_1h", getPlayerSkillWeapon(playerId, WEAPON_1H) ],
            [ "skill_2h", getPlayerSkillWeapon(playerId, WEAPON_2H) ],
            [ "skill_bow", getPlayerSkillWeapon(playerId, WEAPON_BOW) ],
            [ "skill_cbow", getPlayerSkillWeapon(playerId, WEAPON_CBOW) ]
        )
        .where([ "id", playerBaseId, MySQLCondition.Equal ])
        .exec();   

        MySQL.QueryUpdate(SQL_SOCKET, "player_stats", 
            [ "monster_streak", getPlayerMonsterStreak(playerId) ],
            [ "player_streak", getPlayerHeroStreak(playerId) ],
            [ "kills", getPlayerKills(playerId) ],
            [ "deaths", getPlayerDeaths(playerId) ],
            [ "assists", getPlayerAssists(playerId) ],
            [ "dealt_damage", getPlayerDealtDamage(playerId) ]
        )
        .where([ "player_id", playerBaseId, MySQLCondition.Equal ])
        .exec();   
    }

    static function updatePlayerStats(playerId, value)
    {
        local playerBaseId = getPlayerBaseId(playerId);
        
        MySQL.QueryUpdate(SQL_SOCKET, "player_stats", 
            [ "monster_streak", getPlayerMonsterStreak(playerId) ],
            [ "player_streak", getPlayerHeroStreak(playerId) ],
            [ "kills", getPlayerKills(playerId) ],
            [ "deaths", getPlayerDeaths(playerId) ],
            [ "assists", getPlayerAssists(playerId) ],
            [ "dealt_damage", getPlayerDealtDamage(playerId) ]
        )
        .where([ "player_id", playerBaseId, MySQLCondition.Equal ])
        .exec();   
    }

    static function updatePlayerDisconnect(playerId)
    {
        local playerBaseId = getPlayerBaseId(playerId),
            position = getPlayerPosition(playerId);
            
        MySQL.QueryUpdate(SQL_SOCKET, "player", 
            [ "pos_x", position.x ],
            [ "pos_y", position.y ],
            [ "pos_z", position.z ],
            [ "angle", getPlayerAngle(playerId) ],
            [ "health", getPlayerHealth(playerId) ],
            [ "mana", getPlayerMana(playerId) ],
            [ "experience", getPlayerExperience(playerId) ],
            [ "experience_next", getPlayerExperienceNextLevel(playerId) ],
            [ "online_time", getPlayerOnlineTime(playerId) + (time() - getPlayerSessionStartTime(playerId)) ],
            [ "mute", isPlayerMuted(playerId) ],
            [ "online", 0 ]
        )
        .where([ "id", playerBaseId, MySQLCondition.Equal ])
        .exec();

        MySQL.QueryUpdate(SQL_SOCKET, "player_stats", 
            [ "monster_streak", getPlayerMonsterStreak(playerId) ],
            [ "player_streak", getPlayerHeroStreak(playerId) ],
            [ "kills", getPlayerKills(playerId) ],
            [ "deaths", getPlayerDeaths(playerId) ],
            [ "assists", getPlayerAssists(playerId) ],
            [ "dealt_damage", getPlayerDealtDamage(playerId) ]
        )
        .where([ "player_id", playerBaseId, MySQLCondition.Equal ])
        .exec();   
    }

    static function resetPlayerData(playerId)
    {        
        local playerBaseId = getPlayerBaseId(playerId);

        local world = getServerWorld(), defaultPosition = DEFAULT_POSITION(world);
                            
        MySQL.QueryUpdate(SQL_SOCKET, "player", 
            [ "guild", getPlayerGuild(playerId) ],
            [ "world", mysql_escape_string(world) ],
            [ "pos_x", defaultPosition.x ],
            [ "pos_y", defaultPosition.y ],
            [ "pos_z", defaultPosition.z ],
            [ "angle", defaultPosition.angle ],    
            [ "strength", DEFAULT_STATS.strength ],
            [ "dexterity", DEFAULT_STATS.dexterity ],
            [ "health", DEFAULT_STATS.health ],
            [ "max_health", DEFAULT_STATS.health ],
            [ "mana", DEFAULT_STATS.mana ],
            [ "max_mana", DEFAULT_STATS.mana ],
            [ "magic_level", 0 ],
            [ "experience", 0 ],
            [ "experience_next", getNextLevelExperience(0) ],
            [ "level", 0 ],
            [ "learn_points", 0 ],
            [ "skill_1h", 0 ],
            [ "skill_2h", 0 ],
            [ "skill_bow", 0 ],
            [ "skill_cbow", 0 ]
        )
        .where([ "id", playerBaseId, MySQLCondition.Equal ])
        .exec();   

        MySQL.QueryUpdate(SQL_SOCKET, "player_stats", 
            [ "monster_streak", 0 ],
            [ "player_streak", 0 ],
            [ "kills", 0 ],
            [ "deaths", 0 ],
            [ "assists", 0 ],
            [ "dealt_damage", 0 ]
        )
        .where([ "player_id", playerBaseId, MySQLCondition.Equal ])
        .exec(); 

        MySQL.QueryDelete(SQL_SOCKET, "player_eq")
        .where( 
            [ "player_id", playerBaseId, MySQLCondition.Equal ]
        )
        .exec();

        foreach(item in DEFAULT_STATS.defaultEq)
        {
            MySQL.QueryInsert(SQL_SOCKET, "player_eq",
                [ "player_id", lastId ],
                [ "instance", item[0] ],
                [ "amount", item[1] ]
            )   
            .exec();
        }
    }

    static function resetAllPlayers()
    {        
        local world = getServerWorld(), defaultPosition = DEFAULT_POSITION(world);
                            
        MySQL.QueryUpdate(SQL_SOCKET, "player", 
            [ "guild", null ],
            [ "world", mysql_escape_string(world) ],
            [ "pos_x", defaultPosition.x ],
            [ "pos_y", defaultPosition.y ],
            [ "pos_z", defaultPosition.z ],
            [ "angle", defaultPosition.angle ],    
            [ "strength", DEFAULT_STATS.strength ],
            [ "dexterity", DEFAULT_STATS.dexterity ],
            [ "health", DEFAULT_STATS.health ],
            [ "max_health", DEFAULT_STATS.health ],
            [ "mana", DEFAULT_STATS.mana ],
            [ "max_mana", DEFAULT_STATS.mana ],
            [ "magic_level", 0 ],
            [ "experience", 0 ],
            [ "experience_next", getNextLevelExperience(0) ],
            [ "level", 0 ],
            [ "learn_points", 0 ],
            [ "skill_1h", 0 ],
            [ "skill_2h", 0 ],
            [ "skill_bow", 0 ],
            [ "skill_cbow", 0 ]
        )
        .exec();   

        MySQL.QueryUpdate(SQL_SOCKET, "player_stats", 
            [ "monster_streak", 0 ],
            [ "player_streak", 0 ],
            [ "kills", 0 ],
            [ "deaths", 0 ],
            [ "assists", 0 ],
            [ "dealt_damage", 0 ]
        )
        .exec(); 

        MySQL.QueryDelete(SQL_SOCKET, "player_eq")
            .exec();

        MySQL.Query(SQL_SOCKET, "ALTER TABLE player_eq AUTO_INCREMENT = 1;")
            .exec();

        foreach(item in DEFAULT_STATS.defaultEq)
        {
            MySQL.Query(SQL_SOCKET, "INSERT INTO player_eq (player_id, instance, amount) SELECT id, '" + item[0] + "', '" + item[1] + "' FROM player;")
                .exec();
        }
    }

    static function getPlayersStats() 
    {
        local result = MySQL.Query(SQL_SOCKET, "SELECT player.username, player_stats.kills, player_stats.deaths, player_stats.assists, player_stats.dealt_damage, ROUND(player_stats.kills + (player_stats.assists / 2) - player_stats.deaths, 0) AS score FROM player_stats JOIN player ON player_stats.player_id = player.id WHERE ROUND(player_stats.kills + (player_stats.assists / 2) - player_stats.deaths, 0)!= 0 ORDER BY score DESC LIMIT " + getMaxSlots() + ";")
            .exec();

        if(result)
        {
            if(result.row_num() > 0)
            {
                local row = null, rankList = [];

                while(row = result.assoc())
                {
                    rankList.push([ row["username"], row["kills"], row["deaths"], row["assists"], row["score"], row["dealt_damage"] ]);
                }

                return rankList;
            }
        }

        return null;
    }

    static function init_OnlineStatus()
    {
        local world = getServerWorld(), defaultPosition = DEFAULT_POSITION(world);

        MySQL.QueryUpdate(SQL_SOCKET, "player", 
            [ "online", 0 ]
        )
        .exec();
    }
}

getDataBase <- @() Data.Manager;