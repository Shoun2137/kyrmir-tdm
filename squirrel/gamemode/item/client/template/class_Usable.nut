
// Food, Potions, Lootboxes
class Item.Usable extends Item.Standard
{
    _effectList = null;

    constructor(id, instance, visualId, typeId)
    {
            _effectList = {};
        base.constructor(id, instance, visualId, typeId);
    }

//:public
    function setEffect(effecType, effect)
    {
        _effectList[effecType] <- effect;
    }

    function getEffect(effecType) { return effecType in _effectList ? _effectList[effecType] : 0; }

    function getEffects() { return _effectList; }
}