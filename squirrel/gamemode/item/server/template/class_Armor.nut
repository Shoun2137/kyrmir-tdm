
class Item.Armor extends Item.Gear
{
    _protection = null;

    constructor(instance, visualId, typeId)
    {
            _protection = {};
        base.constructor(instance, visualId, typeId);
    }

    function setProtection(protectionType, protection)
    {
        _protection[protectionType] <- protection;
            return this;
    }

    function getProtection(protectionType) { return protectionType in _protection ? _protection[protectionType] : 0; }
}