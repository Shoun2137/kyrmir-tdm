
Translation <- {
    _language = LANG.EN,
    setLanguage = function(language) 
    {
        _language = language;
        
        switch(language)
        {
            case LANG.EN: 
            case LANG.RU:
                setKeyLayout(KEY_LAYOUT_EN); 
            break;
            case LANG.PL: setKeyLayout(KEY_LAYOUT_PL); break;
            case LANG.DE: setKeyLayout(KEY_LAYOUT_DE); break;
        }

        refreshGUIText();
        refreshNpcNames();
    },
    getLanguage = function() 
    {
        return _language;
    },
    getLanguageFont = function(fontFile, langTarget = -1) 
    {
        if(langTarget == -1) langTarget = _language;

        switch(fontFile)
        {
            case "FONT_OLD_10_WHITE_HI": 
                fontFile = (langTarget == LANG.RU ? "FONT_OLD_10_WHITE_HI_RU.TGA" : "FONT_OLD_10_WHITE_HI.TGA");
            break;
            case "FONT_OLD_20_WHITE_HI": 
                fontFile = (langTarget == LANG.RU ? "FONT_OLD_20_WHITE_HI_RU.TGA" : "FONT_OLD_20_WHITE_HI.TGA");
            break;
            case "FONT_NEW_20_EMOTICONS_HI":
                fontFile = (langTarget == LANG.RU ? "FONT_NEW_EMOTICONS_HI_RU.TGA" : "FONT_NEW_EMOTICONS_HI.TGA");
            break;
            default: fontFile = (langTarget == LANG.RU ? "FONT_DEFAULT_RU.TGA" : "FONT_DEFAULT.TGA");
        }

        return fontFile;
    }
};

// Translation
setLanguage <- @(language) Translation.setLanguage(language);

getLanguage <- @() Translation.getLanguage();
getLanguageFont <- @(fontFile, langTarget = -1) Translation.getLanguageFont(fontFile, langTarget);