
class Translation.Manager
{
    static _translationList = {};

//:public
    static function registerTranslation(instance, lang, text)
    {
        if(instance in _translationList)
            _translationList[instance].setText(lang, text);
        else 
        {
            _translationList[instance] <- Translation.Standard();
            _translationList[instance].setText(lang, text);
        }
    }

    static function getTranslation(instance)
    {
        local translation = "";
        local separate = split(instance, ";");

        foreach(word in separate)
            translation += word in _translationList ? _translationList[word].getText(getLanguage()) : word;
            
        return translation; 
    }
}

getTranslationManager <- @() Translation.Manager;

// Translation
registerTranslation <- @(instance, lang, text) Translation.Manager.registerTranslation(instance, lang, text);
getTranslation <- @(instance) Translation.Manager.getTranslation(instance);

function getFormatTranslation(instance, ...)
{   
    vargv.insert(0, this);
    vargv.insert(1, getTranslation(instance));

    return format.acall(vargv);
}