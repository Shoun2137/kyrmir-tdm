
class Dialog.List 
{
    _optionList = null;

    constructor()
    {
        _optionList = [];
    }

//:public 
    function addOption(option)
    {
        _optionList.push(option);
            return this;
    }

    function getOption(playerId, instance)
    {
        foreach(option in _optionList)
        {
            if(option.getInstance() == instance && option.checkRequirement(playerId))
                return option;
        }

        return null;
    }

    function getOptions(playerId)
    {
        local options = [];

        foreach(option in _optionList)
        {
            if(option.checkRequirement(playerId))
                options.push(option);
        }

        return options;
    }
}
