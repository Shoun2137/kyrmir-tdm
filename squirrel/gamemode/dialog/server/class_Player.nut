
class Dialog.Player 
{
    _playerId = -1;

    _finishedDialogList = null;

    _partnerId = -1;
    _currentData = null;

    constructor(playerId)
    {
        _playerId = playerId;

        _finishedDialogList = [];

        _partnerId = -1;
        _currentData = null;
    }

//:public
    function client_beginConversation(npcId)
    {
        if(_partnerId == -1)
        {
            local initiator = getDialogManager().getNpcInitiator(_playerId, npcId);
            if(initiator == null) return;

            local packet = Packet(PacketIDs.Dialog, Dialog_PacketIDs.BeginConversation);

            if(initiator.checkRequirement(_playerId))
            {
                initiator.startEffect(_playerId);
                
                _partnerId = npcId;
                                    
                local typeId = initiator.getType();

                packet.writeString(initiator.getInstance());
                packet.writeInt8(typeId);
            
                switch(typeId)
                {
                    case DialogType.None: 
                        _currentData = null;
                    break;
                    case DialogType.Buy: 
                    {
                        local itemList = initiator.getItems();

                            packet.writeInt16(initiator.getCurrency());      
                            packet.writeInt8(itemList.len());    

                        foreach(item in itemList)
                        {
                            packet.writeInt16(item.itemId);
                            packet.writeInt32(item.price);
                        }

                        _currentData = initiator;
                    } 
                    break; 
                    case DialogType.List: 
                    {
                        local listObject = initiator.getList();
                        local list = listObject.getOptions(_playerId);
                            
                            packet.writeInt8(list.len());      

                        foreach(option in list)
                        {
                            packet.writeInt8(option.getType());
                            packet.writeString(option.getInstance());
                        }

                        _currentData = listObject;
                    } 
                    break;        
                }                        
            }

            packet.send(_playerId, RELIABLE); 
        }
    }

    function client_finishConversation()
    {
        _partnerId = -1;
        _currentData = null;
    }

    function client_selectOption(instance)
    {
        if(_partnerId != -1)
        {
            local option = _currentData.getOption(_playerId, instance);
            if(option == null) return;

            option.startEffect(_playerId);

            local typeId = option.getType();

            local packet = Packet(PacketIDs.Dialog, Dialog_PacketIDs.SelectOption);
                packet.writeInt8(typeId);

            switch(typeId)
            {
                case DialogType.None: 
                {             
                    if(_currentData != null)
                    {
                        packet.writeBool(true);

                        local list = _currentData.getOptions(_playerId);
                            packet.writeInt8(list.len());   

                        foreach(option in list)
                        {
                            packet.writeInt8(option.getType());
                            packet.writeString(option.getInstance());
                        }
                    }
                    else 
                        packet.writeBool(false);
                } 
                break;
                case DialogType.Buy: 
                {
                    local itemList = option.getItems();

                        packet.writeInt16(option.getCurrency());      
                        packet.writeInt8(itemList.len());    

                    foreach(item in itemList)
                    {
                        packet.writeInt16(item.itemId);
                        packet.writeInt32(item.price);
                    }

                    _currentData = option;
                } 
                break; 
                case DialogType.List: 
                {
                    local listObject = option.getList();
                    local list = listObject.getOptions(_playerId);
                        
                        packet.writeInt8(list.len());      

                    foreach(option in list)
                    {
                        packet.writeInt8(option.getType());
                        packet.writeString(option.getInstance());
                    }

                    _currentData = listObject;
                } 
                break;  
            }

            packet.send(_playerId, RELIABLE); 
        }
    }

    function client_buyItem(itemId, amount)
    {
        if(_partnerId != -1 && amount >= 0 && amount <= 10000)
        {
            local listItem = _currentData.checkItem(_playerId, itemId);
            if(listItem == null) return;

            local currencyId = _currentData.getCurrency();
            local currencyItem = hasItem(_playerId, currencyId);

            if(currencyItem)
            {
                local price = listItem.price * amount;
                
                if(currencyItem.getAmount() >= price)
                {
                    removeItem(_playerId, currencyId, price);
                    giveItem(_playerId, itemId, amount);
                }
            }
        }
    }
}