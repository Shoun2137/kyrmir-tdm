
class Dialog.Manager
{
    static _npcList = [];
    static _dialogList = {};

//:protected
    static function server_packetToList(packet)
    {
        local packetSize = packet.readInt16();

        for(local i = 0; i < packetSize; i++)
        {
            _addNpc(packet.readInt16());
        }
    }

    static function _addNpc(npcId)
    {
        _npcList.push(npcId);
    }

//:public
    static function server_beginConversation(packet)
    {        
        getHero().getDialogModule().server_beginConversation(packet);
    }

    static function server_selectOption(packet)
    {        
        getHero().getDialogModule().server_selectOption(packet);
    }

    static function registerTranscription(transcription)
    {
        _dialogList[transcription.getInstance()] <- transcription;
    }

    static function isNpcTalkative(npcId) { return _npcList.find(npcId) != null; }   
    
    static function getDialogTranscription(instance) { return instance in _dialogList ? _dialogList[instance] : null; }

    static function packetRead(packet)
    {
        local packetId = packet.readInt8();

        switch(packetId)
        {
            case Dialog_PacketIDs.List: server_packetToList(packet); break;
            case Dialog_PacketIDs.BeginConversation: server_beginConversation(packet); break;
            case Dialog_PacketIDs.SelectOption: server_selectOption(packet); break;
       }
    }
}

getDialogManager <- @() Dialog.Manager;

// Dialog
isNpcTalkative <- @(npcId) Dialog.Manager.isNpcTalkative(npcId);

registerDialog <- @(translation) Dialog.Manager.registerTranscription(translation);
getDialog <- @(instance) Dialog.Manager.getDialogTranscription(instance);

beginConversation <- @(npcId) getHero().getDialogModule().beginConversation(npcId);
selectDialogOption <- @(instance) getHero().getDialogModule().selectOption(instance);
buyItem <- @(itemId, amount) getHero().getDialogModule().buyItem(itemId, amount);

getDialogPartnerId <- @() getHero().getDialogModule().getPartnerId();
getCurrentDialogData <- @() getHero().getDialogModule().getData();