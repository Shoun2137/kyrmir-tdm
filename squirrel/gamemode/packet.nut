
enum PacketIDs 
{
    Npc,
    Player,
    Reward,
    Item,
    Dialog,
    Equipment,
    Damage,
    Guild,
    Round,
    Chat,
    Area,
    Admin,
    VoiceCall,
    InfoBox,
}

enum Player_PacketIDs 
{
    List,
    Login,
    Register,
    Visual,
    Permission,
    Guild,
    Level,
    LearnPoints,
    Experience,
    ExperienceNextLevel,
    MonsterStreak,
    PlayerStreak,
    Kills,
    Deaths,
    Assists,
    Transformation,
    Reset
}

enum Reward_PacketIDs 
{
    MonsterStreak,
    PlayerStreak
}

/*enum Item_PacketIDs 
{
    List,
}*/

enum Dialog_PacketIDs 
{
    List,
    BeginConversation,
    FinishConversation,
    SelectOption,
    BuyItem,
}

enum Equipment_PacketIDs 
{
    Give,
    Give_Multiple,
    Remove,
    SetInventory,
    Clear,
    Equip,
    Unequip,
    Use,
    Drop,
}

enum Damage_PacketIDs 
{
    Damage_Fall,
    Parade
}

enum Guild_PacketIDs 
{
    List,
    Change,
    Members,
    Points,
    Lock,
    Reset
}

enum Round_PacketIDs 
{
    Initial,
    Leaderboard,
    Start,
    Stop,
    Target,
    Bonus,
}

enum Chat_PacketIDs 
{
    List,
    Add,
    Remove,
}

enum Area_PacketIDs 
{
    Enter,
    Leave,
    Clear,
}

enum Admin_PacketIDs 
{
    //List,
    Spawn,
    Unspawn,
    Union
}

enum VoiceCall_PacketIDs
{
    Scream,
}

enum InfoBox_PacketIDs
{
    Kill,
    ThirdLevel,
    BossList,
    BossDead,
    BossRespawn
}