
class Reward.Content 
{
    _experience = 0;
    _dropList = null;

    constructor(experience)
    {
        _experience = experience;
            _dropList = [];
    }

//:public 
    function addDrop(drop)
    {
        _dropList.push(drop);
            return this;
    }

    function getDrop() { return _dropList; }

    function getExperience() { return _experience; }
}