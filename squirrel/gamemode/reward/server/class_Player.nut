
class Reward.Player 
{
    _playerId = -1;

    _monsterStreak = 0;
    _playerStreak = 0;

    constructor(playerId)
    {
        _playerId = playerId;

        _monsterStreak = 0;
        _playerStreak = 0;
    }

//:public
    function setStats(statsTable)
    {
        _monsterStreak = statsTable["monster_streak"];
        _playerStreak = statsTable["player_streak"];

        // Transfer - maybe refractor
        local packet = Packet(PacketIDs.Reward, Reward_PacketIDs.MonsterStreak)
            packet.writeInt32(_monsterStreak);    
        packet.send(_playerId, UNRELIABLE);  
        
        // Transfer - maybe refractor
        local packet = Packet(PacketIDs.Reward, Reward_PacketIDs.PlayerStreak)
            packet.writeInt32(_playerStreak);    
        packet.send(_playerId, UNRELIABLE); 
    }

    function setMonsterStreak(streak)
    {
        if(streak != _monsterStreak)
        {
            local packet = Packet(PacketIDs.Reward, Reward_PacketIDs.MonsterStreak)
                packet.writeInt32(streak);    
            packet.send(_playerId, UNRELIABLE);    

            callEvent("Reward.onPlayerMonsterStreak", _playerId, _monsterStreak = streak);
        }
    }

    function getMonsterStreak() { return _monsterStreak; }

    function setPlayerStreak(streak)
    {
        if(streak != _playerStreak)
        {
            local packet = Packet(PacketIDs.Reward, Reward_PacketIDs.PlayerStreak)
                packet.writeInt32(streak);    
            packet.send(_playerId, UNRELIABLE);    

            callEvent("Reward.onPlayerHeroStreak", _playerId, _playerStreak = streak);
        }
    }

    function getPlayerStreak() { return _playerStreak; }
}