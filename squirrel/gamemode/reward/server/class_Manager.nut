
/* ****************
    Level 

    250
    500
    1000
    1500
    2000
    2500
    3000
    3500
    4000
    4500

    22 750
**************** */

class Reward.Manager
{
    static _rewardList = {};

//:public
    static function registerReward(identifier, reward)
    {
        _rewardList[identifier] <- reward;
    }
    
    static function getNextLevelExperience(currentLevel)
    {
        if(currentLevel == 0)
            return 250;
        else 
            return currentLevel * 500; 
    }

    static function playerDead(playerId, killerId, hitList)
    {
        if(isRoundStarted())
        {
            setPlayerMonsterStreak(playerId, 0);
            setPlayerHeroStreak(playerId, 0);

            if(getPlayerLevel(playerId) >= MAX_SWAP_LEVEL) 
            {
                playerGainRewardFromPlayer(playerId, killerId, hitList);
            }
        }
    }

    static function monsterDead(bot, killerId, hitList)
    {
        local template = bot.getTemplate(), identifier = template.getIdentifier();

        if(identifier in _rewardList)
        {
            local reward = _rewardList[identifier]; 

            local rewardExp = reward.getExperience() * (getRoundBonus() + 1),
                rewardDrop = reward.getDrop();

            foreach(playerId, lastHitTime in hitList)
            {
                if(playerId == killerId)
                {
                    playerGainExperience(playerId, rewardExp);  
                    playerGainDrop(playerId, rewardDrop);
                }
                else 
                {
                    // ONLY FOR TDM
                    playerGainExperience(playerId, rewardExp * 0.70);  

                    local dropIncrease = getPlayerMonsterStreak(playerId);

                    if(dropIncrease != 0) 
                        giveItem(playerId, getItemTemplateIdByInstance("ITMI_GOLD"), dropIncrease * 2);
                }

                if(isRoundStarted()) 
                {
                    setPlayerMonsterStreak(playerId, getPlayerMonsterStreak(playerId) + 1);
                }
            }
        }
    }

    static function playerGainRewardFromPlayer(playerId, killerId, hitList)
    {
        if(hitList.len() == 0) return;

        local playerLevel = getPlayerLevel(playerId);

        if(killerId == -1 || killerId >= getMaxSlots())
        {
            local minValue = 0;
            killerId = -1; 

            foreach(lastId, lastHitTime in hitList)
            {
                if(killerId == -1)
                {
                    minValue = lastHitTime;
                    killerId = lastId;       
                }
                else 
                {
                    if(lastHitTime < minValue)
                    {
                        minValue = lastHitTime;
                        killerId = lastId;
                    }
                }
            }
        }
  
        local killerStreak = getPlayerHeroStreak(killerId);

        playerGainExperience(killerId, playerLevel * 100 * (getRoundBonus() + 1));
        giveItem(killerId, getItemTemplateIdByInstance("ITMI_GOLD"), playerLevel * 100 + killerStreak * 50);

        setPlayerHeroStreak(killerId, killerStreak + 1);
    }

//:protected
    static function calculateLevel(currentLevel, newExperience)
    {
        local nextLevelExperience = getNextLevelExperience(currentLevel);

        if(newExperience >= nextLevelExperience)
        {
            return calculateLevel(currentLevel + 1, newExperience - nextLevelExperience);
        }

        return currentLevel;
    }

    static function playerGainExperience(playerId, experience)
    {
        local playerLevel = getPlayerLevel(playerId);
        if(playerLevel >= LEVEL_CAP) return;

        local newExperience = getPlayerExperience(playerId) + experience, newLevel = calculateLevel(playerLevel, newExperience);

        if(playerLevel != newLevel)
        {
            local diff = newExperience - getPlayerExperienceNextLevel(playerId), newLevel = playerLevel + 1;

            if(newLevel == LEVEL_CAP)
            {
                setPlayerExperience(playerId, 0);
                setPlayerExperienceNextLevel(playerId, -1);
            }            
            else 
            {
                setPlayerExperience(playerId, diff);
                setPlayerExperienceNextLevel(playerId, getNextLevelExperience(newLevel));            
            }
            
            local levelDiff = newLevel - playerLevel;
            
            setPlayerMaxHealth(playerId, getPlayerMaxHealth_Core(playerId) + levelDiff * 10);
            setPlayerLearnPoints(playerId, getPlayerLearnPoints(playerId) + levelDiff * 20);

            setPlayerLevel(playerId, newLevel);
        }
        else 
        {
            setPlayerExperience(playerId, newExperience);
        }
    }

    static function playerGainDrop(playerId, dropList)
    {
        local rewardList = [];

        foreach(drop in dropList)
        {
            if(drop.checkRequirement(playerId) != true) continue;
                        
            local amountMin = drop.getAmountMin(), amountMax = drop.getAmountMax();
            local chance = drop.getChance();

            if(chance != -1)
            {
                local rand = random(0, 100);

                if(rand <= chance)
                {                        
                    if(amountMin != amountMax)
                        rewardList.push( [ drop.getItemId(), random(amountMin, amountMax) ] );
                    else
                        rewardList.push( [ drop.getItemId(), amountMin ] );
                }   
            }
            else 
            {   
                // ONLY FOR TDM
                local dropIncrease = getPlayerMonsterStreak(playerId);

                if(amountMin != amountMax)
                    rewardList.push( [ drop.getItemId(), random(amountMin, amountMax) ] );
                else
                {
                    if(drop.getItemInstance() != "ITMI_GOLD")
                    {
                        rewardList.push( [ drop.getItemId(), amountMin ] );
                    }
                    else            
                        rewardList.push( [ drop.getItemId(), amountMin + dropIncrease ] );
                }
            }
        }
        
        if(rewardList.len() != 0) giveItemMultiple(playerId, rewardList);
    }
}

getRewardManager <- @() Reward.Manager;

// Reward
registerMonsterReward <- @(identifier, reward) Reward.Manager.registerReward(identifier, reward);
getNextLevelExperience <- @(currentLevel) Reward.Manager.getNextLevelExperience(currentLevel);

function setPlayerMonsterStreak(playerId, streak)
{
    local player = getPlayer(playerId);
    if(player) player.getRewardModule().setMonsterStreak(streak);
}

function getPlayerMonsterStreak(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getRewardModule().getMonsterStreak();

    return 0;
}

function setPlayerHeroStreak(playerId, streak)
{
    local player = getPlayer(playerId);
    if(player) player.getRewardModule().setPlayerStreak(streak);
}

function getPlayerHeroStreak(playerId)
{
    local player = getPlayer(playerId);

    if(player) 
        return player.getRewardModule().getPlayerStreak();

    return 0;
}