
class Round.Manager
{
    static _round = null;
    static _roundRank = null;

//:protected 
    static function server_packetInitial(packet)
    {
        local beginingTime = packet.readInt32(),
            targetPoints = packet.readInt32(),
            bonus = packet.readFloat();

        _round <- Round.Instance(beginingTime, targetPoints, bonus);
            callEvent("Round.onCreateRound", _round);
    }

    static function server_packetRank(packet)
    {
        local packetSize = packet.readInt16();

        _roundRank <- [];

        for(local i = 0; i < packetSize; i++)
        {         
            local row = { 
                username = packet.readString(), 
                kills = packet.readInt8(), 
                deaths = packet.readInt8(), 
                assists = packet.readInt8(), 
                kda = packet.readInt16(), 
                dealt_damage = packet.readInt32() 
            };   

            _roundRank.push(row);
        }

        callEvent("Round.onUpdateLeaderboard", _roundRank);
    }

    static function server_start()
    {
        if(_round) _round.start();
    }

    static function server_stop()
    {
        if(_round) _round.stop();
    }

    static function server_target(packet)
    {
        if(_round) _round.setTargetPoints(packet.readInt16());
    }

    static function server_bonus(packet)
    {
        _round.setExperienceBonus(packet.readFloat());
    }

//:public
    static function isRoundStarted() { return _round != null ? _round.isStarted() : false; }

    static function getRound() { return _round; }

    static function getLeaderboard() { return _roundRank };

    static function packetRead(packet)
    {
        local packetType = packet.readInt8();
        
        switch(packetType)
        {
            case Round_PacketIDs.Initial: server_packetInitial(packet); break;	
            case Round_PacketIDs.Leaderboard: server_packetRank(packet); break;	
            case Round_PacketIDs.Start: server_start(); break;	
            case Round_PacketIDs.Stop: server_stop(); break;	
            case Round_PacketIDs.Target: server_target(packet); break;	
            case Round_PacketIDs.Bonus: server_bonus(packet); break;	
        }
    }
}

getRoundManager <- @() Round.Manager;

// Round
isRoundStarted <- @() Round.Manager.isRoundStarted();
getRound <- @() Round.Manager.getRound();

getLeaderboard <- @() Round.Manager.getLeaderboard();