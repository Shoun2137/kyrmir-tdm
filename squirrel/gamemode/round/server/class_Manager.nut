
const UPDATE_TIME = 180000;

class Round.Manager
{
    static _round = null;
    static _roundRank = { list = null, nextUpdate = null };
    
//:protected 
    static function _dataToPacket()
    {
        local packet = Packet(PacketIDs.Round, Round_PacketIDs.Initial);

            packet.writeInt32(_round.getBeginingTime());
            packet.writeInt32(_round.getTargetPoints());
            packet.writeFloat(_round.getBonus());

        return packet;
    }

    static function _rankToPacket()
    {
        local rankList = _roundRank.list;

        if(rankList != null)
        {
            local packet = Packet(PacketIDs.Round, Round_PacketIDs.Leaderboard);
            
            local rankListSize = rankList.len();
            packet.writeInt16(rankListSize);

            for(local i = 0; i < rankListSize; i++)
            {
                local rankData = rankList[i];

                packet.writeString(rankData[0]);
                packet.writeInt8(rankData[1]);
                packet.writeInt8(rankData[2]);
                packet.writeInt8(rankData[3]);
                packet.writeInt16(rankData[4]);
                packet.writeInt32(rankData[5]);
            }

            return packet;
        }

        return null;
    }

//:public
    static function init()
    {
        _roundRank.list = getDataBase().getPlayersStats();
        _roundRank.nextUpdate = getTickCount() + UPDATE_TIME; 
    }

    static function playerJoin(playerId)
    {
        if(_round != null) 
        {
            _dataToPacket().send(playerId, RELIABLE);
        }

        local rank = _rankToPacket();
        if(rank) rank.send(playerId, RELIABLE);
    }

    static function pointsUpdate(points)
    {
        if(_round != null)
        {
            local target = _round.getTargetPoints();

            if(target == points)
            {
                _roundRank.list = getDataBase().getPlayersStats();
                _roundRank.nextUpdate = getTickCount() + UPDATE_TIME; 

                local rank = _rankToPacket();
                if(rank) rank.sendToAll(RELIABLE_ORDERED);

                _round.stop();
                _round <- null;
            }
            else
            {
                local guildList = getGuilds(),
                    points = 0;
                
                foreach(guild in guildList) points += guild.getPoints();

                _round.setExperienceBonus(6.0 * (points / guildList.len() / target.tofloat()));
            }
        }
    }   

    static function roundStart()
    {
        local players = getPlayers(), guilds = getGuilds();
        
        getDataBase().resetAllPlayers();
        
        foreach(id, player in players) 
        {
                player.reset();
            unspawnPlayer(id);
        }

        getArtifactManager().resetArtifacts();

        local currentTime = date();
        
        setTime(currentTime.hour - 2, currentTime.min);
        setDayLength(86400000);

        resetAIState();
      
        // We are clearing data from previous round
        foreach(guild in guilds) 
            guild.reset();

        clearGroundItems();
    }

    static function startRound(points, beginingTime)
    {
        if(_round == null)
        {
            _round <- Round.Instance(time() + beginingTime, points);
                _dataToPacket().sendToAll(RELIABLE);
        }
    }

    static function stopRound()
    {
        if(_round != null)
        {
            _round.stop();
            _round <- null;
        }
    }

    static function changeTarget(points)
    {
        if(_round != null && _round.isStarted())
        {
            _round.setTargetPoints(points);
                return true;
        }

        return false;
    }
    
    static function isRoundStarted() { return _round != null ? _round.isStarted() : false; }

    static function getRound() { return _round; }

    static function checkRound() 
    {
        if(_round != null)
        {     
            if(_round.isStarted() == false && _round.getBeginingTime() <= time())
            {
                _round.start();
            }
        
            local tick = getTickCount();

            if(_roundRank.nextUpdate < tick)
            {
                _roundRank.list = getDataBase().getPlayersStats();
                _roundRank.nextUpdate = tick + UPDATE_TIME; 
                
                local rank = _rankToPacket();
                if(rank) rank.sendToAll(RELIABLE);
            }
        }
    }
}

getRoundManager <- @() Round.Manager;

// Round
startRound <- @(points, beginingTime) Round.Manager.startRound(points, beginingTime); 
changeRoundTarget <- @(points) Round.Manager.changeTarget(points); 
stopRound <- @() Round.Manager.stopRound();

isRoundStarted <- @() Round.Manager.isRoundStarted();
getRound <- @() Round.Manager.getRound();

function getRoundBonus()
{
    local round = getRound();

    if(round != null) 
        return round.getBonus();

    return 0;
}