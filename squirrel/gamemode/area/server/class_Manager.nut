
class Area.Manager
{
    static _areaList = {};

//:public
    static function addArea(area)
    {
        _areaList[area.getInstance()] <- area;
            return this;
    }

    static function removeArea(instance)
    {
        if(instance in _areaList)
        {
            delete _areaList[instance];
        }

        return this;
    }

    static function playerDead(playerId)
    {
        getPlayer(playerId).getAreaModule().playerDead();   
    }

    static function checkArea()
    {
        local players = getPlayers();

        foreach(playerId, player in players)
		{	    
            if(player.isLogged() == false || isPlayerDead(playerId)) continue;

            local position = getPlayerPosition(playerId), playerModule = player.getAreaModule();

            foreach(instance, area in _areaList)
            {
                if(area.checkPoint(getPlayerWorld(playerId), position.x, position.z))
                {
                    if(!playerModule.isInArea(area))
                    {
                        playerModule.enterArea(area);
                        callEvent("Area.onPlayerEnterArea", playerId, instance);
                    }
                }
                else
                {
                    if(playerModule.isInArea(area))
                    {
                        playerModule.leaveArea(area);
                        callEvent("Area.onPlayerLeaveArea", playerId, instance);
                    }
                }
            }
        }   
    }
}

getAreaManager <- @() Area.Manager;

// Area
addArea <- @(area) Area.Manager.addArea(area);
removeArea <- @(instance) Area.Manager.removeArea(instance);
