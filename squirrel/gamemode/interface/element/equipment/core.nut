
local activeItem = null;
local activeRenderId = -1;

local rotationCamera = false;

// INTERFACE
local equipment_View = GUI.View("VIEW_EQUIPMENT"),
    drop_View = GUI.View();

// INTERFACE - DROP
local drop_Size = {
    texture = GUI.SizePr(25, 17.5),
    input = GUI.SizePr(20, 5),
    button = GUI.SizePr(20, 5),
};

local drop_Position = {
    texture = GUI.PositionPr(37.5, 27.5),
    input = GUI.PositionPr(40, 30),
    button = GUI.PositionPr(40, 37.5),
};

local drop_Elements = {
    texture = GUI.Texture("BACKGROUND_BORDER_512x512.TGA", drop_Position.texture, drop_Size.texture),
    input = GUI.Input("EQUIPMENT_VIEW_DROP_INPUT", drop_Position.input, drop_Size.input)
        .setTexture("INPUT_UNI.TGA")
        .setType(GUIInputType.Number),
    button = GUI.Button("EQUIPMENT_VIEW_DROP_BUTTON", drop_Position.button, drop_Size.button)
        .setTexture("BUTTON_RED.TGA")
};

// INTERFACE - EQUIPMENT
local widthMultipler = getResolution().x.tofloat() / getResolution().y.tofloat();

local equipment_Size = {
    textureEquipment = GUI.SizePr(30, 35 * widthMultipler),
    textureDescription = GUI.SizePr(57.5, 40),
    renderDescription = GUI.SizePr(15, 15 * widthMultipler),
    renderEquipment = GUI.SizePr(5, 5 * widthMultipler),
    buttonGold = GUI.SizePr(12.5, 5)
};

local equipment_Position = {
    textureEquipment = GUI.PositionPr(65.5, 15),
    textureDescription = GUI.PositionPr(5, 50),
    renderDescription = null,
    drawDescription = GUI.PositionPr(7.5, 52.5),
    drawInfo = GUI.PositionPr(65.5, 35 * widthMultipler + 17), // textureEquipment + 2
    buttonGold = GUI.PositionPr(83, 7.5),
};

equipment_Position.renderDescription = GUI.PositionPr(42.5, 
    equipment_Position.textureDescription.getYPr() + (equipment_Size.textureDescription.getHeightPr() / 2.0) - (equipment_Size.renderDescription.getHeightPr() / 2.0));

local equipment_Elements = {
    textureEquipment = GUI.Texture("BACKGROUND_1024x2048.TGA", equipment_Position.textureEquipment, equipment_Size.textureEquipment),
    textureDescription = GUI.Texture("BACKGROUND_2048x2048.TGA", equipment_Position.textureDescription, equipment_Size.textureDescription),
    renderDescription = GUI.Render(null, equipment_Position.renderDescription, equipment_Size.renderDescription)
        .setRotate(true),
    drawDescription = GUI.MultiDraw(equipment_Position.drawDescription, 15)
        .setScrollable(true),
    gridEquipment = GUI.Grid(equipment_Position.textureEquipment, 6, 7)  
        .setCellSize(equipment_Size.renderEquipment)
        .setBorderGap(0)	
        .setScrollable(true),
    drawInfo = GUI.Draw("EQUIPMENT_VIEW_INFO", equipment_Position.drawInfo)
        .setTextScale(0.8, 0.8),      
    buttonGold = GUI.Button("ITMI_GOLD;: 0", equipment_Position.buttonGold, equipment_Size.buttonGold)
        .setTexture("BACKGROUND_BAR.TGA") 
};

local function canUseEquipment()
{
    return getPlayerInstance(heroId) == "PC_HERO" && getPlayerHealth(heroId) > 0;
}

local function load_itemDescription(selectId)
{
    drop_View.setVisible(false);

    local draw = equipment_Elements.drawDescription;

    local item = equipment_Elements.gridEquipment.getRenders()[selectId].getItem();
    local template = item.getTemplate();

    draw.clearDraws();

    equipment_Elements.renderDescription.setItemInstance(template.getVisual());
    draw.pushDraw(template.getInstance());

    if(template instanceof Item.Gear)
    {    
        local requirementList = template.getRequirements();

        if(requirementList.len() > 0)
        {
            draw.pushDraw("");
            draw.pushDraw("EQUIPMENT_VIEW_REQUIREMENT_TITLE;:");
            
            foreach(index, requirement in requirementList)
            {
                if(requirement > 0)
                    draw.pushDraw("REQUIREMENT_TYPE_" + index + "; - "+  requirement);     
            }
        }

        if(template instanceof Item.Armor)
        {
            draw.pushDraw("");
            draw.pushDraw("EQUIPMENT_VIEW_PROTECTION_TITLE;:");     

            local protectionList = template.getProtections();
            local text = "";

            foreach(index, protection in protectionList)
            {
                if(protection > 0)
                {
                    draw.pushDraw("DAMAGE_TYPE_" + index + "; - " + protection);     
                }
            }
        }
        else if(template instanceof Item.Weapon)
        {
            local damageType = template.getDamageType();
            draw.pushDraw("");

            if(damageType != -1) draw.pushDraw("DAMAGE_TYPE_" + template.getDamageType() + ";: " + template.getDamage());      
            if(template instanceof Item.Spell) draw.pushDraw("EQUIPMENT_VIEW_MANA_USAGE;: " + template.getManaUsage());   
        }
    }
    else if(template instanceof Item.Usable)
    {
        draw.pushDraw("");
        draw.pushDraw("EQUIPMENT_VIEW_EFFECT_TITLE;:");

        local effectList = template.getEffects();

        foreach(index, effect in effectList)
        {
            if(typeof(effect) == "integer")
            {
                if(effect > 0)
                {
                    draw.pushDraw("EFFECT_TYPE_" + index + "; - " + effect);     
                }
            }
            else 
                draw.pushDraw(effect);     
        }
    }
    
    local translationId = template.getInstance() + "_DESC";
    local description = getTranslation(translationId);

    if(translationId != description)
    {
        draw.pushDraw("");
        draw.pushDraw("EQUIPMENT_VIEW_DESCRIPTION_TITLE;:");     

        local lines = textSlice(description, 50);

        foreach(line in lines)
            draw.pushDraw(line);
    }
}

local function load_gridItems()
{       
    local equipmentList = getEq();

    if(equipmentList.len() <= 0)
    {
        equipment_Elements.textureDescription.setVisible(false);
        return;
    }

    foreach(index, item in equipmentList)
    {
        local template = item.getTemplate();

        local render = GUI.GridRender(template.getVisual())
            .setColor(GUI.Color(255, 255, 255))
            .setItem(item);

        local amount = item.getAmount();

        if(amount != 1) 
        {
            render.setText(amount);
        }

        if(isItemEquipped(template.getId()))
        {
            render
            .setTexture("SLOT_EQ_EQUIPPED.TGA")
            .setFocusTexture("SLOT_EQ_EQUIPPED_HIGHLIGHTED.TGA");          
        }
        else
        {
            render
            .setTexture("SLOT_EQ.TGA")
            .setFocusTexture("SLOT_EQ_HIGHLIGHTED.TGA");
        }

        equipment_Elements.gridEquipment.pushRender(render);  
    } 

    load_itemDescription(0);
}

local function updateGridTextures()
{
    local grid = equipment_Elements.gridEquipment.getRenders();

    foreach(render in grid)
    {
        local template = render.getItem().getTemplate();

        if(isItemEquipped(template.getId())) 
            render.setTexture("SLOT_EQ_EQUIPPED.TGA").setFocusTexture("SLOT_EQ_EQUIPPED_HIGHLIGHTED.TGA")           
        else
            render.setTexture("SLOT_EQ.TGA").setFocusTexture("SLOT_EQ_HIGHLIGHTED.TGA");
    }
}

local function updateItemInSlot(slotId)
{
    local template = activeItem.getTemplate(), itemId = template.getId();
    
    if(isItemEquipped(itemId))
    {
        if(unequipItem(heroId, itemId) == false)
        {
            GUI.GameText(GUI.PositionPr(50, 50), "EQUIPMENT_VIEW_EQUIP_FAULT", "FONT_DEFAULT", 1500.0, true);               
        }
    }
    else equipItem(heroId, itemId, slotId);

    updateGridTextures();
}

local function useItem_Mechanic(item)
{
    local template = item.getTemplate();

    if(template.isEquippable())
    {
        local itemId = template.getId();

        if(isItemEquipped(itemId) == false)
        {
            if(equipItem(heroId, itemId) == false)
            {
                GUI.GameText(GUI.PositionPr(50, 50), "EQUIPMENT_VIEW_EQUIP_FAULT", "FONT_DEFAULT", 1500.0, true);               
            }
        }
        else 
        {
            if(unequipItem(heroId, itemId) == false)
            {
                GUI.GameText(GUI.PositionPr(50, 50), "EQUIPMENT_VIEW_EQUIP_FAULT", "FONT_DEFAULT", 1500.0, true);               
            }
        }
    }
    else 
    {
        if(template.isUsable())
        {
            local oldAmount = item.getAmount();

            if(useItem(heroId, template.getId())) 
            {
                local grid = equipment_Elements.gridEquipment,
                    amount = oldAmount - 1;

                if(amount > 0)
                    grid.getRenders()[activeRenderId].setText(amount);        
                else 
                    grid.removeRender(activeRenderId);    
            }
        }
    }

    updateGridTextures();
}

local function dropActiveItem()
{
    if(drop_View.isVisible() == false)
    {
        local template = activeItem.getTemplate();

        if(isItemEquipped(template.getId()))
        {
            GUI.GameText(GUI.PositionPr(50, 50), "EQUIPMENT_VIEW_DROP_EQUIPED", "FONT_DEFAULT", 1500.0, true);               
        }
        else if(template.isBounded())
        {
            GUI.GameText(GUI.PositionPr(50, 50), "EQUIPMENT_VIEW_DROP_BOUNDED", "FONT_DEFAULT", 1500.0, true);               
        }
        else
            drop_View.setVisible(true);
    }
    else 
        drop_View.setVisible(false);
}

local function rotation() 
{
    if(isKeyPressed(KEY_LCONTROL))
    {
        if(rotationCamera == false)
        {
            Camera.mode = "CAMMODINVENTORY";
            Camera.movementEnabled = true;   

                setCursorVisible(false);   
                
            rotationCamera = true;
        }
    }
    else 
    {
        if(rotationCamera == true)
        {
            Camera.mode = "CAMMODNORMAL";
            Camera.movementEnabled = false;   

                setCursorVisible(true);   

            rotationCamera = false;    
        }
    }
}

equipment_Elements.buttonGold
.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible == false) return;

    local item = hasItem(getItemTemplateIdByInstance("ITMI_GOLD"));
   
    if(item)
        setText("ITMI_GOLD;: " + item.getAmount());
    else 
        setText("ITMI_GOLD;: 0");
});

equipment_Elements.gridEquipment
.addEvent(GUIEventInterface.FocusElement, function(focusId) 
{
    if(drop_View.isVisible()) return;
    
    activeItem = getRenders()[focusId].getItem();
    activeRenderId = focusId;

    load_itemDescription(focusId);
});

equipment_Elements.gridEquipment
.addEvent(GUIEventInterface.Click, function(selectId) 
{
    if(canUseEquipment() == false) return;
    
    if(isMouseBtnPressed(MOUSE_LMB))
    {
            activeRenderId = selectId;
        useItem_Mechanic(getRenders()[selectId].getItem()); 
    }
});

drop_Elements.button
.addEvent(GUIEventInterface.Click, function() 
{
    local inputAmount_Value = drop_Elements.input.getInputValue();
    
    if(inputAmount_Value <= 0)
    {
        GUI.GameText(GUI.PositionPr(50, 50), "EQUIPMENT_VIEW_DROP_AMOUNT", "FONT_DEFAULT", 1500.0, true);               
    }
    else 
    {
        local oldAmount = activeItem.getAmount();

        if(dropItem(activeItem.getTemplate().getId(), inputAmount_Value))
        {
            drop_View.setVisible(false);
            
            local grid = equipment_Elements.gridEquipment;

            if(oldAmount <= inputAmount_Value)
            {
                grid.removeRender(activeRenderId);   
            }
            else 
            {
                grid.getRenders()[activeRenderId].setText(oldAmount - inputAmount_Value);        
            }
        }
        else 
            GUI.GameText(GUI.PositionPr(50, 50), "EQUIPMENT_VIEW_DROP_AMOUNT", "FONT_DEFAULT", 1500.0, true);               
    }
});

addEventHandler("View.onViewVisible", function(visible, viewId)
{
    if(viewId != "VIEW_EQUIPMENT") return;
    
    qs_checkPositionForEquipment(visible);

    if(visible)
    {
            load_gridItems();
        addEventHandler("onRender", rotation);
    }
    else 
    {
        if(rotationCamera) Camera.mode = "CAMMODNORMAL";

        removeEventHandler("onRender", rotation);

        equipment_Elements.gridEquipment.clearRenders();
        equipment_Elements.drawDescription.clearDraws();

        activeItem = null;
        activeRenderId = -1;

        drop_View.setVisible(visible);
    }   
});

addEventHandler("onKey", function(key)
{
    if(equipment_View.isVisible() && canUseEquipment())
    {
        if(drop_View.isVisible() == false)
        {
            switch(key)
            {
                case KEY_0: updateItemInSlot(0); break;
                case KEY_1: updateItemInSlot(1); break;
                case KEY_2: updateItemInSlot(2); break;
                case KEY_3: updateItemInSlot(3); break;
                case KEY_4: updateItemInSlot(4); break;
                case KEY_5: updateItemInSlot(5); break;
                case KEY_6: updateItemInSlot(6); break;
                case KEY_7: updateItemInSlot(7); break;
                case KEY_8: updateItemInSlot(8); break;
                case KEY_9: updateItemInSlot(9); break;
                case KEY_E: 
                case KEY_RETURN: 
                    useItem_Mechanic(activeItem); 
                break;
            }
        }
        
        if(key == KEY_Q) dropActiveItem();
    }
});

equipment_View
.pushObject(equipment_Elements.textureEquipment)
.pushObject(equipment_Elements.textureDescription)
.pushObject(equipment_Elements.renderDescription)
.pushObject(equipment_Elements.drawDescription)
.pushObject(equipment_Elements.gridEquipment)
.pushObject(equipment_Elements.drawInfo)
.pushObject(equipment_Elements.buttonGold);

drop_View
.pushObject(drop_Elements.texture)
.pushObject(drop_Elements.input)
.pushObject(drop_Elements.button);

registerInterfaceView(equipment_View, KEY_TAB, false);
//registerInterfaceView(equipment_View, KEY_BACK, false);