
local activeOption = 0,
    activeResolution = 0;

// INTERFACE
local options_View = GUI.View("VIEW_OPTIONS");

local options_Size = {
    textureBackground = GUI.SizePr(50, 62.5),
    lineOption = GUI.SizePr(16, 6)
}

local options_Position = {
    textureBackground = GUI.PositionPr(25, 15),
    listOptions = GUI.PositionPr(42, 23),
}

local options_Elements = {
    textureBackground = GUI.Texture("BACKGROUND_BORDER_1024x512.TGA", options_Position.textureBackground, options_Size.textureBackground),
    listOptions = GUI.List(options_Position.listOptions, 7)
        .setLineSize(options_Size.lineOption)	
        .setScrollable(true)
        .setLineGap(75),
    optionResolution = null,
    optionMusic = null,
    optionSoundVolume = null,
    optionMusicVolume = null,
    optionChatLines = null,
    optionSaveLogin = null,
    optionWeaponTrail = null,
};

// Local functions 
local function swipe_left()
{
    switch(activeOption)
    {
        case 0: 
        {   
            local resolutionsList = getAvailableResolutions();
            
            local diff = activeResolution - 1;
            local resolution = resolutionsList[activeResolution = (diff < 0 ? resolutionsList.len() - 1 : diff)];

            options_Elements.optionResolution.setText("OPTIONS_VIEW_RESOLUTION;: " + resolution.x + "x" + resolution.y);
        } 
        break;
        case 2: 
        {   
            local diff = ceil(getSoundVolume() * 100) / 100.0 - 0.02;
            if(diff < 0.0) diff = 0.0;

                getLocalOptions().soundVolume(diff);
            options_Elements.optionSoundVolume.setText("OPTIONS_VIEW_SOUND_VOLUME;: " + (diff * 100) + "%");
        } 
        break;
        case 3: 
        {   
            local diff = ceil(getMusicVolume() * 100.0) / 100.0 - 0.02;
            if(diff < 0.0) diff = 0.0;
                
                getLocalOptions().musicVolume(diff);
            options_Elements.optionMusicVolume.setText("OPTIONS_VIEW_MUSIC_VOLUME;: " + (diff * 100) + "%");
        } 
        break; 
        case 4: 
        {   
            local suma = getLocalOptions().getChatLines() - 1;
            if(suma <= 0) suma = 30;
                        
                getLocalOptions().chatLines(suma);
            options_Elements.optionChatLines.setText("OPTIONS_VIEW_CHAT_LINES;: " + suma);
        } 
        break;    
    }
}

local function swipe_right()
{
    switch(activeOption)
    {
        case 0: 
        {   
            local resolutionsList = getAvailableResolutions();

            local suma = activeResolution + 1;
            local resolution = resolutionsList[activeResolution = ((suma > resolutionsList.len() - 1) ? 0 : suma)];

            options_Elements.optionResolution.setText("OPTIONS_VIEW_RESOLUTION;: " + resolution.x + "x" + resolution.y);
        } 
        break;
        case 2: 
        {   
            local suma = ceil(getSoundVolume() * 100.0) / 100.0 + 0.02;
            if(suma > 1.0) suma = 1.0;

                getLocalOptions().soundVolume(suma);
            options_Elements.optionSoundVolume.setText("OPTIONS_VIEW_SOUND_VOLUME;: " + (suma * 100) + "%");
        } 
        break;
        case 3: 
        {   
            local suma = ceil(getMusicVolume() * 100.0) / 100.0 + 0.02;
            if(suma > 1.0) suma = 1.0;
                        
                getLocalOptions().musicVolume(suma);
            options_Elements.optionMusicVolume.setText("OPTIONS_VIEW_MUSIC_VOLUME;: " + (suma * 100) + "%");
        } 
        break;
        case 4: 
        {   
            local suma = getLocalOptions().getChatLines() + 1;
            if(suma > 30) suma = 0;
                        
                getLocalOptions().chatLines(suma);
            options_Elements.optionChatLines.setText("OPTIONS_VIEW_CHAT_LINES;: " + suma);
        } 
        break;      
    }
}

local function click_resolution()
{
    setInterfaceVisible(true, "VIEW_PHOTO");
    
    local resolution = getAvailableResolutions()[activeResolution];

    setTimer(function() {
        setResolution(resolution.x, resolution.y, 32);
        setInterfaceVisible(false, "VIEW_PHOTO");
    }, 250, 1);
}

local function click_music()
{
    local localOptions = getLocalOptions();
    local status = localOptions.isMusicDisabled();

        localOptions.music(status = !status);
    options_Elements.optionMusic.setText("OPTIONS_VIEW_DISABLE_MUSIC_SYSTEM;: ;" + (status ? "OPTIONS_VIEW_YES" : "OPTIONS_VIEW_NO"));
}

local function click_weaponTrail()
{
    local localOptions = getLocalOptions();
    local status = localOptions.isWeaponTrailEnabled();

        localOptions.weaponTrail(status = !status);
    options_Elements.optionWeaponTrail.setText("OPTIONS_VIEW_WEAPON_TRAIL;: ;" + (status ? "OPTIONS_VIEW_YES" : "OPTIONS_VIEW_NO"));
}

local function click_saveLogin()
{
    local localOptions = getLocalOptions();
    local status = localOptions.isLoginSaved();

        localOptions.saveLogin(status = !status);
    options_Elements.optionSaveLogin.setText("OPTIONS_VIEW_SAVE_LOGIN;: ;" + (status ? "OPTIONS_VIEW_YES" : "OPTIONS_VIEW_NO"));
}

local function getResolutionId()
{
    local currentResolution = getResolution();

    foreach(index, resolution in getAvailableResolutions())
    {
        if(currentResolution.x == resolution.x && currentResolution.y == resolution.y)
        {
            return index;
        }
    }

    return -1;
}

options_Elements.listOptions
.pushListButton(
    options_Elements.optionResolution = GUI.ListText("OPTIONS_VIEW_RESOLUTION") // OPTION 0 
    .setTexture(null)
    .setFont("FONT_OLD_20_WHITE_HI")
    .addEvent(GUIEventInterface.Click, click_resolution)
)
.pushListButton(
    options_Elements.optionMusic = GUI.ListText("OPTIONS_VIEW_DISABLE_MUSIC_SYSTEM") // OPTION 1
    .setTexture(null)
    .setFont("FONT_OLD_20_WHITE_HI")
    .addEvent(GUIEventInterface.Click, click_music)
)
.pushListButton(
    options_Elements.optionSoundVolume = GUI.ListText("OPTIONS_VIEW_SOUND_VOLUME") // OPTION 2
    .setTexture(null)
    .setFont("FONT_OLD_20_WHITE_HI")
)
.pushListButton(
    options_Elements.optionMusicVolume = GUI.ListText("OPTIONS_VIEW_MUSIC_VOLUME") // OPTION 3
    .setTexture(null)
    .setFont("FONT_OLD_20_WHITE_HI")
)
.pushListButton(
    options_Elements.optionChatLines = GUI.ListText("OPTIONS_VIEW_CHAT_LINES") // OPTION 4
    .setTexture(null)
    .setFont("FONT_OLD_20_WHITE_HI")
)
.pushListButton(
    options_Elements.optionSaveLogin = GUI.ListText("OPTIONS_VIEW_SAVE_LOGIN") // OPTION 5
    .setTexture(null)
    .setFont("FONT_OLD_20_WHITE_HI")
    .addEvent(GUIEventInterface.Click, click_saveLogin)
)
.pushListButton(
    options_Elements.optionWeaponTrail = GUI.ListText("OPTIONS_VIEW_WEAPON_TRAIL") // OPTION 6
    .setTexture(null)
    .setFont("FONT_OLD_20_WHITE_HI")
    .addEvent(GUIEventInterface.Click, click_weaponTrail)
);

options_Elements.listOptions
.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible)
    {           
        activeResolution = getResolutionId();

        local elements = options_Elements, 
            resolution = getAvailableResolutions()[activeResolution];

        local localOptions = getLocalOptions();

        elements.optionResolution.setText("OPTIONS_VIEW_RESOLUTION;: " + resolution.x + "x" + resolution.y);
        elements.optionMusic.setText("OPTIONS_VIEW_DISABLE_MUSIC_SYSTEM;: ;" + (localOptions.isMusicDisabled() ? "OPTIONS_VIEW_YES" : "OPTIONS_VIEW_NO"));
        elements.optionSoundVolume.setText("OPTIONS_VIEW_SOUND_VOLUME;: " + (localOptions.getSound() * 100) + "%");
        elements.optionMusicVolume.setText("OPTIONS_VIEW_MUSIC_VOLUME;: " + (localOptions.getMusic() * 100) + "%");
        elements.optionChatLines.setText("OPTIONS_VIEW_CHAT_LINES;: " + (localOptions.getChatLines()));
        elements.optionSaveLogin.setText("OPTIONS_VIEW_SAVE_LOGIN;: ;" + (localOptions.isLoginSaved() ? "OPTIONS_VIEW_YES" : "OPTIONS_VIEW_NO"));
        elements.optionWeaponTrail.setText("OPTIONS_VIEW_WEAPON_TRAIL;: ;" + (localOptions.isWeaponTrailEnabled() ? "OPTIONS_VIEW_YES" : "OPTIONS_VIEW_NO"));
    }
});

options_Elements.listOptions
.addEvent(GUIEventInterface.FocusElement, function(index) 
{
    activeOption = index;
});

addEventHandler("onKey", function(key) 
{
    if(options_View.isVisible() == false || isEventCancelled()) return;

    switch(key)
    {
        case KEY_A:
        case KEY_LEFT: 
            swipe_left(); 
        break;
        case KEY_D:
        case KEY_RIGHT: 
            swipe_right(); 
        break;

    }
});

// REGISTER ELEMENTS
options_View
.pushObject(options_Elements.textureBackground)
.pushObject(options_Elements.listOptions);

registerInterfaceView(options_View);