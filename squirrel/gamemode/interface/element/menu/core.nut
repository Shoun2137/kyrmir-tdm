
local exit_Timer = -1;

// INTERFACE
local main_View = GUI.View("VIEW_MAIN");

local main_Size = {
    textureLogo = GUI.SizePr(28.1, 25),
    textureBackground = GUI.SizePr(50, 62.5),
    lineOption = GUI.SizePr(16, 6),
    buttonFlag = GUI.SizePr(4, 7.1),
    textureFlags = GUI.SizePr(21, 8)
}

local main_Position = {
    textureLogo = GUI.PositionPr(35.95, 16),
    textureBackground = GUI.PositionPr(25, 15),
    listOptions = GUI.PositionPr(42, 40),
    drawFlags = GUI.PositionPr(0, 0),
    buttonsFlags = GUI.PositionPr(0, 0),
    textureFlags = GUI.PositionPr(74, 87)
}

local main_Elements = {
    textureBackground = GUI.Texture("BACKGROUND_BORDER_1024x512.TGA", main_Position.textureBackground, main_Size.textureBackground),
    textureLogo = GUI.Texture("LOGO_KYRMIR.TGA", main_Position.textureLogo, main_Size.textureLogo),
    listOptions = GUI.List(main_Position.listOptions, 5)
        .setLineSize(main_Size.lineOption)	
        .setScrollable(true)
        .setLineGap(50),
    drawFlags = GUI.Draw("MAIN_VIEW_LANG_SELECT", main_Position.drawFlags),
    buttonsFlags = [
        GUI.Button("", main_Position.buttonsFlags, main_Size.buttonFlag)
            .setTexture("ICON_POL.TGA")
            .addEvent(GUIEventInterface.Click, function() 
            {
                setLanguage(LANG.PL);
            }),
        GUI.Button("", main_Position.buttonsFlags, main_Size.buttonFlag)
            .setTexture("ICON_ENG.TGA")
            .addEvent(GUIEventInterface.Click, function() 
            {
                setLanguage(LANG.EN);
            }),
        GUI.Button("", main_Position.buttonsFlags, main_Size.buttonFlag)
            .setTexture("ICON_GER.TGA")
            .addEvent(GUIEventInterface.Click, function() 
            {
                setLanguage(LANG.DE);
            }),
        GUI.Button("", main_Position.buttonsFlags, main_Size.buttonFlag)
            .setTexture("ICON_RU.TGA")
            .addEvent(GUIEventInterface.Click, function() 
            {
                setLanguage(LANG.RU);
            })
    ]
};

// SIDE FLAG DRAW
main_Position.drawFlags.setXPr(main_Position.textureFlags.getXPr() + 1);
main_Position.drawFlags.setYPr(main_Position.textureFlags.getYPr() - 2.5);

// SIDE FLAG BUTTONS TO LEFT WITH GAPS
main_Position.buttonsFlags.setXPr(main_Position.textureFlags.getXPr() + 1);
main_Position.buttonsFlags.setYPr(main_Position.textureFlags.getYPr() + 0.45);

foreach(index, button in main_Elements.buttonsFlags)
{
    button.setPosition(GUI.PositionPr(main_Position.buttonsFlags.getXPr() + index * (main_Size.buttonFlag.getWidthPr() + 1), main_Position.buttonsFlags.getYPr()));
}

// FUNCTIONS 
function exitTimer()
{
    local exitTick = 10;

    setInterfaceVisible(true, "VIEW_PHOTO");
    lockInterface(true);
    
    GUI.GameText(GUI.PositionPr(50, 50), getFormatTranslation("POP-UP_MSG_EXIT", exitTick), "FONT_OLD_20_WHITE_HI", 1000.0, true, 255, 0, 0); 

    exit_Timer = setTimer(function() 
    {            
        GUI.GameText(GUI.PositionPr(50, 50), getFormatTranslation("POP-UP_MSG_EXIT", --exitTick), "FONT_OLD_20_WHITE_HI", 1000.0, true, 255, 0, 0); 
    
        if(exitTick == 0)
            exitGame();
    }
    , 1000, exitTick);
}

main_Elements.listOptions
.pushListButton(
    GUI.ListText("MAIN_VIEW_OPTIONS")
    .setTexture(null)
    .setFont("FONT_OLD_20_WHITE_HI")
    .addEvent(GUIEventInterface.Click, function() 
    {
        setInterfaceVisible(true, "VIEW_OPTIONS"); 
    })
)
.pushListButton(
    GUI.ListText("MAIN_VIEW_VISUAL")
    .setTexture(null)
    .setFont("FONT_OLD_20_WHITE_HI")
    .addEvent(GUIEventInterface.Click, function() 
    {
        setInterfaceVisible(true, "VIEW_VISUAL"); 
    })
)
.pushListButton(
    GUI.ListText("MAIN_VIEW_GUILD")
    .setTexture(null)
    .setFont("FONT_OLD_20_WHITE_HI")
    .addEvent(GUIEventInterface.Click, function() 
    {
        if((isHeroInArea(getPlayerGuild(heroId)) && getLevel() <= 3) || isRoundStarted() == false)
        {
            setInterfaceVisible(true, "VIEW_GUILD"); 
        }
        else 
            GUI.GameText(GUI.PositionPr(50, 7.5), "POP-UP_MSG_GUILD", "FONT_DEFAULT", 2000.0, true, 255, 0, 0);               
    })
)
.pushListButton(
    GUI.ListText("MAIN_VIEW_HELP")
    .setTexture(null)
    .setFont("FONT_OLD_20_WHITE_HI")
    .addEvent(GUIEventInterface.Click, function() 
    {
        setInterfaceVisible(true, "VIEW_HELP"); 
    })
)
.pushListButton(
    GUI.ListText("MAIN_VIEW_EXIT")
    .setTexture(null)
    .setFont("FONT_OLD_20_WHITE_HI")
    .addEvent(GUIEventInterface.Click, function() 
    {
        exitTimer();
    })
);

// REGISTER ELEMENTS
main_View
.pushObject(main_Elements.textureBackground)
.pushObject(main_Elements.textureLogo)
.pushObject(main_Elements.listOptions)
.pushObject(main_Elements.drawFlags);
//.pushObject(main_Elements.textureFlags)

foreach(button in main_Elements.buttonsFlags) main_View.pushObject(button);

registerInterfaceView(main_View, KEY_ESCAPE, true);