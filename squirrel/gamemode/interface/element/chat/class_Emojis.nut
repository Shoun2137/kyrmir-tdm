
class ChatInterface.Emojis
{
    static _emojiList = {};

    static function registerEmoji(lang, id, char)
    {
        if(lang in _emojiList)
        {
            _emojiList[lang][id] <- char;
        }
        else 
        {
            _emojiList[lang] <- {};
            _emojiList[lang][id] <- char;
        }
    }

    static function letterFillter(lang, letter)
    {
        local emojiList = _emojiList[lang];

        foreach(index, char in emojiList)
        {    
            if(char == letter)
                return index;
        }

        return false;
    }

    static function translateToEmojis(lang, message)
    {
        local messageFiltred = "";

        foreach(letter in message)
        {
            if(letterFillter(lang, letter) == false)
                messageFiltred += letter.tochar();
        }
           
        local translation = "", separate = split(messageFiltred, " ");
        local emojiList = _emojiList[lang];

        foreach(index, word in separate)
        {
            if(word in emojiList)
            {
                translation += emojiList[word].tochar();
            }
            else 
                translation += (" " + word);
        }

        return lstrip(translation);
    }

    static function getEmojis(lang)
    {
        return lang in _emojiList ? _emojiList[lang] : null;
    }
}

// GLOBALS
function registerEmoji(lang, id, char)
{
    ChatInterface.Emojis.registerEmoji(lang, id, char);
}

function translateToEmojis(lang, message)
{
    return ChatInterface.Emojis.translateToEmojis(lang, message);
}

function getEmojis(lang)
{
    return ChatInterface.Emojis.getEmojis(lang);
}