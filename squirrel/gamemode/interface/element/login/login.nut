
local loginTheme = Sound("GOTHIC_MAIN_THEME.WAV");

loginTheme.looping = true;
loginTheme.volume = 0.6;

// INTERFACE
local login_View = GUI.View("VIEW_LOGIN");

local login_Size = {
    textureBackground = GUI.SizePr(45, 38),
    inputs = GUI.SizePr(28, 4.25),
    textureIcons = null,
    buttonsLogin = GUI.SizePr(13.5, 6),
    buttonExit = null,
    buttonFlag = GUI.SizePr(4, 7.1),
    textureFlags = GUI.SizePr(21, 8),
};

// COPY HEIGHT FROM INPUTS FOR ICON TEXTURES AND EXIT BUTTON
local inputHeight = login_Size.inputs.getHeightPx();

login_Size.textureIcons = GUI.SizePx(inputHeight, inputHeight);
login_Size.buttonExit = GUI.SizePx(inputHeight, inputHeight);

local login_Position = {
    textureBackground = GUI.PositionPr(27.5, 20),
    gameText = GUI.PositionPr(50, 15),
    drawTitle = GUI.PositionPr(50, 21.5),
    drawSubtitle = GUI.PositionPr(50, 26),
    inputUsername = GUI.PositionPr(0, 32),
    inputPassword = GUI.PositionPr(0, 38),
    textureUsername = GUI.PositionPr(0, 32),
    texturePassword = GUI.PositionPr(0, 38),
    buttonLogin = GUI.PositionPr(0, 45),
    buttonRegister = GUI.PositionPr(0, 45),
    buttonExit = GUI.PositionPr(0, 20),
    drawFlags = GUI.PositionPr(0, 0),
    buttonsFlags = GUI.PositionPr(0, 0),
    textureFlags = GUI.PositionPr(74, 87)
};

// CENTER INPUTS = TEXTURE.WIDTH + INPUT.WIDTH
local iconsWidth = login_Size.textureIcons.getWidthPx();
local centerX = getResolution().x / 2 - (iconsWidth + login_Size.inputs.getWidthPx()) / 2;

login_Position.textureUsername.setXPx(centerX);
login_Position.texturePassword.setXPx(centerX);

login_Position.inputUsername.setXPx(iconsWidth + centerX);
login_Position.inputPassword.setXPx(iconsWidth + centerX);

// CENTER LOGIN BUTTONS 
login_Position.buttonLogin.setXPr(login_Position.textureUsername.getXPr() + 1);
login_Position.buttonRegister.setXPr(login_Position.inputUsername.getXPr() + login_Size.inputs.getWidthPr() - login_Size.buttonsLogin.getWidthPr() - 1);

// SIDE EXIT BUTTON TO RIGHT
login_Position.buttonExit.setXPr(login_Position.textureBackground.getXPr() + login_Size.textureBackground.getWidthPr() - login_Size.buttonExit.getWidthPr());

local login_Elements = {
    textureBackground = GUI.Texture("BACKGROUND_BORDER_1024x512.TGA", login_Position.textureBackground, login_Size.textureBackground),
    drawTitle = GUI.Draw("LOGIN_VIEW_TITLE", login_Position.drawTitle)
        .setFont("FONT_OLD_20_WHITE_HI")
        .setCenterAtPosition(true),
    drawSubtitle = GUI.Draw("LOGIN_VIEW_SUBTITLE", login_Position.drawSubtitle)
        .setFont("FONT_OLD_10_WHITE_HI")
        .setCenterAtPosition(true),
    inputUsername = GUI.Input("LOGIN_VIEW_USERNAME", login_Position.inputUsername, login_Size.inputs)
        .setTexture("INPUT_512X64.TGA"),
    inputPassword = GUI.Input("LOGIN_VIEW_PASSWORD", login_Position.inputPassword, login_Size.inputs, true)
        .setTexture("INPUT_512X64.TGA"),
    textureUsername = GUI.Texture("ICON_USERNAME_64X64.TGA", login_Position.textureUsername, login_Size.textureIcons),
    texturePassword = GUI.Texture("ICON_PASSWORD_64X64.TGA", login_Position.texturePassword, login_Size.textureIcons),
    buttonLogin = GUI.Button("LOGIN_VIEW_BTN_LOGIN", login_Position.buttonLogin, login_Size.buttonsLogin)
        .setTexture("BUTTON_YELLOW_256X64.TGA"),
    buttonRegister = GUI.Button("LOGIN_VIEW_BTN_REGISTER", login_Position.buttonRegister, login_Size.buttonsLogin)
        .setTexture("BUTTON_RED_256X64.TGA"),
    buttonExit = GUI.Button("X", login_Position.buttonExit, login_Size.buttonExit)
    //textureFlags = GUI.Texture("MENU_INGAME.TGA", login_Position.textureFlags, login_Size.textureFlags),
    drawFlags = GUI.Draw("LOGIN_VIEW_LANG_SELECT", login_Position.drawFlags),
    buttonsFlags = [
        GUI.Button("", login_Position.buttonsFlags, login_Size.buttonFlag)
            .setTexture("ICON_POL.TGA")
            .addEvent(GUIEventInterface.Click, function() 
            {
                setLanguage(LANG.PL);
            }),
        GUI.Button("", login_Position.buttonsFlags, login_Size.buttonFlag)
            .setTexture("ICON_ENG.TGA")
            .addEvent(GUIEventInterface.Click, function() 
            {
                setLanguage(LANG.EN);
            }),
        GUI.Button("", login_Position.buttonsFlags, login_Size.buttonFlag)
            .setTexture("ICON_GER.TGA")
            .addEvent(GUIEventInterface.Click, function() 
            {
                setLanguage(LANG.DE);
            }),
        GUI.Button("", login_Position.buttonsFlags, login_Size.buttonFlag)
            .setTexture("ICON_RU.TGA")
            .addEvent(GUIEventInterface.Click, function() 
            {
                setLanguage(LANG.RU);
            })
    ]
};

// SIDE FLAG DRAW
login_Position.drawFlags.setXPr(login_Position.textureFlags.getXPr() + 1);
login_Position.drawFlags.setYPr(login_Position.textureFlags.getYPr() - 2.5);

// SIDE FLAG BUTTONS TO LEFT WITH GAPS
login_Position.buttonsFlags.setXPr(login_Position.textureFlags.getXPr() + 1);
login_Position.buttonsFlags.setYPr(login_Position.textureFlags.getYPr() + 0.45);

foreach(index, button in login_Elements.buttonsFlags)
{
    button.setPosition(GUI.PositionPr(login_Position.buttonsFlags.getXPr() + index * (login_Size.buttonFlag.getWidthPr() + 1), login_Position.buttonsFlags.getYPr()));
}

login_View
.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible)
    {
        local options = getLocalOptions();

        local username = options.getUsername();
        local password = options.getPassword();

        if(username != null && password != null)
        {
            login_Elements.inputUsername.setInputText(username);
            login_Elements.inputPassword.setInputText(password);
        }
    }
});

login_Elements.buttonLogin
.addEvent(GUIEventInterface.Click, function() 
{
    local username = login_Elements.inputUsername.getInputValue();
    local password = login_Elements.inputPassword.getInputValue();

    if(username != null && password != null)
    {        
        if(validate_username(username) && validate_password(password))
        {
            getHero().login(username, password);
            
            local options = getLocalOptions();
            
            if(options.isLoginSaved())
            {
                options.saveLoginData(username, password);
            }
        }
        else
            GUI.GameText(GUI.PositionPr(50, 7.5), "LOGIN_VIEW_MSG_USERNAME", "FONT_DEFAULT", 5000.0, true, 190, 15, 15);               
    }
    else 
        GUI.GameText(GUI.PositionPr(50, 7.5), "LOGIN_VIEW_MSG_EMPTY_FIELD", "FONT_DEFAULT", 5000.0, true, 190, 15, 15);               
});

login_Elements.buttonRegister
.addEvent(GUIEventInterface.Click, function() 
{
    lockInterface(false);
	setInterfaceVisible(true, "VIEW_REGISTER");
});

login_Elements.buttonExit
.addEvent(GUIEventInterface.Click, exitGame);

login_View
.addEvent(GUIEventInterface.Visible, function(visible) 
{
    if(visible) lockInterface(true);
});

// CAMERA
local cameraPath = CameraPath()

cameraPath.createPoint({ position = [10498, 9847.35, 38658.9], rotation = [17.3891, 590.494, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [4852.25, 8344.59, 34225.8], rotation = [5.99206, 582.681, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [3593.37, 7963.92, 32135.6], rotation = [8.49206, 539.322, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [4187.61, 7365.3, 28652.5], rotation = [14.3009, 556.639, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [1933.49, 4712.1, 21945.7], rotation = [23.8597, 534.764, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [2402.06, 2976.26, 17655.8], rotation = [21.2127, 552.733, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [172.315, 1121.58, 12209.6], rotation = [14.3744, 568.488, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [-4252.4, -190.083, 9756.37], rotation = [9.88914, 608.592, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [-7257.73, -586.938, 8664.49], rotation = [6.72738, 588.54, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [-9511.85, -774.175, 7303.36], rotation = [4.37444, 499.607, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [-7780.21, -944.726, 5665.07], rotation = [2.53621, 494.79, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [-6676.68, -845.488, 4103.98], rotation = [-10.9197, 503.644, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [-5173.43, -438.885, 3192.06], rotation = [-12.9785, 475.389, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [-3519.37, -78.3623, 2258.52], rotation = [-4.74321, 501.3, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [-2457.91, 137.448, 878.64], rotation = [-15.6256, 500.519, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [736.879, 1965.87, -992.551], rotation = [-5.33146, 494.139, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [2767.81, 1760.93, -3712.94], rotation = [12.5362, 502.342, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [4719.8, 1416.68, -7026.65], rotation = [-0.846157, 513.8, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [5211.44, 2800.59, -13062], rotation = [29.1539, 391.144, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [8414.99, 1971.83, -12621.6], rotation = [15.1097, 467.576, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [10966.2, 1863.49, -14229], rotation = [-3.9344, 504.425, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [13754.1, 2752.21, -18880.1], rotation = [-12.2432, 501.43, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [17458, 2992.23, -21146.5], rotation = [20.9921, 454.685, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [21256.4, 2485.54, -20618.4], rotation = [3.56562, 413.8, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [24624, 3139.5, -19497.1], rotation = [-5.77262, 454.816, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [28061.4, 2797.66, -19822.6], rotation = [19.1539, 412.107, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [30466.7, 2289.49, -17350.4], rotation = [2.24209, 409.373, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [33582.2, 1823.02, -17349], rotation = [8.2715, 471.222, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [35521.7, 1651.45, -18834.7], rotation = [0.771502, 493.357, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [39575.4, 2001.09, -19219.3], rotation = [8.63916, 408.201, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [42280.2, 609.258, -16265.3], rotation = [21.1392, 405.857, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [43379.7, -327.307, -11634.4], rotation = [13.4186, 377.212, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [45017.8, -1607.56, -7287.62], rotation = [17.1686, 397.004, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [48841.1, 1288.28, -3401.55], rotation = [51.2862, 518.748, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [49670.3, 4264.2, -978.317], rotation = [47.1686, 626.821, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [46257.1, 2654.95, 1292.78], rotation = [17.3892, 661.847, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [42370.5, 2395.91, 3571.84], rotation = [0.109769, 654.816, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [40354.8, 570.883, 8944.66], rotation = [24.0068, 643.748, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [37453.5, -858.28, 13764.2], rotation = [20.4774, 615.232, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [32426.5, -910.436, 12170.9], rotation = [-9.30198, 591.925, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [25512.8, 2082.63, 10797.6], rotation = [-9.0814, 638.28, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [21436, 1227.57, 13294.7], rotation = [40.6245, 679.947, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [18044.2, 154.726, 15177], rotation = [9.30093, 642.968, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [13234.9, 594.004, 15908.7], rotation = [8.12446, 612.889, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [7771.84, 562.119, 16003.2], rotation = [7.16858, 642.446, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [4407.47, 1919.19, 17104.9], rotation = [1.50679, 680.597, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [3690.66, 3972.98, 20920.9], rotation = [-15.8462, 679.686, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [2384.49, 5208.85, 23596.8], rotation = [-2.53733, 708.592, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [2739.78, 6405.85, 26160.6], rotation = [3.41854, 752.472, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [5374.1, 6911.29, 28280.1], rotation = [15.698, 665.362, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [5036.79, 6967.35, 30319.6], rotation = [20.845, 629.294, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [4501.01, 6637.68, 31576], rotation = [16.8009, 641.273, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [4047.35, 6897.82, 32992], rotation = [5.03618, 718.357, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [4289.64, 6721.08, 34345.4], rotation = [8.27148, 747.002, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [6282.68, 6388.61, 35319], rotation = [11.2126, 777.601, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [8637.52, 6850.82, 36837.2], rotation = [-9.89029, 751.169, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [11106.9, 9900.87, 37886.7], rotation = [50.3303, 645.179, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [12843.2, 11307.6, 40241], rotation = [37.7568, 632.289, 0.0197823], lerpSpeed = 600 });
cameraPath.createPoint({ position = [10498, 9847.35, 38658.9], rotation = [17.3891, 590.494, 0.0197823], lerpSpeed = 600 });

cameraPath.setLooping(true)

// REGISTER ELEMENTS
login_View
.pushObject(login_Elements.textureBackground)
.pushObject(login_Elements.drawTitle)
.pushObject(login_Elements.drawSubtitle)
.pushObject(login_Elements.inputUsername)
.pushObject(login_Elements.inputPassword)
.pushObject(login_Elements.textureUsername)
.pushObject(login_Elements.texturePassword)
.pushObject(login_Elements.buttonLogin)
.pushObject(login_Elements.buttonRegister)
.pushObject(login_Elements.drawFlags)
//.pushObject(login_Elements.textureFlags)
.pushObject(login_Elements.buttonExit);

foreach(button in login_Elements.buttonsFlags) login_View.pushObject(button);

registerInterfaceView(login_View);

// FUNCTIONS
local inactiveFunc = function(timeMS) 
{
    if(isViewVisible("VIEW_LOGIN"))
    {
        login_View.setVisible(timeMS < 15000);
    }
}

// CALLBACKS
addEventHandler("onInit", function()
{
	setInterfaceVisible(true, "VIEW_LOGIN");
    lockInterface(true);
    
    loginTheme.play();
    cameraPath.start(); 
});

addEventHandler("onInactive", inactiveFunc);

addEventHandler("Player.onHeroLogin", function(responseCode)
{
    switch(responseCode)
    {
        case LoginCode.Succes:
        {
            lockInterface(false);
            setInterfaceVisible(false, "VIEW_LOGIN");

            if(getPlayerGuild(heroId) != null)
            {
                ch_setVisibility(true);
           
                loginTheme.stop();  
                cameraPath.stop();               
            }
            else 
            {
                setInterfaceVisible(true, "VIEW_GUILD");
                    lockInterface(true);
            }

            removeEventHandler("onInactive", inactiveFunc);
        } 
        break;
        case LoginCode.FirstLogin:
        {
            cameraPath.stop();   

            lockInterface(false);
            setInterfaceVisible(true, "VIEW_VISUAL");

            removeEventHandler("onInactive", inactiveFunc); 
        } 
        break;
        break;    
        case LoginCode.Fault:
            GUI.GameText(login_Position.gameText, "LOGIN_VIEW_MSG_FAULT", "FONT_DEFAULT", 5000.0, true, 190, 15, 15);            
        break;
        case LoginCode.NotExist:
            GUI.GameText(login_Position.gameText, "LOGIN_VIEW_MSG_NOT_EXIST", "FONT_DEFAULT", 5000.0, true, 190, 15, 15);            
        break;       
    }
});

addEventHandler("Player.onPlayerChangeGuild", function(playerId, oldGuild, newGuild)
{
    if(playerId != heroId) return;

    if(newGuild != null)
    {
        loginTheme.stop();  
        cameraPath.stop();    
    }
});
