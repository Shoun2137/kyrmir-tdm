
class GUI.Grid extends GUI.Event
{
	_position = null;

    _columnNumber = 0;
    _rowNumber = 0;

	_borderGap = 0;
	
	_cellSize = null;

	_scrollable = false;
	_visible = false;

    _focusedRender = 0;
	_focused = false;

    _renderList = null;

	constructor(position, columnNumber = 5, rowNumber = 5)
    {
        base.constructor();

		_position = position;

        _columnNumber = columnNumber;
        _rowNumber = rowNumber;

		_borderGap = 0;

		_cellSize = GUI.SizePr(5, 5);

		_scrollable = false;
		_visible = false;
		
		_focusedRender = 0;
		_focused = false;

		_renderList = [];
    }

//:public
	function destroy()
	{
		setVisible(false);
			clearRenders();
		
		_renderList = null;
			base.destroy();
	}

	function setScrollable(scroll) 
	{
		_scrollable = scroll;
			return this;
	}

	function isScrollable() { return _scrollable; }

	function setPosition(position)
	{
		local position = (_position = position).getPosition();

		if(_visible) 	
		{
			local numberInLine = 0, numberInColumn = 0;

			foreach(i, render in _renderList)
			{
				if(render.isVisible())
				{
					local size = render.getSize();
	
					if(i % _columnNumber != 0 || i == 0)
						render.setPosition(GUI.Position(position.x + (size.getWidth() + _borderGap) * numberInLine++, position.y + (size.getHeight() + _borderGap) * numberInColumn))
							.setVisible(true);
					else 
					{
						numberInLine = 0;
						
						render.setPosition(GUI.Position(position.x + (size.getWidth() + _borderGap) * numberInLine++, position.y + (size.getHeight() + _borderGap) * ++numberInColumn))
							.setVisible(true);
					}
				}
			}
		}
		
		return this;
	}

    function getPosition()
    {
        return _position;
    }

    function setBorderGap(gap)
    {
		_borderGap = gap;
			return this;
    }

    function getBorderGap() { return _borderGap; }

	function setCellSize(size)
	{
		_cellSize = size;

		foreach(render in _renderList)
			render.setSize(size);
		
		return this;
	}

	function getCellSize() { return _cellSize; }

    function setVisible(visible)
    {
		if(visible != _visible)
		{
			if(visible)
			{								
				local gridSize = _renderList.len();
				local lastRender = _rowNumber * _columnNumber;

				for(local i = 0; i < lastRender; i++)
				{
					if(i >= gridSize) break;

					local noZero = i + 1;
					local numberInColumn = ceil(noZero / _columnNumber.tofloat()) - 1;	
					
					local render = _renderList[i];

					local position = _position.getPosition(),
						size = render.getSize();

					render
					.setPosition(GUI.Position(position.x + (size.getWidth() + _borderGap) * (noZero - numberInColumn * _columnNumber - 1), 
						position.y + (size.getHeight() + _borderGap) * numberInColumn))
					.setVisible(true);
				}

				if(gridSize == 1) _renderList[_focusedRender].setFocus(true);

				getSceneManager().pushObject(this);					
			}
			else
			{
				_focusedRender = 0;

				foreach(render in _renderList)
					render.setVisible(false);
		
				_focused = false;

				getSceneManager().removeObject(this);	
			}
			
			callEvent(GUIEventInterface.Visible, _visible = visible);
		}

		return this;
    }

	function reloadGrid()
	{
		if(_visible)
		{
			foreach(render in _renderList)
				render.setVisible(false);

			local gridSize = _renderList.len();
			local lastRender = _rowNumber * _columnNumber;
		
			for(local i = 0; i < lastRender; i++)
			{				
				if(i >= gridSize) break;
				
				local noZero = i + 1;
				local numberInColumn = ceil(noZero / _columnNumber.tofloat()) - 1;	

				local render = _renderList[i];

				local position = _position.getPosition(),
					size = render.getSize();

				render
				.setPosition(GUI.Position(position.x + (size.getWidth() + _borderGap) * (noZero - numberInColumn * _columnNumber - 1), 
					position.y + (size.getHeight() + _borderGap) * numberInColumn))
				.setVisible(true);
			}

			_renderList[_focusedRender].setFocus(true);
		}
	}

	function setFocusedRender(focusedRender) 
	{		
		foreach(render in _renderList)
		{
			if(render != focusedRender)
				render.setFocus(false);
		}

		if(typeof(focusedRender) == "instance")
		{
			local focusId = _renderList.find(focusedRender);
			
			if(focusId != null)
			{ 
				_focusedRender = focusId;
			}
		}
		
		callEvent(GUIEventInterface.FocusElement, _focusedRender);
	}

	function getFocusedRender() { return _focusedRender; }

	function mouseClick()
	{
		return callEvent(GUIEventInterface.Click, _focusedRender);
	}

	function pushRender(render)
	{
		if(render instanceof GUI.GridRender)
		{
			_renderList.push(render.setGrid(this).setSize(_cellSize));

			if(_visible)
			{
				local lastRender = _rowNumber * _columnNumber;
				local arraySize = _renderList.len();

				if(arraySize <= lastRender)
				{
					local numberInColumn = ceil(arraySize / _columnNumber.tofloat()) - 1;	
					local numberInLine = arraySize - numberInColumn * _columnNumber - 1;
					
					local position = _position.getPosition(),
						size = render.getSize();

					render
					.setPosition(GUI.Position(position.x + (size.getWidth() + _borderGap) * numberInLine, 
						position.y + (size.getHeight() + _borderGap) * numberInColumn))
					.setVisible(true);
				}
				
				if(arraySize == 1) render.setFocus(true);
			}
		}
	}

	function removeRender(render)
	{
		if(typeof(render) == "integer")
		{
			if(render < _renderList.len())
			{
				render = _renderList[render];
			}
		}			

		local id = _renderList.find(render);
				
		if(id != null)
		{
			render.destroy();
				_renderList.remove(id);
		}
		
		local renderSize = _renderList.len();

		if(renderSize > 0)
		{
			if(_focusedRender == id)
			{
				setFocusedRender(0);
			}

			reloadGrid();
		}
		else 
			setVisible(false);
	}

	function clearRenders()
	{
		_focusedRender = 0;

		foreach(render in _renderList)
			render.destroy();

		_renderList.clear();
	}
	
	function getRenders() { return _renderList; }	

//:protected
	function updateGrid(newFocusNumber)
	{
		local gridSize = _renderList.len();

		if(newFocusNumber < 0) 
			newFocusNumber = 0;
		else 
		{
			if(newFocusNumber >= gridSize)
				newFocusNumber = gridSize - 1;
		}
		
		_renderList[_focusedRender].setFocus(false);
	
		local numberInLine = 0, numberInColumn = 0;

		if(newFocusNumber > _focusedRender)
		{
			if(!_renderList[newFocusNumber].isVisible())
			{
				local currentRow = ceil((_focusedRender + 1) / _columnNumber.tofloat());	

				local lastCurrentTopRender = (currentRow - _rowNumber + 1) * _columnNumber;
				local lastNewBottomRender = currentRow * _columnNumber + _columnNumber;

				foreach(i, render in _renderList)
				{
					if(i < lastCurrentTopRender)
					{
						render.setVisible(false);
					}
					else if(i < lastNewBottomRender)
					{
						local size = render.getSize();
						local position = _position.getPosition();

						if(i % _columnNumber != 0 || i == lastCurrentTopRender)
							render.setPosition(GUI.Position(position.x + (size.getWidth() + _borderGap) * numberInLine++, position.y + (size.getHeight() + _borderGap) * numberInColumn));
						else 
						{
							numberInLine = 0;
							render.setPosition(GUI.Position(position.x + (size.getWidth() + _borderGap) * numberInLine++, position.y + (size.getHeight() + _borderGap) * ++numberInColumn));
						}					

						render.setVisible(true);
					}
				}
			}
		}
		else 
		{	
			if(!_renderList[newFocusNumber].isVisible())
			{
				local currentRow = ceil((_focusedRender + 1) / _columnNumber.tofloat());	
				local newRow = currentRow - 1;

				local firstCurrentBottomRender = (currentRow + _rowNumber - 1) * _columnNumber - _columnNumber;
				local firstNewTopRender = (newRow - 1) * _columnNumber; 

				foreach(i, render in _renderList)
				{
					if(i >= firstNewTopRender && i < firstCurrentBottomRender)
					{
						local size = render.getSize();
						local position = _position.getPosition();

						if(i % _columnNumber != 0 || i == firstNewTopRender)
							render.setPosition(GUI.Position(position.x + (size.getWidth() + _borderGap) * numberInLine++, position.y + (size.getHeight() + _borderGap) * numberInColumn)).setVisible(true);
						else 
						{
							numberInLine = 0;
							render.setPosition(GUI.Position(position.x + (size.getWidth() + _borderGap) * numberInLine++, position.y + (size.getHeight() + _borderGap) * ++numberInColumn)).setVisible(true);
						}
					}
					else render.setVisible(false);
				}				
			}
		}

		_renderList[_focusedRender = newFocusNumber].setFocus(true);
	}

	function mouseWheel(direction)
	{
		if(_scrollable && _focused)
		{
			switch(direction)
			{
				case 1: updateGrid(_focusedRender - _columnNumber); break;
				case -1: updateGrid(_focusedRender + _columnNumber); break;
			}
		}		
	}

//:public
	function setFocus(focus)
	{
		if(_visible)
		{
			if(focus != _focused)
				callEvent(GUIEventInterface.Focus, _focused = focus);
		}
	}

	function isFocused() { return _focused; }

	function cursor(cursorPosition)
	{
		local size = _cellSize.getSize();
		local position = _position.getPosition();
		
		if(GUI.isCursorIn(position.x, position.y, _columnNumber * (size.width + _borderGap), _rowNumber * (size.height + _borderGap), cursorPosition.x, cursorPosition.y))
		{
			setFocus(true);
		}
		else setFocus(false);
	}		
}