
class GUI.View extends GUI.Event
{
	_id = null;

	_objectList = null;
	_visible = false;

	constructor(viewId = null)
	{
        base.constructor();

		_id = viewId;
	
		_objectList = [];
		_visible = false;
	}

//:public
	function destroy()
	{
		clearObjects();
		
		_objectList = null;
			base.destroy();
	}

	function getId() { return _id; }
	
	function setVisible(visible)
	{
		if(_visible != visible)
		{			
			foreach(object in _objectList)
				object.setVisible(visible);

			callEvent(GUIEventInterface.Visible, _visible = visible);
		}

		return this;
	}
    
	function isVisible() { return _visible; }

	function pushObject(object)
	{
		_objectList.push(object);
			return this;
	}

	function removeObject(object)
	{
		local id = _objectList.find(object);
		
		if(id != null)
			_objectList.remove(id);
		
		return this;
	}

	function clearObjects()
	{
		foreach(object in _objectList)
			object.destroy();

		_objectList.clear();

		return this;
	}	

	function getObjects() { return _objectList; }
}