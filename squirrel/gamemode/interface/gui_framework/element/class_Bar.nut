
class GUI.Bar extends GUI.Event 
{
    _position = null;
    _size = null;

    _marginSize = null;

    _textureBackground = null;
    _texture = null;
        
    _textureBackgroundFile = "BAR_BACK.TGA";
    _textureFile = "BAR_MISC.TGA";

    _progress = 0.0;
    _visible = false;

    constructor(position, size, marginSize = null, textureBackground = null, texture = null)
    {
        _position = position;
        _size = size;

        _marginSize = marginSize;

        _textureBackground = null;
        _texture = null;

        if(textureBackground != null) _textureBackgroundFile = textureBackground;
        if(texture != null) _textureFile = texture;

        _progress = 1.0;
        _visible = false;
        
        base.constructor();
    }

//:public
    function setSize(size)
    {
        _size = size;

        if(_visible)
        {
            if(_marginSize)
            {
                _texture.setPosition(position.x + _marginSize.getWidth(), position.y + _marginSize.getHeight());
                _texture.setSize(size.width - _marginSize.getWidth() * 2, size.height - _marginSize.getHeight());
            }
            else 
            {
                _textureBackground.setPosition(position.x, position.y);
                _texture.setPosition(position.x, position.y);
            }
        }

        return this;
    }

    function getSize() { return _size; }

    function setPosition(position)
    {
        _position = position;
        
        if(_visible)
        {
            if(_marginSize)
            {
                _texture.setPosition(position.x + _marginSize.getWidth(), position.y + _marginSize.getHeight());
                _texture.setSize(size.width - _marginSize.getWidth() * 2, size.height - _marginSize.getHeight());
            }
            else 
            {
                _textureBackground.setPosition(position.x, position.y);
                _texture.setPosition(position.x, position.y);
            }
        }

        return this;
    }

    function getPosition() { return _position; }

    function setVisible(visible)
    {
        if(visible != _visible)
        {
            if(visible)
            {
                local position = _position.getPosition(), size = _size.getSize();

                _textureBackground = Texture(position.x, position.y, size.width, size.height, _textureBackgroundFile);
                _texture = Texture(position.x, position.y, size.width * _progress, size.height, _textureFile);
                
                if(_marginSize)
                    _texture = Texture(position.x + _marginSize.getWidth(), position.y + _marginSize.getHeight(), (size.width - _marginSize.getWidth() * 2) * _progress, size.height - _marginSize.getHeight() * 1.5, _textureFile);
                else 
                    _texture = Texture(position.x, position.y, size.width * _progress, size.height, _textureFile);

                _textureBackground.visible = true;
                _texture.visible = true;

                getSceneManager().pushObject(this);	
            }
            else 
            {
                _textureBackground = null;
                _texture = null;

				getSceneManager().removeObject(this);					
            }

			callEvent(GUIEventInterface.Visible, _visible = visible);
		}

        return this;
    }

    function isVisible() { return _visible; }

    function setProgress(progress)
    {
        if(progress >= 0.0 && progress <= 1.0)
        {
            if(_visible)
            {
                local position = _position.getPosition(), 
                    size = _size.getSize();

                if(_marginSize)
                    _texture.setSize((size.width - _marginSize.getWidth() * 2) * (_progress = progress), size.height - _marginSize.getHeight() * 1.5);
                else 
                    _texture.setSize(size.width * _progress, size.height);
            }
            else 
                _progress = progress;
        }

        return this;
    }
}