
class GUI.Render extends GUI.Event
{
    static BORDER_SPACE = 50;
    static BORDER_CORRECTION = 25;

	_pureText = true;

    _instance = null;
    _text = null;

	_textureFile = null;
	_focusTextureFile = null;

	_color = null;
	_focusColor = null;

	_alpha = -1;
	_focusAlpha = -1;

    _format = 0;

    _render = null;
	_draw = null;
	_texture = null;

	_size = null;
    _position = null;

    _visible = false;
    _focused = false;

    _rotate = false;

    constructor(itemInstance, position = null, size = null)
    {
        base.constructor();

	    _pureText = true;

        _instance = itemInstance;
        _text = null;

        _textureFile = null;
        _focusTextureFile = null;

        _color = null;
        _focusColor = null;

        _alpha = -1;
        _focusAlpha = -1;

        _format = 2;

        _render = null;
        _draw = null;
        _texture = null;

        _position = position ? position : GUI.Position(0, 0);            
        _size = size ? size : GUI.Size(1000, 1000);

        _visible = false;
        _focused = false;

        _rotate = false; 
    }

//:public
    function destroy()
    {
        setVisible(false);
            base.destroy();
    }

	function setPureText(pureText)
	{
		_pureText = pureText;
			return this;
	}	

	function isPureTextEnabled() { return _pureText; }

    function setItemInstance(itemInstance)
    {
        _instance = itemInstance;

        if(_visible)
        {
            local position = _position.getPosition();
            local size = _size.getSize();
            
            _render = ItemRender(position.x + BORDER_CORRECTION, position.y + BORDER_CORRECTION, size.width - BORDER_SPACE, size.height - BORDER_SPACE, itemInstance);
            _render.visible = true;
            
            if(_rotate)
            {
                _render.rotY = 180;
                _render.rotZ = 45;
            }

            _render.top();
        }

        return this;
    }

    function getInstance() { return _instance; }
    
	function setText(text)
	{
		_text = text.tostring();

		if(_visible)
			_draw.text = _pureText ? text : getTranslation(text);

        _textAdjust();

		return this;
	}

    function refreshText()
    {
		if(_visible)
        {
            _draw.font = getLanguageFont("FONT_DEFAULT");
			_draw.text = _pureText ? _text : getTranslation(_text);

            _textAdjust();
        }
    }

    function getText() { return _text; } 

    function setTexture(texture)
    {
		_textureFile = texture;

		if(_texture && !_focused)
			_texture.file = texture;

		return this;
    }

    function getTexture() { return _textureFile; }

    function setFocusTexture(texture)
    {
		_focusTextureFile = texture;

		if(_texture && _focused)
			_texture.file = texture;
		
		return this;
    }

    function getFocusTexture() { return _focusTextureFile; }

	function setColor(color)
	{
		local color = (_color = color).getColor();

		if(_visible && !_focused)
			_draw.setColor(color.r, color.g, color.b);
		
		return this;
	}

	function getColor() { return _color; }

	function setFocusColor(color)
	{
		local color = (_focusColor = color).getColor();
		
		if(_visible && _focused)
			_draw.setColor(color.r, color.g, color.b);

		return this;
	}

	function getFocusColor() { return _focusColor; }
    
	function setAlpha(alpha)
	{		
        _alpha = alpha;

		if(_visible && !_focused)
		{
			_draw.alpha = alpha;

			if(_texture)		
				_texture.alpha = alpha;
		}

		return this;
	}

    function getAlpha() { return _alpha; }

	function setFocusAlpha(alpha)
	{
        _focusAlpha = alpha;

		if(_visible && _focused)
		{
            if(_draw) _draw.alpha = alpha;
			if(_texture) _texture.alpha = alpha;
		}

		return this;
	}

    function getFocusAlpha() { return _focusAlpha; }

	function setFormat(format)
	{
		_format = format;

		if(_draw)
			_textAdjust();

		return this;
	}

	function getFormat() { return _format; }

	function setPosition(position)
	{
		local position = (_position = position).getPosition();
                	
        if(_visible)
        {
            if(_render) _render.setPosition(position.x + BORDER_CORRECTION, position.y + BORDER_CORRECTION);

			if(_draw) 
            { 
                _draw.setPosition(position.x, position.y + _size.getSize().height);
                _textAdjust();
            }

            if(_texture) _texture.setPosition(position.x, position.y);
        }

		return this;
	}

    function getPosition() { return _position; }

    function setSize(size) 
    {
		local size = (_size = size).getSize();

        if(_visible)
        {   
            if(_render) 
            {
                local position = _position.getPosition();

                _render.setSize(size.width - BORDER_SPACE, size.height - BORDER_SPACE);
                _render.setPosition(position.x + BORDER_CORRECTION, position.y + BORDER_CORRECTION);
            }
            
            if(_texture)
			    _texture.setSize(size.width, size.height);
        }

		return this;
    }

    function getSize() { return _size; }

	function setVisible(visible)
	{
		if(visible != _visible)
		{
            _visible = visible;
            
			if(visible)
			{
				local position = _position.getPosition();
                local size = _size.getSize();
			    
                if(_instance != null)
                {
                    _render = ItemRender(position.x + BORDER_CORRECTION, position.y + BORDER_CORRECTION, size.width - BORDER_SPACE, size.height - BORDER_SPACE, _instance);
                    _render.visible = true;

                    if(_rotate)
                    {
                        _render.rotY = 180;
                        _render.rotZ = 45;
                    }
                }
                
                if(_textureFile)
                {
                    local position = _position.getPosition();

                    _texture = Texture(position.x, position.y, size.width size.height, _textureFile);
                    _texture.visible = true;
                    
                    if(_alpha != -1)
                        _texture.alpha = _alpha;

                    if(_draw) _draw.top();
                    if(_render) _render.top();
                }		

                if(_text)
                {
                    _draw = Draw(position.x, position.y, getTranslation(_text));
                    
                    if(_color)
                    {
                        local color = _color.getColor();		
                            _draw.setColor(color.r, color.g, color.b);
                    }
                    
                    _draw.font = getLanguageFont("FONT_DEFAULT");

                    if(_alpha != -1)
                        _draw.alpha = _alpha;

                    _draw.visible = true;

                    _textAdjust();
                }

				getSceneManager().pushObject(this);				
			}
			else 
			{	
				_focused = false;

                if(_render)
                {
                    _render.visible = false;
                    _render = null;
                }

                if(_draw)
                {
				    _draw.visible = false;
				    _draw = null;
                }

                if(_texture)
                {
                    _texture.visible = false;
                    _texture = null;
                }

				getSceneManager().removeObject(this);				
			}
		
            callEvent(GUIEventInterface.Visible, visible);
		}

		return this;
	}

    function isVisible() { return _visible; }

//:private
	function setFocus_Graphical(focus)
	{
		if(focus)
		{
            if(_draw)
            {
                if(_focusColor)
                {
                    local color = _focusColor.getColor();
                        _draw.setColor(color.r, color.g, color.b);
                }
                else _draw.setColor(255, 255, 255);

                if(_focusAlpha != -1)
                    _draw.alpha = _focusAlpha;
            }
	
    		if(_texture)
			{
				if(_focusTextureFile)
					_texture.file = _focusTextureFile;
				if(_focusAlpha != -1)
					_texture.alpha = _focusAlpha;
			}
		}
		else 
		{
            if(_draw)
            {
                if(_color)
                {
                    local color = _color.getColor();
                        _draw.setColor(color.r, color.g, color.b);
                }
				else _draw.setColor(200, 184, 152);

                if(_alpha != -1)
                    _draw.alpha = _alpha;
            }

            if(_textureFile)
                _texture.file = _textureFile;
            if(_alpha != -1)
                _texture.alpha = _alpha;            
        }	
	}

//:public
	function setFocus(focus)
	{
		if(_visible) 
		{
			if(focus != _focused)
				callEvent(GUIEventInterface.Focus, _focused = focus);
			
			setFocus_Graphical(focus);
		}
	}

    function isFocused() { return _focused; }
    
    function setRotate(rotate)
    {
        if(rotate != _rotate)
        {
            if(_render)
            {
                if(rotate)
                {
                    _render.rotY = 180;
                    _render.rotZ = 45;
                }
                else
                {
                    _render.rotX = 0;
                    _render.rotY = 0;
                    _render.rotZ = 0;
                }
            }

            _rotate = rotate;
        }

        return this;
    }
    
    function isRotating() { return _rotate; }

	function cursor(cursorPosition)
	{
		local position = _position.getPosition();
		local size = _size.getSize();

		if(GUI.isCursorIn(position.x, position.y, size.width, size.height, cursorPosition.x, cursorPosition.y))
		{
			setFocus(true);
		}
		else setFocus(false);
	}

	function mouse(cursorPosition, button, event)
	{
		local position = _position.getPosition();
		local size = _size.getSize();

        if(GUI.isCursorIn(position.x, position.y, size.width, size.height, cursorPosition.x, cursorPosition.y))
        {
			return callEvent(GUIEventInterface.Click);
        }      

        return false;
	}

    function rotate()
    {
        if(_rotate)
        {
            if(_render)
            {
                if(_render.rotX >= 360)
                    _render.rotX = 0;
                
                _render.rotX += 1;
            }
        }
    }   

//:private	
	function _textAdjust()
	{
		if(_visible == false) return;

        textSetFont(getLanguageFont("FONT_DEFAULT"));

		local position = _position.getPosition();
		local size = _size ? _size.getSize() : _draw;

		local bottom = position.y + size.height / 1.20 - _draw.height / 2;
		
		switch(_format)
		{
			case GUITextFormat.Center: _draw.setPosition(position.x + size.width / 2 - textWidth(_draw.text) / 2, bottom); break;
			case GUITextFormat.Left: _draw.setPosition(position.x + 100, bottom); break;
			case GUITextFormat.Right: _draw.setPosition(position.x + size.width - textWidth(_draw.text) - 50, bottom); break;
		}
	}    
}
