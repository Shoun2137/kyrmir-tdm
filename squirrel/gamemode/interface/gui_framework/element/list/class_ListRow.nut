
class GUI.ListRow extends GUI.Event 
{
	_list = null;

	_buttonList = null;
	
    _position = null;
    _size = null;

    _visible = false;
    _focused = false;

	_focusWholeRow = false;

	constructor(focusWholeRow = false)
	{
        base.constructor();

		_list = null;

		_buttonList = [];

		_position = GUI.Position(0, 0);
        _size = GUI.Size(0, 0);
        
        _visible = false;
        _focused = false;

		_focusWholeRow = focusWholeRow;
    }

//:public
	function destroy()
	{
		_list = null;
			base.destroy();
	}

	function setList(list)
	{
		_list = list;
	}

	function getList() { return _list; }

	function pushButton(button)
	{
		_buttonList.push(button.setDisableFocus(_focusWholeRow));
			return this;
	}

	function getColumn(column) { return column in _buttonList ? _buttonList[column] : null; }
	
	function clickFromList()
	{
		return callEvent(GUIEventInterface.Click);
	}

    function setPosition(position)
	{
		local position = (_position = position).getPosition();

		if(_visible) 	
		{
            local widthSuma = 0;

            foreach(index, button in _buttonList)
            {	
                button.setPosition(GUI.Position(_position.getX() + widthSuma, _position.getY()));
                    widthSuma += button.getSize().getWidth();
            }
		}

		return this;
	}

    function getPosition() { return _position; }

	function setVisible(visible)
	{
        if(visible != _visible)
        {
            if(visible)
            {
                local widthSuma = 0;

                foreach(index, button in _buttonList)
                {	
                    button.setPosition(GUI.Position(_position.getX() + widthSuma, _position.getY()));
                    button.setVisible(visible);

                    widthSuma += button.getSize().getWidth();
                }

                _size.setWidth(widthSuma);
                _size.setHeight(_buttonList[0].getSize().getHeight());

				getSceneManager().pushObject(this);		     
            }
            else 
            {
                _focused = false;
        
                foreach(index, button in _buttonList)
                {	
                    button.setVisible(visible);
                }
				
                getSceneManager().removeObject(this);	
            }
        
            callEvent(GUIEventInterface.Visible, _visible = visible);
        }

        return this;
	}

    function isVisible() { return _visible; }

	function setFocus(focus)
	{
		if(focus != _focused)
		{
			if(_focusWholeRow)
			{
				foreach(button in _buttonList)
				{
					button.setFocus(focus);
				}
			}

			if(focus) 
                _list.setCurrentButton(this);

            callEvent(GUIEventInterface.Focus, _focused = focus);
		}
	}
	
	function isFocusWholeRowEnabled() { return _focusWholeRow; }

	function cursor(cursorPosition)
	{
		local position = _position.getPosition();
		local size = _size.getSize();
            
		if(GUI.isCursorIn(position.x, position.y, size.width, size.height, cursorPosition.x, cursorPosition.y))
		{
			setFocus(true);
		}
		else
		{
			if(_list.getButtons()[_list.getCurrentButton()] != this)
			{
				setFocus(false);
			}
		}
	}

	function mouse(cursorPosition, button, event)
	{
		local position = _position.getPosition();
		local size = _size.getSize();

		if(GUI.isCursorIn(position.x, position.y, size.width, size.height, cursorPosition.x, cursorPosition.y))
		{
			local value = callEvent(GUIEventInterface.Click);

			if(_list.mouseClick() == false)
			{
				return value;
			}

			return true;
		}

		return false;
	}
}
