
class Effect.Template 
{
    _id = null;
    
    _time = -1;
    _repeatTime = -1;

    _beginingFunc = null;
    _repeatFunc = null;
    _finishFunc = null;

    constructor(id, time)
    {
        _id = id;

        if(time > 0)
            _time = time * 1000;
        else 
            _time = -1;        
        
        _repeatTime = -1;

        _beginingFunc = null;
        _repeatFunc = null;
        _finishFunc = null;
    }

//:public
    function getId() { return _id; }

    function setTime(time)
    {
        if(time > 0)
            _time = time * 1000;
        else 
            _time = -1;      
    }

    function getTime() { return _time; }

    function getRepeatTime() { return _repeatTime; }

    function isRepetitive() { return _repeatTime != -1 };
    
    function bindBeginingFunc(beginingFunc)
    {
        _beginingFunc = beginingFunc;
            return this;
    } 

    function bindRepeatFunc(repeatFunc)
    {
        _repeatFunc = repeatFunc;
            return this;
    }

    function bindFinishFunc(finishFunc)
    {
        _finishFunc = finishFunc;
            return this;
    }

    function callBeginingFunc(playerId)
    {
        if(_beginingFunc != null)
            return _beginingFunc(playerId);

        return null;
    }

    function callRepeatFunc(playerId)
    {
        if(_repeatFunc != null)
            return _repeatFunc(playerId);

        return null;
    }

    function callFinishFunc(playerId)
    {
        if(_finishFunc != null)
            return _finishFunc(playerId);

        return null;
    }
}