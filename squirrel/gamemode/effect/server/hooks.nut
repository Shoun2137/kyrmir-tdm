
_applyPlayerOverlay <- applyPlayerOverlay;
_removePlayerOverlay <- removePlayerOverlay;

function applyPlayerOverlay(playerId, overlayId)
{
    local player = getPlayer(playerId);
    if(player) return player.getEffectModule().applyOverlay(overlayId);
}

function removePlayerOverlay(playerId, overlayId)
{
    local player = getPlayer(playerId);
    if(player) return player.getEffectModule().removeOverlay(overlayId);
}