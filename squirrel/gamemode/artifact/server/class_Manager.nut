
class Artifact.Manager 
{
    static _artifactList = {};

    static function registerArtifact(artifact)
    {
        local instance = artifact.getInstance();

        if((instance in _artifactList) == false)
        {
            _artifactList[instance] <- artifact;
        }
    }    

    static function playerSpellCast(playerId, template)
    {
        local instance = template.getInstance();
        
        if(instance in _artifactList)
        {
            local artifact = _artifactList[instance];

            artifact.setSummoned(false);
            artifact.setOwnerId(-1);
        }
    }

    static function playerItemUpdate(playerId, instance, oldAmount, newAmount)
    {
        if(instance in _artifactList)
        {
            local artifact = _artifactList[instance];

            if(oldAmount < newAmount)
            {
                local diff = newAmount - oldAmount;

                artifact.callReceivingFunc(playerId, diff);

                artifact.setOwnerId(playerId);
                artifact.setSummoned(true);
            }
            else 
            {
                local diff = oldAmount - newAmount;

                artifact.callLossingFunc(playerId, diff);
                artifact.setOwnerId(-1);
            }
        }
    }

    static function resetArtifacts()
    {
        foreach(artifact in _artifactList)
        {
            artifact.setSummoned(false);
        }
    }

    static function playerDropArtifact(playerId)
    {
        local list = _artifactList;

        local dropList = getEq(playerId).filter(function(index, value) {
            return value.getTemplate().getInstance() in list;
        });
        
        foreach(item in dropList)
        {
            dropItem(playerId, item.getTemplate().getId(), item.getAmount());
        }
    }

    static function itemVanish(item)
    {
        local template = item.getTemplate(),
            instance = template.getInstance();

        if(instance in _artifactList)
        {
            _artifactList[instance].setSummoned(false);
        }   
    }

    static function getArtifacts() { return _artifactList; }

    static function getArtifactByInstance(instance) { return instance in _artifactList ? _artifactList[instance] : null; }
}

getArtifactManager <- @() Artifact.Manager;

// Artifact
registerArtifact <- @(artifact) Artifact.Manager.registerArtifact(artifact);
getArtifactByInstance <- @(instance) Artifact.Manager.getArtifactByInstance(instance);