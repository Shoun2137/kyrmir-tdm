
class Artifact.Instance
{
    _instance = null;

    _summoned = false;
    _ownerId = -1;

    _receivingFunc = null;
    _lossingFunc = null;

    constructor(instance)
    {
        _instance = instance;

        _summoned = false;
        _ownerId = -1;

        _receivingFunc = null;
        _lossingFunc = null;
    }

    function getInstance() { return _instance; }
    
    function setSummoned(summoned)
    {
        _summoned = summoned;
    }

    function isSummoned() { return _summoned; }

    function setOwnerId(ownerId)
    {
        _ownerId = ownerId;
    }

    function getOwnerId() { return _ownerId; }
    
    function bindReceivingFunc(receiving)
    {
        _receivingFunc = receiving;
            return this;
    }

    function callReceivingFunc(playerId, amount)
    {
        if(_receivingFunc != null)
            return _receivingFunc(playerId, amount);

        return null;
    }

    function bindLossingFunc(lossing)
    {
        _lossingFunc = lossing;
            return this;
    }

    function callLossingFunc(playerId, amount)
    {
        if(_lossingFunc != null)
            return _lossingFunc(playerId, amount);

        return null;
    }
}