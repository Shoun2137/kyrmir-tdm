
class GroundDrop.Manager
{
    static _itemList = [];

//:public
    static function createItem(itemTemplate, amount, x, y, z, world, virtualWorld = 0, time = -1)
    {   
        local item = GroundDrop.Instance(itemTemplate, amount, x, y, z, world, virtualWorld, time);
            _itemList.push(item);
        return item;
    }

    static function destroyItem(itemGround)
    {
        foreach(id, item in _itemList)
        {
            if(item.getObject() == itemGround)
            {
                _itemList.remove(id);
                item.destroy();

                break;   
            }
        }
    }

    static function clearItems()
    {
        foreach(item in _itemList)
        {
            item.destroy();
        }

        _itemList.clear();
    }
    
    static function getItems() { return _itemList; }

    static function playerTakeItem(playerId, itemGround)
    {
        foreach(id, item in _itemList)
        {
            if(item.getObject() == itemGround)
            {
                callEvent("GroundDrop.onPlayerTakeItem", playerId, item);
                    item.destroy();

                _itemList.remove(id);

                return true;
            }
        }

        return false;
    }

    static function checkItem()
    {
        local tickCount = getTickCount();

        foreach(id, item in _itemList)
        {
            if(item.getDisapperanceTime() != -1)
            {
                if(tickCount > item.getDisapperanceTime())
                {
                    callEvent("GroundDrop.onItemVanish", item);

                    _itemList.remove(id);
                    item.destroy();
                }
            }
        }
    }
}

getGroundDropManager <- @() GroundDrop.Manager;

// Effect
createGroundItem <- @(itemTemplate, amount, x, y, z, world, virtualWorld = 0, time = -1) GroundDrop.Manager.createItem(itemTemplate, amount, x, y, z, world, virtualWorld, time);
destroyGroundItem <- @(itemGround) GroundDrop.Manager.destroyItem(itemGround);
clearGroundItems <- @() GroundDrop.Manager.clearItems();

getGroundItems <- @() GroundDrop.Manager.getItems();