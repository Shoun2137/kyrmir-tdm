
function validate_username(username)
{
    return regexp(@"[_0-9a-zA-Z]{3,12}").match(username);
}

function validate_password(password)
{    
    return regexp(@"[_0-9a-zA-Z]{6,}").match(password);
}

function random(min, max)
{
    return (floor(rand() % (max - min + 1)) + min).tointeger();
}

function textSlice(text, maxSize)
{
    local chunks = [];

    local textSize = text.len(), 
        lines = ceil(textSize / maxSize.tofloat());

    if(lines > 1)
    {
        for(local i = 0; i < lines; i++)
        {
            local end = (i + 1) * maxSize;
            
            if(i != (lines - 1))
                chunks.push(strip(text.slice(i * maxSize, end <= textSize ? end : textSize)) + "-");
            else 
                chunks.push(strip(text.slice(i * maxSize, end <= textSize ? end : textSize)));
        }
    }
    else 
    {
        chunks.push(strip(text));
    }

    return chunks;
}

function code(length, start = 48, end = 89)
{
    local code = "";
    
    for (local i = 0; i < length; i++)						
        code += random(start, end).tochar();
        
    return code;	
}
