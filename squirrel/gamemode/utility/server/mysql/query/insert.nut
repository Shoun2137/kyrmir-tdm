
class MySQL.QueryInsert
{
    _result = null;

    _table = null;
    _columns = null;
    _values = null;

    _where = null;

    _connection = null;

    constructor(connection, table, ...)
    {
        _connection = connection;
      
        local columns = "", 
            values = "";

        local lastIndex = vargv.len() - 1;

        foreach(i, arg in vargv)
        {
            if(i == lastIndex)
            {
                columns += arg[0]; 
                    values += ("'" + arg[1] + "'");
            }
            else 
            {
                columns += (arg[0] + ", "); 
                    values += ("'" + arg[1] + "'" + ", ");    
            }
        }
        
        _table = table;

        _columns = columns;
        _values = values;

        _where = null;           
    }

//:protected 
    function generateQuery()
    {
        return "INSERT INTO " + _table + "(" + _columns + ") VALUES(" + _values + ")" + (_where != null ? _where : "") + ";";
    }

//:public
    function where(...)
    {
        local where = "",
            lastIndex = vargv.len() - 1;

        foreach(i, arg in vargv)
        {
            if(i == lastIndex)
                columns += ("`" + arg[0] + "` " + arg[2] + " '" + arg[1] + "'"); 
            else 
                where += ("`" + arg[0] + "` " + arg[2] + " '" + arg[1] + "' " + vargv[i][3] + " "); 
        }

        _where = " WHERE " + where;
            return this;
    }

    function exec()
    {
        _result = _connection.query(generateQuery());
            return this;
    }

    function getQuery() { return generateQuery(); }

    function getResult() { return _result; }
}