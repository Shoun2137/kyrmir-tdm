
class Damage.Manager 
{
//:public
    static function playerParade(playerId)
    {
        local player = getPlayer(playerId), parade = false;

        if(player) 
        {
            local module = player.getDamageModule();

            parade = !module.canParade();
                module.parade();
        }

        return parade;
    }

    static function playerDamageFall(damage)
    {
        local hero = getHero();
        if(hero) hero.getDamageModule().damageFall(damage);
    }

    static function server_parade(packet)
    {
        local player = getPlayer(packet.readInt16());
        if(player) player.getDamageModule().server_parade(packet.readInt8());
    }

    static function packetRead(packet)
    {
        local packetId = packet.readInt8();

        switch(packetId)
        {
            case Damage_PacketIDs.Parade: server_parade(packet); break;
        }
    } 
}

getDamageManager <- @() Damage.Manager;

// Parade
function getPlayerParades(playerId)
{
    local player = getPlayer(playerId);
    if(player) return player.getDamageModule().getParades();
}