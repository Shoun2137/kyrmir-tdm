
// Register packet callback
PacketReceive.add(PacketIDs.Player, getPlayerManager());

addEventHandler("onPlayerJoin", Player.Manager.playerJoin.bindenv(Player.Manager));

addEventHandler("onPlayerDisconnect", function(playerId, reason)
{
    getPlayerManager().playerDisconnect(playerId);
});

addEventHandler("onPlayerRespawn", Player.Manager.playerRespawn.bindenv(Player.Manager));

addEventHandler("Damage.onPlayerDead", Player.Manager.playerDead.bindenv(Player.Manager));