
class Chat.Manager 
{
//:protected 
    static function server_packetToList(packet)
    {
        local packetSize = packet.readInt8();

        for(local i = 0; i < packetSize; i++)
        {
            addChat(packet.readInt8(), packet.readString());
        }
    }

    static function server_addChat(packet)
    {
        addChat(packet.readInt8(), packet.readString());
    }

    static function server_removeChat(packet)
    {
        removeChat(packet.readInt8());
    }

//:protected
    static function addChat(prefix, name)
    {        
        callEvent("Chat.onChatListUpdate", prefix, name);
    }

    static function removeChat(prefix)
    {
        callEvent("Chat.onChatListUpdate", prefix, false);
    }
    
//:public
    static function packetRead(packet)
    {
        local packetType = packet.readInt8();
        
        switch(packetType)
        {
            case Chat_PacketIDs.List: server_packetToList(packet); break;
            case Chat_PacketIDs.Add: server_addChat(packet); break;
            case Chat_PacketIDs.Remove: server_removeChat(packet); break;
        }   
    } 
}

getChatManager <- @() Chat.Manager;